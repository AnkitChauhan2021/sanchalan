<?php

namespace App\Helpers;

use App\Library\Sms;
use App\Models\Config;
use App\Models\Magazine;
use App\Models\Otp;
use App\Models\Pole;
use App\Models\Chat;
use App\Models\Post;
use App\Models\PostBlock;
use App\Models\User;
use App\Models\UserCompany;
use App\Models\UserFollower;
use App\Models\UserPole;
use App\Models\UserPermission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;

class CommonHelper
{
    const ACTIVE = true;
    const INACTIVE = false;
    const ACTIVE_BOOLEAN = 'Active';

    const INACTIVE_BOOLEAN = 'Inactive';
    const LikeToJoinFalse = '<i class="fas fa-times-circle fa-2x" style="font-size: 1.7em"></i>';
    const LikeToJoinTrue = '<i class="fas fa-check fa-2x" style="font-size: 1.7em"></i>';
    const PRELIMINARY = 'Preliminary';
    const PENDING = 'Pending';
    const COMPLETED = 'Completed';
    const DRAFT = 'draft';
    const UNPUBLISH = 'unpublish';
    const PUBLISH = 'publish';
    const Expire = 'Expire';
    const No = 'No';
    const Yes = 'Yes';
    const USER_ACTIVE = 'Active';
    const USER_BLOCK = 'Block';
    const BLOCK = 'Block';
    const DISPLAY_PRICE = 1;
    const HIDE_PRICE = 0;
    const Approved = 1;
    const Reject = 2;
    const Pending = 0;
    const IsApproved = "Approved";
    const IsReject = "Reject";
    const IsPending = "Pending";
    const FindRecordsSuccess= "Records Found Successfully!";
    const FindRecordSuccess= "Record Found Successfully!";
    const CreateRecordSuccess= "Create Record Successfully!";
    const FindRecordsUnable= "Records Not Found!";
    const FindRecordUnable= "Record Not Found!";
    const CreateRecordUnable= "Unable to create Record!";
    const SendRequest= "Send request successfully!";
    const SendUnable= "Unable To Send request!";


    public static function getLimitOption()
    {
        return [
            '10' => '10',
            '20' => '20',
            '30' => '30',
            '50' => '50',
            '70' => '70',
            '100' => '100'
        ];
    }

    public static function countryName($country_code)
    {
        $countries = array
        (
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );
        return $countries[strtoupper($country_code)];
    }

    public static function getUserStatus($routeName, $status, $id)
    {
        if ($status == self::USER_ACTIVE) {
            return '<a href="javascript:void(0);"><span class="badge badge-success change_user_status" data-action="' . route($routeName) . '" data-status="' . self::USER_BLOCK . '" data-id="' . $id . '">' . self::USER_ACTIVE . '</span></a>';
        } elseif ($status == self::USER_BLOCK) {
            return '<a href="javascript:void(0);"><span class="badge badge-danger change_user_status" data-action="' . route($routeName) . '" data-status="' . self::USER_ACTIVE . '" data-id="' . $id . '">' . self::USER_BLOCK . '</span></a>';
        }
    }

    public static function getStatusUrl($routeName, $status, $id)
    {
        if ($status == self::ACTIVE) {

            return '<a href="javascript:void(0);" class="status_s" ><span style="width: 51px;" class="btn btn-success change_status" data-action="' . route($routeName) . '" data-status="' . '0' . '" data-id="' . $id . '">' . self::ACTIVE_BOOLEAN . '</span></a>';
        } elseif ($status == self::INACTIVE) {
            return '<a href="javascript:void(0);" class="status_s" ><span style="width: 51px;" class="btn btn-danger change_status" data-action="' . route($routeName) . '" data-status="' . '1' . '" data-id="' . $id . '">' . self::INACTIVE_BOOLEAN . '</span></a>';
        }
    }
    public static function getPermissionUrl($routeName, $status, $id)
    {
        if ($status == self::ACTIVE) {

            return '<a href="javascript:void(0);" class="status_s" ><span style="width: 51px;" data-toggle="tooltip" data-placement="top" title="Set by default Like To Join " class="btn btn-success Like_to_join" data-action="' . route($routeName) . '" data-status="' . '0' . '" data-id="' . $id . '">' . self::LikeToJoinTrue . '</span></a>';
        } elseif ($status == self::INACTIVE) {
            return '<a href="javascript:void(0);" class="status_s " ><span style="width: 51px;" data-toggle="tooltip" data-placement="top" title="Set by default Like To Join " class="btn btn-danger Like_to_join" data-action="' . route($routeName) . '" data-status="' . '1' . '" data-id="' . $id . '">' . self::LikeToJoinFalse . '</span></a>';
        }
    }
    public static function setNotification($routeName, $status, $id)
    {
        if ($status == self::ACTIVE) {

            return '<a href="javascript:void(0);" class="status_s" ><span style="width: 51px;" class="btn btn-success set_notifications" data-action="' . route($routeName) . '" data-status="' . '0' . '" data-id="' . $id . '">' . self::ACTIVE_BOOLEAN . '</span></a>';
        } elseif ($status == self::INACTIVE) {
            return '<a href="javascript:void(0);" class="status_s" ><span style="width: 51px;" class="btn btn-danger set_notifications" data-action="' . route($routeName) . '" data-status="' . '1' . '" data-id="' . $id . '">' . self::INACTIVE_BOOLEAN . '</span></a>';
        }
    }

    public static function response($data,$SuccessMessage,$failedMessage ){

        if($data){
            return response()->json([
                'status_code' =>1,
                'message'=>$SuccessMessage,
                'data'=>$data
            ]);
        }else{
            return response()->json([
                'status_code' =>0,
                'message'=>$failedMessage,
                'data'=>null
            ]);
        }
    }


    public static function approvedAndRejectUser($routeName, $status, $id)
    {

        $cont = static::ApprovedUser($routeName, $status, $id);

        if ($status == self::Approved) {
            return '<a href="#myModal" data-toggle="modal" class="dropdown-item getId" data-action="' . route($routeName) . '"  data-id="' . $id . '" data-status="' . $status . '">  <span style="width: 51px;" class="text-success "  ><i class="fas fa-user-check"></i> ' . self::IsApproved . '</span></a>';
        } elseif ($status == self::Reject) {
            return '<a href="#myModal" data-toggle="modal" class=" dropdown-item getId " data-action="' . route($routeName) . '"  data-id="' . $id . '" data-status="' . $status . '">  <span style="width: 51px;" class="text-danger " ><i class="fas fa-times-circle"></i> ' . self::IsReject . '</span></a>';
        } else {
            return '<a href="#myModal" data-toggle="modal" class=" dropdown-item getId" data-action="' . route($routeName) . '"  data-id="' . $id . '" data-status="' . $status . '">  <span style="width: 51px;" class="text-warning "><i class="fas fa-sync-alt"></i> ' . self::IsPending . '</span></a>';
        }

    }

    public static function ApprovedUser($routeName, $companyId, $id)
    {

        return '
 <div class="modal-body text-center " >
 <div class="row"><div class="col-lg-6" id="0">
        <a type="button" class="fw-btn-fill bg-warning btn-gradient-yellow provedOrNot addtoModel" data-id="' . $companyId . '" data-action="' . route($routeName) . '" data-status="' . '0' . '" ><span class="text-light"><i class="fas fa-sync-alt"></i> Pending</span></a>
        </div>
        <div class="col-lg-6" id="2">
        <a type="button" class="fw-btn-fill bg-danger btn-gradient-yellow provedOrNot addtoModel" data-id="' . $companyId . '" data-action="' . route($routeName) . '"  data-status="' . '2' . '" ><span class="text-light"> <i class="fas fa-times-circle"></i> Reject</span></a>
        </div>
        <div class="col-lg-6" id="1">
        <a type="button" class="fw-btn-fill bg-success btn-hover-yellow provedOrNot addtoModel" data-id="' . $companyId . '" data-action="' . route($routeName) . '"  data-status="' . '1' . '" ><span class="text-light"><i class="fas fa-user-check"></i> Approved</span></a>
        </div>

<div>';
    }

    public static function getUserTblDetails($id)
    {
        if ($id) {
            $getUserTblDetails = User::where('id', '=', $id)->get()->toArray();
            return $getUserTblDetails;
        } else {
            return false;
        }

    }

//	 Resend OTP
    public static function resetSentOTP($mobile, $otp)
    {
        try {
            $isOtp = Otp::where('mobile', $mobile)->first();
            if ($isOtp) {
                $isOtp->update(['otp' => $otp]);
                return response()->json([
                    'status_code' => 1,
                    'message' => 'One Time Passwords (OTPs) Resend Successfully!',
                    'data' => $mobile
                ]);
            } else {
                $create_otp = new Otp();
                $create_otp->mobile = $mobile;
                $create_otp->otp = $otp;
                if ($create_otp->save()) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'One Time Passwords (OTPs) Resend Successfully!',
                        'data' => $mobile
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Something went Wrong!',
                        'data' => null
                    ]);
                }

            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

//    magazine likes

    public static function increaseLikesCount($magazine_id)
    {
        try {
            $magazine = Magazine::find($magazine_id);
            $count = $magazine->like_count;
            $magazine->like_count = $count + 1;
            if ($magazine->save()) {
                return "success";
            } else {
                return "error";
            }
        } catch (Exception $e) {
            Log::info("log Message", ['error' => $e->getMessage()]);
        }
    }

    public static function decreaseLikesCount($magazine_id)
    {
        try {
            $magazine = Magazine::find($magazine_id);
            $count = $magazine->like_count;
            if ($count == null || $count == 0) {
                $magazine->like_count = $count;
            } else {
                $magazine->like_count = $count - 1;
            }

            if ($magazine->save()) {
                return "success";
            } else {
                return "error";
            }

        } catch (Exception $e) {
            Log::info("Log message", ['error' => $e->getMessage()]);
        }
    }

//        magazine Comments
    public static function countComment($magazine_id)
    {
        try {
            if ($magazine_id != null) {
                $comment = Magazine::find($magazine_id);
                if ($comment) {
                    $count = $comment->comment_count;
                    $comment->comment_count = $count + 1;
                    if ($comment->save()) {
                        return "success";
                    } else {
                        return "error";
                    }
                }
            }

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }
    }

    public static function increasePostLikesCount($post_id)
    {
        try {
            $Post = Post::find($post_id);
            $count = $Post->like_count;
            if ($count == null || $count == 0) {
                $Post->like_count = 1;
            } else {
                $Post->like_count = $count + 1;
            }

            if ($Post->save()) {
                return "success";
            } else {
                return "error";
            }
        } catch (Exception $e) {
            Log::info("log Message", ['error' => $e->getMessage()]);
        }
    }

    public static function decreasePostLikesCount($post_id)
    {

        try {
            $Post = Post::find($post_id);
            $count = $Post->like_count;
            if ($count == null || $count == 0 || $count == 1) {
                $Post->like_count = $count;
            } else {
                $Post->like_count = $count - 1;
            }
            if ($Post->save()) {
                return "success";
            } else {
                return "error";
            }

        } catch (Exception $e) {
            Log::info("Log message", ['error' => $e->getMessage()]);
        }
    }

    public static function WantTOJoin()
    {
        $records = UserCompany::where(['user_id' => Auth::user()->id, 'is_like' => 1, 'isApproved' => 0])->groupBy('company_id')->pluck('company_id');
        if (count($records) > 0) {
            $getCompanyId = UserCompany::whereIn('company_id', $records)->where('role_id', 3)->pluck('user_id');
//            $posts = Post::WhereIn('user_id',$getCompanyId)->where('status',1)->orderBy('created_at','DESC')->get();
            return $getCompanyId;
        } else {
            return [];
        }
    }

    public static function isApproved()
    {

        $records = UserCompany::where(['user_id' => Auth::user()->id, 'is_like' => 0, 'isApproved' => 1])->pluck('company_id');

        if (count($records) > 0) {
            $getCompanyId = UserCompany::whereIn('company_id', $records)->pluck('user_id')->toArray();
//          $posts = Post::WhereIn('user_id',$getCompanyId)->where('status',1)->orderBy('created_at','DESC')->get();
            return $getCompanyId;
        } else {
            return [];
        }

    }

    public static function superAdminID()
    {
        $user = User::whereIn('role_id', [1])->pluck('id')->toArray();
//      $posts = Post::WhereIn('user_id',$user)->where('status',1)->orderBy('created_at','DESC')->get();

        return $user;
    }

    public static function isFollower()
    {
        $byFollower = UserFollower::where('following_id', Auth::user()->id)->pluck("follower_id")->toArray();

        return $byFollower;

    }
    public static function isFollowing(){
        $byFollowing =  UserFollower::where('following_id', Auth::user()->id)->pluck("follower_id")->toArray();

        return $byFollowing;
    }
    public static  function followerFollowing(){
        $isFollower= CommonHelper::isFollower();
        $isFollowing= CommonHelper::isFollowing();
        $user_ids = array_merge( $isFollower,$isFollowing);
//        $users=array_unique($user_ids);
        return $user_ids;

    }

    public static function BlockedPost()
    {
        $blockedPostIds = PostBlock::where('user_id', Auth::user()->id)->groupBy('post_id')->pluck("post_id")->toArray();
        return $blockedPostIds;
    }

    public static function submitedPolls()
    {
        $userPolls = UserPole::where('user_id', Auth::user()->id)->groupBy('pole_id')->pluck('pole_id');

        return $userPolls;
    }

    public static function ApprovedCompanyPost(){
        $isApproved=  CommonHelper::isApproved();
        $superAdmin=  CommonHelper::superAdminID();
        $isFollowers=  CommonHelper::isFollower();
        $user_ids = array_merge( $superAdmin,$isFollowers,$isApproved);
        $users=array_unique($user_ids);
        return $users;
    }
    public static function polls(){
        $isApproved=  CommonHelper::isApproved();
        $answeredPolls=  CommonHelper::submitedPolls();
        $poles = Pole::whereIn('user_id', $isApproved)->whereNotIn('id',$answeredPolls)->where('status', 1)->with('users')->get();
        return $poles;
    }
    public static function Posts(){

        $BlockedPost=  CommonHelper::BlockedPost();

        $ApprovedCompanyPosts=  CommonHelper::isFollowing();


        $user_ids = array_merge( $ApprovedCompanyPosts,[Auth::user()->id]);


        $records = Post::whereIn('type',['personal','user'])->WhereIn('user_id',$user_ids)->whereNotIn('id', $BlockedPost)->where('status',Post::Active)->with('postBy','getPostImage','getPostVideo','comments','comments.commentBy','likes','likes.likeBy','replies')->with('report',function ($q){

            $q->where('user_id',Auth::user()->id);
        })->orderBy('id','DESC');

        return $records;
    }

    public static function approved(){
        $records = UserCompany::where(['user_id' => Auth::user()->id,'isApproved' => 1])->pluck('company_id')->toArray();
        return $records;

    }
    public static function userPermissions(){
        $records = UserPermission::where(['user_id' => Auth::user()->id])->get();
        return $records;

    }

    public static function chatCount(){
        $chat = Chat::where('receiver_id',Auth::user()->id)->where('status',0)->get();
        $chatcount = count($chat);
        return $chatcount;

    }



      //     otp_generate

    public static function otpGenerate($mobile)
    {

//        $validator = Validator::make($request->all(), [
//            'mobile' => 'required'
//        ], ['mobile.required' => 'Please enter the mobile no']);
//        if ($validator->fails()) {
//            $errors = $validator->errors()->first();
//            return response()->json([
//                'status_code' => 0,
//                'message' => $errors,
//                'data' => null
//            ]);
//        }

        if ($mobile) {
            $count = strlen($mobile);
            if ($count < 10 || $count > 10) {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Please enter the 10 digit mobile no',
                    'data' => null
                ]);
            }
        }

        $model = OTP::query()->where('mobile', $mobile)->first() ? OTP::query()->where('mobile', $mobile)->first() : OTP::make();
        $model->mobile = $mobile;
        $model->expired_at = date("Y-m-d H:i:s", strtotime('1 day'));
        $otp = rand(111111, 999999);
        $appName = env('APP_NAME');
        $sms_api_setting = Config::query()->where('sms_api', true)->first();
        if ($sms_api_setting) {
            $model->otp = $otp;
            $msg = "Your OTP is $otp";
            $sms = new Sms($mobile, $msg);
            $res = $sms->send();
            $res = json_decode($res);
            if ($res->status == 'failure') {
                $warning = $res->errors ? $res->errors : $res->warnings;
                return response()->json([
                    'status_code' => 0,
                    'message' => 'error',
                    'data' => $warning
                ], 200);
            } else {
                $model->save();
                return response()->json([
                    'status_code' => 1,
                    'message' => 'OTP sent successfully!',
                    'data' => $res
                ], 200);
            }
        } else {
            $model->otp = '1234';
            $model->save();
            return response()->json([
                'status_code' => 1,
                'message' => 'OTP sent successfully!',
                'data' => null
            ], 200);
        }

    }


}
