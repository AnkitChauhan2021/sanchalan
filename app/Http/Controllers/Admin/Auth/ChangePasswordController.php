<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\ChangePasswordRequest;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function __construct(){
        //
    }
    // Get Change Password
    public function index(){
        return view('admin.auth.passwords.ChangePassword');
    }
    // Post Change Password After validate
    public function changePassword(ChangePasswordRequest $request){
        try{
            $password = bcrypt($request->password);
            $admin = User::findOrFail(Auth::user()->id);
            $admin->update(['password'=>$password]);

            return redirect()->route('admin.change_password.index')
                ->with(["success"=>"Password changed successfully."]);
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }
}
