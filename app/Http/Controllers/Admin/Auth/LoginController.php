<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     * @return void
     */

    public function adminLoginForm()
    {

        if (Auth::check()){
            if(Auth::user()->role_id == User::SuperAdmin){
                return redirect()->route('admin.dashboard.index');
            }else{
                return view('admin.auth.login')->with("error","You don't have admin access.");;
            }

        }else{
            return view('admin.auth.login')->with("error","You don't have admin access.");;
        }

    }
    public function postLogin(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $request->session()->flash('title'," Hello ".Auth::user()->full_name);
            $request->session()->flash('success',"You are now logged in.");
            return redirect()->route('admin.dashboard.index')->with('success','You are now logged in.');
        }
      $request->session()->flash('error',"Oppes! You have entered invalid credentials");
       return redirect()->route("admin.showLoginForm");
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return back()->with(['error'=> trans('auth.failed')]);
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        $request->session()->flash("success","You have been logged out.");
        return redirect()->route('admin.showLoginForm');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
//    protected function guard()
//    {
//        return Auth::guard('admin');
//    }


}
