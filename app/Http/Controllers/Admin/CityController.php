<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\CityRequest;
use App\Imports\CountryImort;
use App\Models\City;
use App\Models\District;
use App\Models\State;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;

class CityController extends Controller
{
    public function __construct(){
        //
    }
    public function index(Request $request){
        $records = City::query();
        $search =$request->query('search');
        if ( $search) {
            $records = $records->where(function ($q) use ( $search) {
                $q->where('name', 'like', '%' .  $search . '%');
                $q->orwhere('created_at', 'like', '%' .  $search . '%');

            })->orWhereHas("district",function($query) use ($search){
                    $query->where(function($data) use ($search){
                        $data->orWhere('name','like','%'. $search.'%');
                    });
                });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records=$records->with(['district','district.state']);
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


        return view('admin.master.cities.index',compact('records'));
    }


        public function create(Request $request){

        $states = State::where('status',1)->get();
        $districts = District::where(['status'=>1,'state_id'=>$request->state_id])->get();
        return view('admin.master.cities.create',compact('states','districts'));
        }
        public function getDistrict(Request $request){
            $districts = District::where(['status'=>1,'state_id'=>$request->state_id])->get();
            return response()->json($districts);
        }
        public function store(CityRequest $request){

        try{
            $city = new City();
            $city->name=$request->name;
            $city->state_id=$request->state_id;
            $city->district_id=$request->district_id;
            $city->status=$request->status;
            $city->save();
            return redirect()->route('admin.city.index')->with('success','City Added Successfully!');
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

        }
    public function view(Request $request){
        $cities = City::query()->where('id',$request->id)->with(['district','district.state'])->first();


        $view=view('admin.master.cities.view')->with('record', $cities )->render();
        return response()->json(['modal_content'=>$view]);
    }
    public function edit($id){
        $city =City::find($id);
        $states = State::where('status',1)->get();
        $districts =District::where('status',1)->get();
        return view('admin.master.cities.edit',compact('states','districts','city'));
    }
    public function update(CityRequest $request){

        try{
            $stateUpdate = City::find($request->id);
            $requestData = $request->validated();
            $stateUpdate->fill($requestData);
            $stateUpdate->save();
            return redirect()->route('admin.city.index')->with('success','City Updated Successfully!');
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
    public function remove($id){
        $state = City::find($id)->delete();
        return back()->with('success','City Removed Successfully!');
    }
    public function changeStatus(Request $request)
    {
        $record = City::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }
        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }

}
