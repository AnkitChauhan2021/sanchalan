<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\CmsRequest;
use App\Models\Cms;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CmsController extends Controller
{
    public function __construct(){

    }

    public function index(Request $request){
        $records = Cms::whereIn('status',[0,1]);
        if ($request->query('search')) {
            $records = $records->where(function ($q) use ($request) {
                $q->where('title', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('url', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('meta_keyword', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('meta_desc', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('url', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('status', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('created_at', 'like', '%' . $request->query('search') . '%');

            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        return view('admin.cms.index',compact("records"));
    }
    public function Create(){
        return view('admin.cms.create');
    }
    public function Store(CmsRequest $request){
//        dd( $request);
        try{
            $name=null;
//
            if ($request->hasfile('icon')) {
                $images= $request->file('icon');
                $name = time() . rand(0, 9999) . '.' . $images->extension();
                $images->move(public_path() . '/icons/', $name);
            }

            $cms = new Cms();
            $cms->title =$request->title;
            $cms->slug =Str::slug($request->title);
            $cms->status =$request->status;
            $cms->meta_keyword =$request->meta_keyword;
            $cms->meta_desc =$request->meta_description;
            $cms->url =$request->url;
            $cms->icon =$name;
            $cms->content=$request->editor1;


            if($cms->save()){
                return redirect()->route('admin.cms.index')->with('success','Record Added Successfully!');
            }else{
                return redirect()->route('admin.cms.index')->with('error','Something Went Wrong!');
            }

        }catch(Exception $e){
            Log::info('Log message',['error'=>$e->getMessage()]);
        }

    }
     public function destroy($id){
         $record = Cms::findOrFail($id);
         if ($record->delete()) {
             return back()->with(['success' => 'Record deleted successfully']);
         } else {
             return back()->with(['error' => 'Unable to delete this record']);
         }

        }
    public function changeStatus(Request $request)
    {
        $record = Cms::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }

        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }
    public function edit($id){
        $record= Cms::find($id);
        return view('admin.cms.edit',compact('record'));
    }
    public function update(CmsRequest $request){
     try{
         $cms = Cms::find($request->id)->update([
                                                "title"=>$request->title,
                                                "slug"=>Str::slug($request->title),
                                                "content"=>$request->editor1,
                                                "meta_keyword"=>$request->meta_keyword,
                                                "meta_desc"=>$request->meta_description,
                                                "url"=>$request->url,
                                                "status"=>$request->status,
                                                ]);
         if($cms){
             return redirect()->route('admin.cms.index')->with('success','Record Updated Successfully!');
         }else{
             return redirect()->back()->with('error','Unable to Update this record');
         }

     }catch(Exception $e){
         Log::info('Log message',['error'=>$e->getMessage()]);
     }
    }

}
