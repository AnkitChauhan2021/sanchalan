<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Http\Requests\admin\configRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ConfigurationController extends Controller
{
    public function __construct(){
//
    }
    public function index(Request $request){
       $records = Config::all();
       //dd($records);die();
        return view('admin.configurations.index',compact('records'));
    }

    public  function edit($id){
        $Config_edit = Config::find($id);
        return view ('admin.configurations.edit',compact('Config_edit'));
    }

    public function create(){
      return view('admin.configurations.create');

    }

      public function store(configRequest $request){
          try{
              $config = new Config();
              $config->image_limit= $request->image_limit;
              $config->trending_post= $request->trending_post;
              $config->app_name= $request->app_name;
              $config->contact_no= $request->contact_no;
              $config->email= $request->email;
              $config->title= $request->title;
              $config->subtitle= $request->subtitle;
              $config->address= $request->address;
              $config->save();
              return redirect()->route('admin.config.index')->with('success','Config Added Successfully!');

          }catch (Exception $e) {
              Log::info('Log message', ['error' => $e->getMessage()]);
          }
    }

    public function update(configRequest $request){
        try{
            $configUpdate = Config::find($request->id);
            $requestData = $request->validated();
            $configUpdate->fill($requestData);
            $configUpdate->save();
            return redirect()->route('admin.config.index')->with('success','Config Updated Successfully!');
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }


    public function trendingPost(){

             $records = Config::all();
            return view('admin.configurations.posts.trending.index',compact('records'));
    }
    public function trendingPostEdit($id){
        $record = Config::find($id);
        return view('admin.configurations.posts.trending.edit',compact('record'));

    }
    public function trendingPostUpdate(Request  $request){
        try{
             $record = Config::find($request->id);
             if($record){
                 $record->update(["trending_post"=>$request->trending_post]);
                 return redirect()->route('admin.setting.trending.post')->with('success','Trending Post Setting Updated Successfully!');
             }else{
                 return redirect()->back()->with('error','Unable to update Setting!');

             }
        }catch (Exception $e){
            Log:: info("Log message",['error'=>$e->getMessage()]);
        }

    }

    public function changeStatus(Request $request)
    {
        $record = Config::findOrFail($request->id);
        $record->sms_api = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='SMS API <strong>'."Inactive".'</strong>';
            }else{
                $message ='SMS API <strong>'."Active".'</strong>';
            }
        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }


}
