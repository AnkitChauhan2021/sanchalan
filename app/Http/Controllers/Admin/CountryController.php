<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\countryRequest;
use App\Imports\CountryImort;
use App\Models\Country;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class CountryController extends Controller
{
    public function __construct(){
//
    }
    public function index(Request $request){
        $records = Country::whereIn('status',[0,1]);

        if ($request->query('search')) {
            $records = $records->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->query('search') . '%');
                $q->orwhere('created_at', 'like', '%' . $request->query('search') . '%');

            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        return view('admin.master.countries.index',compact('records'));
    }
    public function create(){
        return view('admin.master.countries.create');
    }
    public function store(countryRequest $request){
        try{
            $state = new Country();
            $state->name= $request->name;
            $state->status= $request->status;
            $state->save();
            return redirect()->route('admin.country.index')->with('success','Country Added Successfully!');

        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
    public function changeStatus(Request $request)
    {
        $record = Country::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }
        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }
    public function remove($id){

        $record = Country::findOrFail($id);
        if($record->delete()){
            return back()->with(['success'=>'Record deleted successfully']);
        } else {
            return back()->with(['error'=>'Unable to delete this record']);
        }

    }
    public  function edit($id){
        $Country_edit = Country::find($id);
        return view ('admin.master.countries.edit',compact('Country_edit'));
    }
    public function update(countryRequest $request){

        try{
            $stateUpdate = Country::find($request->id);
            $requestData = $request->validated();
            $stateUpdate->fill($requestData);
            $stateUpdate->save();
            return redirect()->route('admin.country.index')->with('success','Country Updated Successfully!');
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }
//    public function import(Request $request){
////        $this->validate($request, [
////            'file'  => 'required'
////        ]);
//dd($request->file());
//        Excel::import(new CountryImort, $request->file('file'));
//        return back();
//
//
//
//
//    }
}
