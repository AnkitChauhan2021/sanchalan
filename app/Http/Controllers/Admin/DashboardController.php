<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Inquiry;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('is_admin');
    }
//  admin dashboard index
    public function index()
    {
        $users = User::whereNotIn('role_id',[1])->get();
        $organizations = Company::all();
        $posts = Post ::all();
        $inquiries = Inquiry::all();

       return view ('admin.dashboard.dashboard',compact('users','organizations','posts','inquiries'));
    }
}
