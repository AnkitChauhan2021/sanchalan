<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\DistrictRequest;
use App\Models\District;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class DistrictsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {

        $records = District::query();
        $search = $request->query('search');
        if ($search) {
            $records = $records->where(function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
                $q->orwhere('created_at', 'like', '%' . $search . '%');

            })->orWhereHas("state", function ($query) use ($search) {
                $query->where(function ($data) use ($search) {
                    $data->orWhere('name', 'like', '%' . $search . '%');
                });
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->with(['state']);
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        return view('admin.master.districts.index', compact('records'));
    }

    public function create()
    {
        $states = State::where('status', 1)->get();
        return view('admin.master.districts.create', compact('states'));
    }

    public function store(DistrictRequest $request)
    {
        try {
            $district = new District();
            $district->name = $request->name;
            $district->state_id = $request->state;
            $district->status = $request->status;
            $district->save();
            return redirect()->route('admin.district.index')->with('success', 'District Added Successfully');
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function view(Request $request)
    {
        $districts = District::find($request->id);


        $view = view('admin.master.districts.view')->with('record', $districts)->render();
        return response()->json(['modal_content' => $view]);

    }

    public function edit($id)
    {
        $district = District::find($id);
        $states = State::where('status', 1)->get();
        return view('admin.master.districts.edit', compact('district', 'states'));

    }

    public function update(DistrictRequest $request)
    {

        try {
            $stateUpdate = District::find($request->id);
            $requestData = $request->validated();
            $stateUpdate->fill($requestData);
            $stateUpdate->save();
            return redirect()->route('admin.district.index')->with('success', 'District Updated Successfully!');
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function remove($id)
    {
        $state = District::find($id)->delete();
        return back()->with('success', 'District Removed Successfully!');
    }

    public function changeStatus(Request $request)
    {
        $record = District::findOrFail($request->id);
        $record->status = $request->status;
        if ($record->save()) {
            $error = 0;
            if ($record->status == false) {
                $message = 'Status changed to <strong>' . "Inactive" . '</strong>';
            } else {
                $message = 'Status changed to <strong>' . "Active" . '</strong>';
            }
        } else {
            $error = 1;
            $message = 'Unable to change status';
        }
        return response()->json(['error' => $error, 'message' => $message]);
    }
}
