<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\CreateGroupRequest;
use App\Http\Requests\admin\GroupUpdateRequest;
use App\Models\ChatGroup;
use App\Models\Company;
use App\Models\Level;
use App\Models\User;
use App\Models\UserChatGroup;
use App\Models\UserCompany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class GroupsController extends Controller
{
    public function index()
    {
        $records = ChatGroup::with('Members')->get();
//        return $records;

        return view('admin.chats.groups.index', compact('records'));
    }

    public function create()
    {
        $organizations = Company::where('status', 1)->get();
        $levels = Level::where('status', 1)->get();
        $records = User::with(['userTopCompany' => function ($query) {
            $query->orderBy('level_id','DESC')->with(['level','company']);
        }]);

//        return $records;

        $records = $records->leftJoin("user_companies","users.id","=","user_companies.user_id")
            ->leftJoin("levels","user_companies.level_id","=","levels.id");

        $records = $records->select("users.*", DB::raw("MAX(levels.position) as level_position"));

        $records = $records->groupBy("users.id")
            ->orderByRaw("IF(MAX(levels.position) IS NULL , 9999999, MAX(levels.position))");

        $records= $records->get();

        return view('admin.chats.groups.create', compact('records', 'organizations', 'levels'));
    }

    public function imageUpload(Request $request)
    {
        try {

            $folderPath = public_path('groups/icons/');
            $image_parts = explode(";base64,", $request->image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = uniqid() . '.jpg';
            $file_ = $folderPath . $file;

            file_put_contents($file_, $image_base64);

            return response()->json(['type' => 'success', 'message' => "Group Icon Updated Successfully", 'fileName' => $file, 'url' => asset(ChatGroup::GROUP_ICON_PATH . $file)]);
        } catch (Exception $e) {
            Log::info('Group Icon', ['error' => $e->getMessage()]);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $group = new ChatGroup();
            $group->name = $request->name;
            $group->icon = $request->icon;
            $group->type = ChatGroup::GROUP;
            $group->is_super_admin = true;
            $group->status = ChatGroup::ACTIVE;
            $group->user_id = Auth::user()->id;
            $group->save();

            $userChatGroups = [];
            if ($request->members) {
                foreach ($request->members as $member) {
                    $userChatGroups[] = [
                        "user_id" => $member,
                        "status" => UserChatGroup::ACTIVE,
                    ];
                }

            }
            if ($request->admins) {
                foreach ($request->admins as $admin) {
                    $userChatGroups[] = [
                        "user_id" => $admin,
                        "group_admin" => UserChatGroup::IS_ADMIN,
                        "status" => UserChatGroup::ACTIVE,
                    ];
                }

            }

            if (!empty($userChatGroups)) {
                $group->userChatGroup()->createMany($userChatGroups);
            }
            DB::commit();

            return redirect()->route('admin.groups.index')->with('success', 'Group Created Successfully!');

        } catch (Exception $e) {
            Log::info('create Group ', ['error' => $e->getMessage()]);
            DB::rollback();
        }
    }

    public function edit($id)
    {
        try {
            $record = ChatGroup::find($id);
            //  return $record;

            return view('admin.chats.groups.edit', compact('record'));

        } catch (Exception $e) {
            Log::info('Edit Group ', ['error' => $e->getMessage()]);
        }
    }

    public function update(GroupUpdateRequest $request)
    {
        try {

            $record = ChatGroup::find($request->id);
            if ($record) {
                $record->name = $request->name;
                if ($request->icon) {
                    $record->icon = $request->icon;
                }
                if ($record->save()) {
                    return redirect()->route('admin.groups.index')->with('success', 'Group Update Successfully!');
                } else {
                    return back()->with('error', 'unable to Update Group!');
                }
            } else {
                return back()->with('error', 'unable to Update Group!');
            }

        } catch (Exception $e) {
            Log::info('Update Group ', ['error' => $e->getMessage()]);
        }
    }

    public function addMember()
    {
        $organizations = Company::where('status', 1)->get();
        $levels = Level::where('status', 1)->get();
        $users = User::where(['isVerified' => 1, 'status' => 1])->get();
        return view('admin.chats.groups.add_members', compact('organizations', 'levels', 'users'));
    }

    public function organization(Request $request)
    {
//        return $request;
        if ($request->organization && $request->level) {

            $users = UserCompany::where(['company_id' => $request->organization, 'level_id' => $request->level])->with(['users', 'company', 'level'])->orderBy('level_id',"ASC")->get();

        } elseif ($request->organization && !$request->level) {

            $users = UserCompany::where('company_id', $request->organization)->with('users', 'company', 'level')->orderBy('level_id',"ASC")->get();
        } else {
            $users = UserCompany::where('level_id', $request->level)->with('users', 'company', 'level')->orderBy('level_id',"ASC")->get();
        }


        if (count($users) > 0) {
            return response()->json([
                'status' => 1,
                'message' => "Members Found Successfully!",
                'data' => $users
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Members Not Found!",
                'data' => null
            ]);
        }


    }

    public function level(Request $request)
    {
        $users = UserCompany::where('company_id', $request->level_id)->with('users', 'company', 'level')->get();


        if (count($users) > 0) {
            return response()->json([
                'status' => 1,
                'message' => "Members Found Successfully!",
                'data' => $users
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Members Not Found!",
                'data' => null
            ]);
        }

    }

//     group member list
    public function GroupsMembers(Request $request, $id)
    {
        try {
            $record = ChatGroup::find($id);
            $records = UserChatGroup::where('chat_groups_id', $id)->with('member');
            $search = $request->query('search');

            $records = $records->where(function ($q) use ($search) {
                $q->where('id', 'like', '%' . $search . '%');
            })->orWhereHas('member', function ($query) use ($search) {
                $query->where(function ($data) use ($search) {
                    $data->where('first_name', 'like', '%' . $search . '%');
                    $data->orWhere('last_name', 'like', '%' . $search . '%');
                });
            });

            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy("group_admin", "DESC");

            }
            $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));
//        return $records;


            return view('admin.chats.groups.group_members', compact('records', 'record'));


        } catch (Exception $e) {
            Log::info("Group Member List", ['error' => $e->getMessage()]);
        }
    }

// remove member from the group
    public function GroupsMembersRemove($id)
    {
        try {
            $record = UserChatGroup::findOrFail($id);

            if ($record->delete()) {
                return back()->with(['success' => 'Member deleted successfully']);
            } else {
                return back()->with(['error' => 'Unable to delete this Member']);
            }

        } catch (Exception $e) {
            Log::info("Group Member remove ", ['error' => $e->getMessage()]);
        }


    }

    public function remove($id)
    {
        try {
            $record = ChatGroup::findOrFail($id);

            if ($record->delete()) {
                return back()->with(['success' => 'Member deleted successfully']);
            } else {
                return back()->with(['error' => 'Unable to delete this Member']);
            }

        } catch (Exception $e) {
            Log::info("Group Member remove ", ['error' => $e->getMessage()]);
        }
    }
}
