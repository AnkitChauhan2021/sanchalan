<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Inquiry;
use Illuminate\Http\Request;

class InquiryController extends Controller
{
    public function __construct(){

    }
    public function index(Request $request){
        $records = Inquiry::query();
        $search = $request->query('search');
        if($search){
            $records->where(function($q) use ($request, $search) {
                $q->where('name','like','%'. $search.'%');
                $q->orwhere('email','like','%'. $search.'%');
                $q->orwhere('subject','like','%'. $search.'%');
                $q->orwhere('body','like','%'. $search.'%');
                $q->orwhere('created_at','like','%'. $search.'%');
            });


        }
        if($request->sort && $request->direction){

            $records->orderBy($request->sort,$request->direction );
        }else{
            $records->orderBy("id","DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit'):env('PAGINATION_LIMIT') ));

        return view('admin.inquiries.index',compact('records'));
    }
    public function destroy($id)
    {
        $record = Inquiry::findOrFail($id);

        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }
    public function view(Request $request){

        $record = Inquiry::where('id',$request->id)->first();
        $view=view('admin.inquiries.view')->with(['record'=>$record])->render();
        return response()->json(['modal_content'=>$view]);
    }
}
