<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\LevelsRequest;
use App\Models\Level;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LevelsController extends Controller
{
    public function __construct(){
        //
    }
        // listed levels
    public function index(Request $request){
        $records = Level::whereIn('status',[0,1]);

        if ($request->query('search')) {
            $records = $records->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->query('search') . '%');
                $q->orwhere('created_at', 'like', '%' . $request->query('search') . '%');

            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("position", "asc");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        return view('admin.master.levels.index',compact('records'));
    }
        // create  levels page  show
    public function create(){
        return view('admin.master.levels.create');
    }

    public  function store(LevelsRequest $request)
    {
        try{
        $levels = new Level();
        $levels->name = $request->name;
        $levels->position = $request->position;
        $levels->slug = Str::slug($request->name);
        $levels->status = $request->status;
        if ($levels->save()) {
            return redirect()->route('admin.level.index')->with('success', 'Level Added Successfully!');
        } else {
            return redirect()->back()->with('error', 'Oppes!, Something Went Wrong Please try Again');
        }
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
    public  function edit($id){
        $level = Level::find($id);
        return view ('admin.master.levels.edit',compact('level'));
    }
    public function update(LevelsRequest $request){

        try{
            $stateUpdate = Level::find($request->id);
            $requestData = $request->validated();
            $stateUpdate->slug = Str::slug($request->name);
            $stateUpdate->fill($requestData);

            $stateUpdate->save();
            return redirect()->route('admin.level.index')->with('success','level Updated Successfully!');
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }
    public function remove($id){
        $state = Level::find($id)->delete();
        return back()->with('success','Level Removed Successfully!');
    }
    public function changeStatus(Request $request)
    {
        $record = Level::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }
        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }

}
