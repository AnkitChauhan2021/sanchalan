<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\OrganizationRequest;
use App\Http\Requests\admin\UpdateOrganizationRequest;
use App\Models\City;
use App\Models\Company;
use App\Models\Country;
use App\Models\District;
use App\Models\Level;
use App\Models\State;
use App\Models\User;
use App\Models\CompanyDesignation;
use App\Models\UserCompany;
use App\Models\UserPermission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class OrganizationController extends Controller
{
    //construct
    public function __construct()
    {
        //
    }

    //  show Organization list page
    public function index(Request $request)
    {
        $records = Company::query();
        $search = $request->query('search');
        if ($search) {
            $records = $records->where(function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


        return view('admin.organizations.index', compact('records'));

    }


    //  create organization
    public function create()
    {

        $counties = Country::all();
        $destinations = (Level::orderBy('name')->get());
        $designation = json_encode($destinations);
        return view('admin.organizations.create', compact('destinations', 'designation', 'counties'));
    }

    public function getState(Request $request)
    {

        $country = State::where(['status' => "1", 'country_id' => $request->country_id])->get();
        return response()->json($country);
    }

    public function getDistrict(Request $request)
    {
        $districts = District::where(['status' => 1, 'state_id' => $request->state_id])->get();
        return response()->json($districts);
    }

    public function getCities(Request $request)
    {

        $cities = City::where(['status' => 1, 'district_id' => $request->dist_id])->get();

        return response()->json($cities);
    }


    // store organization
    // OrganizationRequest
    public function store(OrganizationRequest $request)
    {
//return $request;
        try {

            $organization = new Company();
            $organization->name = $request->org_name;
            $organization->name_in_hindi = $request->name_in_hindi;
            $organization->sort_name = $request->sort_name;
            $organization->state_id = $request->state_id;
            $organization->country_id = $request->country_id;
            $organization->district_id = $request->district_id;
            $organization->city_id = $request->city_id;
            $organization->status = $request->status;
            $organization->address = $request->address;
            $organization->postal_code = $request->postal_code;
            $organization->save();

//  create orgnization head or user
            $users = new User();
            $users->first_name = $request->first_name;
            $users->last_name = $request->last_name;
            $users->role_id = User::Admin;
            $users->is_company = User::Iscompany;
            $users->mobile = $request->mobile;
            $users->email = $request->email;
            $users->isVerified = true;
            $users->password = Hash::make($request->password);
            $users->save();

//            add company and user
            $UserCompany = new UserCompany();
            $UserCompany->user_id = $users->id;
            $UserCompany->company_id = $organization->id;
            $UserCompany->level_id = $request->designation;
            $UserCompany->role_id = User::Admin;
            $UserCompany->save();

// create Designation in orgnization

            foreach ($request->input('companydesignations') as $adddesignation) {

                $companyDesinations = new CompanyDesignation ();
                $companyDesinations->company_id = $organization->id;
                $companyDesinations->desgination_id = $adddesignation;
                $companyDesinations->save();
            }


            return redirect()->route('admin.organization.index')->with('success', 'Organization Created Successfully!');

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }


    public function edit($id)
    {
        try {
            $record = Company::where('id', $id)->first();
            if ($record->id) {
                $user = UserCompany::where('company_id', $record->id)->first();


                $district = District::find($record->district_id);

                $city = City::find($record->city_id);
                $levels = Level:: all();
            }

//            return $record;


//return $record;
            $states = State::all();
            return view('admin.organizations.edit', compact('record', 'states', 'user', 'district', 'city', 'levels'));
        } catch (\Exception $e) {

            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

    public function update(UpdateOrganizationRequest $request)
    {


        try {
//            $user = User::find($request->user_id);
//            if($user){
//                $user->update([
//                    'first_name' => $request->first_name,
//                    'last_name' => $request->last_name,
//                    'email' => $request->user_email,
//                    'mobile' => $request->mobile,
//                ]);
//            }else{
//                $user = new User();
//                $user->first_name = $request->first_name;
//                $user->role_id = 2;
//                $user->last_name = $request->last_name;
//                $user->designation = $request->designation;
//                $user->email = $request->user_email;
//                $user->mobile = $request->mobile;
//                $user->password =bcrypt($request->first_name);
//                $user->save();
//                if($request->user_id !== null){
//                    $id= $request->user_id;
//               }else{
//                    $id= $user->id;
//                }


            $company = Company::find($request->company_id)->update([
                'name' => $request->org_name,
                'name_in_hindi' => $request->name_in_hindi,
                'sort_name' => $request->sort_name,
                'email' => $request->email,
//                'user_id' => $id,
                'state_id' => $request->state_id,
                'district_id' => $request->district_id,
                'city_id' => $request->city_id,
                'status' => $request->status,
                'address' => $request->address,
                'postal_code' => $request->postal_code,
            ]);

            if ($company) {
                return redirect()->route('admin.organization.index')->with('success', 'Company Updated Successfully!');
            } else {
                return redirect()->back()->with('error', 'Oppes!, Something Went Wrong! please Try Again!');

            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function changeStatus(Request $request)
    {
        $record = Company::findOrFail($request->id);
        $record->status = $request->status;
        if ($record->save()) {
            $error = 0;
            if ($record->status == false) {
                $message = 'Status changed to <strong>' . "Inactive" . '</strong>';
            } else {
                $message = 'Status changed to <strong>' . "Active" . '</strong>';
            }

        } else {
            $error = 1;
            $message = 'Unable to change status';
        }
        return response()->json(['error' => $error, 'message' => $message]);
    }

    public function toggleAdminHead(Request $request)
    {
        $user_id = $request->user_id;
        $company_id = $request->company_id;
        $status = 0;
        $message = '';
        $data = null;
        try {
            DB::beginTransaction();
            $record = User::findOrFail($user_id);
            $updateRoleId = $record->role_id === 3 ? 2 : 3;
            $record->role_id = $updateRoleId;
            if ($record->save()) {
                $status = 1;
                UserCompany::where(['user_id' => $record->id, 'company_id' => $company_id])->update(['role_id' => $updateRoleId]);
                if ($updateRoleId === 3) {
                    $permission = new UserPermission();
                    $permission->permission_id = 1;
                    $permission->user_id = $record->id;
                    $permission->company_id = $company_id;
                    $permission->save();
                } else {
                    UserPermission::where(['company_id' => $company_id, 'user_id' => $record->id])->delete();
                }
            } else {
                $status = 0;
                $message = 'Unable to process request';
            }
            DB::commit();
            $data['user'] = $record;
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function likeTojoin(Request $request)
    {


        $record = Company::findOrFail($request->id);
        $record->likeToJoin = $request->status;
        if ($record->save()) {
            $set = Company::whereNotIn('id', array($request->id))->where('likeToJoin', 1)->update(['likeToJoin' => 0]);
            $error = 0;
            if ($record->status == false) {
                $message = 'Like To Join <strong>' . "Unset" . '</strong>';
            } else {
                $message = 'Like To Join <strong>' . "Set" . '</strong>';
            }

        } else {
            $error = 1;
            $message = 'Unable to change status';
        }
        return response()->json(['error' => $error, 'message' => $message]);

    }


    public function approvedOrNot(Request $request)
    {
        $record = User::findOrFail($request->id);
        $record->status = $request->status;
        if ($record->save()) {
            $error = 0;
            if ($record->status == 1) {
                $message = 'User Permission changed to <strong>' . "Approved" . '</strong>';
            } elseif ($record->status == 2) {
                $message = 'User Permission changed to <strong>' . "Reject" . '</strong>';
            } else {
                $message = 'User Permission changed to <strong>' . "Pending" . '</strong>';
            }

        } else {
            $error = 1;
            $message = 'Unable to change status';
        }
        return response()->json(['error' => $error, 'message' => $message]);
    }

    public function remove($id)
    {
        $record = Company::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    public function memberList(Request $request, $id)
    {
        $org = Company::where(['id' => $id, 'status' => 1])->first();
        $records = UserCompany::where(['company_id' => $id])->with('users', 'level');
        $search = $request->query('search');
        if ($search) {
            $records = $records->orWhereHas("users", function ($query) use ($search) {
                ;
                $query->where('email', 'like', '%' . $search . '%');
//                $query->orWhere('mobile', 'like', '%' . $search . '%');
//                $query->orWhere('first_name', 'like', '%' .$search . '%');
//                $query->orWhere('last_name', 'like', '%' . $search . '%');
//                $query->orWhere('designation', 'like', '%' . $search . '%');
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }


        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

//        return $records;
        if (count($records) > 0) {

            return view('admin.organizations.memberList', compact('records', 'org'));
        } else {
            return back()->with('error', "Member Does Not exist In This Organization! ");
        }
    }

    public function memberDelete($id)
    {
        $record = User::findOrFail($id);
        if ($record->delete()) {
            return redirect()->route('admin.organization.index')->with(['success' => 'Record deleted successfully']);
        } else {
            return redirect()->route('admin.organization.index')->with('error', "Member Does Not exist In This Organization! ");
        }
    }

    public function addMember(Request $request, $company_id)
    {

        $levels = Level::all();

        $users = User::where('isVerified', 1)->where('role_id', '<>', 1)->with('userComapny', function ($query) use ($company_id) {
            $query->where('company_id', '<>', $company_id);
        });
        if (!empty($request->search)) {
            $search = $request->search;
            $users->where(function ($q) use ($search) {

                $q->orWhereRaw("CONCAT(first_name,' ',last_name) like '$search%'");

                $q->orWhere('first_name', 'like', $search . '%');

                $q->orWhere('last_name', 'like', $search . '%');

                $q->orWhere('mobile', $search);
            });
        }
        $company = Company::find($company_id);
        $records = $users->paginate(15);
//        return $records;

        return view('admin.organizations.addMember', compact('company', 'levels', 'records'));

    }


    public function addMemberStore(Request $request)
    {

        try {
            $levels = array_values(array_filter($request->level));


            foreach ($levels as $key => $level) {

                $numberOfCompany = UserCompany::where('user_id', $request->user[$key])->count();
                $userCompany = UserCompany::where(['user_id' => $request->user[$key], 'company_id' => $request->company_id])->first();

                if ($numberOfCompany < 4) {
                    if (!$userCompany) {
                        $userCompany = new UserCompany();
                        $userCompany->user_id = $request->user[$key];
                        $userCompany->level_id = $level;
                        $userCompany->company_id = $request->company_id;
                        $userCompany->save();
                    }
                }
            }

            return redirect()->back()->with('success', 'Member Added Successfully!');


        } catch (Exception $e) {
            Log::info('Add member ', ['error' => $e->getMessage()]);
        }

    }
}





