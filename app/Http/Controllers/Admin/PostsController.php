<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\PostRequest;
use App\Models\Post;
use App\Models\Report;
use App\Models\PostImage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PostsController extends Controller
{
    public function __construct(){

    }
    public function index(Request $request){
        $records = Post::query();

        $search = $request->query('search');
        if($search){
            $records->where(function($q) use ($request, $search) {
                $q->where('description','like','%'. $search.'%');
            })->orWhereHas("getUser",function($query) use ($search){
                $query->where(function($data) use ($search){
                    $data->orWhere('first_name','like','%'. $search.'%');
                    $data->orWhere('last_name','like','%'. $search.'%');
                });
            });

        }
        if($request->sort && $request->direction){

            $records->orderBy($request->sort,$request->direction );
        }else{
            $records->orderBy("id","DESC");

        }
        $records=$records->with(['getPostImage','getUser']);
        $records = $records->paginate(($request->query('limit') ? $request->query('limit'):env('PAGINATION_LIMIT') ));

        return view('admin.posts.index',compact('records'));
    }
    public function reportsIndex(Request $request){

        $records = Post::withcount('reports')->having('reports_count', '>', 0)->orderBy('reports_count', 'desc');
        $search = $request->query('search');
        if($search){
            $records->where(function($q) use ($request, $search) {
                $q->where('description','like','%'. $search.'%');
            });
        }

        $records = $records->paginate(($request->query('limit') ? $request->query('limit'):env('PAGINATION_LIMIT') ));

        return view('admin.reports.index',compact('records'));

    }
    public function destroyReport($id)
    {
        $record = Post::findOrFail($id);

        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }
    public function create(){
        return view('admin.posts.create');

    }
    public function store(PostRequest $request){
        DB::beginTransaction();
       try{
           $post = new Post();
           $post->slug= Str::random(20);
           $post->user_id= Auth::user()->id;
           $post->description= $request->editor1;
           $post->type= "super_admin";
           $post->save();
           $id = $post->id;

           if ($request->hasfile('images')) {

               foreach ($request->file('images') as $images) {
                   $name = time() . rand(0, 9999) . '.' . $images->extension();
                   $images->move(public_path() . '/post/images/', $name);
                   $file = new PostImage();
                   $file->post_id = $id;
                   $file->image = $name;
                   $file->save();
               }
           }
           DB::commit();
           $addedPost= Post::where('id',$id)->with('getPostImage')->first();
           if($addedPost){
               return redirect()->route('admin.posts.index')->with('success','Post create Successfully!');
           }else{
               return redirect()->with('error','Unable to Create Post!');
           }

       }catch(Exception $e){
           Log::info('Log message', ['error' => $e->getMessage()]);
           DB::rollback();
       }

    }
    public function edit($id){
        $post = Post::find($id);

        $postImage = PostImage::where('post_id',$id)->get();

        if($post){
           return view('admin.posts.edit',compact('post','postImage'));
        }else{
            return back()->with('error','unable To Update Post');
        }

    }
    public function update(PostRequest $request){

        $post = Post::find($request->id);
        if($post){
            if ($request->hasfile('images')) {

                foreach ($request->file('images') as $images) {
                    $name = time() . rand(0, 9999) . '.' . $images->extension();
                    $images->move(public_path() . '/post/images', $name);
                    $file = new PostImage();
                    $file->post_id = $request->id;
                    $file->image = $name;
                    $file->save();
                }
            }
            $post->update([
                'slug' =>Str::random(20),
                'user_id' =>$post->user_id,
                'description'=>$request->editor1,
                'type'=>"super_admin",
            ]);
            return redirect()->route('admin.posts.index')->with('success','Record Updated Successfully');
        }else{
            return redirect()->back()->with('error','unable to updated Record');
        }

    }
    public function destroy($id)
    {
        $record = Post::findOrFail($id);

        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }
    public function changeStatus(Request $request)
    {
        $record = Post::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }

        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }
    public function destroyImage($id){
         $removeIMage = PostImage::find($id);
         if($removeIMage->delete()){
             return response()->json(['type' => 'success','message'=>'Picture Removed Successfully!']);
         }else{
             return response()->json(['type' => 'error','message'=>'unable to remove Picture!']);
         }
    }
    public function view(Request $request){

        $post = Post::where('id',$request->id)->with(['getPostImage','getUser','getCompany'])->first();

        $postImage = PostImage::where('post_id',$request->id)->get();

        $view=view('admin.posts.view')->with(['record'=>$post,'postImage'=>$postImage])->render();
        return response()->json(['modal_content'=>$view]);
    }


}
