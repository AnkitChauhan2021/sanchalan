<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\admin\AdminUpdateProfileRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Validation\Rule;

class ProfileConntroller extends Controller
{
    public function __construct(){

    }

//    admin Profile View
    public function index(){
        $user = User::find(Auth::user()->id);

        return view('admin.profile.index',compact('user'));
    }
    public function edit(){
        $user = User::find(Auth::user()->id);
        return view('admin.profile.edit',compact('user'));
    }

    public function upload(Request $request)
    {
        try{
        $user = User::find(Auth::user()->id);
        $oldImage= $user->profile_pic;
        if($oldImage && file_exists(public_path(User::PROFILE_PIC_PATH.$oldImage))){
            @unlink(public_path(User::PROFILE_PIC_PATH.$oldImage));
        }
        $folderPath = public_path('admin/profile/');

        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file =  uniqid() . '.jpg';
        $file_ = $folderPath . $file;

        file_put_contents($file_, $image_base64);
       User::find(Auth::user()->id)->update(['profile_pic' =>$file ]);
        return response()->json(['type'=>'success','message'=>"Profile Image Updated Successfully",'url'=> asset(User::PROFILE_PIC_PATH.$file)]);
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function update(AdminUpdateProfileRequest $request){
        try{

         //   $role = Role::where('slug','super_admin')->first();
            $editProfile = User::find(Auth::user()->id);
            $requestData = $request->validated();
            $editProfile->fill($requestData);
           // $editProfile->role_id=$role->id;
            $editProfile->save();
            $request->session()->flash('title'," Hello ".Auth::user()->first_name." ".Auth::user()->last_name);
            $request->session()->flash('success',"Profile Update Successfully.");
               return back();


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            }

    }
}
