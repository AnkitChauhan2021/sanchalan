<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\StatesRequest;
use App\Models\Country;
use App\Models\State;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StatesController extends Controller
{
    public function __construct(){
        //
    }

    public function index(Request $request){
        $records = State::query();
//        with('Country');
        $search = $request->query('search');
        if ($search) {
            $records = $records->where(function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
                $q->orwhere('created_at', 'like', '%' . $search . '%');
            })->orWhereHas("Country",function($query) use ($search){
                $query->where(function($data) use ($search){
                    $data->orWhere('name','like','%'. $search.'%');
                });
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records=$records->with(['Country']);
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


        return view('admin.master.states.index',compact('records'));
    }
    public function create(){
        $countries= Country::all();
        return view('admin.master.states.create',compact('countries'));
    }
    public function store(StatesRequest $request){
          try{
              $country = Country::where(['name'=>'india','status'=>1])->first();
              $state = new State();
              $state->name= $request->name;
              $state->country_id= $country->id;
              $state->status= $request->status;
              $state->save();
              return redirect()->route('admin.state.index')->with('success','State Added Successfully!');

          }catch (Exception $e) {
              Log::info('Log message', ['error' => $e->getMessage()]);
          }
    }
//    edit state
    public  function edit($id){
         $state_edit = State::find($id);
         $countries = Country::all();
         return view ('admin.master.states.edit',compact('state_edit','countries'));
    }
   public function update(StatesRequest $request){

        try{
            $stateUpdate = State::find($request->id);
            $requestData = $request->validated();
            $stateUpdate->fill($requestData);
            $stateUpdate->save();
            return redirect()->route('admin.state.index')->with('success','State Updated Successfully!');
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

   }
   public function remove($id){
        $state = State::find($id)->delete();
        return back()->with('success','State Removed Successfully!');
   }
   public function view(Request $request){
       $state = State::where('id',$request->id)->with('Country')->first();

       $view=view('admin.master.states.view')->with('record', $state )->render();
       return response()->json(['modal_content'=>$view]);


   }
    public function changeStatus(Request $request)
    {
        $record = State::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }
        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }
}
