<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\RegisterUserRequest;
use App\Http\Requests\admin\UpdateUserRequest;
use App\Models\Company;
use App\Models\CompanyDesignation;
use App\Models\Level;
use App\Models\User;
use App\Models\UserCompany;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class  UsersController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {

        $records = User::whereNotIn('role_id', [1])->with('company', 'level');
        $search=$request->query('search');
        if ( $search) {
            $records = $records->where(function ($q) use ($request) {
                $q->where('email', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('mobile', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('first_name', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('last_name', 'like', '%' . $request->query('search') . '%');
                $q->orWhere('designation', 'like', '%' . $request->query('search') . '%');

            })->orWhereHas('company',function($query) use ($search){
                $query->where(function($data) use ($search){
                    $data->where('name', 'like', '%' .$search  . '%');
                });
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));
//        return $records;
        return view('admin.users.index', compact('records'));
    }

        public function userOrganization($id){
         try{
             $records = UserCompany::where('user_id',$id)->with('company','level')->paginate(20);
             $user = User::find($id);
//             dd( $records);
              if(count($records)>0){
                  return view('admin.users.userCompany', compact('records','user'));
              }else{
                  return back()->with('error','Company Not Exist on this User Profile');
              }

         }catch(Exception $e){
             Log::info('Log message', ['error=>'=>$e->getMessage()]);
         }

        }

    public function remove($id)
    {
        $record = User::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }
//    delete User company from multiple company
    public function destroyUserCompany($id){
        $record = UserCompany::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }
    }

    public function getLevels(Request $request)
    {
        $levels = CompanyDesignation::where('company_id', $request->organization_id)->with('levels')->get();
        return response()->json($levels);
    }

    public function create()
    {
        $organizations = Company::where('status', 1)->orderBy('name')->get();
        $levels = Level::where('status', 1)->get();
        return view('admin.users.create', compact('organizations','levels'));

    }

    public function store(RegisterUserRequest $request)
    {
        try {
            $user = new User();
            $user->role_id = User::Admin;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->isVerified = 1;
            $user->password = Hash::make($request['password']);
            $user->save();

            $userComapny = new UserCompany();
            $userComapny->role_id = User::UserRole;
            $userComapny->user_id= $user->id;
            $userComapny->company_id= $request->organization;
            $userComapny->level_id = $request->level;
            $userComapny->designation = $request->designation;


            if ($userComapny->save()) {
                return redirect()->route('admin.users.index')->with('success', 'User Added Successfully!');

            } else {
                return redirect()->back()->with('error', 'Something Went Wrong!');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }
    public function edit($id){
        $organizations = Company::where('status', 1)->orderBy('name')->get();
        $user = User::where('id',$id)->with('userComapny.company','userComapny.level')->get();
        return view('admin.users.edit',compact('organizations','user'));
    }
    public function update(Request $request){

        $validated = $request->validate([
            "first_name"=>'nullable|string',
            "last_name"=>'nullable|string',
            'designation'=>'nullable|string',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($request->id, 'id')
            ],'mobile' => [
                'nullable',
                Rule::unique('users')->ignore($request->id, 'id')
            ],

        ]);
          try{
              $user = User::where('id',$request->id)->first();
                 $user->update([
                  'first_name'=>$request->first_name,
                  'last_name'=>$request->last_name,
                  'email'=>$request->email,
                  'mobile'=>$request->mobile,
              ]);
//                 $usercomapny = UserCompany::where('user_id',$request->id)->dete
              if($user){
                  return redirect()->route('admin.users.index')->with('success','User Update Successfully!');
              }else{
                  return back()->with('error','Something Went Wrong');
              }

          }catch (Exception $e){
              Log::info('Log message', ['error' => $e->getMessage()]);
          }

    }
    public function changeUserPassword(Request $request){


        $validated = $request->validate([
              'password'=>'required|string|min:8',
              'password_confirmation' => 'required|min:8|same:password',
        ]);
        $user = User::find($request->id);
        if($user){
            $user->update(["password" => Hash::make($request['password'])]);
         return response()->json(['type'=>'success','message'=>'User Password Changed Successfully!']);
        }else{
            return response()->json(['type'=>'error','message'=>'Something Went Wrong!']);
        }


    }
    public function approvedOrNot(Request $request)
    {

        $record = UserCompany::findOrFail($request->id);
        $record->isApproved = $request->isApproved;
        if($record->save()){
            $user =UserCompany::where('id',$request->id)->first();
            $company =Company::where(['permission_to_make_manch_org'=>1,'status'=>1])->first();

            $check_manch_org =UserCompany::where(['user_id'=>$user->user_id,'company_id'=>$company->id])->first();
            $find_user = User::find($user->user_id);


            if(!$check_manch_org){
                $add_manch_org = new UserCompany();
                $add_manch_org->user_id= $user->user_id;
                $add_manch_org->company_id= $company->id;
                $add_manch_org->role_id= $find_user->role;
                $add_manch_org->is_like= 1;
                $add_manch_org->likeToJoin= 1;
                $add_manch_org->isApproved= 1;
                $add_manch_org->permission= 'all';
                $add_manch_org->save();
            }

            $error = "success";
            if($record->isApproved=="1"){
                $message ='User Permission changed to <strong>'."Approved".'</strong>';
            }elseif($record->isApproved=="2"){
                $message ='User Permission changed to <strong>'."Reject".'</strong>';
            }else{
                $message ='User Permission changed to <strong>'."Pending".'</strong>';
            }

        } else {
            $error = "error";
            $message ='Unable to change status';
        }
        return response()->json(['type' => $error,'message' => $message]);
    }
    public function changeStatus(Request $request)
    {
        $record = User::findOrFail($request->id);
        $record->status = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }

        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }
    public function notifications(Request $request)
    {
        $record = User::findOrFail($request->id);
        $record->notification = $request->status;
        if($record->save()){
            $error = 0;
            if($record->status==false){
                $message ='Status changed to <strong>'."Inactive".'</strong>';
            }else{
                $message ='Status changed to <strong>'."Active".'</strong>';
            }

        } else {
            $error = 1;
            $message ='Unable to change status';
        }
        return response()->json(['error' => $error,'message' => $message]);
    }


    public function addOrganizationOnUserProfile($id){
        $organizations = Company::where('status',1)->get();
        $user = User::find($id);
        $levels = Level::where('status', 1)->get();
        return view('admin.users.addUserorganization', compact('organizations','user','levels'));

    }
    public function UserOrganizationStore(UpdateUserRequest $request){

               try{
                   $user = User::find($request->id);
                   if($user){
                       $user->update([
                          'first_name'=> $request->first_name,
                          'last_name'=> $request->last_name,
                          'email'=> $request->email,
                          'mobile'=> $request->mobile,
                       ]);
                       if($request->organization){
                           $usercompany = new UserCompany();
                           $usercompany->user_id =$request->id;
                           $usercompany->company_id  =$request->organization;
                           $usercompany->level_id  =$request->level;
                           $usercompany->designation =$request->designation;
                           $usercompany->role_id =$user->role_id;
                           $usercompany->save();
                       }
                       return redirect()->route('admin.users.index')->with('User Update Successfully');
                   }else{
                       return redirect()->back()->with('error','Unable to Update User');
                   }

               }catch(Exception $e){
                   Log::info('Log message', ['error' => $e->getMessage()]);
               }

    }
}
