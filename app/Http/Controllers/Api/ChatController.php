<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SendChatRequest;
use App\Models\Chat;
use App\Models\MessageReceiver;
use App\Models\User;
use App\Models\UserChatGroup;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ChatController extends Controller
{

    public function chatRequestStore(SendChatRequest  $request)
    {
        $status = 0;
        $message = "";
        try{
            Chat::updateOrCreate(
                ["sender_id" => $request->sender_id , "receiver_id" => $request->receiver_id],
                ["sender_id" => $request->sender_id , "receiver_id" => $request->receiver_id, 'status' => Chat::PENDING]
            );
            $status = 1;
            $message = "Request Send Successfully!";
        } catch (Exception $e){
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code'=> $status,
            'Message'=> $message,
            'data' => [],
        ]);
    }

    public function AcceptRequest(Request $request)
    {
        $status = 0;
        $message = "";

        try{
            $accept = Chat::find($request->id);
            if( $request->status == Chat::ACCEPT ) {
                $accept->status = Chat::ACCEPT;
                $accept->save();
                $message = "Request Accepted Successfully!";

                Chat::orWhere(function ($query) use($accept) {
                    $query->where("sender_id", $accept->sender_id);
                    $query->where("receiver_id", $accept->receiver_id);
                    $query->where("status", Chat::PENDING);
                })->orWhere(function ($query) use($accept) {
                    $query->where("sender_id", $accept->receiver_id);
                    $query->where("receiver_id", $accept->sender_id);
                    $query->where("status", Chat::PENDING);
                })->delete();

            } else {
//                $accept->status = Chat::REJECT;
//                $accept->save();
                $accept->delete();
                $message = "Request Rejected Successfully!";
            }

            $status = 1;
        }catch (Exception $e){
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code'=> $status,
            'message'=> $message,
            'data' => [],
        ]);
    }


     public function SendRequestList(){
        try{
             $records= Chat::where('sender_id',Auth::user()->id)->with('sendChatRequest')->get();
             if($records){
                 return response()->json([
                     'status'=>1,
                     'message'=>"Records Found Successfully!",
                     'data'=>$records
                 ]);
             }else{
                 return response()->json([
                     'status_code'=>0,
                     'message'=>"unable to Records",
                     'data'=>null
                 ]);
             }

        }catch (Exception $e){
            Log::info("Log message",["error"=>$e->getMessage()]);
        }

     }
     public function ReceiveRequestList(){

         $status = 0;
         $message = "";
         $data = [];

         try{

             $records= Chat::where([
                 'receiver_id' => auth()->user()->id,
                 'status'=> Chat::PENDING
             ])->has('receiveChatRequest')->with('receiveChatRequest.topLevelUserCompany.level')->orderBy("id", "DESC")->get();

             if($records->count()) {
                 foreach ($records as $key => $record) {
                     $data[$key]['id'] = $record->id;
                     $data[$key]['sender_id'] = $record->sender_id;
                     $data[$key]['receiver_id'] = $record->receiver_id;
                     $data[$key]['status'] = $record->status;
                     $data[$key]['user_id'] = $record->receiveChatRequest->id;
                     $data[$key]['user_name'] = $record->receiveChatRequest->full_name;
                     $data[$key]['user_profile_pic'] = $record->receiveChatRequest->logo;
                     $data[$key]['is_chat_request'] = $record->receiveChatRequest->is_chat_request;
                     $data[$key]['is_follow'] = $record->receiveChatRequest->is_follow;
                     $data[$key]['company'] = ($record->receiveChatRequest->topLevelUserCompany && $record->receiveChatRequest->topLevelUserCompany->company ? $record->receiveChatRequest->topLevelUserCompany->company->name : "");
                     $data[$key]['level_name'] = ($record->receiveChatRequest->topLevelUserCompany && $record->receiveChatRequest->topLevelUserCompany->level ? $record->receiveChatRequest->topLevelUserCompany->level->name : "");
                     $data[$key]['level_position'] = ($record->receiveChatRequest->topLevelUserCompany && $record->receiveChatRequest->topLevelUserCompany->level ? $record->receiveChatRequest->topLevelUserCompany->level->position : 0);
                     $data[$key]['designation'] = ($record->receiveChatRequest && !is_null($record->receiveChatRequest->topLevelUserCompany->designation) ? $record->receiveChatRequest->topLevelUserCompany->designation : "");
                 }
             }

             $status = 1;
             $message = "Records Found Successfully!";

         }catch (Exception $e){
             $message = $e->getMessage();
         }

         return response()->json([
             'status_code'=> $status,
             'Message'=> $message,
             'data' => $data,
         ]);

     }

     public function ConfirmChatUsers(Request $request){
        try{
            $receiver = Chat::where(['receiver_id' => Auth::user()->id,'status'=> Chat::ACCEPT])->pluck('sender_id')->toArray();
            $Sender = Chat::where(['sender_id' => Auth::user()->id,'status' => Chat::ACCEPT])->pluck('receiver_id')->toArray();

            $users = array_merge($receiver,$Sender);

            $uniqueUser = array_unique($users);

            $addedUserIds = [];
            if($request->chat_group_id) {
                $addedUserIds = UserChatGroup::where('chat_group_id',$request->chat_group_id)->pluck("user_id")->toArray();
            }

            $uniqueUser = array_diff($uniqueUser, $addedUserIds);

            $records = User::whereIn('id',$uniqueUser);

            $records = $records->with([
                "topLevelUserCompany.company:id,name",
                "topLevelUserCompany.level:id,name,position"
            ]);

            $search = $request->search;
            if ( $search) {
                $records = $records->where(function ($q) use ($search) {
                    $q->where('email', 'like', $search . '%');
                    $q->orWhere('first_name', 'like', $search . '%');
                    $q->orWhere('last_name', 'like', $search . '%');
                });
            }

            $records = $records->orderBy("first_name")->get();

            $data = [];

            if($records->count()) {
                foreach ($records as $key => $record) {
                    $data[$key]['user_id'] = $record->id;
                    $data[$key]['user_name'] = $record->full_name;
                    $data[$key]['user_profile_pic'] = $record->logo;
                    $data[$key]['user_profile_thumbnail'] = $record->thumbnail_url;
                    $data[$key]['is_chat_request'] = $record->is_chat_request;
                    $data[$key]['is_follow'] = $record->is_follow;
                    $data[$key]['company'] = ($record->topLevelUserCompany && $record->topLevelUserCompany->company ? $record->topLevelUserCompany->company->name : "");
                    $data[$key]['level_name'] = ($record->topLevelUserCompany && $record->topLevelUserCompany->level ? $record->topLevelUserCompany->level->name : "");
                    $data[$key]['level_position'] = ($record->topLevelUserCompany && $record->topLevelUserCompany->level ? $record->topLevelUserCompany->level->position : 0);
                    $data[$key]['designation'] = ($record->topLevelUserCompany && !is_null($record->topLevelUserCompany->designation) ? $record->topLevelUserCompany->designation : "");
                }
            }

            return response()->json([
                'status' => 1,
                'message' => "Records Found Successfully!",
                'data' => $data
            ]);

        }catch (Exception $e){
            return response()->json([
                'status' => 1,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
     }



     public function getNotificationCount(){

        try{
            $chat_request  = Chat::where(['receiver_id'=>Auth::user()->id,'status'=>0])->has('receiveChatRequest')->count();
            $messageCount  = MessageReceiver::where(['user_id'=>Auth::user()->id,'is_read'=>0])->count();


                return response()->json([
                    'status_code'=>1,
                    'message'=>"Records Found Successfully!",
                    'chat_request_count'=>$chat_request,
                    'message_count'=>$messageCount

                ]);


        }catch (Exception $e){
            Log::info("Get Notification Count",['error'=>$e->getMessage()]);
        }

     }
}
