<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CityController extends Controller
{
    public function __construct(){
//
    }
    public function index(Request $request){
        try{
           if($request->district_id==''){
               $cities = City::where('status','1')->select('id','state_id','district_id','name','status')->get();
            }else{
              $cities = City::where('district_id',$request->district_id)->where('status','1')->select('id','state_id','district_id','name','status')->get();

            }

             if(count($cities)>0){
                 return response()->json([
                      'status_code'=>1,
                     'message'=>'Cities Found Successfully',
                     'data'=>[
                         'cities'=>$cities
                     ]
                 ]);
             }else{
                  return response()->json([
                      'status_code'=>0,
                      'message'=>'Cities Not Found',
                      'data'=>null
                  ]);
             }

        }catch (Exception $e){
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
}
