<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cms;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    public function __construct(){
//
    }
    public function index(Request $request){
        $records = Cms::where('status',1)->select('id','title','slug','content','meta_keyword','meta_desc','url','status','icon');
        $search =$request->query('search');
        if($search){
            $records =  $records->where(function($q) use ($search) {
                $q->where('id','like','%'.$search.'%');
            });
        }
        if($request->sort && $request->direction){

            $records->orderBy($request->sort,$request->direction );
        }else{
            $records->orderBy("id","DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit'):env('PAGINATION_LIMIT') ));

        if(count($records)>0){
            return response()->json([
                'status_code' => 1,
                'message' => 'Record Found Successfully!',
                'data'=>$records
            ]);
        }else{
            return response()->json([
                'status_code'=>0,
                'message' => 'Record Not Found!',
                'data'=>null
            ]);
        }
    }
    public function cmsList(){
        $cms = Cms::where('status',1)->select('id','title','status','icon')->get();
        if(count($cms)>0){
            return response()->json([
                'status_code' => 1,
                'message' => 'Record Found Successfully!',
                'data'=>$cms
            ]);
        }else{
            return response()->json([
                'status_code'=>0,
                'message' => 'Record Not Found!',
                'data'=>null
            ]);
        }
    }
}
