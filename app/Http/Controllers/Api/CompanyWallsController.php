<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Company;
use App\Models\CompanyWallVisit;
use App\Models\Magazine;
use App\Models\Pole;
use App\Models\Post;
use App\Models\PostBlock;
use App\Models\UserCompany;
use App\Models\UserPermission;
use App\Models\UserPole;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class CompanyWallsController extends Controller
{
    public function index(Request $request,$company_id){
        try{
            $company = Company::find($company_id);
            if($company) {
                //check last visit on this company for this user

                $visit =CompanyWallVisit::where(['user_id'=>Auth::user()->id,'company_id'=>$company->id])->first();


                // if user last visit exist on this company then update current visit time
                if($visit){

                    $visit->update(['last_visit_at'=>Carbon::now()]);

                }else{
//                    else create current visit time on this company

                    $visits = new CompanyWallVisit();
                    $visits->user_id= Auth::user()->id;
                    $visits->company_id= $company->id;
                    $visits->last_visit_at= Carbon::now();
                    $visits->save();

                }

                $records= Post::where(['status' => Post::Active, 'type' => 'admin','company_id'=>$company_id])->with('users', 'getPostImage', 'getPostVideo', 'comments', 'comments.commentBy', 'likes', 'likes.likeBy')->orderBy('id', 'DESC')->get();

//                $limit = $request->record_per_page;
//                $records = $records->paginate((int)$limit);
                $permissions = UserPermission::where(['company_id'=>$company_id,'user_id'=>Auth::user()->id])->with('permission')->select('id','company_id','user_id','permission_id')->get();

                if ($records || $permissions) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Records Found Successfully!',
                        'data' => [
                            'posts'=>$records,
                            'permissions'=>$permissions
                        ]

                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Records not found!',
                        'data' => null
                    ]);
                }
            }else{
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Invalid Company Id!',
                    'data' => null
                ]);
            }

        }catch (Exception $e){

        }
    }
}
