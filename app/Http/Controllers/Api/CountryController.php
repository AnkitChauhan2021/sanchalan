<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;

class CountryController extends Controller
{
    public function __construct(){
//
    }
    public function index(){
        try{
            $countries = Country::where('status','1')->select('id','name','status')->get();
            if(count($countries)>0){
                return response()->json([
                    'status_code'=>1,
                    'message'=>'Country Found Successfully',
                    'data'=>[
                        'countries'=>$countries
                    ]
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Country Not found',
                    'data'=>null
                ]);
            }

        }catch (Exception $e){
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
}
