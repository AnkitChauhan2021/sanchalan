<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\District;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DistrictController extends Controller
{
    public function __construct(){
//
    }
    public function index(Request $request){
        try{
            if($request->state_id==''){
               $districts = District::where('status','1')->select('id','state_id','name','status')->get();
            }else{
                $districts = District::where('state_id',$request->state_id)->where('status','1')->select('id','state_id','name','status')->get();

            }


            if(count($districts)>0){
                return response()->json([
                    'status_code'=>1,
                    'message'=>'Districts Found Successfully',
                    'data'=>[
                        'districts'=>$districts
                    ]
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Districts Not Found',
                    'data'=>null
                ]);
            }

        }catch (Exception $e){
            Log::info('Log message', ['error' => $e->getMessage()]);
            }
    }
}
