<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CompanyDesignation;
use App\Models\Level;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LevelsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        try{
            $records = Level::where('status',1)->select('id','name','position','status')->get();
            if(count($records)>0){
                return response()->json([
                    'status_code'=>1,
                    'message'=>'Levels Found Successfully!',
                    'data'=>[
                        'levels'=>$records
                    ]
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Levels Not Found',
                    'data'=>null
                ]);
            }

        }catch (Exception $e){
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function Get_All_Level(Request $request){
        try{
//             return "Hello";
            $records = Level::where('status',1)->select('id','name','position','status');


            $search = $request->search;

            if($search){
                $records->where(function($q) use ($request, $search) {
                    $q->where('name','like','%'. $search.'%');
                });

            }
//            if($request->sort && $request->direction){
//
//                $records->orderBy($request->sort,$request->direction );
//            }else{
//                $records->orderBy("position","");
//
//            }
            $limit = $request->record_per_page;
            $records = $records->paginate((int)$limit);





            if(count($records)>0){
                return response()->json([
                    'status_code'=>1,
                    'message'=>"Records found Successfully!",
                    'data'=>$records
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Records Not Found',
                    'data'=>null
                ]);
            }

        }catch (Exception $e){
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
}
