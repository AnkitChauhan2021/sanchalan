<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;

use App\Http\Requests\Api\MagazineCommentRequest;
use App\Http\Requests\Api\MagazinesRequest;
use App\Http\Requests\Api\MagazinesUpdateRequest;
use App\Models\Magazine;
use App\Models\MagazineComment;
use App\Models\MagazineImage;
use App\Models\MagazineLike;
use App\Models\MagazineVideo;
use App\Models\User;
use App\Models\UserCompany;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MagazinesController extends Controller
{

    public function index(Request $request)
    {

        $users = CommonHelper::isApproved();
        $records = Magazine::whereIn('user_id', $users)->where('status', Magazine::Active)->with('user', 'images', 'videos', 'likes.users', 'comments.CommentBy');
        $limit = $request->record_per_page;
        $records = $records->paginate($limit);
        if (count($records) > 0) {
            return response()->json([
                'status_code' => 1,
                'message' => "Records Found Successfully!",
                'data' => $records
            ]);

        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Records Not Found!',
                'data' => null
            ]);
        }


    }

    public function store(MagazinesRequest $request)
    {

        DB::beginTransaction();
        try {
            $approved = CommonHelper::approved();
            if (count($approved) > 0) {
                $magazine = new Magazine();
                $magazine->user_id = Auth::user()->id;
                $magazine->title = $request->title;
                $magazine->slug = Str::slug($request->title);
                $magazine->description = $request->description;
                $magazine->save();

                if ($request->hasfile('images')) {
                    foreach ($request->file('images') as $images) {
                        $name = time() . rand(0, 9999) . '.' . $images->extension();
                        $images->move(public_path() . '/magazine/images/', $name);
                        $file = new MagazineImage();
                        $file->magazine_id = $magazine->id;
                        $file->image = $name;
                        $file->save();
                    }
                }
                if ($request->hasfile('videos')) {
                    foreach ($request->file('videos') as $videos) {
                        $name = time() . rand(0, 9999) . '.' . $videos->extension();
                        $videos->move(public_path() . '/magazine/Videos/', $name);
                        $file = new MagazineVideo();
                        $file->magazine_id = $magazine->id;
                        $file->video = $name;
                        $file->save();
                    }
                }
                DB::commit();
                $records = Magazine::query()->where('id', $magazine->id)->with('images', 'videos')->first();
                if ($records) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Records create Successfully!',
                        'data' => $records
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'unable to create Record!',
                        'data' => null
                    ]);

                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Please wait for Approval your Profile',
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();
        }
    }

    public function destroy($id)
    {
        $approved = CommonHelper::approved();
        if (count($approved) > 0) {
            $record = Magazine::find($id);
            if ($record) {
                $record->delete();
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Record deleted successfully!',
                    'data' => $record

                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable to delete record!',
                    'data' => null
                ]);
            }

        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }
    }

    public function destroyImage($id)
    {
        $approved = CommonHelper::approved();
        if (count($approved) > 0) {
            $image = MagazineImage::find($id);
            if ($image) {
                $image->delete();
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Record deleted successfully!',
                    'data' => $image

                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable to delete record!',
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }
    }

    public function destroyVideo($id)
    {
        $approved = CommonHelper::approved();
        if (count($approved) > 0) {
            $video = MagazineVideo::find($id);
            if ($video) {
                $video->delete();
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Record deleted successfully!',
                    'data' => $video

                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable to delete record!',
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }
    }

    public function update(MagazinesUpdateRequest $request)
    {

        DB::beginTransaction();
        try {
            $approved = CommonHelper::approved();
            if (count($approved) > 0) {
                $magazine = Magazine::find($request->magazine_id);
                $magazine->user_id = Auth::user()->id;
                $magazine->title = $request->title;
                $magazine->slug = Str::slug($request->title);
                $magazine->description = $request->description;
                $magazine->save();

                if ($request->hasfile('images')) {
                    foreach ($request->file('images') as $images) {
                        $name = time() . rand(0, 9999) . '.' . $images->extension();
                        $images->move(public_path() . '/magazine/images/', $name);
                        $file = new MagazineImage();
                        $file->magazine_id = $magazine->id;
                        $file->image = $name;
                        $file->save();
                    }
                }
                if ($request->hasfile('videos')) {
                    foreach ($request->file('videos') as $videos) {
                        $name = time() . rand(0, 9999) . '.' . $videos->extension();
                        $videos->move(public_path() . '/magazine/Videos/', $name);
                        $file = new MagazineVideo();
                        $file->magazine_id = $magazine->id;
                        $file->video = $name;
                        $file->save();
                    }
                }
                DB::commit();
                $records = Magazine::query()->where('id', $request->magazine_id)->with('images', 'videos')->first();
                if ($records) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Records create Successfully!',
                        'data' => $records
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'unable to create Record!',
                        'data' => null
                    ]);

                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Please wait for Approval your Profile',
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();
        }
    }

    public function likesUnlike(Request $request)
    {
        try {
            if ($request->magazine_id != null) {
                $magazine = Magazine::find($request->magazine_id);
                $user = User::find(Auth::user()->id);
                if ($magazine->company_approved == 0) {
                    if (($magazine) && ($user)) {
                        $magazineLikes = MagazineLike::where(['user_id' => $user->id, 'magazine_id' => $magazine->id])->first();
                        if ($magazineLikes != null) {
                            $magazineLikes->delete();
                            CommonHelper::decreaseLikesCount($magazine->id);
                            return response()->json([
                                'status_code' => 1,
                                'message' => "Thank You for Response",
                                'data' => $magazineLikes
                            ]);
                        } else {
                            $magazineLike = new MagazineLike();
                            $magazineLike->user_id = $user->id;
                            $magazineLike->magazine_id = $magazine->id;
                            $magazineLike->likes = 1;
                            if ($magazineLike->save()) {
                                CommonHelper::increaseLikesCount($magazine->id);
                                return response()->json([
                                    'status_code' => 1,
                                    'message' => "Thank You for Response",
                                    'data' => $magazineLike
                                ]);
                            } else {
                                return response()->json([
                                    'status_code' => 0,
                                    'message' => "unable to like Magazine",
                                    'data' => null
                                ]);
                            }

                        }
                    } else {
                        return response()->json([
                            'status_code' => 0,
                            'message' => "unable to like Magazine",
                            'data' => 0
                        ]);
                    }
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => "Your are Not Approved For this Company Post!",
                        'data' => 0
                    ]);
                }

            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "Magazine id Invalid",
                    'data' => 0
                ]);
            }


        } catch (Exception $e) {
            Log::info("Log message", ['error' => $e->getMessage()]);
        }
    }

    public function comments(MagazineCommentRequest $request)
    {

        try {
            $comment = new MagazineComment();
            $comment->user_id = $request->user_id;
            $comment->magazine_id = $request->magazine_id;
            $comment->comment = $request->comment;
            if ($comment->save()) {
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Record Create successfully',
                    'data' => $comment
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable create record',
                    'data' => null
                ]);
            }

        } catch (Exception $e) {
            Log::info("Log Message", ['error' => $e->getMessage()]);
        }

    }

    public function magazineDetail($id)
    {
        $magazine = Magazine::where(['id'=>$id,'status'=>Magazine::Active])->with(['user', 'images', 'videos', 'likes.users', 'comments.CommentBy'])->first();

        if($magazine){
            return response()->json([
                'status_code' =>1,
                'message' => 'Record found successfully!',
                'data'=>$magazine,
            ]);
        }else{
            return response()->json([
                'status_code' => 0,
                'message' => 'Record not found!',
                'data' => null
            ]);
        }

    }
}

