<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UserMeetingRequest;
use App\Models\Meeting;
use App\Models\MeetingUser;
use App\Models\UserCompany;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MeetingController extends Controller
{

    public function index(){
        try{
            $meeting = MeetingUser::where(['user_id'=>Auth::user()->id,'status'=>0])->with('meeting','organization')->get();
             if($meeting){
                 return response()->json([
                    'status_code'=>1,
                    "message"=>"Records Found SuccessFully!",
                    'meetings'=> $meeting
                 ]);
             }else{
                 return response()->json([
                     'status_code'=>0,
                     "message"=>"Record Not Found!",
                     'meetings'=> null
                 ]);
             }
        }catch (Exception $e){
          Log::info("log Mesaage",['error'=>$e->getMessage()]);
        }
    }
       public function changeStatus(UserMeetingRequest  $request){
        try{


            $meeting = MeetingUser::findOrFail($request->id);
            $meeting->status = $request->status;
            if($meeting->save()){

                return response()->json([
                    'status_code'=>1,
                    "message"=>"Records Found SuccessFully!",
                    'meetings'=> $meeting
                ]);

            }else{
                return response()->json([
                    'status_code'=>0,
                    "message"=>"Record Not Found!",
                    'meetings'=> null
                ]);
            }

        }catch (Exception $e){
            Log::info("log Mesaage",['error'=>$e->getMessage()]);
        }

    }

    public  function  create(Request $request){

        try {

//            return ($request->companies);
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
            $userAdmin= UserCompany::where('user_id', array(Auth::user()->id))->pluck('company_id')->first();
            if(!$request->levels){
                $adminCompanyAllUsersLIst = UserCompany::where('company_id',$userAdmin)->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();
            }else{
                $adminCompanyAllUsersLIst = UserCompany::where(['company_id'=>$userAdmin,'level_id'=>$request->levels])->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();

            }

            $string = str::random(10);
            foreach ($request->companies as $company) {
//                return $company['company_id'];
                $meeting = new Meeting();
                $meeting->user_id = Auth::user()->id;
                $meeting->description = $request->description;
                $meeting->company_id = $company['company_id'];
                $meeting->meeting_code = $string;
                $meeting->level_id = $request->levels;
                $meeting->save();
            }
            if (!empty($logInUser)) {
                foreach ($logInUser as $companyId) {
                    $meeting = new Meeting();
                    $meeting->user_id = Auth::user()->id;
                    $meeting->description = $request->description;
                    $meeting->company_id = $companyId;
                    $meeting->meeting_code = $string;
                    $meeting->status = 1;
                    $meeting->level_id = $request->levels;
                    $meeting->save();

                }
                foreach($adminCompanyAllUsersLIst as $user){
                    $userMeetings= new MeetingUser();
                    $userMeetings->user_id= $user;
                    $userMeetings->company_id= $userAdmin;
                    $userMeetings->meeting_id= $meeting->id;
                    $userMeetings->meeting_code= $string;
                    $userMeetings->save();
                }
            }
            if ($meeting){
                return response()->json([
                    'status_code'=>1,
                    "message"=>"Records Create SuccessFully!",
                    'meetings'=> $meeting
                ]);
            } else {
                return response()->json([
                    'status_code'=>0,
                    "message"=>"Unable Create Record",
                    'meetings'=> null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }

    }

    public function acceptMeetingByOrganization(Request  $request){
        try{
//            return $request;
            $record = Meeting::findOrFail($request->id);
            $record->status = $request->status;


            if($record->save()){
                $userAdmin= UserCompany::where('user_id', array(Auth::user()->id))->pluck('company_id')->first();
                if(!$record->level_id){
                    $adminCompanyAllUsersLIst = UserCompany::where('company_id',$userAdmin)->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();
                }else{
                    $adminCompanyAllUsersLIst = UserCompany::where(['company_id'=>$userAdmin,'level_id'=>$record->level_id])->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();
                }
                $uniqueUserLIst= array_unique($adminCompanyAllUsersLIst);

                if($record->status==1){
                    foreach($uniqueUserLIst as $user){
                        $userMeetings= new MeetingUser();
                        $userMeetings->user_id= $user;
                        $userMeetings->company_id= $userAdmin;
                        $userMeetings->meeting_id= $request->id;
                        $userMeetings->meeting_code= $record->meeting_code;
                        $userMeetings->save();
                    }

                }elseif($record->status==2){
                    $meeting = MeetingUser::where('meeting_code',$record->meeting_code)->delete();
                }else{
                    $meeting = MeetingUser::where('meeting_code',$record->meeting_code)->delete();
                }

                $error = 1;
                if($record->isApproved=="1"){
                    $message ='Meeting accepted Successfully!';
                }elseif($record->isApproved=="2"){
                    $message ='Meeting Rejected Successfully!';
                }else{
                    $message ='Meeting is pending';
                }

            } else {
                $error = 0;
                $message ='Unable to change status';
            }
            return response()->json([
                'type' => $error,
                'message' => $message,
                'data'=>$record

            ]);

        }catch (Exception $e){
            Log::info('Log Message',['error'=>$e->getMessage()]);
        }

    }

    public function sendMeetingList(){
        try {
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();


            $records = Meeting::select('meeting_code', DB::raw('MIN(description) as description'), DB::raw('MAX(created_at) as created_at'))
                ->where(['user_id' => Auth::user()->id])->whereNotIN('company_id', $logInUser)->whereNotNull('meeting_code');

            $records = $records->groupBy('meeting_code');

            $records = $records->paginate();
          if($records){
              return response()->json([
                  'status_code'=>1,
                  'message'=>'Records Found Successfully',
                  'data'=>$records
              ]);
          }else{
                  return response()->json([
                      'status_code'=>0,
                      'message'=>'Records Not Found!',
                      'data'=>null
                  ]);
          }

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }
    }

    public function organizationReceivedMeetingsList(Request $request){
        try{
            //        meeting received
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
            $records = Meeting::whereIn('company_id',$logInUser)->whereNotIn('user_id',[Auth::user()->id])->with('Organization');
            if ($request->query('search')) {
                $records = $records->where(function ($q) use ($request) {
                    $q->where('description', 'like', '%' . $request->query('search') . '%');
                    $q->orwhere('created_at', 'like', '%' . $request->query('search') . '%');

                });
            }
            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy("id", "DESC");

            }
            $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

            if($records){
                return response()->json([
                   'status_code'=>1,
                   'message'=>"Records Found Successfully!",
                   'data'=> $records
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>"Records Not Found!",
                    'data'=> null
                ]);
            }

        }catch (Exception $e){
            Log::info('Log Message',['error'=>$e->getMessage()]);
        }
    }

    public function OrganizationRemoveFromMeeting($id)
    {
        $record = Meeting::findOrFail($id);

        if ($record->delete()) {
            return response()->json([
                'status_code'=>1,
                'message'=>'Record deleted successfully!',
                'data'=>$record
            ]);
        } else {
            return response()->json([
                'status_code'=>0,
                'message'=>'Unable to  deleted Record!',
                'data'=>null
            ]);
        }

    }

    public function meetingDelete($id)
    {
        $record = Meeting::where('meeting_code', $id)->delete();

        if ($record) {
            return response()->json([
                'status_code'=>1,
                'message'=>'Record deleted successfully!',
                'data'=>$record
            ]);
        } else {
            return response()->json([
                'status_code'=>0,
                'message'=>'Unable to  deleted Record!',
                'data'=>null
            ]);
        }
    }

    public function meetingUpdate(Request $request)
    {


        try {
            $meeting = Meeting::where('meeting_code', $request->meeting_code);

            $meeting->update([
                "user_id" => Auth::user()->id,
                "description" => $request->description,
                "meeting_code" => $request->meeting_code,
            ]);
            $meeting = Meeting::where('meeting_code', $request->meeting_code)->first();
            if($meeting){
                return response()->json([
                    'status_code'=>1,
                    'message'=>'Record Updated successfully!',
                    'data'=>$meeting
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Unable to  Updated Record!',
                    'data'=>null
                ]);
            }

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }


    }


}

