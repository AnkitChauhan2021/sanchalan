<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Message\UploadFileRequest;
use App\Models\ChatGroup;
use App\Models\Message;
use App\Models\MessageReceiver;
use App\Models\UserChatGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;

class MessageController extends Controller
{
    public function uploadFile(UploadFileRequest $request)
    {

        $status = 0;
        $message = "";
        $data = null;

        try {
            DB::beginTransaction();

            $chatGroup = ChatGroup::with(["userChatGroup" => function($query) {
                $query->where("status", UserChatGroup::ACTIVE);
            }])->findOrFail($request->chat_group_id);


            $file = $request->file('file');

            $extension = $file->getClientOriginalExtension();

            $chatMessage = Message::create([
                "user_id" => auth()->user()->id,
                "chat_group_id" => $chatGroup->id,
                "message" => "",
                "type" => $request->type,
                "file_name" => $file->getClientOriginalName(),
                "file_extension" => $extension,
            ]);

            $chatMessage->save();

            $messageReceivers = [];
            $color_code = "";
            foreach ($chatGroup->userChatGroup as $user) {
                if ($user->user_id == auth()->user()->id) {
                    $color_code = $user->color_code;
                }
                $messageReceivers[] = [
                    "chat_group_id" => $chatGroup->id,
                    "user_id" => $user->user_id,
                    "is_read" => ($user->user_id == auth()->user()->id ? 1 : 0),
                    "read_at" => ($user->user_id == auth()->user()->id ? date("Y-m-d H:i:s") : null),
                    "is_sender" => ($user->user_id == auth()->user()->id ? 1 : 0),
                ];
            }

            $chatMessage->messageReceivers()->createMany($messageReceivers);

            $imageDir = Message::UPLOAD_PATH . $chatMessage->id . "/";

            if(!File::exists(public_path($imageDir))){
                File::makeDirectory(public_path($imageDir) , 0755, true);
            }

            $thumbDir = $imageDir . "thumb/";

            if(!File::exists(public_path($thumbDir))){
                File::makeDirectory(public_path($thumbDir) , 0755, true);
            }

            $thumb_image_name = $image_name = uniqid() . '.'. $extension;

            $file->move(public_path($imageDir), $image_name);

            switch ($request->type) {
                case Message::TYPE_IMAGE:
                    // create a new image directly from Laravel file upload
                    $image = Image::make(public_path($imageDir). $image_name);

                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $image->orientate()->fit(400, 400, function ($constraint) {
                        $constraint->upsize();
                    });
                    // save file
                    $image->save(public_path($thumbDir.$image_name));

                    $chatMessage->thumbnail = $thumbDir.$thumb_image_name;

                    break;

                case Message::TYPE_VIDEO:
                    $thumb_image_name = str_replace($extension,"jpeg",$image_name);
                    $thumbnail = Thumbnail::getThumbnail(public_path($imageDir). $image_name, $thumbDir,$thumb_image_name, 1);

					if($thumbnail) {
						// create new Intervention Image
						$image = Image::make(public_path($thumbDir). $thumb_image_name);

						// resize the image to a width of 300 and constrain aspect ratio (auto height)
						$image->orientate()->fit(400, 400, function ($constraint) {
							$constraint->upsize();
						});
						// save file
						$image->save(public_path($thumbDir.$thumb_image_name));

						// create a new Image instance for inserting
						$watermark = Image::make(public_path("watermark/play_button.png"));
						$image->insert($watermark, 'center');

						$image->save(public_path($thumbDir.$thumb_image_name));

                        $chatMessage->thumbnail = $thumbDir.$thumb_image_name;
					}
                    break;

            }

            $chatMessage->file = $imageDir.$image_name;
            $chatMessage->save();

            // update last updated for sorting list
            $chatGroup->updated_at =date("Y-m-d H:i:s");
            $chatGroup->save();
            DB::commit();

            $data['message_id'] = $chatMessage->id;
            $data['created_at'] = $chatMessage->created_at->format("h:i A");
            $data['chat_group_id'] = $chatMessage->chat_group_id;
            $data['message'] = $chatMessage->message;
            $data['message_type'] = $chatMessage->type;
            $data["file_name"] = $chatMessage->file_name;
            $data["file_extension"] = $chatMessage->file_extension;
            $data['thumbnail_url'] = $chatMessage->thumbnail_url;
            $data['file_url'] = $chatMessage->file_url;
            $data["sender_id"] = $chatMessage->sender->id;
            $data["sender_name"] = $chatMessage->sender->full_name;
            $data["delivered"] = 0;
            $data["color_code"] = $color_code;
            $data['receivers'] = [];
            if ($chatMessage->messageReceivers->count() > 0) {
                foreach ($chatMessage->messageReceivers as $receiver) {
                    $unreadCount = MessageReceiver::where("user_id", $receiver->user_id)
                        ->where("chat_group_id", $chatMessage->chat_group_id)
                        ->where("is_read",0)->count();

                    $data['receivers'][] = [
                        "user_id" => $receiver->user_id,
                        "unread_count" => ($unreadCount > 0 ? ($unreadCount > 100 ? "99+" : (string)$unreadCount) : ""),
                    ];
                }
            }
            $status = 1;


        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }
}
