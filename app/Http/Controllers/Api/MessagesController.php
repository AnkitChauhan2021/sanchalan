<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Message\AddGroupUser;
use App\Http\Requests\Api\Message\AssignAdminRightsRequest;
use App\Http\Requests\Api\Message\BlockUserRequest;
use App\Http\Requests\Api\Message\ChatHistoryRequest;
use App\Http\Requests\Api\Message\ClearChatHistoryRequest;
use App\Http\Requests\Api\Message\CreateConversationRequest;
use App\Http\Requests\Api\Message\CreateGroupRequest;
use App\Http\Requests\Api\Message\LeaveGroupRequest;
use App\Http\Requests\Api\Message\RemoveGroupMemberRequest;
use App\Models\Message;
use App\Models\MessageReceiver;
use App\Models\User;
use App\Models\UserChatGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Models\ChatGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MessagesController extends Controller
{

    public function createConversation(CreateConversationRequest $request, ChatGroup $chatGroup)
    {
        $status = 0;
        $message = "";
        $data = null;

        try {
            DB::beginTransaction();

            $validated = $request->validated();

            $group = $chatGroup->between($validated['sender_id'], $validated['receiver_id']);

            if (!$group) {
                $group = ChatGroup::create([
                    "user_id" => $validated['sender_id'],
                    "name" => ChatGroup::INDIVIDUAL,
                    "type" => ChatGroup::INDIVIDUAL,
                    "status" => ChatGroup::ACTIVE,
                ]);

                $group->userChatGroup()->createMany([
                    ["user_id" => $validated['sender_id'], 'status' => UserChatGroup::ACTIVE],
                    ["user_id" => $validated['receiver_id'], 'status' => UserChatGroup::ACTIVE],
                ]);
            }

            DB::commit();

            $status = 1;
            $data['chat_group_id'] = $group->id;

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function createGroup(CreateGroupRequest $request, ChatGroup $chatGroup)
    {

        $status = 0;
        $message = "";
        $data = null;

        try {
            DB::beginTransaction();
            $validatedData = $request->validated();

            if (isset($validatedData['icon'])) {
                $folderPath = ChatGroup::GROUP_ICON_PATH;
                if (!File::exists(public_path($folderPath))) {
                    File::makeDirectory(public_path($folderPath), 0755, true);
                }

                $file = $validatedData['icon'];

                $extension = $file->getClientOriginalExtension();

                $image_name = uniqid() . '.' . $extension;


                $file->move(public_path($folderPath), $image_name);

                $thumbDir = $folderPath . "thumb/";

                if (!File::exists(public_path($thumbDir))) {
                    File::makeDirectory(public_path($thumbDir), 0755, true);
                }

                $imageResize = Image::make(public_path($folderPath) . $image_name);

                $imageResize->orientate()->fit(400, 400, function ($constraint) {
                    $constraint->upsize();
                });
                // save file
                $imageResize->save(public_path($thumbDir . $image_name));





                $validatedData['icon'] = $image_name;
            }


            $chatGroup->fill($validatedData);
            $chatGroup->save();

            // Adding Sender to receivers table
            $validatedData['receivers'][] = [
                "user_id" => $validatedData['user_id'],
                "group_admin" => 1,
                "status" => ChatGroup::ACTIVE
            ];

            foreach ($validatedData['receivers'] as &$receiver) {
                $receiver['color_code'] = "#" . strtoupper(str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT));
            }

            $chatGroup->userChatGroup()->createMany($validatedData['receivers']);

            DB::commit();

            $status = 1;
            $message = "Group created successfully";
            $data['chat_group_id'] = $chatGroup->id;

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    public function chatHistory(ChatHistoryRequest $request)
    {

        $status = 0;
        $message = "";
        $data = null;

        try {
            $validated = $request->validated();

            $chatGroup = ChatGroup::findOrFail($validated['chat_group_id']);

            $messages = Message::whereHas('messageReceivers', function ($query) {
                $query->where("user_id", auth()->user()->id);
            })
                ->withCount("delivered")
                ->where("chat_group_id", $validated['chat_group_id'])
                ->with("sender");

            if ($chatGroup->type == ChatGroup::GROUP) {
                $messages = $messages->select("*", DB::raw("(SELECT color_code FROM user_chat_groups WHERE user_chat_groups.chat_group_id = messages.chat_group_id AND user_chat_groups.user_id = messages.user_id) as color_code"));
            }

            $messages = $messages
                ->orderBy("id")
                //->offset(($validated['page'] - 1) * 20)
                //->limit(20)
                ->get();

            // update Read message
            $notifications = MessageReceiver::where('user_id', auth()->user()->id)
                ->where('chat_group_id', $validated['chat_group_id']);

            $notifications->update(['is_read' => 1, "read_at" => date("Y-m-d H:i:s")]);

            $status = 1;
            $data['messages'] = [];
            $data['chat_info']["icon"] = "";
            $data['chat_info']["thumbnail_icon"] = "";
            $data['chat_info']["receiver_name"] = "";
            $data['chat_info']["receiver_id"] = 0;
            $data['chat_info']["chat_type"] = $chatGroup->type;
            $data['chat_info']["is_block"] = $chatGroup->is_block;
            $data['chat_info']["block_by"] = $chatGroup->block_by;

            $isLeft = UserChatGroup::where('user_id', auth()->user()->id)
                ->where('chat_group_id', $validated['chat_group_id'])
                ->where('status', UserChatGroup::LEFT)
                ->count();

            $data['chat_info']["is_left"] = $isLeft;

            if ($chatGroup->type == ChatGroup::GROUP) {
                $data['chat_info']["icon"] = $chatGroup->icon_url;
                $data['chat_info']["receiver_name"] = $chatGroup->name;
            } else {
                $receiver = UserChatGroup::with('user')->where('user_id', "<>", auth()->user()->id)
                    ->where('chat_group_id', $validated['chat_group_id'])
                    ->first();

                $data['chat_info']["icon"] = $receiver->user->logo;
                $data['chat_info']["thumbnail_icon"] = $receiver->user->thumbnail_url;
                $data['chat_info']["receiver_name"] = $receiver->user->full_name;
                $data['chat_info']["receiver_id"] = $receiver->user->id;
            }

            if ($messages->count()) {
                foreach ($messages as $key => $value) {
                    $data['messages'][$key]["message_id"] = $value->id;
                    if (date("Y-m-d") > $value->created_at->format("Y-m-d")) {
                        $data['messages'][$key]["created_at"] = $value->created_at->diffForHumans();
                    } else {
                        $data['messages'][$key]["created_at"] = $value->created_at->format("h:i A");
                    }
                    $data['messages'][$key]["chat_group_id"] = $value->chat_group_id;
                    $data['messages'][$key]["message"] = $value->message;
                    $data['messages'][$key]["message_type"] = $value->type;
                    $data['messages'][$key]["file_name"] = $value->file_name;
                    $data['messages'][$key]["file_extension"] = $value->file_extension;
                    $data['messages'][$key]["thumbnail_url"] = $value->thumbnail_url;
                    $data['messages'][$key]["file_url"] = $value->file_url;
                    $data['messages'][$key]["sender_id"] = $value->sender->id;
                    $data['messages'][$key]["sender_name"] = $value->sender->full_name;
                    $data['messages'][$key]["delivered"] = $value->delivered_count ? 1 : 0;
                    $data['messages'][$key]["color_code"] = $value->color_code ? $value->color_code : "";
                }
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    public function groups(Request $request)
    {
        $type = $request->type;
        $status = 0;
        $message = "";
        $data = null;
        $user = Auth::user();
//        return $user;
        try {
            switch ($type) {
                case "":
                    $status = 0;
                    $message = "Please send group type to get list.";
                    break;
                case 'Individual':
                    $groups = ChatGroup::query()->where('type', ChatGroup::INDIVIDUAL)
                        ->whereHas('userChatGroup', function ($query) {
                            $query->where('user_id', auth()->user()->id);
                        })->with([
                            'receiver.user:id,first_name,last_name,profile_pic',
                            "receiver.user.topLevelUserCompany.company:id,name",
                            "receiver.user.topLevelUserCompany.level:id,name",
                            'lastMessage'
                        ])->withCount('unSeenMessages')->orderBy("updated_at","DESC")->get();
                    if ($groups->count()) {
                        $status = 1;
                        $message = "Group list found successfully.";
                        $groupData = [];
                        foreach ($groups as $key => $group) {
                            $groupData['id'] = $group->id;
                            $groupData['name'] = $group->receiver->user->full_name;
                            $groupData['type'] = $group->type;
                            $groupData['status'] = $group->status;
                            $groupData['icon'] = $group->receiver->user->logo;
                            $groupData['thumbnail_icon'] = $group->receiver->user->thumbnail_url;
                            $groupData['last_message'] = $group->lastMessage ? $group->lastMessage->message : "";
                            $groupData['unread_count'] = $group->un_seen_messages_count > 0 ? ($group->un_seen_messages_count > 100 ? "99+" : (string)$group->un_seen_messages_count) : "";
                            $groupData['last_message_created'] = $group->lastMessage ? $group->lastMessage->created_at->format("h:i A") : '';
                            $groupData['senderName'] = $group->lastMessage ? $group->lastMessage->sender->fullName : "";
                            $groupData['senderId'] = $group->lastMessage ? $group->lastMessage->sender->id : 0;
                            $groupData['senderAvatar'] = $group->lastMessage ? $group->lastMessage->sender->logo : "";
                            $groupData["company"] = ($group->receiver->user->topLevelUserCompany && $group->receiver->user->topLevelUserCompany->company ? $group->receiver->user->topLevelUserCompany->company->name : "");
                            $groupData["level_id"] = ($group->receiver->user->topLevelUserCompany && $group->receiver->user->topLevelUserCompany->level ? $group->receiver->user->topLevelUserCompany->level->id : 0);
                            $groupData["level_name"] = ($group->receiver->user->topLevelUserCompany && $group->receiver->user->topLevelUserCompany->level ? $group->receiver->user->topLevelUserCompany->level->name : "");
                            $groupData["company_id"] = ($group->receiver->user->topLevelUserCompany && $group->receiver->user->topLevelUserCompany->company ? $group->receiver->user->topLevelUserCompany->company->id : 0);
                            $groupData["designation"] = ($group->receiver->user->topLevelUserCompany && !is_null($group->receiver->user->topLevelUserCompany->designation) ? $group->receiver->user->topLevelUserCompany->designation : "");
                            $groupData['position'] = $key;
                            $data[] = $groupData;
                        }
                        //$data = $groups;
                    } else {
                        $status = 1;
                        $data = [];
                    }
                    break;
                case 'Group':
                    $status = 1;
                    $groups = ChatGroup::query()->where('type', ChatGroup::GROUP)
                        ->whereHas('userChatGroup', function ($query) {
                            $query->where('user_id', auth()->user()->id);
                        })->with(['lastMessage'])->withCount('unSeenMessages')->orderBy("updated_at","DESC")->get();
                    if ($groups->count()) {
                        $status = 1;
                        $message = "Group list found successfully.";
                        $groupData = [];
                        foreach ($groups as $key => $group) {
                            $groupData['id'] = $group->id;
                            $groupData['name'] = $group->name;
                            $groupData['type'] = $group->type;
                            $groupData['status'] = $group->status;
                            $groupData['icon'] = $group->icon_url;
                            $groupData['thumbnail_icon'] = $group->thumbnail_icon_url;
                            $groupData['last_message'] = $group->lastMessage ? $group->lastMessage->message : "";
                            $groupData['unread_count'] = $group->un_seen_messages_count > 0 ? ($group->un_seen_messages_count > 100 ? "99+" : (string)$group->un_seen_messages_count) : "";
                            $groupData['last_message_created'] = $group->lastMessage ? $group->lastMessage->created_at->format("h:i A") : '';
                            $groupData['senderName'] = $group->lastMessage ? $group->lastMessage->sender->fullName : "";
                            $groupData['senderId'] = $group->lastMessage ? $group->lastMessage->sender->id : 0;
                            $groupData['senderAvatar'] = $group->lastMessage ? $group->lastMessage->sender->logo : "";
                            $groupData['position'] = $key;
                            $data[] = $groupData;
                        }
//                        $data = $groups;
                    } else {
                        $status = 1;
                        $data = [];
                    }
                    break;
                default:
                    $status = 0;
                    $message = "Something went wrong.";
                    $data = [];
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [];
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    public function getGroupMembers(Request $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        try {
            $chatGroup = ChatGroup::with([
                "userChatGroup" => function($query) {
                    $query->select("id","user_id","chat_group_id","group_admin");
                    $query->where("status", UserChatGroup::ACTIVE);
                },
                "userChatGroup.user:id,first_name,last_name,profile_pic",
                "userChatGroup.user.topLevelUserCompany.company:id,name",
                "userChatGroup.user.topLevelUserCompany.level:id,name,position"
            ])->whereHas("userChatGroup", function ($query) {
                $query->where("status", UserChatGroup::ACTIVE);
            })->findOrFail($request->chat_group_id);
            $data = [];
            if ($chatGroup->userChatGroup->count()) {
                foreach ($chatGroup->userChatGroup as $receiver) {
                    if ($receiver->user) {
                        $data[] = [
                            "name" => $receiver->user->full_name,
                            "user_id" => $receiver->user->id,
                            "icon" => $receiver->user->logo,
                            "is_super_admin" => ($receiver->user->id == $chatGroup->user_id ? 1 : 0),
                            "group_admin" => $receiver->group_admin,
                            "company" => ($receiver->user->topLevelUserCompany && $receiver->user->topLevelUserCompany->company ? $receiver->user->topLevelUserCompany->company->name : ""),
                            "level_name" => ($receiver->user->topLevelUserCompany && $receiver->user->topLevelUserCompany->level ? $receiver->user->topLevelUserCompany->level->name : ""),
                            "level_position" => ($receiver->user->topLevelUserCompany && $receiver->user->topLevelUserCompany->level ? $receiver->user->topLevelUserCompany->level->position : 0),
                            "company_id" => ($receiver->user->topLevelUserCompany && $receiver->user->topLevelUserCompany->company ? $receiver->user->topLevelUserCompany->company->id : 0),
                            "designation" => ($receiver->user->topLevelUserCompany && !is_null($receiver->user->topLevelUserCompany->designation) ? $receiver->user->topLevelUserCompany->designation : ""),
                        ];
                    }
                }

                $status = 1;
                $data = array_values(Arr::sort($data, function ($value) {
                    return $value['name'];
                }));
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    public function addGroupUser(AddGroupUser $request, UserChatGroup $userChatGroup)
    {
        $status = 0;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            $validatedData = $request->validated();
            foreach ($validatedData['user_id'] as $userId) {
                UserChatGroup::updateOrCreate(
                    ["user_id" => $userId, "chat_group_id" => $validatedData['chat_group_id']],
                    [
                        "user_id" => $userId,
                        "chat_group_id" => $validatedData['chat_group_id'],
                        "status" => UserChatGroup::ACTIVE,
                        "color_code" => "#" . strtoupper(str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT))
                    ]
                );
            }
            DB::commit();
            $status = 1;
            $message = Str::plural("User", count($validatedData['user_id'])) . ' added successfully';

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function toggleAdminRights(AssignAdminRightsRequest $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            $validateData = $request->validated();
            $chatUser = UserChatGroup::where(['user_id' => $validateData['user_id'], 'chat_group_id' => $validateData['chat_group_id']])->first();
            $chatUser->group_admin = $chatUser->group_admin ? 0 : 1;
            $chatUser->save();
            DB::commit();

            $status = 1;
            $message = "Status updated successfully.";
            $data['user'] = $chatUser;

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);

    }

    public function leaveGroup(LeaveGroupRequest $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            $validateData = $request->validated();
            $chatUser = UserChatGroup::where(['user_id' => auth()->user()->id, 'chat_group_id' => $validateData['chat_group_id']])->first();
            $chatUser->status = UserChatGroup::LEFT;
            $chatUser->save();

            if ($chatUser->group_admin) {
                $totalAdmin = UserChatGroup::where([
                    'chat_group_id' => $validateData['chat_group_id'],
                    'group_admin' => UserChatGroup::IS_ADMIN,
                    'status' => UserChatGroup::ACTIVE,
                ])->count();

                if (!$totalAdmin) {
                    $totalSameLevelUsers = UserChatGroup::with("user.topLevelUserCompany.level:id,name,position")
                        ->where([
                            'chat_group_id' => $validateData['chat_group_id'],
                            'status' => UserChatGroup::ACTIVE,
                        ])
                        ->get();

                    $assignedAdmin = false;
                    foreach ($totalSameLevelUsers as $chatGroup) {
                        if ($chatGroup->user->topLevelUserCompany
                            && $chatGroup->user->topLevelUserCompany->level
                            && $chatUser->user->topLevelUserCompany
                            && $chatUser->user->topLevelUserCompany->level
                        ) {
                            if ($chatGroup->user->topLevelUserCompany->level->position == $chatUser->user->topLevelUserCompany->level->position) {
                                $assignedAdmin = true;
                                UserChatGroup::where([
                                    'chat_group_id' => $validateData['chat_group_id'],
                                    'user_id' => $chatGroup->user_id,
                                ])->update(['group_admin' => UserChatGroup::IS_ADMIN]);
                            }
                        }
                    }

                    if (!$assignedAdmin) {
                        UserChatGroup::where([
                            'chat_group_id' => $validateData['chat_group_id'],
                        ])->update(['status' => UserChatGroup::LEFT]);
                    }
                }
            }

            DB::commit();

            $status = 1;
            $message = "You have successfully leaved the group";
            $data['user'] = $chatUser;

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function clearChatHistory(ClearChatHistoryRequest $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            $validateData = $request->validated();
            MessageReceiver::where(['user_id' => auth()->user()->id, 'chat_group_id' => $validateData['chat_group_id']])->delete();
            DB::commit();

            $status = 1;
            $message = "Chat history cleared successfully.";

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function removeGroupMember(RemoveGroupMemberRequest $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            $validateData = $request->validated();
            $admin = ChatGroup::where(['id' => $validateData['chat_group_id'], 'user_id' => $validateData['user_id']])->first();
            if ($admin) {
                $status = 0;
                $message = "You can't delete group admin";
            } else {
                $status = 1;
                $message = "User removed successfully.";
                $user = UserChatGroup::where(['user_id' => $validateData['user_id'], 'chat_group_id' => $validateData['chat_group_id']])->first();
                $user->status = 0;
                $user->save();
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    public function createMessage(Request $request)
    {

        $status = 0;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            $chatGroup = ChatGroup::with(["userChatGroup" => function ($query) {
                $query->where("status", UserChatGroup::ACTIVE);
            }])->findOrFail($request->chat_group_id);

            $chatMessage = Message::create([
                "user_id" => $request->sender_id,
                "chat_group_id" => $chatGroup->id,
                "message" => $request->message,
            ]);

            $messageReceivers = [];
            $color_code = "";
            foreach ($chatGroup->userChatGroup as $user) {
                if ($user->user_id == $request->sender_id) {
                    $color_code = $user->color_code;
                }
                $messageReceivers[] = [
                    "chat_group_id" => $chatGroup->id,
                    "user_id" => $user->user_id,
                    "is_read" => ($user->user_id == $request->sender_id ? 1 : 0),
                    "read_at" => ($user->user_id == $request->sender_id ? date("Y-m-d H:i:s") : null),
                    "is_sender" => ($user->user_id == $request->sender_id ? 1 : 0),
                ];
            }

            $chatMessage->messageReceivers()->createMany($messageReceivers);

            // update last updated for sorting list
            $chatGroup->updated_at =date("Y-m-d H:i:s");
            $chatGroup->save();

            DB::commit();

            $chatMessage = Message::with("messageReceivers")->find($chatMessage->id);

//            $unreadCount = MessageReceiver::where("user_id","<>", $request->sender_id)
//                ->where("chat_group_id", $chatMessage->chat_group_id)
//                ->where("is_read",0)
//                ->groupBy("message_id")->get()->count();

            $status = 1;
            $data["message_id"] = $chatMessage->id;
            if (date("Y-m-d") > $chatMessage->created_at->format("Y-m-d")) {
                $data["created_at"] = $chatMessage->created_at->diffForHumans();
            } else {
                $data["created_at"] = $chatMessage->created_at->format("h:i A");
            }
            $data["chat_group_id"] = $chatMessage->chat_group_id;
            $data["message"] = $chatMessage->message;
            $data["message_type"] = $chatMessage->type;
            $data["file_name"] = "";
            $data["file_extension"] = "";
            $data["thumbnail_url"] = "";
            $data["file_url"] = "";
            $data["sender_id"] = $chatMessage->sender->id;
            $data["sender_name"] = $chatMessage->sender->full_name;
            $data["delivered"] = 0;
            $data["color_code"] = $color_code;
            //$data['unread_count'] = ($unreadCount > 0 ? ($unreadCount > 100 ? "99+" : (string)$unreadCount) : "");
            $data['receivers'] = [];
            if ($chatMessage->messageReceivers->count() > 0) {
                foreach ($chatMessage->messageReceivers as $receiver) {
                    $unreadCount = MessageReceiver::where("user_id", $receiver->user_id)
                        ->where("chat_group_id", $chatMessage->chat_group_id)
                        ->where("is_read",0)->count();

                    $data['receivers'][] = [
                        "user_id" => $receiver->user_id,
                        "unread_count" => ($unreadCount > 0 ? ($unreadCount > 100 ? "99+" : (string)$unreadCount) : ""),
                    ];
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function messageDelivered(Request $request)
    {
        $status = 1;
        $message = "";
        $data = null;
        try {
            DB::beginTransaction();
            MessageReceiver::where("chat_group_id", $request->chat_group_id)
                ->where("user_id", $request->receiver_id)
                ->update(['is_read' => 1, "read_at" => date('Y-m-d H:i:s')]);

            $unread = MessageReceiver::where("chat_group_id", $request->chat_group_id)->where("is_read", 0)->count();
            if($unread) {
                $status = 0;
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    public function getUnreadCount(Request $request)
    {
        $status = 0;
        $message = "";
        $data = [];
        try {

            if($request->chat_group_id) {
                $receivers = UserChatGroup::where("chat_group_id",$request->chat_group_id)->get();
                if($receivers->count()) {
                    foreach ($receivers as $key => $receiver) {
                        $personal_unread = MessageReceiver::where("user_id", $receiver->user_id)
                            ->where('is_read',0)
                            ->whereHas("chatGroup", function ($query) {
                                $query->where("type", ChatGroup::INDIVIDUAL);
                                $query->where("status", ChatGroup::ACTIVE);
                            })
                            ->count();

                        $group_unread = MessageReceiver::where("user_id", $receiver->user_id)
                            ->where('is_read',0)
                            ->whereHas("chatGroup", function ($query) {
                                $query->where("type", ChatGroup::GROUP);
                                $query->where("status", ChatGroup::ACTIVE);
                            })
                            ->count();

                        $data[$key]['personal_unread'] = ($personal_unread > 0 ? (string)$personal_unread : "");
                        $data[$key]['group_unread'] = ($group_unread > 0 ? (string)$group_unread : "");
                        $data[$key]['user_id'] = $receiver->user_id;
                    }
                }
            } else {
                $personal_unread = MessageReceiver::where("user_id", $request->user_id)
                    ->where('is_read',0)
                    ->whereHas("chatGroup", function ($query) {
                        $query->where("type", ChatGroup::INDIVIDUAL);
                        $query->where("status", ChatGroup::ACTIVE);
                    })
                    ->count();

                $group_unread = MessageReceiver::where("user_id", $request->user_id)
                    ->where('is_read',0)
                    ->whereHas("chatGroup", function ($query) {
                        $query->where("type", ChatGroup::GROUP);
                        $query->where("status", ChatGroup::ACTIVE);
                    })
                    ->count();

                $data[0]['personal_unread'] = ($personal_unread > 0 ? (string)$personal_unread : "");
                $data[0]['group_unread'] = ($group_unread > 0 ? (string)$group_unread : "");
                $data[0]['user_id'] = $request->user_id;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function blockUser(BlockUserRequest $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        $validatedData = $request->validated();
        try {
            DB::beginTransaction();
            $user = User::findOrFail($validatedData['user_id']);
            ChatGroup::where("id", $validatedData['chat_group_id'])
                ->update(['is_block' => 1, "block_by" => $validatedData['user_id']]);
            $status = 1;
            $message = "You blocked $user->full_name.";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }
    public function unblockUser(BlockUserRequest $request)
    {
        $status = 0;
        $message = "";
        $data = null;
        $validatedData = $request->validated();
        try {
            DB::beginTransaction();
            $user = User::findOrFail($validatedData['user_id']);
            ChatGroup::where("id", $validatedData['chat_group_id'])
                ->update(['is_block' => 0, "block_by" => null]);
            $status = 1;
            $message = "You unblocked $user->full_name.";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }
}
