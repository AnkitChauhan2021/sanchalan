<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;

class OrganizationController extends Controller
{
    public function index(Request $request){

        $records = Company::where('status','1')->where('permission_to_make_manch_org','<>',Company::PERMISSION_TO_MAKE_MANCH_ORN)->select('id','name','name_in_hindi','sort_name','status','likeToJoin');

        $search = $request->query('search');
        if ($search) {
            $records = $records->where(function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
            });
        }
        if ($request->sort && $request->direction) {
            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("name");

        }
        $limit = $request->record_per_page;


        $limit = 10000;

        $records = $records->paginate((int)$limit);


        if(count($records)>0){
            return response()->json([
               'status_code'=>1,
               'message'=>'Organizations Found SuccessFully',
                'data'=> $records
            ]);
        }else{
            return response()->json([
                'status_code'=>0,
                'message'=>'Organizations Not found!',
                'data'=>null

            ]);
        }
    }
}
