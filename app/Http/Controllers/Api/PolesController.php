<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PolesRequest;
use App\Http\Requests\Api\SubmitPoleRequest;
use App\Models\Pole;
use App\Models\UserPole;
use App\Models\UserCompany;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PolesController extends Controller
{
    public function create(PolesRequest $request)
    {
//        return $request;
        try {
//            if (Auth::user()->isApproved == 1) {
                $poles = new Pole();
                $poles->user_id = $request->user_id;
                $poles->company_id = $request->company_id;
                $poles->question = $request->question;
                $poles->answer1 = $request->answer1;
                $poles->answer2 = $request->answer2;
                $poles->answer3 = $request->answer3;
                $poles->answer4 = $request->answer4;
                $poles->status = $request->status;
                $poles->isShow = $request->isShow;
                if ($poles->save()) {
                    return response()->json([
                        'status' => 1,
                        'message' => "Record Added Successfully!",
                        'data' => $poles
                    ]);
                } else {
                    return response()->json([
                        'status' => 0,
                        'message' => "Unable to Create Record",
                        'data' => null
                    ]);
                }
//            } else {
//                return response()->json([
//                    'status' => 0,
//                    'message' => "Please Wait For Approval Your Profile",
//                    'data' => null
//                ]);
//            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function index($id)
    {
//          return $id;
        try {
//            if (Auth::user()->isApproved == 1) {
                $userComapny = UserCompany::where('user_id', $id)->pluck('company_id', 'id');
                $company = [];
                foreach ($userComapny as $value) {
                    $company[] = $value;
                }
                $myComapany = array_unique($company);
                $users = UserCompany::whereIn('company_id', $myComapany)->pluck('user_id', 'id');
                $user_ids = [];
                foreach ($users as $user) {
                    $user_ids [] = $user;
                }
                $getUsers = array_unique($user_ids);
                $all_users = [];
                foreach ($getUsers as $getUser) {
                    $all_users[] = $getUser;
                }
                $userPolls = UserPole::where('user_id',Auth::user()->id)->pluck('pole_id','id');
                $polls_ids =[];
                foreach ( $userPolls as  $userPoll){
                    $polls_ids[]=$userPoll;
                }
                $pollsIds =array_unique($polls_ids);


                $poles = Pole::whereIn('user_id', $all_users)->whereNotIn('id',$pollsIds)->where('status', 1)->with('users.userComapny.company')->get();
                if (count($poles) > 0) {
                    return response()->json([
                        'status' => 1,
                        'message' => "Record Found Successfully!",
                        'data' => $poles
                    ]);

                } else {
                    return response()->json([
                        'status' => 0,
                        'message' => "Record not Found!",
                        'data' => null
                    ]);
                }
//            } else {
//                return response()->json([
//                    'status' => 0,
//                    'message' => "Please Wait For Approval Your Profile",
//                    'data' => null
//                ]);
//            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);

        }

    }

    public function submitPole(SubmitPoleRequest $request)
    {

        try {
//            if (Auth::user()->isApproved == 1) {
                $userPoles = new UserPole();
                $userPoles->user_id = $request->user_id;
                $userPoles->pole_id = $request->pole_id;
                $userPoles->answer = $request->answer;

                if ($userPoles->save()) {
                    $this->calculatePoleResult($userPoles->answer, $userPoles->pole_id);
                    $pollResult = Pole::where('id',$userPoles->pole_id)->select('answer_count1','answer_count2','answer_count3','answer_count4','isShow')->first();

                    return response()->json([
                        'status' => 1,
                        'message' => "Pole Submit Successfully!",
                        'data' =>$userPoles,
                        'Result'=>$pollResult
                    ]);
                } else {
                    return response()->json([
                        'status' => 0,
                        'message' => "Unable to Submit Pole!",
                        'data' => null
                    ]);
                }
//            } else {
//                return response()->json([
//                    'status' => 0,
//                    'message' => "Please Wait For Approval Your Profile",
//                    'data' => null
//                ]);
//            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function calculatePoleResult($answer, $pole_id)
    {


        try {
            $pole = Pole::where('id', $pole_id)->first();
            if ($pole) {
                if ($answer == "answer1") {
                    $answer1 = $pole->answer_count1;
                    $pole->update([
                        "answer_count1" => $answer1 + 1,
                    ]);

                } elseif ($answer == "answer2") {
                    $answer2 = $pole->answer_count2;
                    $pole->update([
                        "answer_count2" => $answer2 + 1,
                    ]);

                } elseif ($answer == "answer3") {
                    $answer3 = $pole->answer_count3;
                    $pole->update([
                        "answer_count3" => $answer3 + 1,
                    ]);

                } elseif ($answer == "answer4") {
                    $answer4 = $pole->answer_count4;
                    $pole->update([
                        "answer_count4" => $answer4 + 1,
                    ]);

                } else {
                    $answer5 = $pole->answer_count5;
                    $pole->update([
                        "answer_count5" => $answer5 + 1,
                    ]);

                }
            } else {
                return response()->json([
                    'status' => 0,
                    'message' => "Unable to Submit Pole!",
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }
}
