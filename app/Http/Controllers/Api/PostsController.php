<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PostBlockUnblock;
use App\Http\Requests\Api\PostRequest;
use App\Http\Requests\Api\PostCommentRequest;
use App\Models\Company;
use App\Models\Config;
use App\Models\Post;
use App\Models\Report;
use App\Models\ReportNotification;
use App\Models\PostBlock;
use App\Models\PostComment;
use App\Models\PostImage;
use App\Models\PostLike;
use App\Models\PostVideo;
use App\Models\User;
use App\Models\UserCompany;
use App\Models\UserFollower;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Illuminate\Support\Facades\File;


class PostsController extends Controller
{
    public function index(Request $request)
    {


        $records = CommonHelper::Posts();
        $limit = $request->record_per_page;
        $records = $records->paginate((int)$limit);
//        $records=CommonHelper::isFollowing();
//        return $records;

        if (count($records) > 0) {
            return response()->json([
                'status_code' => 1,
                'message' => "Records Found Successfully!",
                'data' => $records
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Records Not Found!',
                'data' => null
            ]);
        }

    }

    public function testing(Request $request)
    {

        $file = $request->file;
        $name = time() . rand(0, 9999) . '.' . $file->extension();
        if ($file->move(public_path() . '/post/audio', $name)) {
            return response()->json([
                'status_code' => 1,
                'message' => 'Record added successfully!',
                'data' => 'saved!'
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Something Went Wrong!',

                'data' => null
            ]);
        }
    }

    public static function store(PostRequest $request)
    {

        $status = 0;
        $message = "";
        $data = null;

        DB::beginTransaction();
        try {

            $post = new Post();
            $post->user_id = Auth::user()->id;
            $post->slug = Str::random(20);
            $post->company_id = $request->company_id;
            $post->description = $request->description;
            $post->type = $request->type;
            $post->save();


            if ($request->hasfile('images')) {

                $imageDir = PostImage::POST_IMAGE_PATH . $post->id . "/";

                if (!File::exists(public_path($imageDir))) {
                    File::makeDirectory(public_path($imageDir), 0755, true);
                }

                $thumbDir = $imageDir . "thumb/";

                if (!File::exists(public_path($thumbDir))) {
                    File::makeDirectory(public_path($thumbDir), 0755, true);
                }

                $images = [];
                foreach ($request->file('images') as $image) {

                    $extension = $image->getClientOriginalExtension();

                    $image_name = uniqid() . '.' . $extension;

                    $image->move(public_path($imageDir), $image_name);

                    $imageResize = Image::make(public_path($imageDir) . $image_name);
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $imageResize->orientate()->fit(400, 400, function ($constraint) {
                        $constraint->upsize();
                    });
                    // save file
                    $imageResize->save(public_path($thumbDir . $image_name));

                    $images[] = [
                        "image" => $image_name
                    ];
                }

                if (!empty($images)) {
                    $post->getPostImage()->createMany($images);
                }
            }


            if ($request->hasfile('videos')) {

                $imageDir = PostVideo::POST_VIDEO_PATH . $post->id . "/";

                if (!File::exists(public_path($imageDir))) {
                    File::makeDirectory(public_path($imageDir), 0755, true);
                }

                $thumbDir = $imageDir . "thumb/";

                if (!File::exists(public_path($thumbDir))) {
                    File::makeDirectory(public_path($thumbDir), 0755, true);
                }

                $videos = [];
                foreach ($request->file('videos') as $video) {

                    $extension = $video->getClientOriginalExtension();

                    $name = uniqid() . '.' . $extension;

                    $video->move(public_path($imageDir), $name);

                    $thumb_image_name = str_replace($extension, "jpeg", $name);

                    $thumbnail = Thumbnail::getThumbnail(public_path($imageDir . $name), $thumbDir, $thumb_image_name, 1);

                    if ($thumbnail) {
                        $imageResize = Image::make(public_path($thumbDir) . $thumb_image_name);
                        // resize the image to a width of 300 and constrain aspect ratio (auto height)
                        $imageResize->orientate()->fit(400, 400, function ($constraint) {
                            $constraint->upsize();
                        });
                        // save file
                        $imageResize->save(public_path($thumbDir . $thumb_image_name));

                        // create a new Image instance for inserting
                        $watermark = Image::make(public_path("watermark/play_button.png"));
                        $imageResize->insert($watermark, 'center');

                        $imageResize->save(public_path($thumbDir . $thumb_image_name));
                    }

                    $videos[] = [
                        "video" => $imageDir . $name,
                        "thumbnail" => $thumbDir . $thumb_image_name,
                    ];
                }

                if (!empty($videos)) {
                    $post->getPostVideo()->createMany($videos);
                }
            }
            DB:: commit();
            $data = Post::where('id', $post->id)
                ->select('id', 'company_id', 'user_id', 'slug', 'description', 'like_count', 'comment_count', 'type', 'status')
                ->with('getPostImage', 'getPostVideo')
                ->first();

            $status = 1;
        } catch (Exception $e) {
            $message = $e->getMessage();
            DB::rollback();
        }

        return response()->json([
            'status_code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {




            $post = Post::find($request->id);
            if ($post) {
                $post->user_id = Auth::user()->id;
                $post->slug = Str::random(20);
                $post->company_id = $request->company_id;
                $post->description = $request->description;
                $post->type = $request->type;
                $post->save();

                if ($request->hasfile('images')) {

                    $imageDir = PostImage::POST_IMAGE_PATH . $request->id . "/";

                    if (!File::exists(public_path($imageDir))) {
                        File::makeDirectory(public_path($imageDir), 0755, true);
                    }

                    $thumbDir = $imageDir . "thumb/";

                    if (!File::exists(public_path($thumbDir))) {
                        File::makeDirectory(public_path($thumbDir), 0755, true);
                    }


                    foreach ($request->file('images') as $image) {

                        $extension = $image->getClientOriginalExtension();

                        $image_name = uniqid() . '.' . $extension;

                        $image->move(public_path($imageDir), $image_name);

                        $imageResize = Image::make(public_path($imageDir) . $image_name);
                        // resize the image to a width of 300 and constrain aspect ratio (auto height)
                        $imageResize->orientate()->fit(400, 400, function ($constraint) {
                            $constraint->upsize();
                        });
                        // save file
                        $imageResize->save(public_path($thumbDir . $image_name));


                        $image = new PostImage();
                        $image->post_id =$request->id;
                        $image->image =$image_name;
                        $image->save();
                    }


                }


                if ($request->hasfile('videos')) {

                    $imageDir = PostVideo::POST_VIDEO_PATH . $request->id . "/";

                    if (!File::exists(public_path($imageDir))) {
                        File::makeDirectory(public_path($imageDir), 0755, true);
                    }

                    $thumbDir = $imageDir . "thumb/";

                    if (!File::exists(public_path($thumbDir))) {
                        File::makeDirectory(public_path($thumbDir), 0755, true);
                    }

                    $videos = [];
                    foreach ($request->file('videos') as $video) {

                        $extension = $video->getClientOriginalExtension();

                        $name = uniqid() . '.' . $extension;

                        $video->move(public_path($imageDir), $name);

                        $thumb_image_name = str_replace($extension, "jpeg", $name);

                        $thumbnail = Thumbnail::getThumbnail(public_path($imageDir . $name), $thumbDir, $thumb_image_name, 1);

                        if ($thumbnail) {
                            $imageResize = Image::make(public_path($thumbDir) . $thumb_image_name);
                            // resize the image to a width of 300 and constrain aspect ratio (auto height)
                            $imageResize->orientate()->fit(400, 400, function ($constraint) {
                                $constraint->upsize();
                            });
                            // save file
                            $imageResize->save(public_path($thumbDir . $thumb_image_name));

                            // create a new Image instance for inserting
                            $watermark = Image::make(public_path("watermark/play_button.png"));
                            $imageResize->insert($watermark, 'center');

                            $imageResize->save(public_path($thumbDir . $thumb_image_name));
                        }

                        $image = new PostVideo();
                        $image->post_id =$request->id;
                        $image->video =$imageDir . $name;
                        $image->thumbnail =$thumbDir . $thumb_image_name;
                        $image->save();
                    }

                }


                $postUpdate = Post::where('id', $request->id)->with('getPostImage', 'getPostVideo')->get();
                DB::commit();
                if ($post->id) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Record Updated Successfully!',
                        'data' => $postUpdate
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'unable to update Record!',
                        'data' => null
                    ]);
                }
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();
        }

    }

    public function deletePostImage($id)
    {
        $approved = CommonHelper::approved();
        if (count($approved) > 0) {
            $deleteImage = PostImage::find($id);
            if ($deleteImage) {
                $deleteImage->delete();
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Record deleted successfully!',
                    'data' => $deleteImage
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable to delete record!',
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }
    }

    public function deletePostVideo($id)
    {
        $approved = CommonHelper::approved();
        if (count($approved) > 0) {
            $deleteVideo = PostVideo::find($id);
            if ($deleteVideo) {
                $deleteVideo->delete();
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Record deleted successfully!',
                    'data' => $deleteVideo
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable to delete record!',
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }
    }

    public function destroy($id)
    {

        // $approved = CommonHelper::approved();

        // if (count($approved) > 0) {
        $post = Post::find($id);
        // return $post;
        if ($post) {
            $post->delete();
            $PostImage = PostImage::where('post_id', $id)->delete();
            return response()->json([
                'status_code' => 1,
                'message' => 'Record deleted successfully!',
                'data' => [
                    'post' => $post,
                    'PostImage' => $PostImage
                ]
            ]);


        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'unable to delete record!',
                'data' => null
            ]);
        }

        /* }*/ /*else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }*/
    }

    public function changeStatus(Request $request)
    {
        $approved = CommonHelper::approved();
        if (count($approved) > 0) {
            $record = Post::findOrFail($request->id);
            $record->status = $request->status;
            if ($record->save()) {
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Status changed to Successfully!',
                    'data' => $record
                ]);
//
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Unable to change status!',
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Please wait for Approval your Profile',
                'data' => null
            ]);
        }
    }

    // add comments
    public function storeComment(PostCommentRequest $request)
    {

        try {
            $post = Post::find($request->post_id);

            if ($post->company_approved == 0) {
                $postComment = new PostComment();
                $postComment->user_id = $request->user_id;
                $postComment->post_id = $request->post_id;
                $postComment->parent_id = $request->parent_id;
                $postComment->comment = $request->comment;
                if ($postComment->save()) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Comment added Successfully!',
                        'data' => $postComment,
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Unable to add comment!',
                        'data' => null,
                    ]);
                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Your are Not Approved For this Company Post!',
                    'data' => null,
                ]);
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function UserPosts(Request $request)
    {

        try {
            if ($request->user_id) {
                $posts = Post::where(['user_id'=> $request->user_id])->whereIn('type',['personal','user'])->with('getPostImage', 'getPostVideo', 'comments', 'comments.commentBy', 'likes.likeBy')->withCount('likes', 'comments', 'report')->orderBy('created_at', 'DESC')->get();

                if (count($posts) > 0) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Records found Successfully!',
                        'data' => $posts
                    ]);

                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Record not Found!',
                        'data' => null
                    ]);
                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Record not Found!',
                    'data' => null
                ]);
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

//    get all likes
    public function getLikes(Request $request)
    {
        try {
            $records = PostLike::where('post_id', $request->post_id)->with('likeBy')->get();

            if (count($records) > 0) {
                return response()->json([
                    'status_code' => 1,
                    'message' => 'Records found Successfully!',
                    'data' => $records
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Records not found',
                    'data' => null

                ]);
            }

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }
    }

    public function LikesUnlike(Request $request)
    {

        try {

            if ($request->post_id !== null) {
                $id = $request->post_id;
                $post = Post::find($id);
                $user = User::find(Auth::user()->id);
                if (($post) && ($user)) {
                    $records = PostLike::where('post_id', $request->post_id)->with('likeBy')->get();
                    $postLikes = PostLike::where(['user_id' => $user->id, 'post_id' => $post->id])->first();
                    if ($postLikes) {
                        $postLikes->delete();
                        return response()->json([
                            'status_code' => 1,
                            'message' => "Thank You for Response",
                            'data' => $postLikes,
                            'likes' => $records
                        ]);
                    } else {
                        $postandLikes = new PostLike();
                        $postandLikes->user_id = $user->id;
                        $postandLikes->post_id = $post->id;
                        $postandLikes->likes = 1;
                        $postandLikes->save();
                        return response()->json([
                            'status_code' => 1,
                            'message' => "Thank You for Response",
                            'data' => $postandLikes,
                            'likes' => $records
                        ]);
                    }
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => "unable to like Post",
                        'data' => 0
                    ]);
                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "Post id Invalid",
                    'data' => 0
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }


    public function blockUnblock(PostBlockUnblock $request)
    {
        try {

            $block = PostBlock::where(['user_id' => $request->user_id, 'post_id' => $request->post_id])->first();
            if ($block) {
                if ($block->delete()) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Post Unblock Successfully',
                        'data' => $block
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Unable to unblock Post',
                        'data' => null
                    ]);
                }

            } else {

                $blocks = new PostBlock();
                $blocks->user_id = $request->user_id;
                $blocks->post_id = $request->post_id;
                $blocks->status = 1;
                if ($blocks->save()) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Post Block Successfully',
                        'data' => $blocks
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Unable to block Post',
                        'data' => null
                    ]);
                }

            }


        } catch (Exception $e) {

        }
    }

    public function postDetail($id)
    {
        try {
            if ($id != null) {
                $record = Post::where('id', $id)->with('users', 'getPostImage', 'getPostVideo', 'comments', 'comments.commentBy', 'likes', 'likes.likeBy')->get();
                if ($record) {
                    return response()->json([
                        'status_code' => 1,
                        'message' => "Record Found Successfully!",
                        'data' => $record
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Record Not Found',
                        'data' => null
                    ]);
                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Record Not Found',
                    'data' => null
                ]);

            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function trendingPosts(Request $request)
    {
        try {
            //  settings for how many like to make trending post this setting menage by super admin

            $setting = Config::first();

            //  Get user of company in this company loggedIn user likeToJoin And full Joined


//            $userCompanyLike = UserCompany::where(['user_id'=>Auth::user()->id,])->pluck('company_id')->toArray();
            $userCompanies = UserCompany::where(['user_id' => Auth::user()->id])->pluck('company_id')->toArray();
//            $company_id =array_merge($userCompanyLike,$UserCompanies);

//            $UserCompanies = UserCompany::where(['user_id' => Auth::user()->id])->pluck('company_id')->toArray();

            //make user record (ids) unigue
            $UserCompanies = array_unique($userCompanies);

            // user tags name

            $companyTags = Company::whereIn('id', $UserCompanies)->where('status', 1)->orderBy("id", "DESC")->get()->each->setAppends(['is_post_exist_after_visit', 'is_approve']);


            // after getting company user get in these company how many user exist
            $users = UserCompany::whereIn('company_id', $UserCompanies)->pluck('user_id')->toArray();

//            make user unique
            $users = array_unique($users);
//            these users store in the array
            $usersArray = [];

            foreach ($users as $user) {
                $usersArray[] = $user;
            }
//                get block post remove from list, this methode exist on common helper app\helpers\CommonHelper.php

            $BlockedPost = CommonHelper::BlockedPost();
//            get post using user array and remove blocked posts and take only 3 post
            $posts = Post::whereIn('user_id', $usersArray)->whereNotIn('id', $BlockedPost)->where('status', 1);

//            make post count and check setting limits trending post how many like to make
            $posts = $posts->withCount('likes')->with('getPostImage', 'getPostVideo')->having('likes_count', '>=', $setting->trending_post)->take(3)->get();

//            return $posts;


//           1manch organization get 6 post

            $manchOrganization = Company::where(['permission_to_make_manch_org' => 1, 'status' => 1])->first();

//

//                get Manch users post and remove blocked posts
            $ManchUsersPost = Post::where(['company_id' => $manchOrganization->id, 'type' => 'admin'])->whereNotIn('id', $BlockedPost)->where('status', 1)->withCount('likes')->with('getPostImage', 'getPostVideo')->having('likes_count', '>=', $setting->trending_post)->take(6)->get();

//           return $ManchUsersPost;

//                remaining all company rending post and also remove manch org.. and user join org...

            $remainingCompanies = Company::whereNotIn('id', $UserCompanies)->whereNotIn('id', $manchOrganization)->pluck('id')->toArray();

//            get All users  with RemainingCompanies
            $remainingUsers = UserCompany::whereIn('company_id', $remainingCompanies)->pluck('user_id')->toArray();


//            get Remaining posts and also remove blocked post
            $remainingPosts = Post::whereIn('user_id', $remainingUsers)->whereNotIn('user_id', $users)->whereNotIn('id', $BlockedPost)->where('status', 1);
            $remainingPosts = $remainingPosts->withCount('likes')->with('getPostImage', 'getPostVideo')->having('likes_count', '>=', $setting->trending_post);


            $limit = $request->record_per_page;
            $records = $remainingPosts->paginate((int)$limit);

//            after geting three type of post marge these array in one array
//

            $allPost = new \Illuminate\Database\Eloquent\Collection;

            $allPost = $allPost->merge($posts);
            $allPost = $allPost->merge($ManchUsersPost);

            if ($allPost || $records) {
                return response()->json([
                    'status_code' => 1,
                    'message' => "Records Found Successfully!",
                    'data' => $allPost,
                    'RemainingAllTrendingPost' => $records,
                    'companyTags' => $companyTags
                ]);

            } else {
                return Response()->json([
                    'status_code' => 0,
                    'message' => "records not found!",
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function reportPost($id)
    {

//        return "HEllo";

        $reportPost = Report::where(['post_id' => $id, 'user_id' => Auth::user()->id])->first();
//        return $reportPost;
        $post = Post::where('id', $id)->first();
        $i = @$post->report_count;
        if (!$reportPost) {
            if ($post) {
                $reports = new Report();
                $reports->user_id = Auth::user()->id;
                $reports->post_id = $post->id;

                if ($reports->save()) {
                    $post->report_count = $i + 1;
                    $post->save();

                    $notification = new ReportNotification();
                    $notification->user_id = $post->user_id;
                    $notification->post_id = $post->id;
                    $notification->save();

                    return response()->json([
                        'status_code' => 1,
                        'message' => "Reported Successfully!",
                        'data' => $reports

                    ]);
                } else {
                    return Response()->json([
                        'status_code' => 0,
                        'message' => "Unable to Report!",
                        'data' => null
                    ]);
                }
            } else {
                return Response()->json([
                    'status_code' => 0,
                    'message' => "Unable to Report!",
                ]);
            }

        } else {
            return Response()->json([
                'status_code' => 0,
                'message' => "Already reported!",
            ]);
        }

    }

}
