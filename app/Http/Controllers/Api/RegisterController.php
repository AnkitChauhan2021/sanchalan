<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\OtpVerifyRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UpdateUserProfile;

use App\Models\Country;
use App\Models\Otp;
use App\Models\User;
use App\Models\UserCompany;
use Exception;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Illuminate\Support\Facades\File;



class RegisterController extends Controller
{
    public function __construct()
    {
//
    }

    public function index()
    {
//
    }

    public function store(RegisterUserRequest $request)
    {


        DB::beginTransaction();
        try {



            $country = Country::where(['name' => "india", 'status' => 1])->first();
//            $Company = Company::where(['name' => "Manch Organization", 'status' => 1])->first();

            $users = $user = new User();
            $user->role_id = User::UserRole;
            $user->gender = $request['gender'];

            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->country_id = $country->id;

            $user->state_id = $request['state_id'];
            $user->district_id = $request['district_id'];
            $user->city_id = $request['city_id'];

            $user->mobile = $request['mobile'];
            $user->email = $request['email'];
            $user->password = Hash::make($request['password']);

            $user->save();
            $id = $user->id;


            if ($request['wantJoin'] == UserCompany::WantTOJoin  || $request['organization_id']) {

                if (!empty($request->likeToJoinCompanies)) {

                    foreach ($request->likeToJoinCompanies as $likeToJoinCompany) {
                        $companyRequestExist = UserCompany::where(['user_id' => $id, 'company_id' => $likeToJoinCompany['company_id']])->first();
                       if(!$companyRequestExist){
                           $likeToJoin = new UserCompany();
                           $likeToJoin->user_id = $id;
                           $likeToJoin->role_id = User::UserRole;
                           $likeToJoin->company_id = $likeToJoinCompany['company_id'];
                           $likeToJoin->is_like = $likeToJoinCompany['status'];
                           $likeToJoin->save();
                       }
                    }
                }
                if (!empty($request['organization_id'])) {
                    $UserCompany = new UserCompany();
                    $UserCompany->user_id = $id;
                    $UserCompany->role_id = User::UserRole;
                    $UserCompany->company_id = $request['organization_id'];
                    $UserCompany->level_id = $request['level_id'];
                    $UserCompany->designation = $request['designation'];
                    $UserCompany->save();
                }


            }else{
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Please Select organization!',
                    'data' => null
                ]);
            }





            $otp = CommonHelper::otpGenerate($user->mobile);
            DB::commit();
            if ($user->id) {
//                Auth::login($user);
                return response()->json([
                    'status_code' => 1,
                    'message' => "User Register Successfully!",
                    'data' => [
                        'user' => $users,

                    ]
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'Something Went Wrong!',
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();
        }

    }

    public function isVerifiedPhone(OtpVerifyRequest $request)
    {

        $isVerified = User::with('topLevelUserCompany.level:id,name,position')->where('mobile', $request->mobile)->first();

        $getOtp = Otp::where(['mobile' => $isVerified->mobile, 'otp' => $request->otp])->where('expired_at', '>' ,date("Y-m-d H:i:s"))->delete();
        if ($getOtp) {
            $isVerified->update(['isVerified' => true]);

            $isVerified->position = 0;
            if($isVerified->topLevelUserCompany && $isVerified->topLevelUserCompany->level) {
                $isVerified->position = $isVerified->topLevelUserCompany->level->position;
            }

            unset($isVerified['topLevelUserCompany']);

            Auth::login($isVerified);
            if(Auth::check()){
                $user = Auth::user();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $token->save();
            }
            return response()->json([
                'status_code' => 1,
                'message' => 'Phone number verified',
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'data' => $isVerified
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Invalid verification code entered!',
                'data' => null
            ]);
        }

    }

    public function Profileupdate(UpdateUserProfile $request)
    {
//              return $request;
//                return $request;
        DB::beginTransaction();
        try {
            $user = User::find(Auth::user()->id);
            $user->country_id = $request->country_id;
            $user->state_id = $request->state_id;
            $user->district_id = $request->district_id;
            $user->city_id = $request->city_id;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->gender = $request->gender;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->about_us = $request->about_us;


            $limit_ckeck = UserCompany::where('user_id', Auth::user()->id)->get();
            if(count($limit_ckeck)<5){
                if ($request['wantJoin'] == UserCompany::WantTOJoin) {
                    $likeToJoinCompanies = $request->likeToJoinCompanies;
                    if (!empty($likeToJoinCompanies)) {
                        foreach ($likeToJoinCompanies as $likeToJoinCompany) {

                            $likeToJoin = new UserCompany();
                            $likeToJoin->user_id = Auth::user()->id;
                            $likeToJoin->role_id = Auth::user()->role_id;
                            $likeToJoin->company_id = $likeToJoinCompany['company_id'];
                            $likeToJoin->is_like = $likeToJoinCompany['status'];
                            $likeToJoin->save();
                        }
                    }
                }elseif($request['ifExistMember'] == UserCompany::IfExistMember){
                    $ifExistCompanies = $request->ifexistCompanies;
                    if (!empty($ifExistCompanies)) {
                        foreach ($ifExistCompanies as $ifExistCompany) {

                            $ifExistMember = new UserCompany();
                            $ifExistMember->user_id = Auth::user()->id;
                            $ifExistMember->role_id = Auth::user()->role_id;
                            $ifExistMember->company_id = $ifExistCompany['company_id'];
                            $ifExistMember->level_id= $ifExistCompany['level_id'];
                            $ifExistMember->designation= $ifExistCompany['designation'];
                            $ifExistMember->save();
                        }
                    }
                }
            }else{
                return response()->json([
                    'status_code' => 0,
                    'message' => "you have been crossed limit to join to Organization",
                    "data" => null
                ]);
            }



            if ($user->save()) {
                DB::commit();
                if (($request->mobile) !== (Auth::user()->mobile)) {
                    $user->update(['isVerified' => 0]);
                }

                return response()->json([
                    'status_code' => 1,
                    'message' => 'Profile Updated Successfully!',
                    'data' => $user
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => 'unable to Updated Profile!',
                    'data' => null
                ]);
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();
        }

    }

    public function profileImageUpdate(Request $request){
        $user = User::find(Auth::user()->id);
        $name = '';



        if ($user) {
            if ($request->hasfile('image')) {
//
                $images = $request->file('image');
//                $name = time() . rand(0, 9999) . '.' . $images->extension();
//                $path=  $images->move(public_path() . 'user/profile/', $name);


                $imageDir = User::PROFILE_PIC_PATH . $user->id . "/";

                if (!File::exists(public_path($imageDir))) {
                    File::makeDirectory(public_path($imageDir), 0755, true);
                }

                $thumbDir = $imageDir . "thumb/";

                if (!File::exists(public_path($thumbDir))) {
                    File::makeDirectory(public_path($thumbDir), 0755, true);
                }

                $extension = $images->getClientOriginalExtension();

                $image_name = uniqid() . '.' . $extension;

                $images->move(public_path($imageDir), $image_name);

                $imageResize = Image::make(public_path($imageDir) . $image_name);
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $imageResize->orientate()->fit(400, 400, function ($constraint) {
                    $constraint->upsize();
                });
                // save file
                $imageResize->save(public_path($thumbDir . $image_name));
            }
        }
        if ($image_name) {
            $user->profile_pic = $image_name;
        }
       if($user->save()){
           return response()->json([
               'status_code' => 1,
               'message' => 'Profile Image Successfully!',
               'data' => $user
           ]);
       }else{
           return response()->json([
               'status_code' => 0,
               'message' => 'unable to Updated Profile!',
               'data' => null
           ]);
       }
    }

    public function exitCompany(Request $request)
    {

        try {

            if ($request->user_id !== null && $request->company_id !== null) {
                $userCompany = UserCompany::where(['user_id' => $request->user_id, 'company_id' => $request->company_id])->first();
                if ($userCompany) {
                    $userCompany->delete();
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Record deleted successfully',
                        'data' => $userCompany,
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => 'Unable to deleted Record',
                        'data' => null,
                    ]);
                }
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
}
