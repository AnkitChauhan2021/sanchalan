<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\NotificationSetting;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function notificationSetting(NotificationSetting $request){

        try{
            $record = User::findOrFail($request->user_id);
            $record->notification = $request->status;
            $record->save();
            $SuccessMessage=CommonHelper::SendRequest;
            $failedMessage=CommonHelper::SendUnable;
            return CommonHelper::response($record,$SuccessMessage,$failedMessage);


        }catch (Exception $e){

        }



    }
}
