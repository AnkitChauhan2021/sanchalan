<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\State;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StateController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        try {
            if($request->country_id==''){
                $states = State::where('status','1')->select('id','name','country_id','status')->get();
            }else{
                $states = State::where('country_id',$request->country_id)->where('status','1')->select('id','name','country_id','status')->get();
            }

            if(count($states)>0){
                return response()->json([
                   'status_code'=>1,
                   'message'=>'States Found Successfully!',
                   'data'=>[
                       'states'=>$states
                   ]
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'States Not Found',
                    'data'=>null
                ]);
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }
}
