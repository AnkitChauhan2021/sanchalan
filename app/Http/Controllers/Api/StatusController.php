<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StatusRequest;
use App\Models\Status;
use App\Models\UserFollower;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;

class StatusController extends Controller
{
    public function index(){
        try{
            //  find out user followers
            $followers = UserFollower::where('following_id',Auth::user()->id)->pluck('follower_id')->toArray();

            // make unique user id
            $followers_ids = array_unique($followers);

            // call this function to remove after 24 h status
            $this->removeOldStatus();

            $records = Status::whereIn('user_id',$followers_ids)->with('user')->get();
            $myStatus =Status::where('user_id',auth()->user()->id)->with('user')->first();
            if($myStatus){
                $records->prepend($myStatus);
            }

//            $records= array_filter($records);


           if($records){
               return response()->json([
                   'status_code'=>1,
                   'message'=>"Records found Successfully!",
                   'data'=>$records
               ]);
           }else{
               return response()->json([
                   'status_code'=>0,
                   'message'=>"Unable to found records!",
                   'data'=>null
               ]);
           }


        }catch(Exception $e){
            Log::info('log message',['error'=>$e->getMessage()]);
        }


    }
    public function createStatus(StatusRequest $request){

        try{
//            return 'hello';
            $name='';
            if ($request->hasfile('image')) {
                $images=  $request->file('image');
                    $name = time() . rand(0, 9999) . '.' . $images->extension();
                $path=  $images->move(public_path() . '/status/images/', $name);


                if($path){
                    $thumbDir = '/status/thumb/' ;
                    $image = Image::make($path);
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $image->orientate()->fit(400, 400, function ($constraint) {
                        $constraint->upsize();
                    });
                    // save file
                    $image->save(public_path($thumbDir.$name));

                }



            }

            $status=Status::where('user_id',Auth::user()->id)->first();
           if($status){
               $status->update(['user_id'=>$status->user_id,'image'=>$name]);
           }else{
               $status = new Status();
               $status->user_id= Auth::user()->id;
               $status->image=$name;
               $status->save();
           }

            if($status->id){
                return response()->json([
                    'status_code'=>1,
                    'message'=>'Record Create Successfully!',
                    'data'=>$status
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Unable to create record',
                    'data'=>null
                ]);
            }
        }catch (Exception $e){
            Log::info('log Message',['error'=>$e->getMessage()]);
        }

    }

    public function removeStatus($id){

        try{
            $record = Status::find($id);

            if($record){
                $record->delete();
                return response()->json([
                    'status_code'=>1,
                    'message'=>'Record deleted successfully!',
                    'data'=>$record
                ]);
            }else{
                return response()->json([
                    'status_code'=>0,
                    'message'=>'Unable to  deleted Record!',
                    'data'=>null
                ]);
            }

        }catch (Exception $e){
            Log::info('log Message',['error'=>$e->getMessage()]);
        }

    }

    public function removeOldStatus(){

        $current_data_time= date("Y-m-d H:i:s", strtotime('-24 hours'));
        $records = Status::where('created_at','<',$current_data_time)->delete();
            return $records;


        }






}

