<?php
/**
 * Created by PhpStorm.
 * User: Braintech
 * Date: 15-04-2021
 * Time: 11:51
 */

namespace App\Http\Controllers\Api;


use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\JoinCompanyRequest;
use App\Http\Requests\ForgetPassword;
use App\Http\Requests\MobileVerifiyRequest;
use App\Http\Requests\OtpVerifyRequest;
use App\Models\City;
use App\Models\Company;
use App\Models\District;
use App\Models\Otp;
use App\Models\State;
use App\Models\User;
use App\Models\UserCompany;
use App\Models\UserFollower;
use Exception;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    public function __construct()
    {

    }


    public function users()
    {


        $users = User::all();
        if (count($users) > 0) {
            return response()->json([
                'status_code' => 1,
                'message' => "Users list found successfully!",
                'data' => [
                    'users' => $users,
                ]
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => "Users not found",
                'data' => null
            ]);
        }
    }

    public function login(Request $request)
    {
        $mobile = $request->mobile;
        $password = $request->password;
        $credentials = $request->only('mobile', 'password');
        $check_user_verified = User::where('mobile', $request->mobile)->first();

        if ($check_user_verified) {
            if (($check_user_verified->isVerified) === 1) {
                if (Auth::attempt($credentials)) {
                    $user = Auth::user();
                    $tokenResult = $user->createToken('Personal Access Token');
                    $token = $tokenResult->token;
                    $token->save();
                    $userDetail = User::where('id', Auth::user()->id)->withCount('follower', 'following')->first();
                    return response()->json([
                        'status_code' => 1,
                        'message' => 'Login Successfully!',
                        'data' => [
                            'access_token' => $tokenResult->accessToken,
                            'token_type' => 'Bearer',
                            'user' => $userDetail,
                        ]
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'message' => "Invalid credentials",
                        'data' => null
                    ]);
                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "Please verifying Your Mobile Number",
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => "User not Registered!",
                'data' => null
            ]);
        }


    }

    public function profile(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->with('userComapny.company', 'userComapny.level', 'level', 'country', 'state', 'district', 'city')->get();

        $following = UserFollower::where("following_id", Auth::user()->id)->count();
        $follower = UserFollower::where("follower_id", Auth::user()->id)->count();

        $likeToJoin = UserCompany::where(['user_id' => Auth::user()->id, 'is_like' => 1])->select('company_id', DB::raw('count(*) as total'))->groupBy('company_id')->with('company')->get();

        if ($user) {
            return response()->json([
                'status_code' => 1,
                'message' => 'Profile details found.',
                'data' => $user,
                'following' => $following,
                'follower' => $follower,
                'likeToJoinOrg' => $likeToJoin,
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => "Profile details not found.",
                'data' => null
            ]);
        }
    }

    public function logout(Request $request)
    {
        if ($token=Auth::user()->token()) {


             $token->revoke();

            return response()->json([
                'status_code' => 1,
                'message' => 'Logout successfully!',
                'data' => null
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Unable to Logout',
                'data' => null
            ]);
        }
    }

    public function generateOtpForGetPassword(MobileVerifiyRequest $request)
    {
        $getUser = User::where('mobile', $request->mobile)->first();

        $isVerified = new Otp();
        $isVerified->mobile = $request->mobile;
        $isVerified->otp = 1234;
        $isVerified->save();
        if ($getUser) {
            return response()->json([
                'status_code' => 1,
                'message' => 'Please Enter Valid Code!',
                'data' => $getUser
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'These Mobile Number do not match our records!',
                'data' => null
            ]);
        }

    }

    public function GetVerifyOtp(OtpVerifyRequest $request)
    {
        $getOtp = Otp::where(['mobile' => $request->mobile, 'otp' => $request->otp])->delete();
        if ($getOtp) {
            return response()->json([
                'status_code' => 1,
                'message' => 'OTP code  is verified!',
                'data' => $getOtp
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'These Mobile Number do not match our records!',
                'data' => null
            ]);
        }

    }

    public function forGetPassword(ForgetPassword $request)
    {


        $forgetPassword = User:: where('mobile', $request->mobile)->update([
            'password' => Hash::make($request['password']),
        ]);
        if ($forgetPassword) {
            return response()->json([
                'status_code' => 1,
                'message' => 'Password Forget Successfully!',
                'data' => $forgetPassword
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'These Mobile Number do not match our records!',
                'data' => null
            ]);
        }
    }

    public function resetSent(MobileVerifiyRequest $request)
    {
        $mobile = $request->mobile;

        $SuserAdmin = User::where('mobile', $mobile)->first();


        if ($SuserAdmin->role_id == User::SuperAdmin) {
            return response()->json([
                'status_code' => 0,
                'message' => 'Invalid credentials!',
                'data' => null
            ]);
        }
        $otp = CommonHelper::otpGenerate($mobile);
        if ($otp) {
            return response()->json([
                'status_code' => 1,
                'message' => 'One Time Passwords (OTP) Send Successfully!',
                'data' => $mobile
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'message' => 'Unable to send OTP!',
                'data' => $mobile
            ]);
        }

    }


    public function followUnfollow(Request $request)
    {

        $user = User::find($request->user_id);
        if ($user) {
            $checkFollow = UserFollower::where(['following_id' => Auth::user()->id, 'follower_id' => $user->id])->first();
            if ($checkFollow) {
                $checkFollow->delete();
            } else {
                $follow = new UserFollower();
                $follow->following_id = Auth::user()->id;
                $follow->follower_id = $user->id;
                $follow->save();
            }
        }
        $SuccessMessage = CommonHelper::SendRequest;
        $failedMessage = CommonHelper::SendUnable;
        return CommonHelper::response($user, $SuccessMessage, $failedMessage);

    }

    public function userSearch(Request $request)
    {
        try {

//          return $request;
            $records = User::query()
                ->where('isVerified', 1)
                ->where('users.role_id', "<>", 1)
                ->where('users.id', '<>', Auth::user()->id);

            $search = $request->search;
            if ($search) {
                $records = $records->where(function ($q) use ($search) {

                    $q->orWhereRaw("CONCAT(first_name,' ',last_name) like '$search%'");

                    $q->orWhere('first_name', 'like', $search . '%');

                    $q->orWhere('last_name', 'like', $search . '%');

                    $q->orWhere('mobile', $search);

                    $q->orWhereHas("userComapny.company", function ($query) use ($search) {
                        $query->where('name', 'like', $search . '%');
                    });
                });
            }

            $records = $records->select("users.id", "users.first_name", "users.last_name",  "users.last_name", "users.mobile", "users.profile_pic", "users.email", "users.state_id", "users.district_id", "users.city_id");

            $records = $records
                ->leftJoin("user_companies", "users.id", "=", "user_companies.user_id")
                ->leftJoin("companies", "user_companies.company_id", "=", "companies.id")
                ->where(function ($query) {
                    $query->orWhere("companies.permission_to_make_manch_org", 0);
                    $query->orWhereNull("companies.permission_to_make_manch_org");
                })
                ->leftJoin("levels", "user_companies.level_id", "=", "levels.id");

            $records = $records->addSelect(
                "user_companies.company_id",
                "user_companies.user_id",
                "user_companies.level_id",
                "levels.name as level_name",
                "levels.position as level_position",
                "companies.name as company_name",
                "companies.name_in_hindi as company_name_in_hindi"
            );

            $records = $records
                //->groupBy("users.id")
                ->orderByRaw("IF(levels.position IS NULL , 9999999, levels.position)");


            $records = $records->offset(($request->page - 1) * 20)->limit(20)->get();

            $data = [];
            if ($records->count()) {
                foreach ($records as $key => $record) {
                    $data[$key]["user_id"] = $record->id;
                    $data[$key]["level_id"] = $record->level_id;
                    $data[$key]["level_name"] = $record->level_name;
                    $data[$key]["level_position"] = $record->level_position;
                    $data[$key]["full_name"] = $record->full_name;
                    $data[$key]["logo"] = $record->logo;
                    $data[$key]["thumbnail_url"] = $record->thumbnail_url;
                    $data[$key]["is_follow"] = $record->is_follow;
                    $data[$key]["is_chat_request"] = $record->is_chat_request;
                    $data[$key]["state_name"] = $record->state ? $record->state->name : "";
                    $data[$key]["district_name"] = $record->district ? $record->district->name : "";
                    $data[$key]["city_name"] = $record->city ? $record->city->name : "";
                    $data[$key]["company_name"] = $record->company_name ? $record->company_name : "";
                    $data[$key]["company_name_in_hindi"] = $record->company_name_in_hindi ? $record->company_name_in_hindi : "";
                }
            }

            return response()->json([
                'status_code' => 1,
                'message' => 'Record Found Successfully!',
                'data' => $data
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 0,
                'message' => $e->getMessage(),
                'data' => null
            ]);

        }
    }

    public function LeftCompany(Request $request)
    {

        $company = UserCompany::where(['user_id' => $request->user_id, 'company_id' => $request->company_id]);
        if ($company->delete()) {
            return response()->json([
                'status_code' => 1,
                'Message' => "Record Deleted Successfully!",
                'data' => $company
            ]);
        } else {
            return response()->json([
                'status_code' => 0,
                'Message' => "unable to delete record!",
                'data' => null
            ]);
        }
    }

    public function joinCompany(JoinCompanyRequest $request)
    {

//        return  $request;
        try {
            $limit_ckeck = UserCompany::where('user_id', Auth::user()->id)->get();
//  return count($limit_ckeck);
            if (count($limit_ckeck) < 5) {
                if ($request->wantJoin == UserCompany::WantTOJoin) {
                    $likeToJoinCompanies = $request->likeToJoinCompanies;
                    if (!empty($likeToJoinCompanies)) {

                        foreach ($likeToJoinCompanies as $likeToJoinCompany) {
//                            return $likeToJoinCompany->status;
                            $companyRequestExist = UserCompany::where(['user_id' => Auth::user()->id, 'company_id' => $likeToJoinCompany['company_id']])->first();
                            if (!$companyRequestExist) {
                                $likeToJoin = new UserCompany();
                                $likeToJoin->user_id = Auth::user()->id;
                                $likeToJoin->role_id = Auth::user()->role_id;
                                $likeToJoin->company_id = $likeToJoinCompany['company_id'];
                                $likeToJoin->is_like = $likeToJoinCompany['status'];
                                if ($likeToJoin->save()) {
                                    return response()->json([
                                        'status_code' => 1,
                                        'message' => "Request Send Successfully!",
                                        "data" => $likeToJoin
                                    ]);
                                } else {
                                    return response()->json([
                                        'status_code' => 0,
                                        'message' => "unable to send request!",
                                        'data' => null
                                    ]);
                                }
                            }
                        }
                    }
                } elseif ($request['ifExistMember'] == UserCompany::IfExistMember) {
                    $ifExistCompanies = $request->ifexistCompanies;
                    if (!empty($ifExistCompanies)) {
                        foreach ($ifExistCompanies as $ifExistCompany) {
                            $companyRequestExist = UserCompany::where(['user_id' => Auth::user()->id, 'company_id' => $ifExistCompany['company_id']])->first();
                            if (!$companyRequestExist) {
                                $ifExistMember = new UserCompany();
                                $ifExistMember->user_id = Auth::user()->id;
                                $ifExistMember->role_id = Auth::user()->role_id;
                                $ifExistMember->company_id = $ifExistCompany['company_id'];
                                $ifExistMember->level_id = $ifExistCompany['level_id'];
                                $ifExistMember->designation = $ifExistCompany['designation'];
                                if ($ifExistMember->save()) {
                                    return response()->json([
                                        'status_code' => 1,
                                        'message' => "Request Send Successfully!",
                                        "data" => $ifExistMember
                                    ]);
                                } else {
                                    return response()->json([
                                        'status_code' => 0,
                                        'message' => "unable to send request!",
                                        'data' => null
                                    ]);
                                }
                            }
                        }
                    }
                }
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "you have been crossed limit to join to Organization",
                    "data" => null
                ]);
            }

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }


    }

    public function userDetail($id)
    {

        try {
            $user = User::where('id', $id)->with('userComapny.company', 'userComapny.level')->first();
            $following = UserFollower::where("following_id", $id)->count();
            $follower = UserFollower::where("follower_id", $id)->count();

            if ($user) {
                return response()->json([
                    'status_code' => 1,
                    'Message' => "Record found Successfully!",
                    'data' => $user,
                    'following' => $following,
                    'follower' => $follower
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'Message' => "unable to Found record!",
                    'data' => null
                ]);
            }


        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }
    }

    public function followerAndFollowingList(Request $request)
    {
        try {

            $record = User::with('userFollowers:id,first_name,last_name,profile_pic', 'userFollowings:id,first_name,last_name,profile_pic')
                ->where('id', $request->user_id)
                ->select("id", "first_name", "last_name", "profile_pic")
                ->first()
                ->toArray();

            unset($record['join_company']);
            $record['user_followers_count'] = count($record['user_followers']);
            $record['user_followings_count'] = count($record['user_followings']);

            if (!empty($record['user_followers'])) {
                foreach ($record['user_followers'] as &$follower) {
                    unset($follower['join_company'], $follower['pivot']);
                }
            }

            if (!empty($record['user_followings'])) {
                foreach ($record['user_followings'] as &$follower) {
                    unset($follower['join_company'], $follower['pivot']);
                }
            }

            return response()->json([
                'status_code' => 1,
                'Message' => "Record found Successfully!",
                'data' => $record,
            ]);

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }

    }

    public function AdvanceSearchStepOne(Request $request)
    {

        try {
            $company = Company::find($request->id);
            if ($company) {
                $usersIds = UserCompany::where('company_id', $company->id)->pluck('user_id')->toArray();

                $records = array_unique($usersIds);
                $userArray = [];
                foreach ($records as $record) {
                    $userArray[] = $record;
                }
                //  states ids
                $states = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1])->select('state_id', DB::raw('count(*) as total'))->groupBy('state_id')->whereNotNull('state_id')->get();


                //  These states and company users
                $users = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1])->select('id', 'first_name', 'last_name', 'state_id', 'profile_pic')->whereNotNull('state_id')->get();


                $stateArray = [];
                foreach ($states as $state) {
                    $stateArray[] = $state->state_id;
                }

//                  State::users($users);
                // add filter  on company users states
                $records = State::whereIn('id', $stateArray)->where('status', 1);

                $records->withCount("users");
                $limit = $request->record_per_page;
                $records = $records->paginate((int)$limit);


//                return $userArray;
                if ($records) {
                    return response()->json([
                        'status_code' => 1,
                        'Message' => "Record found Successfully!",
                        'data' => $records,
                        'users' => $users
                    ]);
                } else {
                    return response()->json([
                        'status_code' => 0,
                        'Message' => "unable to Found record!",
                        'data' => null
                    ]);
                }


            } else {
                return response()->json([
                    'status_code' => 0,
                    'Message' => "unable to Found record!",
                    'data' => null
                ]);
            }


        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }

    }


    public function AdvanceSearchStepTwo(Request $request)
    {
        $company = Company::find($request->id);
        if ($company) {
            $usersIds = UserCompany::where('company_id', $company->id)->pluck('user_id')->toArray();

            $records = array_unique($usersIds);
            $userArray = [];
            foreach ($records as $record) {
                $userArray[] = $record;
            }
            $districts = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1, 'state_id' => $request->state_id])->select('district_id', DB::raw('count(*) as total'))->groupBy('district_id')->whereNotNull('state_id')->get();

            $districtArray = [];
            foreach ($districts as $district) {
                $districtArray[] = $district->district_id;
            }
            $District_ = District::whereIn('id', $districtArray)->withCount('users')->where('status', 1)->get();
            $districtArray_ = [];
            foreach ($District_ as $District__) {
                $districtArray_[] = $District__->id;
            }

            $users = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1])->where('district_id', $districtArray_)->select('id', 'first_name', 'district_id', 'last_name', 'profile_pic')->get();

            if ($District_) {
                return response()->json([
                    'status_code' => 1,
                    'Message' => "Record found Successfully!",
                    'data' => $District_,
                    'users' => $users
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'Message' => "unable to Found record!",
                    'data' => null
                ]);
            }

        } else {
            return response()->json([
                'status_code' => 0,
                'Message' => "unable to Found record!",
                'data' => null
            ]);
        }

    }


    public function AdvanceSearchStepThird(Request $request)
    {

        $company = Company::find($request->id);
        if ($company) {
            $usersIds = UserCompany::where('company_id', $company->id)->pluck('user_id')->toArray();

            $records = array_unique($usersIds);
            $userArray = [];
            foreach ($records as $record) {
                $userArray[] = $record;
            }
            $cities = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1, 'district_id' => $request->district_id])->select('city_id', DB::raw('count(*) as total'))->groupBy('city_id')->whereNotNull('city_id')->get();

            $cityArray = [];
            foreach ($cities as $city) {
                $cityArray[] = $city->city_id;
            }
            $userCities = City::whereIn('id', $cityArray)->withCount('users')->where('status', 1)->get();
            $userCityArray_ = [];
            foreach ($userCities as $userCity) {
                $userCityArray_[] = $userCity->id;
            }

            $users = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1])->where('city_id', $userCityArray_)->select('id', 'first_name', 'district_id', 'last_name', 'profile_pic')->get();

            if ($userCities) {
                return response()->json([
                    'status_code' => 1,
                    'Message' => "Record found Successfully!",
                    'data' => $userCities,
                    'users' => $users
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'Message' => "unable to Found record!",
                    'data' => null
                ]);
            }

        } else {
            return response()->json([
                'status_code' => 0,
                'Message' => "unable to Found record!",
                'data' => null
            ]);
        }

    }

    public function AdvanceSearchStepFinal(Request $request)
    {
        $company = Company::find($request->id);
        if ($company) {
            $usersIds = UserCompany::where('company_id', $company->id)->pluck('user_id')->toArray();

            $records = array_unique($usersIds);
            $userArray = [];
            foreach ($records as $record) {
                $userArray[] = $record;
            }

            $records = User::whereIn('id', $userArray)->where(['isVerified' => 1, 'status' => 1, 'city_id' => $request->city_id]);

//                 $search= $request->query('search');
//            if ($search) {
//                $records = $records->where(function ($q) use ($search) {
//                    $q->where('first_name', 'like', '%' . $search . '%');
//                    $q->orWhere('last_name', 'like', '%' . $search . '%');
//                    $q->orWhere('mobile', 'like', '%' . $search. '%');
//
//                });
//            }
//            if ($request->sort && $request->direction) {
//
//                $records->orderBy($request->sort, $request->direction);
//            } else {
//                $records->orderBy("id", "DESC");
//
//            }
            $limit = $request->record_per_page;
            $records = $records->paginate((int)$limit);

            if ($records) {
                return response()->json([
                    'status_code' => 1,
                    'Message' => "Record found Successfully!",
                    'data' => $records,

                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'Message' => "unable to Found record!",
                    'data' => null
                ]);
            }

        } else {
            return response()->json([
                'status_code' => 0,
                'Message' => "unable to Found record!",
                'data' => null
            ]);
        }

    }


    public function UserCitySuggestion()
    {

        try {
            $user = User::find(Auth::user()->id);

            $suggestionsUsers = User::WhereIn('city_id', array($user->city_id))->whereNotIn('id', array(Auth::user()->id))->where('isVerified', 1)->get();

            if ($suggestionsUsers) {
                return response()->json([
                    'status_code' => 1,
                    'message' => "Records Found Successfully!",
                    'data' => $suggestionsUsers
                ]);

            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "unable to found Records!",
                    'data' => 0
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }

    }


//     users filter with org and level state dist city bases

    public function usersListWithFilter(Request $request)
    {
        try {

            $data = [];

            $organizations = $request->organizations;
            $states = $request->states;
            $district = $request->district;
            $city = $request->city;
            $levels = $request->levels;

            $records = User::where('users.isVerified', 1)
                ->where("users.status", 1)
                ->where("users.role_id", "<>", 1)
                ->where('users.id', '<>', auth()->user()->id);

            $records = $records->select("users.id", "users.first_name", "users.last_name", "users.mobile",  "users.profile_pic", "users.email", "users.state_id", "users.district_id", "users.city_id");

            if (count($states) > 0) {
                $states = is_array($states) ? $states : json_decode($states);
                $records = $records->whereIn('users.state_id', $states);
            }

            if (count($district) > 0) {
                $district = is_array($district) ? $district : json_decode($district);
                $records = $records->whereIn('users.district_id', $district);
            }

            if (count($city) > 0) {
                $city = is_array($city) ? $city : json_decode($city);
                $records = $records->whereIn('users.city_id', $city);
            }

            if (count($organizations) > 0) {
                $records = $records->whereIn("user_companies.company_id", $organizations);
            }

            if (count($levels) > 0) {
                $records = $records->whereIn("levels.id", $levels);
            }

            $records = $records->with(['state', 'district', 'city']);

            $records = $records
                ->leftJoin("user_companies", "user_companies.user_id", "=", "users.id")
                ->leftJoin("companies", "user_companies.company_id", "=", "companies.id")
                ->where(function ($query) {
                      $query->orWhere("companies.permission_to_make_manch_org", 0);
                      $query->orWhereNull("companies.permission_to_make_manch_org");
                  })
                ->leftJoin("levels", "user_companies.level_id", "=", "levels.id");


            $records = $records
                ->orderByRaw("IF(levels.position IS NULL , 9999999, levels.position)");

            $records = $records->addSelect(
                "user_companies.company_id",
                "user_companies.user_id",
                "user_companies.level_id",
                "levels.name as level_name",
                "levels.position as level_position",
                "companies.name as company_name",
                "companies.name_in_hindi as company_name_in_hindi"
              );

            $records = $records->offset(($request->page - 1) * 20)->limit(20)->get();

            if ($records->count()) {
                foreach ($records as $key => $record) {
                    $data[$key]["user_id"] = $record->id;
                    $data[$key]["level_id"] = $record->level_id;
                    $data[$key]["level_name"] = $record->level_name;
                    $data[$key]["level_position"] = $record->level_position;
                    $data[$key]["full_name"] = $record->full_name;
                    $data[$key]["logo"] = $record->logo;
                    $data[$key]["thumbnail_url"] = $record->thumbnail_url;
                    $data[$key]["is_follow"] = $record->is_follow;
                    $data[$key]["is_chat_request"] = $record->is_chat_request;
                    $data[$key]["state_name"] = $record->state ? $record->state->name : "";
                    $data[$key]["district_name"] = $record->district ? $record->district->name : "";
                    $data[$key]["city_name"] = $record->city ? $record->city->name : "";
                    $data[$key]["company_name"] = $record->company_name ? $record->company_name : "";
                    $data[$key]["company_name_in_hindi"] = $record->company_name_in_hindi ? $record->company_name_in_hindi : "";
                }
            }

            return response()->json([
                'status_code' => 1,
                'message' => "Records Found Successfully!",
                'data' => $data
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status_code' => 0,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }

    public function states(Request $request)
    {
        try {
            $records = State::where('status', 1);

            $search = $request->search;

            if ($search) {
                $records->where(function ($q) use ($request, $search) {
                    $q->where('name', 'like', '%' . $search . '%');
                });

            }
            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy('name');

            }
            $limit = $request->record_per_page;
            $limit = 10000;
            $records = $records->paginate((int)$limit);


            if (count($records) > 0) {
                return response()->json([
                    'status_code' => 1,
                    'message' => "Records Found Successfully!",
                    'data' => $records
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "Records Not Found!",
                    'data' => null
                ]);
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function districts(Request $request)
    {

        try {
            if (count($request->state_ids) == 0) {
                $records = District::where('status', 1);
            } else {
                $records = District::whereIn('state_id', $request->state_ids)->where('status', 1);
            }


            $search = $request->search;
            if ($search) {
                $records->where(function ($q) use ($request, $search) {
                    $q->where('name', 'like', '%' . $search . '%');
                });

            }
            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy('name');

            }
            $limit = $request->record_per_page;
            $limit = 10000;
            $records = $records->paginate((int)$limit);

            if (count($records) > 0) {
                return response()->json([
                    'status_code' => 1,
                    'message' => "Records Found Successfully!",
                    'data' => $records
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "Records Not Found!",
                    'data' => null
                ]);
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function cities(Request $request)
    {
        try {

            if (count($request->district_ids) == 0) {
                $records = City::where('status', 1);
            } else {
                $records = City::whereIn('district_id', $request->district_ids)->where('status', 1);
            }


            $search = $request->search;
            if ($search) {
                $records->where(function ($q) use ($request, $search) {
                    $q->where('name', 'like', '%' . $search . '%');
                });

            }
            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy('name');

            }
            $limit = $request->record_per_page;
            $limit = 10000;
            $records = $records->paginate((int)$limit);

            if (count($records) > 0) {
                return response()->json([
                    'status_code' => 1,
                    'message' => "Records Found Successfully!",
                    'data' => $records
                ]);
            } else {
                return response()->json([
                    'status_code' => 0,
                    'message' => "Records Not Found!",
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }


}
