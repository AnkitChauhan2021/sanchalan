<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    public function login(User $user, Request  $request){
         if((is_numeric($request->get('login')))){
             $credentials = ['mobile'=>$request->get('login'),'password'=>$request->get('password')];
             $check_exist_user= User::where('mobile',$request->login)->get();
         }elseif (filter_var($request->get('login'))) {

             $check_exist_user= User::where('email',$request->login)->get();
             //dd($check_exist_user);die();
             $credentials = ['email' => $request->get('login'), 'password'=>$request->get('password')];
         }
//        $validated= $request->validated();
        //dd($validated);

//        $credentials = [
//            'mobile' => $validated['mobile'],
//            'password' => $validated['password'],
//
//        ];
        $notmatch =['wrong'=>"These credentials do not match our records."];
        $success =['success'=>"."];


        if(count($check_exist_user)>0){
            foreach($check_exist_user as $user){
                if($user->first_login == 1){
                    $user->first_login = 0;
                    $user->save();
                  }
                }
            if($user->role_id !== 1){
                if ($user=Auth::attempt($credentials)) {
                    $user = Auth::user();
                    $tokenResult = $user->createToken('Personal Access Token');
                    $token = $tokenResult->accessToken;
                    return response()->json(['type'=>'success','message'=>"Hello,You are now logged in".' '.Auth::user()->full_name ,'Token' => $token,'userId'=>Auth::user()->id]);
                }else {
                    return response()->json(['type'=>'error','message'=>"These credentials do not match our records"]);
                }
            }else{
                return response()->json(['type'=>'error','message'=>"Sorry Super Admin, You can't login here"]);
            }
        }else{
            return response()->json(['type'=>'error','message'=>"These credentials do not match our records"]);

        }
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $adminRedirectTo = RouteServiceProvider::AdminDashboard;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
