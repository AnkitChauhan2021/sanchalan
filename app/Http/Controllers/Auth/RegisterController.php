<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\MobileVerifiyRequest;
use App\Http\Requests\RegisterStepOneRequest;
use App\Http\Requests\RegisterStepThirdRequest;
use App\Http\Requests\OtpVerifyRequest;
use App\Http\Requests\RegisterStepTwoRequest;
use App\Models\City;
use App\Models\Company;
use App\Models\CompanyDesignation;
use App\Models\District;
use App\Models\Otp;
use App\Models\State;
use App\Models\UserCompany;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Country;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Helpers\CommonHelper;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//    protected function validator(array $data)
//    {
//        return Validator::make($data, [
//            'first_name' => ['required', 'string', 'max:255'],
//            'last_name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'mobile' => ['required','unique:users','numeric' ]
//        ]);
//    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     */
    public function createStepOne(RegisterStepOneRequest $request)
    {

        $isVerified = Otp::where('mobile', $request->mobile)->first();
        $requestData = $request->validated();
        if ($isVerified) {
            $isVerified->update(['otp' => 1234]);
            Session::put('user', $requestData);
            $data = Session::get('user', 'default');
        } else {
            $isVerified = new Otp();
            $isVerified->mobile = $request->mobile;
            $isVerified->otp = 1234;
            $isVerified->save();
            Session::put('user', $requestData);
            $data = Session::get('user', 'default');
        }


        return response()->json(['type' => 'success', 'data' => $data, 'message' => 'Hello, One Time Passwords (OTPs) sent through SMS On Your Register Mobile Number!']);


    }

    public function createStepTwo(Request $request)
    {
        $otp = $request->first . $request->second . $request->third . $request->fourth;
        $isVerified = Otp::where('mobile', $request->mobile)->first();

        if ($isVerified) {
            if (($isVerified->otp) == $otp) {
                $object = [$request->mobile, true];
                Session::put('setSession', $object);

                $isVerified->delete();
                return response()->json(['type' => 'success', 'message' => 'Hello, One Time Passwords (OTPs) Verified Successfully!']);
            } else {
                return response()->json(['type' => 'error', 'message' => 'Invalid verification code entered!']);
            }
        } else {
            return response()->json(['type' => 'error', 'message' => 'Invalid verification code entered!']);
        }
    }

    public function getLevels(Request $request)
    {
        $levels = CompanyDesignation::where('company_id', $request->organization_id)->with('levels')->get();
        return response()->json($levels);
    }

    public function getStates(Request $request)
    {
        $states = State::where(['status' => 1, 'country_id' => $request->country_id])->get();
        return response()->json($states);
    }

    public function getDistricts(Request $request)
    {
        $districts = District::where(['status' => 1, 'state_id' => $request->State_id])->get();
        return response()->json($districts);
    }

    public function getCity(Request $request)
    {
        $City = City::where(['status' => 1, 'district_id' => $request->district_id])->get();
        return response()->json($City);
    }

    public function createStepThird(RegisterStepThirdRequest $request)
    {
        DB::beginTransaction();
        $country = Country::where('name', 'india')->where('status', 1)->first();
        try {
            $data = Session::get('user', 'default');
            $isverified = Session::get('setSession', 'default');
            $user = new User();
            $user->first_name = $data['first_name'];
            $user->role_id = User::UserRole;
            $user->last_name = $data['last_name'];
            /*$user->email = $data['email'];*/
            $user->mobile = $data['mobile'];
            $user->gender = $data['gender'];
            if ($isverified[1] === true) {
                $user->isVerified = $isverified[1];
            }

            $user->country_id = $country->id;
            $user->state_id = $request->state;
            $user->district_id = $request->district;
            $user->city_id = $request->city;
            $user->first_login = 1;
            $user->password = Hash::make($request['password']);
            $user->save();

            $UserCompany = new UserCompany();
            $UserCompany->user_id = $user->id;
            $UserCompany->role_id = User::UserRole;
            $UserCompany->company_id = $request->organization;
            $UserCompany->level_id = $request->level;
            if ($request->wantJoin == "on") {
                $UserCompany->is_like = UserCompany::WantTOJoin;
            }

            $UserCompany->designation = $request->designation;
            $UserCompany->save();
            //dd($UserCompany);die();
            DB::commit();
            if ($user->id) {
                Session::forget('user');
                Session::forget('setSession');
                Auth::login($user);

// send mail when User register
//                if ($request->email) {
//                    Mail::to(Auth::user()->email)->send(new \App\Mail\RegisterUser());
//                }

//                return redirect()->route('welcome.index')->with('success','You are  registered Successfully!');

                return response()->json(['type' => 'success', 'message' => 'You are  registered Successfully!']);
            } else {
                return response()->json(['type' => 'error', 'message' => 'SomeThing Went Wrong Please Try Again!']);
            }
        } catch (Exception $e) {
            Log::info('Register issue', ['error' => $e->getMessage()]);
            DB::rollback();
        }


    }

    public function reset_password(Request $request)
    {
        $validated = $request->validate([
            'mobile' => 'required||exists:users,mobile',

        ]);
        $isVerified = CommonHelper::otpGenerate($request->mobile);

        return response()->json(['type' => 'success', 'mobile' => $request->mobile, 'message' => 'Hello, One Time Passwords (OTPs) sent through SMS On Your Register Mobile Number!']);
    }

    public function verifyOTP(Request $request){
        $isVerified = Otp::where('mobile',$request->mobile)->first();

        if($isVerified){
            if(($isVerified->otp)==$request->otp){
                $isVerified->delete();
                $string = str::random(60);
                Session::put('verified_token',  $string);
                return response()->json(['type'=>'success','verified_token'=>$string ,'mobile'=>$request->mobile,'message'=>'Hello, One Time Passwords (OTPs) Verified Successfully!' ]);
            }else{
                return response()->json(['type'=>'error','message'=>'Invalid verification code entered!' ]);
            }

        }else{
            return response()->json(['type'=>'error','message'=>'Invalid verification code entered!' ]);
        }

    }

    public function createNewPassword(Request $request)
    {
        $validated = $request->validate([
            'verified_token' => 'required|string|min:59|max:60',
            'mobile' => 'required||exists:users,mobile',
            'password' => 'required|string|min:8|max:16',
            'password_confirmation' => 'required|min:8|same:password',
        ]);
        $user = User::where('mobile', $request->mobile)->first();
        $isverified = Session::get('verified_token', 'default');
        if (($request->verified_token) == $isverified) {
            if ($user) {
                $user->update([
                    'password' => Hash::make($request['password'])
                ]);
                Session::forget('verified_token');
                return response()->json(['type' => 'success', 'message' => 'Password Changed Successfully!']);
            } else {

            }
        } else {
            return response()->json(['type' => 'error', 'message' => 'SomeThing Went Wrong Please Try Again!']);
        }


    }
}
