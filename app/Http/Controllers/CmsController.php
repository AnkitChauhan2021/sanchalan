<?php

namespace App\Http\Controllers;

use App\Models\Cms;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    public function cmsPage($page_name)
    {

        if($page_name == 'contact-us'){

            return view('cms.contact_us');
        }
        elseif($page_name == 'about'){
            return view('cms.about');
        }
        else {
            $cms=Cms::where('slug', $page_name)->first();
            return view('cms.cms_page', compact('cms'));
        }
    }



}
