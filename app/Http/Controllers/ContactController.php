<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){

        return view('cms.contact_us',compact('config'));
    }
}
