<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelper;
use App\Models\User;
use App\Models\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserFollower;

class FollowsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function follwUserRequest($id){


        $user = User::find($id);
        //return $user;
         if ($user) {
            $checkFollow = UserFollower::where(['following_id' => Auth::user()->id, 'follower_id' => $user->id])->first();

            if ($checkFollow) {
                $checkFollow->delete();
                return redirect()->back()->with('success','Unfollowed Successfully!');
            } else {
                $follow = new UserFollower();
                $follow->following_id = Auth::user()->id;
                $follow->follower_id = $user->id;
                $follow->save();
            }
            return redirect()->back()->with('success','Started Following');
        }else{
        	return redirect()->back()->with('error','Unable to Send Request!');
        }
       
    }
}
