<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelper;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LikesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function LikePost(Request $request){

      try {
          $id =$request->id;
          $post = Post::find($id);
          $user= User::find(Auth::user()->id);
          //return $user;
          if(($post) && ($user)){
              $postLikes= PostLike::where(['user_id'=>$user->id,'post_id'=>$post->id])->first();

              if($postLikes != null){
                  CommonHelper::decreasePostLikesCount($post->id);
                $postLikes->delete();
                return response()->json(['type'=>'success','message'=>"Thank You for Response",'data'=>"unlike"]);
            }else{
                  $postandLikes = new PostLike();
                  $postandLikes->user_id = $user->id;
                  $postandLikes->post_id = $post->id;
                  $postandLikes->likes = true;
                  $postandLikes->save();
                  CommonHelper::increasePostLikesCount($post->id);
                  return response()->json(['type'=>'success','message'=>"Thank You for Response",'data'=>'like']);
            }
          }else{
              return response()->json(['type'=>'error','message'=>"unable to like Post"]);
          }
      }catch (Exception $e){
          Log::info('Log message', ['error' => $e->getMessage()]);
      }

//        return response()->json(['success'=>$response]);
    }
}
