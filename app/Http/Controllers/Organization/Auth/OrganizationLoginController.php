<?php

namespace App\Http\Controllers\Organization\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\organization\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\OrganizationUsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class OrganizationLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/organization/dashboard';

    /**
     * Create a new controller instance.
     * @return void
     */

    public function organizationLoginForm()
    {
        if (Auth::check()){
            if(Auth::user()->role_id == User::Admin){
                return redirect()->route('organization.dashboards.index');
            }else{
                return view('organization.auth.login')->with("error","You don't have admin access.");;
            }

        }else{
            return view('organization.auth.login')->with("error","You don't have admin access.");;
        }

    }
    public function postLogin(LoginRequest $request,User $user)
    {
        if((is_numeric($request->get('email')))){
             $credentials = ['mobile'=>$request->get('email'),'password'=>$request->get('password')];
             $check_exist_user= User::where('mobile',$request->login)->get();
             //dd($credentials);die();
         }elseif (filter_var($request->get('email'))) {
             $check_exist_user= User::where('email',$request->login)->get();
             $credentials = ['email' => $request->get('email'), 'password'=>$request->get('password')];
             //dd($credentials);die();
         }
/*
        if (Auth::attempt(['mobile' => $request->phone, 'mobile' => $request->password, 'role_id' => 3]))*/
        if ($user=Auth::attempt($credentials)) {
            //dd($credentials);die();
            if(Auth::user()->role_id==3){
            $request->session()->flash('title'," Hello ".Auth::user()->full_name);
            $request->session()->flash('success',"You are now logged in.");
            return redirect()->route('organization.dashboards.index')->with('success','You are now logged in.');
            }
        }
      $request->session()->flash('error',"Oppes! You have entered invalid credentials or You dont have access");
       return redirect()->route("organization.showLoginForm");
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return back()->with(['error'=> trans('auth.failed')]);
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        $request->session()->flash("success","You have been logged out.");
         return redirect()->route("organization.showLoginForm");
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
//    protected function guard()
//    {
//        return Auth::guard('admin');
//    }


}
