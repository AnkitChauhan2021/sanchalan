<?php

namespace App\Http\Controllers\Organization\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\organization\passwordChangeRequest;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PasswordChangeController extends Controller
{
    public function __construct(){
        //
    }
    // Get Change Password
    public function index(){
        return view('organization.auth.passwords.ChangePassword');
    }
    // Post Change Password After validate
    public function changePassword(passwordChangeRequest $request){
        try{
            $password = bcrypt($request->password);
            $admin = User::findOrFail(Auth::user()->id);
            $admin->update(['password'=>$password]);

            return redirect()->route('organization.dashboards.index')
                ->with(["success"=>"Password changed successfully."]);
        }catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }
}
