<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Inquiry;
use App\Models\Meeting;
use App\Models\Pole;
use App\Models\Post;
use App\Models\User;
use App\Models\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardsController extends Controller
{
    public function __construct()
    {

    }

//  organization dashboard index
    public function index(Request $request)
    {
        $user = Auth::user()->userComapny()->first();
        $orgUsers = UserCompany::where('company_id', $user->company_id)->pluck('user_id')->toArray();
        $userId = array_values(array_unique($orgUsers));

        $Posts = Post::WhereIn('user_id', $userId)->withCount('likes')->count();
        $users = $orgUsers;


        $polls = Pole::whereIn('user_id', $orgUsers)->where('status', 1)->count();

//        meeting received
        $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
        $records = Meeting::whereIn('company_id', $logInUser)->whereNotIn('user_id', [Auth::user()->id])->with('Organization');
        //dd($records);
        if ($request->query('search')) {
            $records = $records->where(function ($q) use ($request) {
                $q->where('description', 'like', '%' . $request->query('search') . '%');
                $q->orwhere('created_at', 'like', '%' . $request->query('search') . '%');

            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        return view('organization.dashboard.dashboard', compact('users', 'Posts', 'polls', 'records'));
    }
}
