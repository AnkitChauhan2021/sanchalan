<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\MagazinesRequest;
use App\Models\Magazine;
use App\Models\MagazineImage;
use App\Models\MagazineVideo;
use App\Models\User;
use App\Models\UserCompany;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MagazinesController extends Controller
{




    public function index(Request $request)
    {


        $company_id= UserCompany::where('user_id',Auth::user()->id)->pluck('company_id','id')->toArray();
        $companyId=array_unique($company_id);
        $user_id = UserCompany::whereIn('company_id',$companyId)->pluck('user_id','id')->toArray();
        $userId=array_unique($user_id);
        $Admin = User::where('role_id',User::SuperAdmin)->pluck('id')->toArray();

        $records = Magazine::WhereIn('user_id',$userId)->whereNotIn('user_id',$Admin);
        $search = $request->query('search');
        if($search){
            $records->where(function($q) use ($request, $search) {
                $q->where('title','like','%'. $search.'%');
            })->orWhereHas("user",function($query) use ($search){
                $query->where(function($data) use ($search){
                    $data->orWhere('first_name','like','%'. $search.'%');
                    $data->orWhere('last_name','like','%'. $search.'%');
                });
            });

        }
        if($request->sort && $request->direction){

            $records->orderBy($request->sort,$request->direction );
        }else{
            $records->orderBy("id","DESC");

        }
        $records=$records->with('user');

        $records = $records->paginate(($request->query('limit') ? $request->query('limit'):env('PAGINATION_LIMIT') ));
        // return $records;

        return view('organization.magazines.index', compact('records'));
    }

    public function create()
    {
        return view('organization.magazines.create');
    }

    public function store(MagazinesRequest $request)
    {

        DB::beginTransaction();
        try{


            $company_id= UserCompany::where('user_id',Auth::user()->id)->pluck('company_id','id')->toArray();

            $record= $magazine =new Magazine();
            $magazine->user_id = Auth::user()->id;
            $magazine->company_id = Auth::user()->admin_company_id;
            $magazine->title = $request->title;
            $magazine->slug = Str::slug($request->title);
            $magazine->description = $request->editor1;
            $magazine->save();

            if ($request->hasfile('images')) {
                foreach ($request->file('images') as $images) {
                    $name = time() . rand(0, 9999) . '.' . $images->extension();
                    $images->move(public_path() . '/magazine/images/', $name);
                    $file = new MagazineImage();
                    $file->magazine_id = $magazine->id;
                    $file->image = $name;
                    $file->save();
                }
            }
            if ($request->hasfile('Videos')) {
                foreach ($request->file('Videos') as $videos) {
                    $name = time() . rand(0, 9999) . '.' . $videos->extension();
                    $videos->move(public_path() . '/magazine/Videos/', $name);
                    $file = new MagazineVideo();
                    $file->magazine_id = $magazine->id;
                    $file->video = $name;
                    $file->save();
                }
            }
            DB::commit();
            if($record){
             return redirect()->route('organization.magazines.index')->with('success','Records create Successfully!');
         }else{
             return back()->with('error','unable to create Record!');
         }
     }catch (Exception $e) {
        Log::info('Log message', ['error' => $e->getMessage()]);
        DB::rollback();
    }

}
public function destroy($id)
{
    $record = Magazine::findOrFail($id);

    if ($record->delete()) {
        return back()->with(['success' => 'Record deleted successfully']);
    } else {
        return back()->with(['error' => 'Unable to delete this record']);
    }

}
public function changeStatus(Request $request)
{
    $record = Magazine::findOrFail($request->id);
    $record->status = $request->status;
    if ($record->save()) {
        $error = 0;
        if ($record->status == false) {
            $message = 'Status changed to <strong>' . "Inactive" . '</strong>';
        } else {
            $message = 'Status changed to <strong>' . "Active" . '</strong>';
        }

    } else {
        $error = 1;
        $message = 'Unable to change status';
    }
    return response()->json(['error' => $error,'message' => $message]);
}
public function edit($id){
    $record=Magazine::where('id',$id)->with('images','videos')->first();
//        return $record;
    return view('organization.magazines.edit',compact('record'));
}
public function destroyImage($id){
    $removeIMage = MagazineImage::find($id);
    if($removeIMage->delete()){
        return response()->json(['type' => 'success','message'=>'Picture Removed Successfully!']);
    }else{
        return response()->json(['type' => 'error','message'=>'unable to remove Picture!']);
    }
}
public function destroyVideo($id){
    $removeIMage = MagazineVideo::find($id);
    if($removeIMage->delete()){
        return response()->json(['type' => 'success','message'=>'Picture Removed Successfully!']);
    }else{
        return response()->json(['type' => 'error','message'=>'unable to remove Picture!']);
    }
}
public function update(Request $request)
{
//       return $request;
    DB::beginTransaction();
    try {

        $magazine = Magazine::find($request->magazine_id);
        $magazine->user_id = Auth::user()->id;
        $magazine->title = $request->title;
        $magazine->slug = Str::slug($request->title);
        $magazine->description = $request->editor1;
        $magazine->save();

        if ($request->hasfile('images')) {
            foreach ($request->file('images') as $images) {
                $name = time() . rand(0, 9999) . '.' . $images->extension();
                $images->move(public_path() . '/magazine/images/', $name);
                $file = new MagazineImage();
                $file->magazine_id = $request->magazine_id;
                $file->image = $name;
                $file->save();
            }
        }
        if ($request->hasfile('Videos')) {
            foreach ($request->file('Videos') as $videos) {
                $name = time() . rand(0, 9999) . '.' . $videos->extension();
                $videos->move(public_path() . '/magazine/Videos/', $name);
                $file = new MagazineVideo();
                $file->magazine_id = $request->magazine_id;
                $file->video = $name;
                $file->save();
            }
        }
        DB::commit();
        $records = Magazine::query()->where('id', $request->magazine_id)->with('images', 'videos')->first();
        if ($records) {
           return redirect()->route('organization.magazines.index')->with('success','Record Update Successfully!');
       } else {
          return back()->with('error','Unable to update Record!');

      }

  } catch (Exception $e) {
    Log::info('Log message', ['error' => $e->getMessage()]);
    DB::rollback();
}
}

}
