<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Level;
use App\Models\Meeting;
use App\Models\MeetingUser;
use App\Models\UserCompany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Collection;

class MeetingsController extends Controller
{
    public function index()
    {
        try {
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();


            $records = Meeting::select('meeting_code', DB::raw('MIN(description) as description'), DB::raw('MAX(created_at) as created_at'))
                ->where(['user_id' => Auth::user()->id])->whereNotIN('company_id', $logInUser)->whereNotNull('meeting_code');

            $records = $records->groupBy('meeting_code');

            $records = $records->paginate();

            return view('organization.meetings.index', compact('records'));
        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }
    }

    public function create()
    {

        try {
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();

            $companies = Company::where('status', 1)->whereNotIn('id', $logInUser)->get();
            $levels = Level::where('status',1)->get();
//                  return $companies;
            return view('organization.meetings.create', compact('companies','levels'));

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }

    }

    public function store(Request $request)
    {
        try {
//                return$request;
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
            $userAdmin= UserCompany::where('user_id', array(Auth::user()->id))->pluck('company_id')->first();
            if(!$request->levels){
                $adminCompanyAllUsersLIst = UserCompany::where('company_id',$userAdmin)->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();
            }else{
                $adminCompanyAllUsersLIst = UserCompany::where(['company_id'=>$userAdmin,'level_id'=>$request->levels])->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();

            }

            $string = str::random(10);
            foreach ($request->companies as $company) {
                $meeting = new Meeting();
                $meeting->user_id = Auth::user()->id;
                $meeting->description = $request->description;
                $meeting->company_id = $company;
                $meeting->meeting_code = $string;
                $meeting->level_id = $request->levels;
                $meeting->save();
            }
            if (!empty($logInUser)) {
                foreach ($logInUser as $companyId) {
                    $meeting = new Meeting();
                    $meeting->user_id = Auth::user()->id;
                    $meeting->description = $request->description;
                    $meeting->company_id = $companyId;
                    $meeting->meeting_code = $string;
                    $meeting->status = 1;
                    $meeting->level_id = $request->levels;
                    $meeting->save();

                }
                foreach($adminCompanyAllUsersLIst as $user){
                    $userMeetings= new MeetingUser();
                    $userMeetings->user_id= $user;
                    $userMeetings->company_id= $userAdmin;
                    $userMeetings->meeting_id= $meeting->id;
                    $userMeetings->meeting_code= $string;
                    $userMeetings->save();
                }
            }
            if ($meeting) {
                return redirect()->route('organization.meetings.index')->with('Success', "Meeting Created Successfully!");
            } else {
                return back()->with('unable to create Meeting');
            }
        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }

    }

    public function view(Request $request, $id)
    {
        try {
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
            $record = Meeting::where('meeting_code', $id)->first();
//

            $records = Meeting::where('meeting_code', $id)->whereNotIN('company_id', $logInUser);
            $search = $request->query('search');
            if ($search) {
                $records->WhereHas("Organization", function ($query) use ($search) {
                    $query->Where('name', 'like', '%' . $search . '%');
                });
            }
            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy("id", "DESC");
            }
            $records = $records->with(['Organization']);
            $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


            return view('organization.meetings.organization.index', compact('records', 'record'));

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }

    }

    public function OrganizationRemoveFromMeeting($id)
    {
        $record = Meeting::findOrFail($id);

        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    public function meetingDelete($id)
    {
        $record = Meeting::where('meeting_code', $id)->delete();

        if ($record) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }
    }

    public function meetingEdit($id)
    {


        try {
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
            $record = Meeting::where('meeting_code', $id)->whereNotIN('company_id', $logInUser)->select('id', 'description', 'meeting_code')->first();

            return view('organization.meetings.edit', compact('record'));
        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }

    }

    public function meetingUpdate(Request $request)
    {


        try {
            $meeting = Meeting::where('meeting_code', $request->meeting_code);

            $meeting->update([
                "user_id" => Auth::user()->id,
                "description" => $request->description,
                "meeting_code" => $request->meeting_code,
            ]);
            return redirect()->route('organization.meetings.index')->with('Success', "Meeting Update Successfully!");


        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);
        }


    }

    public function approvedOrNot(Request $request)
    {
        $record = Meeting::findOrFail($request->id);
        $record->status = $request->isApproved;
        if($record->save()){
            $userAdmin= UserCompany::where('user_id', array(Auth::user()->id))->pluck('company_id')->first();
            if(!$record->level_id){
                $adminCompanyAllUsersLIst = UserCompany::where('company_id',$userAdmin)->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();
            }else{
                $adminCompanyAllUsersLIst = UserCompany::where(['company_id'=>$userAdmin,'level_id'=>$record->level_id])->whereNotIn('user_id',array(Auth::user()->id))->pluck('user_id')->toArray();
            }
            $uniqueUserLIst= array_unique($adminCompanyAllUsersLIst);
//            print_r($uniqueUserLIst); die;
            if($record->status==1){
                foreach($uniqueUserLIst as $user){
                    $userMeetings= new MeetingUser();
                    $userMeetings->user_id= $user;
                    $userMeetings->company_id= $userAdmin;
                    $userMeetings->meeting_id= $request->id;
                    $userMeetings->meeting_code= $record->meeting_code;
                    $userMeetings->save();
              }

            }elseif($record->status==2){
                $meeting = MeetingUser::where('meeting_code',$record->meeting_code)->delete();
            }else{
                $meeting = MeetingUser::where('meeting_code',$record->meeting_code)->delete();
            }

            $error = "success";
            if($record->isApproved=="1"){
                $message ='User Permission changed to <strong>'."Approved".'</strong>';
            }elseif($record->isApproved=="2"){
                $message ='User Permission changed to <strong>'."Reject".'</strong>';
            }else{
                $message ='User Permission changed to <strong>'."Pending".'</strong>';
            }

        } else {
            $error = "error";
            $message ='Unable to change status';
        }
        return response()->json(['type' => $error,'message' => $message]);
    }


}
