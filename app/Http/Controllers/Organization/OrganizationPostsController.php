<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\organization\OrganizationPostRequest;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\User;
use App\Models\PostImage;
use App\Models\PostVideo;
use App\Models\UserCompany;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Illuminate\Support\Facades\File;

class OrganizationPostsController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request, User $user)
    {

        $user = Auth::user()->userComapny()->first();


        $users = UserCompany::where('company_id', $user->company_id)->pluck('user_id')->toArray();

        $userId = array_unique($users);


        $records = Post::WhereIn('user_id', $userId)->withCount('likes');


        $search = $request->query('search');
        if ($search) {
            $records->where(function ($q) use ($request, $search) {
                $q->where('description', 'like', '%' . $search . '%');
                $q->orWhere('type', 'like', '%' . $search . '%');
            })->orWhereHas("getUser", function ($query) use ($search) {
                $query->where(function ($data) use ($search) {
                    $data->orWhere('first_name', 'like', '%' . $search . '%');
                    $data->orWhere('last_name', 'like', '%' . $search . '%');
                });
            });

        }
        if ($request->type !== '') {
            $types = $request->type === 'company' ? ['admin'] : ['user', 'personal'];
            $records->whereIn('type', $types);
        }

        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }


        $records = $records->with(['getPostImage', 'getUser', 'getPostVideo']);


        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        return view('organization.posts.index', compact('records'));

    }

    public function createPersonalPost()
    {
        return view('organization.posts.create_personal_post');
    }

    public function create()
    {
        return view('organization.posts.create');

    }

    public function store(OrganizationPostRequest $request)
    {
        DB::beginTransaction();
        try {
//
            $post = new Post();
            $post->slug = Str::random(20);
            $post->user_id = Auth::user()->id;
            $post->description = $request->editor1;
            $post->type = "admin";
            $post->save();
            $id = $post->id;

            if ($request->hasfile('images')) {

                $imageDir = PostImage::POST_IMAGE_PATH . $post->id . "/";

                if (!File::exists(public_path($imageDir))) {
                    File::makeDirectory(public_path($imageDir), 0755, true);
                }

                $thumbDir = $imageDir . "thumb/";

                if (!File::exists(public_path($thumbDir))) {
                    File::makeDirectory(public_path($thumbDir), 0755, true);
                }

                $images = [];
                foreach ($request->file('images') as $image) {

                    $extension = $image->getClientOriginalExtension();

                    $image_name = uniqid() . '.' . $extension;

                    $image->move(public_path($imageDir), $image_name);

                    $imageResize = Image::make(public_path($imageDir) . $image_name);
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $imageResize->orientate()->fit(400, 400, function ($constraint) {
                        $constraint->upsize();
                    });
                    // save file
                    $imageResize->save(public_path($thumbDir . $image_name));

                    $images[] = [
                        "image" => $image_name
                    ];
                }

                if (!empty($images)) {
                    $post->getPostImage()->createMany($images);
                }
            }


            if ($request->hasfile('videos')) {

                $imageDir = PostVideo::POST_VIDEO_PATH . $post->id . "/";

                if (!File::exists(public_path($imageDir))) {
                    File::makeDirectory(public_path($imageDir), 0755, true);
                }

                $thumbDir = $imageDir . "thumb/";

                if (!File::exists(public_path($thumbDir))) {
                    File::makeDirectory(public_path($thumbDir), 0755, true);
                }

                $videos = [];
                foreach ($request->file('videos') as $video) {

                    $extension = $video->getClientOriginalExtension();

                    $name = uniqid() . '.' . $extension;

                    $video->move(public_path($imageDir), $name);

                    $thumb_image_name = str_replace($extension, "jpeg", $name);

                    $thumbnail = Thumbnail::getThumbnail(public_path($imageDir . $name), $thumbDir, $thumb_image_name, 1);

                    if ($thumbnail) {
                        $imageResize = Image::make(public_path($thumbDir) . $thumb_image_name);
                        // resize the image to a width of 300 and constrain aspect ratio (auto height)
                        $imageResize->orientate()->fit(400, 400, function ($constraint) {
                            $constraint->upsize();
                        });
                        // save file
                        $imageResize->save(public_path($thumbDir . $thumb_image_name));

                        // create a new Image instance for inserting
                        $watermark = Image::make(public_path("watermark/play_button.png"));
                        $imageResize->insert($watermark, 'center');

                        $imageResize->save(public_path($thumbDir . $thumb_image_name));
                    }

                    $videos[] = [
                        "video" => $imageDir . $name,
                        "thumbnail" => $thumbDir . $thumb_image_name,
                    ];
                }

                if (!empty($videos)) {
                    $post->getPostVideo()->createMany($videos);
                }
            }








            DB::commit();
            $addedPost = Post::where('id', $id)->first();
            if ($addedPost) {
                return redirect()->route('organization.posts.index')->with('success', 'Post created Successfully!');
            } else {
                return redirect()->with('error', 'Unable to Create Post!');
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();

        }


    }

    public function storePersonalPost(OrganizationPostRequest $request)
    {
        DB::beginTransaction();
        try {
//
            $post = new Post();
            $post->slug = Str::random(20);
            $post->user_id = Auth::user()->id;
            $post->description = $request->editor1;
            $post->type = "personal";
            $post->save();
            $id = $post->id;

            if ($request->hasfile('images')) {

                foreach ($request->file('images') as $images) {
                    $name = time() . rand(0, 9999) . '.' . $images->extension();
                    $images->move(public_path() . '/post/', $name);
                    $file = new PostImage();
                    $file->post_id = $id;
                    $file->image = $name;
                    $file->save();
                }
            }
            if ($request->hasfile('videos')) {

                foreach ($request->file('videos') as $videos) {
                    $name = time() . rand(0, 9999) . '.' . $videos->extension();
                    $videos->move(public_path() . '/post/', $name);
                    $file = new PostVideo();
                    $file->post_id = $id;
                    $file->video = $name;
                    $file->save();
                }
            }
            DB::commit();
            $addedPost = Post::where('id', $id)->first();
            if ($addedPost) {
                return redirect()->route('organization.posts.index')->with('success', 'Post created Successfully!');
            } else {
                return redirect()->with('error', 'Unable to Create Post!');
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            DB::rollback();

        }

    }

    public function edit($id)
    {
        $post = Post::find($id);

        $postImage = PostImage::where('post_id', $id)->get();
        $postVideo = PostVideo::where('post_id', $id)->get();
        if ($post) {
            return view('organization.posts.edit', compact('post', 'postImage', 'postVideo'));
        } else {
            return back()->with('error', 'unable To Update Post');
        }

    }

    public function update(OrganizationPostRequest $request)
    {

        $post = Post::find($request->id);
        //dd($post);die();
        if ($post) {
            if ($request->hasfile('images')) {

                foreach ($request->file('images') as $images) {
                    $name = time() . rand(0, 9999) . '.' . $images->extension();
                    $images->move(public_path() . '/post/images', $name);
                    $file = new PostImage();
                    $file->post_id = $request->id;
                    $file->image = $name;
                    $file->save();
                }
            }
            if ($request->hasfile('videos')) {

                foreach ($request->file('videos') as $videos) {
                    $name = time() . rand(0, 9999) . '.' . $videos->extension();
                    $videos->move(public_path() . '/post/', $name);
                    $file = new PostImage();
                    $file->post_id = $request->id;
                    $file->video = $name;
                    $file->save();
                }
            }
            $post->update([
                'slug' => Str::random(20),
                'user_id' => $post->user_id,
                'description' => $request->editor1,
                'type' => "admin",
            ]);
            return redirect()->route('organization.posts.index')->with('success', 'Record Updated Successfully');
        } else {
            return redirect()->back()->with('error', 'unable to updated Record');
        }

    }

    public function destroy($id)
    {
        $record = Post::findOrFail($id);

        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    public function changeStatus(Request $request)
    {
        $record = Post::findOrFail($request->id);
        $record->status = $request->status;
        if ($record->save()) {
            $error = 0;
            if ($record->status == false) {
                $message = 'Status changed to <strong>' . "Inactive" . '</strong>';
            } else {
                $message = 'Status changed to <strong>' . "Active" . '</strong>';
            }

        } else {
            $error = 1;
            $message = 'Unable to change status';
        }
        return response()->json(['error' => $error, 'message' => $message]);
    }

    public function destroyImage($id)
    {
        $removeIMage = PostImage::find($id);
        if ($removeIMage->delete()) {
            return response()->json(['type' => 'success', 'message' => 'Picture Removed Successfully!']);
        } else {
            return response()->json(['type' => 'error', 'message' => 'unable to remove Picture!']);
        }
    }

    public function destroyVideo($id)
    {
        $removeIMage = PostVideo::find($id);
        if ($removeIMage->delete()) {
            return response()->json(['type' => 'success', 'message' => 'Picture Removed Successfully!']);
        } else {
            return response()->json(['type' => 'error', 'message' => 'unable to remove Picture!']);
        }
    }

    public function view(Request $request)
    {

        $post = Post::where('id', $request->id)->with(['getPostImage', 'getUser', 'getCompany'])->first();

        $postImage = PostImage::where('post_id', $request->id)->get();

        $view = view('organization.posts.view')->with(['record' => $post, 'postImage' => $postImage])->render();
        return response()->json(['modal_content' => $view]);
    }

    public function display(Request $request)
    {

        $post = Post::where('id', $request->id)->with(['getPostImage', 'getUser', 'getCompany', 'getPostVideo'])->first();

        $postImage = PostImage::where('post_id', $request->id)->get();
        $postVideo = PostVideo::where('post_id', $request->id)->get();

        $view = view('organization.posts.view')->with(['record' => $post, 'postImage' => $postImage, 'postVideo' => $postVideo])->render();
        return view('organization.posts.display', compact('post', 'postImage', 'view', 'postVideo'));
    }

    public function selectPostType()
    {
        return view('organization.posts.selectPostType');
    }
}
