<?php


namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\organization\RegisterUserRequest;
use App\Http\Requests\organization\UpdateUserRequest;
use App\Http\Requests\PermissionsRequest;
use App\Models\Company;
use App\Models\CompanyDesignation;
use App\Models\Permission;
use App\Models\User;
use App\Models\UserChatGroup;
use App\Models\UserCompany;
use App\Models\UserPermission;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class OrganizationUsersController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {


        $userCompany = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id')->first();
        $records = UserCompany::whereIn('company_id', array($userCompany))->whereNotIn('user_id', [Auth::user()->id])->whereNotIn('role_id', [1]);


        $search = $request->query('search');
        if ($search) {
            $records->WhereHas("users", function ($query) use ($search) {
                $query->Where('first_name', 'like', '%' . $search . '%');
                $query->orWhere('last_name', 'like', '%' . $search . '%');
            });

        }

        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->with(['users'])->has('users');

        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));

        //dd($records);die();
//            return $records;
        return view('organization.users.index', compact('records'));


    }

    public function remove($id)
    {

        $record = User::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    public function getLevels(Request $request)
    {
        $Companies = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id', 'id')->first();
        $levels = CompanyDesignation::where('company_id', $Companies)->with('levels')->get();
        return response()->json($levels);
    }

    public function create()
    {
        $organizations = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id', 'id')->first();
        $levels = CompanyDesignation::where('company_id', $organizations)->with('levels')->get();
//         return $levels;
        return view('organization.users.create', compact('levels'));

    }

    public function storeOrganization(RegisterUserRequest $request)
    {

        try {
            $Companies = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id', 'id')->first();
            $user = new User();
            $user->role_id = User::UserRole;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->isVerified = 1;
            $user->isApproved = 1;
            $user->password = Hash::make($request['password']);
            $user->save();

            $userComapny = new UserCompany();
            $userComapny->role_id = User::Admin;
            $userComapny->user_id = $user->id;
            $userComapny->company_id = $Companies;
            $userComapny->level_id = $request->level;
            $userComapny->designation = $request->designation;

            if ($userComapny->save()) {
                return redirect()->route('organization.users.index')->with('success', 'User Added Successfully!');

            } else {
                return redirect()->back()->with('error', 'Something Went Wrong!');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }


    public function store(RegisterUserRequest $request)
    {

        try {
            $Companies = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id', 'id')->first();
            $user = new User();
            $user->role_id = User::UserRole;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->isVerified = 1;
            $user->isApproved = 1;
            $user->password = Hash::make($request['password']);
            $user->save();

            $userComapny = new UserCompany();
            $userComapny->role_id = User::Admin;
            $userComapny->user_id = $user->id;
            $userComapny->company_id = $Companies;
            $userComapny->level_id = $request->level;
            $userComapny->designation = $request->designation;

            if ($userComapny->save()) {
                return redirect()->route('organization.users.index')->with('success', 'User Added Successfully!');

            } else {
                return redirect()->back()->with('error', 'Something Went Wrong!');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

    public function edit($id)
    {
        $Companies = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id', 'id')->first();
        $levels = CompanyDesignation::where('company_id', $Companies)->with('levels')->get();
        $user = User::where('id', $id)->first();
        $userComapny = UserCompany::where(['user_id' => $user->id, 'company_id' => $Companies])->first();
        return view('organization.users.edit', compact('user', 'levels', 'userComapny'));
    }

    public function update(Request $request)
    {

        $validated = $request->validate([
            "first_name" => 'nullable|string',
            "last_name" => 'nullable|string',
            'designation' => 'nullable|string',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($request->id, 'id')
            ], 'mobile' => [
                'nullable',
                Rule::unique('users')->ignore($request->id, 'id')
            ],

        ]);
        try {
            $user = User::where('id', $request->id)->first();
            $admin = Auth::user()->userComapny()->first();
            $userCompanyDetail = UserCompany::where(['user_id' => $user->id, 'company_id' => $admin->company_id])->first();
            if ($userCompanyDetail->level_id !== $request->level_id) {
                UserChatGroup::where(['user_id' => $user->id])->update(['group_admin' => 0]);
            }

            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'mobile' => $request->mobile,
            ]);


            $Companies = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id', 'id')->first();
            $userComapny = UserCompany::where(['user_id' => $user->id, 'company_id' => $Companies])->update([
                'level_id' => $request->level_id,
                'designation' => $request->designation,
            ]);


            if ($userComapny) {
                return redirect()->route('organization.users.index')->with('success', 'User Update Successfully!');
            } else {
                return back()->with('error', 'Something Went Wrong');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
            return back()->with('error', $e->getMessage());
        }

    }

    public function changeUserPassword(Request $request)
    {


        $validated = $request->validate([
            'password' => 'required|string|min:8',
            'password_confirmation' => 'required|min:8|same:password',
        ]);
        $user = User::find($request->id);
        if ($user) {
            $user->update(["password" => Hash::make($request['password'])]);
            return response()->json(['type' => 'success', 'message' => 'User Password Changed Successfully!']);
        } else {
            return response()->json(['type' => 'error', 'message' => 'Something Went Wrong!']);
        }


    }

    public function approvedOrNot(Request $request)
    {
        $company = UserCompany::where('user_id', Auth::user()->id)->first();
        $record = UserCompany::where(["user_id" => $request->id, 'company_id' => $company->company_id])->firstOrFail();
        //dd($record);die();
        //return $record->isApproved;

        $record->isApproved = $request->isApproved;
        if ($record->save()) {


            $manch = Company::where(['permission_to_make_manch_org' => 1, 'status' => 1])->first();
            //return $manch;
            $check_manch_org = UserCompany::where(['user_id' => $request->id, 'company_id' => $manch->id])->first();
            $find_user = User::find($request->id);

            $permission = Permission::where('name', "Admin")->first();


            if (!$check_manch_org) {

                $add_manch_org = new UserCompany();
                $add_manch_org->user_id = $request->id;
                $add_manch_org->company_id = $manch->id;
                $add_manch_org->role_id = $find_user->role;
                $add_manch_org->is_like = 1;
                $add_manch_org->likeToJoin = 1;
                $add_manch_org->isApproved = 1;
                $add_manch_org->save();


                $permissions = new UserPermission();
                $permissions->user_id = $request->id;
                $permissions->company_id = $manch->id;
                $permissions->permission_id = $permission->id;
                $permissions->save();


            }


            $error = "success";
            if ($record->isApproved == "1") {
                // $company1 = UserCompany::where(["user_id" => $request->id, 'company_id' =>4])->first();
                // dd($company1);die();
                /*if(!$company1){
                    $userComapny = new UserCompany();
                    $userComapny->role_id = 3;
                    $userComapny->user_id = $request->id;
                    $userComapny->company_id = 4;
                    $userComapny->isApproved = 1;
                    $userComapny->level_id = 2;
                    $userComapny->save();
                }*/
                $message = 'User Permission changed to <strong>' . "Approved" . '</strong>';
            } elseif ($record->isApproved == "2") {
                $message = 'User Permission changed to <strong>' . "Reject" . '</strong>';
            } else {
                $message = 'User Permission changed to <strong>' . "Pending" . '</strong>';
            }

        } else {
            $error = "error";
            $message = 'Unable to change status';
        }
        return response()->json(['type' => $error, 'message' => $message]);
    }

    public function permission($id)
    {
        try {
            $adminCompany = UserCompany::where(['user_id' => auth::user()->id, 'role_id' => 3])->pluck('company_id')->first();

            $records = Permission::all();
            $user = User::find($id);
            $userPermissions = UserPermission::where(['user_id' => $id, 'company_id' => $adminCompany])->with('permission')->get();
//            return $userPermissions;

            return view('organization.users.permissions.index', compact('records', 'user', 'userPermissions'));
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

    public function addPermission(PermissionsRequest $request)
    {

        try {

            //get  admin company id
            $adminCompany = UserCompany::where(['user_id' => auth::user()->id, 'role_id' => 3])->pluck('company_id')->first();

            $permission = UserPermission::where(['user_id' => $request->user_id, 'permission_id' => $request->permission, 'company_id' => $adminCompany])->first();
            if ($adminCompany) {
                if (!$permission) {
                    $addPermission = new UserPermission();
                    $addPermission->user_id = $request->user_id;
                    $addPermission->permission_id = $request->permission;
                    $addPermission->company_id = $adminCompany;
                    $addPermission->save();
                    return redirect()->back()->with('success', 'Permission Activated Successfully!');
                } else {
                    return redirect()->back()->with('error', 'This Permission Already Activated!');

                }
            } else {
                return redirect()->back()->with('Error', 'Unable to Active Permission');
            }


        } catch (Exception $e) {
            Log::info("Log Message", ['error' => $e->getMessage()]);
        }
    }

    public function userPermissionRemoved($id)
    {

        $record = UserPermission::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Permission Removed Successfully']);
        } else {
            return back()->with(['error' => 'Unable to Remove this Permission!']);
        }


    }

}
