<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\PollesCreateRequest;
use App\Http\Requests\admin\PollesSubmitRequest;
use App\Models\Pole;
use App\Models\User;
use App\Models\UserCompany;
use App\Models\UserPole;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PollsController extends Controller
{
    public function index(Request $request)
    {
        $companies= UserCompany::where('user_id',Auth::user()->id)->pluck('company_id','id');
          $company_id =[];
          foreach($companies as $company){
              $company_id[] =$company;
          }
         $company_ids = array_unique($company_id);
          $users = UserCompany::whereIn('company_id',$company_ids)->pluck('user_id', 'id');

          $user_id =[];
          foreach ($users as $user){
              $user_id[] =$user;
          }
        $user_ids = array_unique($user_id);
          $usersPoll =[];
          foreach($user_ids as $user_){
              $usersPoll[]=$user_;
          }

          $superAdmin = User::where('role_id',1)->pluck('id')->first();


      $data = array_merge($usersPoll,[$superAdmin]);
        $polls = array_unique($data);

          $records = Pole::whereIn('user_id',$polls)->where('status',1);
        $search = $request->query('search');
        if ($search) {
            $records = $records->where(function ($q) use ($search) {
                $q->where('question', 'like', '%' . $search . '%');
                $q->orwhere('answer1', 'like', '%' . $search . '%');
                $q->orwhere('answer2', 'like', '%' . $search . '%');
                $q->orwhere('answer3', 'like', '%' . $search . '%');
                $q->orwhere('answer4', 'like', '%' . $search . '%');
                $q->orwhere('created_at', 'like', '%' . $search . '%');
            })->orWhereHas("users", function ($query) use ($search) {
                $query->where(function ($data) use ($search) {
                    $data->orWhere('first_name', 'like', '%' . $search . '%');
                });
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->with(['users']);
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


        return view('organization.polls.index', compact('records'));
    }

    public function changeStatus(Request $request)
    {
        $record = Pole::findOrFail($request->id);
        $record->status = $request->status;
        if ($record->save()) {
            $error = 0;
            if ($record->status == false) {
                $message = 'Status changed to <strong>' . "Inactive" . '</strong>';
            } else {
                $message = 'Status changed to <strong>' . "Active" . '</strong>';
            }

        } else {
            $error = 1;
            $message = 'Unable to change status';
        }
    }

    public function pollsDetails(Request $request, $id)
    {

//        return $request;
        $record = Pole::where('id', $id)->with('users')->first();
        $records = UserPole::where('pole_id', $id);
        $search = $request->query('search');
        if ($search) {
            $records = $records->where(function ($q) use ($search) {
                $q->orwhere('created_at', 'like', '%' . $search . '%');
                if ($search == "A" || $search == "a") {
                    $q->orwhere('answer', 'like', '%' . "answer1" . '%');
                } elseif ($search == "B" || $search == "b") {
                    $q->orwhere('answer', 'like', '%' . "answer2" . '%');
                } elseif ($search == "C" || $search == "c") {
                    $q->orwhere('answer', 'like', '%' . "answer3" . '%');
                } elseif ($search == "D" || $search == "d") {
                    $q->orwhere('answer', 'like', '%' . "answer4" . '%');
                }
            })->orWhereHas("users", function ($query) use ($search) {
                $query->where(function ($data) use ($search) {
                    $data->orWhere('first_name', 'like', '%' . $search . '%');
                    $data->orWhere('last_name', 'like', '%' . $search . '%');
                });
            });
        }
        if ($request->sort && $request->direction) {

            $records->orderBy($request->sort, $request->direction);
        } else {
            $records->orderBy("id", "DESC");

        }
        $records = $records->with(['users']);
        $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


//        return $record;
        return view('organization.polls.detail', compact('records', 'record'));

    }

    public function create()
    {

        return view('organization.polls.create');
    }

    public function store(PollesCreateRequest $request)
    {
        try {
//            return $request;
            $polls = new Pole();
            $polls->user_id = Auth::user()->id;
            $polls->question = $request->question;
            $polls->answer1 = $request->Answer1;
            $polls->answer2 = $request->Answer2;
            $polls->answer3 = $request->Answer3;
            $polls->answer4 = $request->Answer4;
            $polls->status = $request->status;
            if ($request->isShow == "on") {
                $polls->isShow = true;
            } else {
                $polls->isShow = false;
            }
            if ($polls->save()) {
                return redirect()->route('organization.polls.index')->with('success', 'Poll Created Successfully!');
            } else {
                return back()->with('error', 'Unable to create Poll');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        $record = Pole::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    public function edit($id)
    {
        $record = Pole::find($id);
        return view('organization.polls.edit', compact('record'));
    }

    public function update(PollesCreateRequest $request)
    {
//   return $request;
        try {
            $record = Pole::where('id', $request->id)->first();
            if ($record) {
                $record->question = $request->question;
                $record->status = $request->status;
                $record->answer1 = $request->Answer1;
                $record->answer2 = $request->Answer4;
                $record->answer3 = $request->Answer3;
                $record->answer4 = $request->Answer4;
                if ($request->isShow == "on") {
                    $record->isShow = true;
                } else {
                    $record->isShow = false;
                }

            }
            if ($record->save()) {
                return redirect()->route('organization.polls.index')->with('success', 'Poll Updated Successfully!');
            } else {
                return back()->with('error', 'Unable to Updated Poll');
            }


        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }


//   __________________________Submit Poll Ans ________________________________________________________________________//


    public function submitPoll($id)
    {


        $poll = Pole::find($id);

        return view('organization.polls.submit_polls.create', compact('poll'));
    }

    public function answerPoll(PollesSubmitRequest $request)
    {
        try {
            $submitPole = new UserPole();
            $submitPole->user_id = Auth::user()->id;
            $submitPole->pole_id = $request->id;
            $submitPole->answer = $request->answer;
            if ($submitPole->save()) {
                $this->calculatePoleResult($submitPole->answer, $submitPole->pole_id);
                return redirect()->route('organization.polls.index')->with('success', 'Poll Submit Subscricessfully!');
            } else {
                return redirect()->back()->with('error', 'Unable to Subit Poll!');
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function calculatePoleResult($answer, $pole_id)
    {


        try {
            $pole = Pole::where('id', $pole_id)->first();
            if ($pole) {
                if ($answer == "answer1") {
                    $answer1 = $pole->answer_count1;
                    $pole->update([
                        "answer_count1" => $answer1 + 1,
                    ]);

                } elseif ($answer == "answer2") {
                    $answer2 = $pole->answer_count2;
                    $pole->update([
                        "answer_count2" => $answer2 + 1,
                    ]);

                } elseif ($answer == "answer3") {
                    $answer3 = $pole->answer_count3;
                    $pole->update([
                        "answer_count3" => $answer3 + 1,
                    ]);

                } elseif ($answer == "answer4") {
                    $answer4 = $pole->answer_count4;
                    $pole->update([
                        "answer_count4" => $answer4 + 1,
                    ]);

                } else {
                    $answer5 = $pole->answer_count5;
                    $pole->update([
                        "answer_count5" => $answer5 + 1,
                    ]);

                }
            } else {
                return back()->with('unable To Submit!');
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

    public function submitPollRemoved($id)
    {
        $record = UserPole::findOrFail($id);
        if ($record) {
            $this->pollsCalculation($record->pole_id, $record->answer);
        }
        if ($record->delete()) {

            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    public function pollsCalculation($id, $answer)
    {

        try {
            $pole = Pole::where('id', $id)->first();
            if ($pole) {
                if ($answer == "answer1") {
                    $answer1 = $pole->answer_count1;
                    $pole->update([
                        "answer_count1" => $answer1 - 1,
                    ]);

                } elseif ($answer == "answer2") {
                    $answer2 = $pole->answer_count2;
                    $pole->update([
                        "answer_count2" => $answer2 - 1,
                    ]);

                } elseif ($answer == "answer3") {
                    $answer3 = $pole->answer_count3;
                    $pole->update([
                        "answer_count3" => $answer3 - 1,
                    ]);

                } elseif ($answer == "answer4") {
                    $answer4 = $pole->answer_count4;
                    $pole->update([
                        "answer_count4" => $answer4 - 1,
                    ]);

                } else {
                    $answer5 = $pole->answer_count5;
                    $pole->update([
                        "answer_count5" => $answer5 - 1,
                    ]);

                }
            } else {
                return back()->with('unable To remove!');
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

}
