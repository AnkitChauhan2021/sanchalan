<?php

namespace App\Http\Controllers;


use App\Helpers\CommonHelper;
use App\Http\Requests\organization\OrganizationPostRequest;
use App\Models\Post;
use App\Models\PostImage;
use App\Models\PostVideo;
use App\Models\Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Http\Requests\PostRequest;
use Intervention\Image\Facades\Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{

    public function create(PostRequest $request){
        DB::beginTransaction();
        $user = Auth::user()->userComapny()->first();
        $image_upload_limit = Config::first();
        $limit = $image_upload_limit->image_limit;

       
        try {
                $posts = new Post();
                $posts->description = $request->description;
                $posts->user_id = Auth::user()->id;
                $posts->slug = Str::random(20);
                $posts->type = "user";
                $posts->save();
                if ($request->hasfile('image')) {

                    $imageDir = PostImage::POST_IMAGE_PATH . $posts->id . "/";

                    if (!File::exists(public_path($imageDir))) {
                        File::makeDirectory(public_path($imageDir), 0755, true);
                    }

                    $thumbDir = $imageDir . "thumb/";

                    if (!File::exists(public_path($thumbDir))) {
                        File::makeDirectory(public_path($thumbDir), 0755, true);
                    }

                    $images = [];
                    foreach ($request->file('image') as $image) {

                        $extension = $image->getClientOriginalExtension();

                        $image_name = uniqid() . '.' . $extension;

                        $image->move(public_path($imageDir), $image_name);

                        $imageResize = Image::make(public_path($imageDir) . $image_name);
                        // resize the image to a width of 300 and constrain aspect ratio (auto height)
                        $imageResize->orientate()->fit(400, 400, function ($constraint) {
                            $constraint->upsize();
                        });
                        // save file
                        $imageResize->save(public_path($thumbDir . $image_name));

                        $images[] = [
                            "image" => $image_name
                        ];
                    }

                    if (!empty($images)) {
                        $posts->getPostImage()->createMany($images);
                    }
                }


                DB::commit();
               if ($posts->id) {
                return redirect()->back()->with('success', 'Post Created Successfully');
            } else {
                return back()->with('error', 'Oops!,Unable to create Post!');
            }


    }
    catch(Exception $e){

        Log::info('Post Log message', ['error' => $e->getMessage()]);
        DB::rollback();
    }
}

public function checkExtension($ext){
    $videoExts=['mp4'];
    $imageExts=['png','jpg'];

    if(in_array($ext, $videoExts)){
        return 'video';
    }else if(in_array($ext, $imageExts)){
       return 'image';
   }else{
       return 'none';
   }

}

public function edit($id){
        $post = Post::where('id',$id)->with(['getPostImage','getPostvideo'])->first();
      //dd($post);die();

        $postImage = PostImage::where('post_id',$id)->get();
        $postVideo = PostVideo::where('post_id',$id)->get();

        if($post){
           return view('posts.edit',compact('post','postImage','postVideo'));
        }else{
            return back()->with('error','unable To Update Post');
        }

    }
    public function update(PostRequest $request){

        $post = Post::find($request->id);

        if($post){

            if ($request->hasfile('image')) {
                   foreach ($request->file('image') as $images) {
                        $ext= $images->extension();
                        $name = time() . rand(0, 9999) . '.' . $ext;
                        $images->move(public_path() . '/post/images/', $name);
                        if($this->checkExtension($ext)==='image'){
                            $files = new PostImage();
                            $files->image = $name;
                            $files->post_id = $post->id;
                            $files->save();
                            //dd($files);die();
                        }
                    }
                }

                if ($request->hasfile('file')) {
                    foreach ($request->file('file') as $images) {
                        $ext= $images->extension();
                        $name = time() . rand(0, 9999) . '.' . $ext;
                        $images->move(public_path() . '/post/videos/', $name);
                        if($this->checkExtension($ext)==='video'){
                            $file = new PostVideo();
                            $file->video = $name;
                            $file->post_id =$post->id;
                            $file->save();
                        }
                   }

               }
            $post->update([
                'slug' =>Str::random(20),
                'user_id' =>Auth::user()->id,
                'description'=>$request->description,
                'type'=>"user",
            ]);
            return redirect()->route('user.post.index')->with('success','Post Updated Successfully');
        }else{
            return redirect()->back()->with('error','unable to updated Post');
        }

    }




}




