<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelper;
use App\Http\Requests\AddUserOrganization;
use App\Http\Requests\admin\ChangePasswordRequest;
use App\Http\Requests\UpdateUserProfile;
use App\Http\Requests\UserProfileUpdate;
use App\Models\City;
use App\Models\Company;
use App\Models\CompanyDesignation;
use App\Models\Country;
use App\Models\District;
use App\Models\Level;
use App\Models\State;
use App\Models\User;
use App\Models\UserCompany;
use App\Models\UserFollower;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $record = User::with('userFollowers', 'userFollowings')->where('id', Auth::user()->id)->with('company', 'userPost.getPostImage', 'userPost.getPostVideo')->first();
        $usercompanies = UserCompany::where('user_id', Auth::user()->id)->select('company_id', DB::raw('count(*) as total'))->groupBy('company_id')->with('company', 'level')->get();

//          return $usercompanies;


        $following = UserFollower::where('following_id', Auth::user()->id)->count();
        $follower = UserFollower::where('follower_id', Auth::user()->id)->count();
        return view('profile.index', compact('record', 'following', 'follower', 'usercompanies'));


    }


    public function followers()
    {


        $record = User::with('userFollowers', 'userFollowings')->where('id', Auth::user()->id)->with('userComapny.company', 'userPost.getPostImage', 'userPost.getPostVideo')->first();

        //dd($record);die();
        return view('profile.follower', compact('record'));
    }

    public function getUsers(Request $request)
    {

        $response = User::query()
            ->where('isVerified', 1)
            ->where('users.role_id', "<>", 1)
            ->where('users.id', '<>', Auth::user()->id);
        $search = $request->search;
        if ($request->search) {
            $response = $response->where(function ($q) use ($search) {

                $q->orWhereRaw("CONCAT(first_name,' ',last_name) like '$search%'");

                $q->orWhere('first_name', 'like', $search . '%');

                $q->orWhere('last_name', 'like', $search . '%');

                $q->orWhere('mobile', $search);

                $q->orWhereHas("userComapny.company", function ($query) use ($search) {
                    $query->where('name', 'like', $search . '%');
                });
            });
        }
        $response = $response->select("users.id", "users.first_name", "users.last_name",  "users.last_name", "users.mobile", "users.profile_pic", "users.email", "users.state_id", "users.district_id", "users.city_id");
        $response = $response
            ->leftJoin("user_companies", "users.id", "=", "user_companies.user_id")
            ->leftJoin("companies", "user_companies.company_id", "=", "companies.id")
            ->where(function ($query) {
                $query->orWhere("companies.permission_to_make_manch_org", 0);
                $query->orWhereNull("companies.permission_to_make_manch_org");
            })
            ->leftJoin("levels", "user_companies.level_id", "=", "levels.id");
        $response = $response->addSelect(
            "user_companies.company_id",
            "user_companies.user_id",
            "user_companies.level_id",
            "levels.name as level_name",
            "levels.position as level_position",
            "companies.name as company_name",
            "companies.name_in_hindi as company_name_in_hindi"
        );
        $response = $response
            //->groupBy("users.id")
            ->orderByRaw("IF(levels.position IS NULL , 9999999, levels.position)");
        $response = $response->offset(($request->page - 1) * 20)->limit(20)->get();
        $data = [];
        if ($response->count()) {
            foreach ($response as $key => $record) {
                $data[$key]["user_id"] = $record->id;
                $data[$key]["level_id"] = $record->level_id;
                $data[$key]["level_name"] = $record->level_name;
                $data[$key]["level_position"] = $record->level_position;
                $data[$key]["full_name"] = $record->full_name;
                $data[$key]["logo"] = $record->logo;
                $data[$key]["is_follow"] = $record->is_follow;
                $data[$key]["is_chat_request"] = $record->is_chat_request;
                $data[$key]["state_name"] = $record->state ? $record->state->name : "";
                $data[$key]["district_name"] = $record->district ? $record->district->name : "";
                $data[$key]["city_name"] = $record->city ? $record->city->name : "";
                $data[$key]["company_name"] = $record->company_name ? $record->company_name : "";
                $data[$key]["company_name_in_hindi"] = $record->company_name_in_hindi ? $record->company_name_in_hindi : "";
            }
        }

        return response()->json($response);
    }

    public function getAllUsers($id)
    {


        $record = User::with('userFollowers', 'userFollowings')->where('id', $id)->with('userComapny.company', 'userComapny.level', 'userPost.getPostImage', 'userPost.getPostVideo')->first();
        //dd($record);die();

        return view('profile.users', compact('record'));
    }

    public function followings()
    {

        $record = User::with('userFollowers.topLevelUserCompany.level', 'userFollowings.topLevelUserCompany.level')->where('id', Auth::user()->id)->with('userComapny.company','userComapny.level', 'userPost.getPostImage', 'userPost.getPostVideo')->first();

       //return $record;
        return view('profile.following', compact('record'));

    }

    public function upload(Request $request)
    {
        try {
            $user = User::find(Auth::user()->id);
            $oldImage = $user->profile_pic;
            if ($oldImage && file_exists(public_path(User::PROFILE_PIC_PATH . $oldImage))) {
                @unlink(public_path(User::PROFILE_PIC_PATH . $oldImage));
            }
            $folderPath = public_path('admin/profile/');

            $image_parts = explode(";base64,", $request->image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = uniqid() . '.jpg';
            $file_ = $folderPath . $file;

            file_put_contents($file_, $image_base64);
            User::find(Auth::user()->id)->update(['profile_pic' => $file]);
            return response()->json(['type' => 'success', 'message' => "Profile Image Updated Successfully", 'url' => asset(User::PROFILE_PIC_PATH . $file)]);
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function edit()
    {
        $record = User::where('id', Auth::user()->id)->with('userComapny.level', 'state', 'district', 'city')->get();
        $usercompanies = UserCompany::where('user_id', Auth::user()->id)->select('company_id', DB::raw('count(*) as total'))->groupBy('company_id')->with('company', 'level')->get();


        $States = State::where(['status' => 1])->get();

        $levels = CompanyDesignation::with('levels')->get();
//        dd($record);
        return view('profile.edit', compact('record', 'States', 'levels', 'usercompanies'));
    }

    public function levels(Request $request)
    {
        $country = CompanyDesignation::where(['company_id' => $request->comapny_id])->with('levels')->get();
        return response()->json($country);
    }

    public function states(Request $request)
    {
        $country = State::where(['status' => 1, 'country_id' => $request->country_id])->get();
        return response()->json($country);
    }

    public function districts(Request $request)
    {
        $state_ids = is_array($request->state_id) ? $request->state_id : [$request->state_id];
        $country = District::where(['status' => 1])->whereIn('state_id', $state_ids)->get();
        return response()->json($country);
    }

    public function city(Request $request)
    {
        $district = is_array($request->district_id) ? $request->district_id : [$request->district_id];
        $country = City::where(['status' => 1])->whereIn('district_id', $district)->get();
        return response()->json($country);
    }

    public function profileUpdate(UserProfileUpdate $request)
    {

        try {
            $user = User::find(Auth::user()->id);

            if ($user) {
                $user->country_id = $request->country_id;
                $user->state_id = $request->state_id;
                $user->district_id = $request->district_id;
                $user->city_id = $request->city_id;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->about_us = $request->about_us;
                $user->gender = $request->gender;
                $user->email = Auth::user()->email;
                $user->mobile = Auth::user()->mobile;
                if ($user->save()) {
                    //dd($user);die();
                    return redirect()->route('user.profile.index')->with('success', 'Profile Updated Successfully!');
                } else {
                    return redirect()->back()->with('error', 'unable to Updated Profile!');
                }

            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }


    }

//     user change Password
    public function changePassword()
    {
        return view('profile.password.changePassword');

    }

    public function passwordUpdate(ChangePasswordRequest $request)
    {

        try {
            $password = bcrypt($request->password);
            $admin = User::findOrFail(Auth::user()->id);
            $admin->update(['password' => $password]);

            return redirect()->route('user.profile.index')
                ->with(["success" => "Password changed successfully."]);
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function addOrganizationOnUserProfile($id)
    {
        $companies = Company::where('permission_to_make_manch_org', '!=', 1)->where('status', 1)->orderBy('name')->get();
        //dd($companies);die();
        return view('profile.addOrganizationOnProfile', compact('companies', 'id'));

    }

    public function destroy($id)
    {
        $record = UserCompany::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }
    }

    public function storeOrganization(AddUserOrganization $request)
    {

        try {

            $usercompanies = UserCompany::where('user_id', Auth::user()->id)->whereHas('company', function ($q) {
                $q->where('permission_to_make_manch_org', '!=', 1);
            })->pluck('company_id')->toArray();
            // dd($usercompanies);die();

            if (count($usercompanies) < 3) {
                $UserCompany = new UserCompany();
                $UserCompany->role_id = Auth::user()->role_id;
                $UserCompany->user_id = $request->id;
                $UserCompany->company_id = $request->company_id;
                if (UserCompany::where('user_id', Auth::user()->id)->where('company_id', $request->company_id)->exists()) {
                    return redirect()->route('user.profile.edit')->with('error', 'Company Already exists!');
                }
                $UserCompany->level_id = $request->level_id;
                $UserCompany->designation = $request->designation;
                if ($UserCompany->save()) {
                    return redirect()->route('user.profile.edit')->with('success', 'Record Added Successfully!');
                } else {
                    return redirect()->back()->with('error', 'Unable to added Record!');
                }
            } else {
                return redirect()->route('user.profile.edit')->with('error', 'Only 3 oganization are allowed');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }
}
