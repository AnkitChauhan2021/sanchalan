<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelper;
use App\Http\Requests\InquiryRequest;
use App\Http\Requests\SubmitPoleRequest;
use App\Models\Inquiry;
use App\Models\Company;
use App\Models\Chat;
use App\Models\CompanyWallVisit;
use App\Models\Level;
use App\Models\Pole;
use App\Models\State;
use App\Models\District;
use App\Models\City;
use App\Models\Report;
use App\Models\ReportNotification;
use App\Models\Config;
use App\Models\PostImage;
use App\Models\PostVideo;
use App\Models\Post;
use App\Models\Magazine;
use App\Models\PostBlock;
use App\Models\PostComment;
use App\Models\User;
use App\Models\UserPermission;
use App\Models\UserCompany;
use App\Models\Meeting;
use App\Models\MeetingUser;
use App\Models\UserFollower;
use App\Models\UserPole;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\PollsCreateRequest;

class WebController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {

        return view('home.index');
    }

//   show post page
    public function GetUserPost(Request $request)
    {
        $report = Post::with(['report' => function ($q) {
            $q->where('user_id', Auth::user()->id);

        }])->get();


        $user = User::get();
        /* All polls*/
        $polls = CommonHelper::polls();
        /*All Posts*/
        $records = CommonHelper::Posts();
        $records = $records->get();
        $records = array_merge([$polls, $records]);

        $poll_post = [];
        foreach ($records as $record) {
            foreach ($record as $value) {
                $poll_post[] = $value;
            }

        }
        $collection = collect($poll_post);
        /*Company of Logged In user*/
        $user_companies = UserCompany::where('user_id', Auth::user()->id)->pluck('company_id')->toArray();
        /*Suggestions if user LoggedIn first time*/
        $users = UserCompany::whereIn('company_id', $user_companies)->whereHas('user', function ($q) {
            $q->where('city_id', Auth::user()->city_id)->where('id', '<>', Auth::user()->id);
        })->with('user')->get();
        $records = $collection->values()->all();

        return view('posts.index', compact('records', 'users', 'report'));
    }

    public function trendingPosts(Request $request)
    {
        try {
            //  settings for how many like to make trending post this setting menage by super admin
            $pageNo = $request->page ?? 0;
            $setting = Config::first();

            //  Get user of company in this company loggedIn user likeToJoin And full Joined
            $userCompanyLike = UserCompany::where(['user_id' => Auth::user()->id, 'is_like' => 1])->pluck('company_id')->toArray();
            $UserCompanies = UserCompany::where(['user_id' => Auth::user()->id, 'isApproved' => 1])->pluck('company_id')->toArray();
            $company_id = array_merge($userCompanyLike, $UserCompanies);

//          $UserCompanies = UserCompany::where(['user_id' => Auth::user()->id])->pluck('company_id')->toArray();

            //make user record (ids) unigue
            $UserCompanies = array_unique($company_id);

            // user tags name
            $companyTags = Company::whereIn('id', $UserCompanies)->where('status', 1)->orderBy("id", "DESC")->get()->each->setAppends(['is_post_exist_after_visit']);


            // after getting company user get in these company how many user exist
            $users = UserCompany::whereIn('company_id', $UserCompanies)->pluck('user_id')->toArray();

//          make user unique
            $users = array_unique($users);
//            these users store in the array
            $usersArray = [];

            foreach ($users as $user) {
                $usersArray[] = $user;
            }
//                get block post remove from list, this methode exist on common helper app\helpers\CommonHelper.php

            $BlockedPost = CommonHelper::BlockedPost();
//          get post using user array and remove blocked posts and take only 3 post
            $posts = Post::whereIn('user_id', $usersArray)->whereNotIn('id', $BlockedPost)->where('status', 1);

//          make post count and check setting limits trending post how many like to make
            $posts = $posts->withCount('likes')->with('getPostImage', 'getPostVideo')->having('likes_count', '>=', $setting->trending_post)->take(3)->get();

//           1manch organization get 6 post
            $manchOrganization = Company::where(['permission_to_make_manch_org' => 1, 'status' => 1])->pluck('id')->toArray();

//            get Users of manch org
            $usersOfManch = UserCompany::whereIn('company_id', $manchOrganization)->pluck('user_id')->toArray();

//                get Manch users post and remove blocked posts
            $ManchUsersPost = Post::whereIn('user_id', $usersOfManch)->whereNotIn('id', $BlockedPost)->where('status', 1);

            $ManchUsersPost = $ManchUsersPost->withCount('likes')->with('getPostImage', 'getPostVideo')->having('likes_count', '>=', $setting->trending_post)->take(6)->get();

//          remaining all company rending post and also remove manch org.. and user join org...

            $remainingCompanies = Company::whereNotIn('id', $UserCompanies)->whereNotIn('id', $manchOrganization)->pluck('id')->toArray();

//          get All users  with RemainingCompanies
            $remainingUsers = UserCompany::whereIn('company_id', $remainingCompanies)->pluck('user_id')->toArray();


//          get Remaining posts and also remove blocked post
            $remainingPosts = Post::whereIn('user_id', $remainingUsers)->whereNotIn('user_id', $usersOfManch)->whereNotIn('user_id', $users)->whereNotIn('id', $BlockedPost)->where('status', 1);
            $remainingPosts = $remainingPosts->withCount('likes')->with('getPostImage', 'getPostVideo')->having('likes_count', '>=', $setting->trending_post);

            $records = $remainingPosts->paginate(9);
//            after geting three type of post marge these array in one array

            $allPost = new \Illuminate\Database\Eloquent\Collection;
            $allPost = $allPost->merge($posts);
            $allPost = $allPost->merge($ManchUsersPost);
//              return $companyTags;
            if ($allPost || $records) {
                return view('posts.otherPosts', compact('allPost', 'records', 'pageNo', 'companyTags'));
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function postDetail($id)
    {
        $post_detail = Post::where('id', $id)->with('postBy', 'getPostImage', 'getPostVideo', 'comments', 'comments.commentBy', 'likes', 'likes.likeBy', 'replies')->first();
        //return $post_detail;
        return view('posts.post_detail', compact('post_detail'));
    }


//inquiry (contact us)

    public function contact_us(InquiryRequest $request)
    {
        try {
            $inquiry = new Inquiry();
            if (Auth::check()) {
                $inquiry->name = Auth::user()->full_name;
                $inquiry->email = Auth::user()->email;
                $inquiry->subject = $request->subject;
                $inquiry->body = $request->body;
                if ($inquiry->save()) {
                    return redirect()->route('welcome.index')->with('success', 'Thank You For Contact, We are Connect To You As soon as possible !');
                } else {
                    return redirect()->back()->with('error', 'unable To Contact, Please Try Again!');
                }
            } else {

                $inquiry->name = $request->name;
                $inquiry->email = $request->email;
                $inquiry->subject = $request->subject;
                $inquiry->body = $request->body;
                if ($inquiry->save()) {
                    return redirect()->route('welcome.index')->with('success', 'Thank You For Contact, We are Connect To You As soon as possible !');
                } else {
                    return redirect()->back()->with('error', 'unable To Contact, Please Try Again!');
                }
            }
        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function comments(Request $request)
    {

        try {
            $post = Post::find($request->post_id);

            if ($post->company_approved == 0) {
                $postcomment = new PostComment();
                if ($request->comment_id) {
                    $postcomment->user_id = Auth::user()->id;
                    $postcomment->parent_id = $request->comment_id;
                    $postcomment->post_id = $request->post_id;
                    $postcomment->comment = $request->comment;
                    $postcomment->save();
                } else {
                    $postcomment->user_id = Auth::user()->id;
                    $postcomment->post_id = $request->post_id;
                    $postcomment->comment = $request->comment;
                    $postcomment->save();
                }
                if ($postcomment->id) {
                    return redirect()->back()->with('Success', 'Thank You For Comment');
                } else {
                    return redirect()->back()->with('error', 'unable to Added comment');
                }
            } else {
                return redirect()->back()->with('error', 'You are Not Approved to Comment on this Company Post!');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }
    }

    public function submitPoll(SubmitPoleRequest $request)
    {
        try {
            $submit_poll = new UserPole();
            $submit_poll->user_id = Auth::user()->id;
            $submit_poll->pole_id = $request->pole_id;
            $submit_poll->answer = $request->answer;
            if ($submit_poll->save()) {
                return response()->json([
                    'status' => 1,
                    'message' => "Poll Submit Successfully!",
                    'data' => $submit_poll
                ]);
            } else {
                return response()->json([
                    'status' => 0,
                    'message' => "unable To submit!",
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            Log::info('Log message', ["error" => $e->getMessage()]);
        }
    }

    public function magazine()
    {

        $magazines = Magazine::where('company_id', Auth::user()->admin_company_id)->with('images', 'videos')->get();
        //dd($magazines);die();
        return view('home.magazine', compact('magazines'));
    }

    public function magazine_detail($id)
    {
        $magazines = Magazine::where('id', $id)->with('images', 'videos')->get();
        return view('home.magazine_detail', compact('magazines'));
    }

    public function companyWall($id)
    {

        $company = Company::where('id', $id)->with('state', 'district', 'city')->first();


        if ($company) {
            //check last visit on this company for this user
            $visit = CompanyWallVisit::where(['user_id' => Auth::user()->id, 'company_id' => $company->id])->first();

            //      if user last visit exist on this company then update current visit time
            if ($visit) {
                $visit->update(['last_visit_at' => Carbon::now()]);
            } else {
//                    else create current visit time on this company
                $visits = new CompanyWallVisit();
                $visits->user_id = Auth::user()->id;
                $visits->company_id = $company->id;
                $visits->last_visit_at = Carbon::now();
                $visits->save();

            }
        }

        $isApproved = CommonHelper::isApproved();
        $answeredPolls = CommonHelper::submitedPolls();
        $posts = Post::where('company_id', $id)->with('getPostImage', 'getPostVideo', 'comments', 'comments.commentBy', 'likes', 'likes.likeBy')->orderBy('id', 'desc')->get();
        $polls = Pole::where('company_id', $id)->whereNotIn('id', $answeredPolls)->where('status', 1)->orderBy('id', 'desc')->get();

        $posts = array_merge([$polls, $posts]);
        $poll_post = [];
        foreach ($posts as $record) {
            foreach ($record as $value) {
                $poll_post[] = $value;
            }

        }
        $collection = collect($poll_post);
        $posts = $collection->values()->all();
        //return $posts;
        $users = UserPermission::where('user_id', Auth::user()->id)->get();
        //return $users;
        return view('companywall.index', compact('posts', 'users', 'company'));
    }

    public function createWallPost(PostRequest $request)
    {
        DB::beginTransaction();
        $image_upload_limit = Config::first();
        $limit = $image_upload_limit->image_limit;


        try {
            $approved = CommonHelper::approved();
            if (count($approved) > 0) {
                $posts = new Post();
                $posts->description = $request->description;
                $posts->user_id = Auth::user()->id;
                $posts->company_id = $request->company_id;
                $posts->slug = Str::random(20);
                $posts->type = "user";
                $posts->save();

                if ($request->hasfile('image')) {
                    foreach ($request->file('image') as $images) {
                        $name = time() . rand(0, 9999) . '.' . $images->extension();

                        $images->move(public_path() . '/post/images', $name);
                        $file = new PostImage();
                        $file->post_id = $posts->id;
                        $file->image = $name;
                        $file->save();
                    }
                }
                if ($request->hasfile('file')) {
                    foreach ($request->file('file') as $images) {
                        $name = time() . rand(0, 9999) . '.' . $images->extension();
                        $images->move(public_path() . '/post/videos/', $name);
                        $file = new PostVideo();
                        $file->video = $name;
                        $file->post_id = $posts->id;
                        $file->save();
                    }
                }
                DB::commit();
                if ($posts->id) {
                    return redirect()->back()->with('success', 'Post Created Successfully');
                } else {
                    return back()->with('error', 'Oops!,Unable to create Post!');
                }
            } else {
                return back()->with('error', 'Oops!,Unable to create Post Please Wait for Approval Your Profile');
            }
        } catch (Exception $e) {
            Log::info('Post Log message', ['error' => $e->getMessage()]);
            DB::rollback();
        }
    }

    public function editWallPost($id)
    {
        $company = Company::where('id', $id)->with('state', 'district', 'city')->get();
        $post = Post::where('id', $id)->with(['getPostImage', 'getPostvideo'])->first();
        $postImage = PostImage::where('post_id', $id)->get();
        $postVideo = PostVideo::where('post_id', $id)->get();

        if ($post) {
            return view('companywall.edit', compact('post', 'postImage', 'postVideo', 'company'));
        } else {
            return back()->with('error', 'unable To Update Post');
        }


    }

    public function updateWallPost(PostRequest $request)
    {

        $post = Post::find($request->id);

        if ($post) {
            if ($request->hasfile('image')) {
                foreach ($request->file('image') as $images) {
                    $ext = $images->extension();
                    $name = time() . rand(0, 9999) . '.' . $ext;
                    $images->move(public_path() . '/post/images/', $name);
                    if ($this->checkExtension($ext) === 'image') {
                        $files = new PostImage();
                        $files->image = $name;
                        $files->post_id = $post->id;
                        $files->save();
                    }
                }
            }

            if ($request->hasfile('file')) {
                foreach ($request->file('file') as $images) {
                    $ext = $images->extension();
                    $name = time() . rand(0, 9999) . '.' . $ext;
                    $images->move(public_path() . '/post/videos/', $name);
                    if ($this->checkExtension($ext) === 'video') {
                        $file = new PostVideo();
                        $file->video = $name;
                        $file->post_id = $post->id;
                        $file->save();
                    }
                }

            }
            $post->update([
                'slug' => Str::random(20),
                'user_id' => Auth::user()->id,
                'description' => $request->description,
                'type' => "user",
            ]);
            return redirect()->route('user.post.index')->with('success', 'Post Updated Successfully');
        } else {
            return redirect()->back()->with('error', 'unable to updated Post');
        }
    }

    public function createPolls($id)
    {
        $company = Company::where('id', $id)->with('state', 'district', 'city')->first();
        return view('companywall.polls.create', compact('company'));

    }

    public function storePolls(PollsCreateRequest $request)
    {
        try {

            $polls = new Pole();
            $polls->user_id = Auth::user()->id;
            $polls->company_id = $request->company_id;
            $polls->question = $request->question;
            $polls->answer1 = $request->Answer1;
            $polls->answer2 = $request->Answer2;
            $polls->answer3 = $request->Answer3;
            $polls->answer4 = $request->Answer4;
            $polls->status = $request->status;

            if ($request->isShow == "on") {
                $polls->isShow = true;
            } else {
                $polls->isShow = false;
            }
            if ($polls->save()) {
                return redirect()->route('user.company_wall', $request->company_id)->with('success', 'Poll Created Successfully!');
            } else {
                return back()->with('error', 'Unable to create Poll');
            }

        } catch (Exception $e) {
            Log::info('Log message', ['error' => $e->getMessage()]);
        }

    }

    public function meetingsIndex($id)
    {
        $company = Company::where('id', $id)->with('state', 'district', 'city')->first();
        $records = Meeting::where('status', 1)->orderBy('id', 'desc');
        $records = $records->paginate();
        $meetingforuser = Meeting::where('company_id', $id)->where('status', 1)->orderBy('id', 'desc');
        $meetingforuser = $meetingforuser->paginate();
        return view('companywall.meetings.index', compact('company', 'records', 'meetingforuser'));

    }

    public function recievedmeetingsIndex($id)
    {

        $company = Company::where('id', $id)->with('state', 'district', 'city')->first();
        $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
        $records = MeetingUser::whereIn('company_id', $logInUser)->where('user_id', Auth::user()->id)->with('Organization');
        $records = $records->paginate();
        //dd($invites);
        return view('companywall.meetings.recievedindex', compact('records', 'company'));

    }

    public function meetingsCreate($id)
    {
        $company = Company::where('id', $id)->with('state', 'district', 'city')->first();
        $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
        $companies = Company::where('status', 1)->whereNotIn('id', $logInUser)->get();
        $levels = Level::where('status', 1)->get();
        return view('companywall.meetings.create', compact('company', 'companies', 'levels'));

    }

    public function meetingsStore(Request $request)
    {
        try {
            //return$request;
            $logInUser = UserCompany::where('user_id', $request->company_id)->first();
            $userAdmin = UserCompany::where('user_id', array(Auth::user()->id))->pluck('company_id')->first();
            if (!$request->levels) {
                $adminCompanyAllUsersLIst = UserCompany::where('company_id', $userAdmin)->whereNotIn('user_id', array(Auth::user()->id))->pluck('user_id')->toArray();
            } else {
                $adminCompanyAllUsersLIst = UserCompany::where(['company_id' => $userAdmin, 'level_id' => $request->levels])->whereNotIn('user_id', array(Auth::user()->id))->pluck('user_id')->toArray();

            }

            $string = str::random(10);
            foreach ($request->companies as $company) {
                $meeting = new Meeting();
                $meeting->user_id = Auth::user()->id;
                $meeting->description = $request->description;
                $meeting->company_id = $request->company_id;
                $meeting->meeting_code = $string;
                $meeting->level_id = $request->levels;
                $meeting->save();
            }
            if (!empty($logInUser)) {
                $meeting = new Meeting();
                $meeting->user_id = Auth::user()->id;
                $meeting->description = $request->description;
                $meeting->company_id = $logInUser->company_id;
                $meeting->meeting_code = $string;
                $meeting->status = 1;
                $meeting->level_id = $request->levels;
                $meeting->save();

            }
            if ($meeting) {

                return redirect()->route('user.meetings.index', $request->company_id)->with('Success', "Meeting Created Successfully!");
            } else {
                return back()->with('unable to create Meeting');
            }
        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }
    }

    public function view(Request $request, $id, $id1)
    {
        //dd($request);
        $company = Company::where('id', $id1)->with('state', 'district', 'city')->first();

        try {
            $logInUser = UserCompany::whereIn('user_id', array(Auth::user()->id))->pluck('company_id')->toArray();
            $record = Meeting::where('meeting_code', $id)->first();
//

            $records = Meeting::where('meeting_code', $id)->whereNotIN('company_id', $logInUser);
            $search = $request->query('search');
            if ($search) {
                $records->WhereHas("Organization", function ($query) use ($search) {
                    $query->Where('name', 'like', '%' . $search . '%');
                });
            }
            if ($request->sort && $request->direction) {

                $records->orderBy($request->sort, $request->direction);
            } else {
                $records->orderBy("id", "DESC");
            }
            $records = $records->with(['Organization']);
            $records = $records->paginate(($request->query('limit') ? $request->query('limit') : env('PAGINATION_LIMIT')));


            return view('companywall.meetings.organization.index', compact('records', 'record', 'company'));

        } catch (Exception $e) {
            Log::info('Log Message', ['error' => $e->getMessage()]);

        }

    }

    public function OrganizationRemoveFromMeeting($id)
    {
        $record = Meeting::findOrFail($id);

        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }

    }

    /*public function advanceSearch(Request $request)
    {

        $organization = Company::get();
        $level = Level::get();
        $state = State::get();
        $districts = District::get();
        $cities = City::get();

        return view('advancesearch.search', compact('organization', 'state', 'level', 'districts', 'cities'));
    }*/


    public function usersListWithFilter(Request $request)
    {


        // all companies to display in options in view
        $organization = Company::get();
        $level = Level::get();
        $state = State::get();
        $districts = District::get();
        $cities = City::get();

        $loggedInUser = User::where('id', Auth::user()->id)->with('userComapny')->first();
        //return $loggedInUser;
        //return $request;


        try {

            $organizations = $request->company_id;
            $states = $request->state_ids;
            $district = $request->district_ids;
            $city = $request->city_id;
            $levels = $request->level_id;
            //Atleast one filter should be selected to go inside

            $chat = Chat::get();
            //return $chat;

            $records = User::where('users.isVerified', 1)
                ->where("users.status", 1)
                ->where("users.role_id", "<>", 1)
                ->where('users.id', '<>', auth()->user()->id);

            $records = $records->select("users.id", "users.first_name", "users.last_name", "users.mobile", "users.profile_pic", "users.email", "users.state_id", "users.district_id", "users.city_id");
//            //If user search with organization only or organization with levels
            if (isset($organizations)) {
                $records = $records->whereIn("user_companies.company_id", $organizations);
            }

            if (isset($levels)) {
                //$levels = array_filter($levels);
                $records = $records->whereIn("levels.id", $levels);
            }

            if (isset($states)) {
                $states = is_array($states) ? $states : json_decode($states);
                $records = $records->whereIn('users.state_id', $states);
            }


            if (isset($district)) {
                $district = is_array($district) ? $district : json_decode($district);
                $records = $records->whereIn('users.district_id', $district);
            }


            // return $request;
            if (isset($city)) {
                if ($city[0] !== null) {
                    $city = is_array($city) ? $city : json_decode($city);
                    $records = $records->whereIn('users.city_id', $city);
                }
            }


            //$limit = $request->record_per_page;
            $records->with('userComapny.level', 'state', 'district', 'city');
            $records = $records
                ->leftJoin("user_companies", "user_companies.user_id", "=", "users.id")
                ->leftJoin("companies", "user_companies.company_id", "=", "companies.id")
                ->where(function ($query) {
                    $query->orWhere("companies.permission_to_make_manch_org", 0);
                    $query->orWhereNull("companies.permission_to_make_manch_org");
                })
                ->leftJoin("levels", "user_companies.level_id", "=", "levels.id");


            $records = $records
                ->orderByRaw("IF(levels.position IS NULL , 9999999, levels.position)");

            $records = $records->addSelect(
                "user_companies.company_id",
                "user_companies.user_id",
                "user_companies.level_id",
                "levels.name as level_name",
                "levels.position as level_position",
                "companies.name as company_name",
                "companies.name_in_hindi as company_name_in_hindi"
            );

            $records = $records->paginate();


            if ($records->count()) {
                foreach ($records as $key => $record) {
                    $data[$key]["user_id"] = $record->id;
                    $data[$key]["level_id"] = $record->level_id;
                    $data[$key]["level_name"] = $record->level_name;
                    $data[$key]["level_position"] = $record->level_position;
                    $data[$key]["full_name"] = $record->full_name;
                    $data[$key]["logo"] = $record->logo;
                    $data[$key]["is_follow"] = $record->is_follow;
                    $data[$key]["is_chat_request"] = $record->is_chat_request;
                    $data[$key]["state_name"] = $record->state ? $record->state->name : "";
                    $data[$key]["district_name"] = $record->district ? $record->district->name : "";
                    $data[$key]["city_name"] = $record->city ? $record->city->name : "";
                    $data[$key]["company_name"] = $record->company_name ? $record->company_name : "";
                    $data[$key]["company_name_in_hindi"] = $record->company_name_in_hindi ? $record->company_name_in_hindi : "";
                }
               //return $records;
                return view('advancesearch.search', compact('chat','records', 'organization', 'level', 'state', 'districts', 'cities', 'loggedInUser'));
            } else {
                return redirect()->route('user.advance-search')->with('error', 'No Records');
            }

        } catch (Exception $e) {
            Log::info("log message", ['error' => $e->getMessage()]);
        }


    }

    public function chat()
    {
        return view('chat.index');
    }

    public function reportPost($id)
    {
        /* $reports = Report::where('post_id',$id)->where('user_id',Auth::user()->id)->first();*/
        // dd($reports);
        $post = Post::where('id', $id)->first();
        $i = $post->report_count;
        if ($post) {
            $reports = new Report();
            $reports->user_id = Auth::user()->id;
            $reports->post_id = $post->id;

            if ($reports->save()) {
                $post->report_count = $i + 1;
                $post->save();

                $notification = new ReportNotification();
                $notification->user_id = $post->user_id;
                $notification->post_id = $post->id;
                $notification->save();


                return redirect()->back()->with('success', 'Reported Successfully');
            } else {
                return back()->with('error', 'Oops!,Unable to report Post!');
            }
        } else {
            return back()->with('error', 'Oops!,Unable to report Post');
        }
    }

    public function ChatRequest(Request $request, $id, $id1)
    {

        try {
            $chat = new Chat();
            $chat->sender_id = $request->id1;
            $chat->receiver_id = $request->id;
            if ($chat->save()) {
                //return $records;
                return redirect()->route('user.advance-search')->with('success', 'Chat request sent successfully');
            } else {
                return redirect()->route('user.advance-search')->with('error', 'Unable to send');
            }

        } catch (Exception $e) {
            Log::info("log message", ['error' => $e->getMessage()]);
        }
    }

    public function ChatReceived()
    {
        $records = Chat::where('receiver_id', Auth::user()->id)->where('status', 0)->with('receiveChatRequest');
//return $records;
        $records = $records->paginate();
        return view('chat.received', compact('records'));
    }

    public function ChatAccept($id)
    {
        $record = Chat::findOrFail($id);
        if ($record->status == 0) {
            $record->status = 1;
            $record->save();
            return back()->with(['success' => 'Request accepted']);
        }

    }
    public function ChatRequestRemove($id){
        $record = Chat::findOrFail($id);
        if ($record->delete()) {
            return back()->with(['success' => 'Record deleted successfully']);
        } else {
            return back()->with(['error' => 'Unable to delete this record']);
        }
    }
}
