<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next ,...$guards)
    {

        $guards = empty($guards) ? [null] : $guards;
        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {

                if(Auth::user()->role_id == User::SuperAdmin){
                    return $next($request);
                }
            }
//            return redirect(RouteServiceProvider::AdminLogin);
            return redirect()->route('admin.showLoginForm')->with("error","You don't have admin access.");
        }

    }

}
