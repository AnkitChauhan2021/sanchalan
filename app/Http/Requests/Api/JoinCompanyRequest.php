<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class JoinCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected function failedValidation(Validator $validator): void
    {
        $errors = $validator->errors();

        throw new HttpResponseException(response()->json([
            'status_code'=> 0,
            'message'=> $errors->first(),
            'data'=> null
        ], Response::HTTP_OK));
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'company_id'=>'required|exists:companies,id',
//            'level_id'=>'required|exists:levels,id',
//            'user_id'=>'required|exists:users,id',
//            'designation'=>'nullable|string',
//            'role_id'=>'required|exists:roles,id',
//            'WantTOJoin'=>'required'
        ];
    }
}
