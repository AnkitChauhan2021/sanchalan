<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class MagazinesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
   protected function failedValidation(Validator $validator){
       $errors = $validator->errors();
       throw new HttpResponseException(response()->json([
           'status_code'=> 0,
           'message'=> $errors->first(),
           'data'=> null
       ], Response::HTTP_OK));

   }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'magazine_id' => 'required|exists:magazines,id',
            "title"=>'required',
            "description"=>"required",
            'images'=>"nullable",
            'videos'=>'nullable'
        ];
    }
}
