<?php

namespace App\Http\Requests\Api\Message;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AddGroupUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "required|array|min:1",
            "user_id.*" => "required|exists:users,id",
            "chat_group_id" => "required|exists:chat_groups,id",
            //"chat_group_id" => "required|exists:chat_groups,id|unique:user_chat_groups,chat_group_id,NULL,id,user_id,{$this->user_id},status,1",
        ];
    }

    public function messages()
    {
        return [
            "chat_group_id.unique"=>'User already exist in the group.'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status_code' => 0,
            'message' => $validator->errors()->first(),
            'data' => null
        ]));
    }
}
