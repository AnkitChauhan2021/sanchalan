<?php

namespace App\Http\Requests\Api\Message;

use App\Models\ChatGroup;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            "user_id" => auth()->user()->id,
            "type" => ChatGroup::GROUP,
            "status" => ChatGroup::ACTIVE,
        ]);

        if($this->receivers && is_array($this->receivers)){
            $receivers = collect($this->receivers)->map(function ($item, $key) {
                $item['status'] = ChatGroup::ACTIVE;
                return $item;
            });

            $this->merge([
                "receivers" => $receivers->all()
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "type" => "required",
            "status" => "required",
            "user_id" => "required|exists:users,id",
            "name" => "required|unique:chat_groups,name,NULL,id,user_id,{$this->user_id}",
            "icon" => "nullable|image",
            "receivers" => ["required", "array", "min:1"],
            "receivers.*.user_id" => "required|exists:users,id",
            "receivers.*.group_admin" => "required|boolean",
            "receivers.*.status" => "required"
        ];
    }

    public function messages()
    {
        return [
            "receivers.*.user_id.required" => "Please select atleast one user",
            "receivers.*.user_id.exists" => "Invalid user",
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status_code'=> 0,
            'message'=> $validator->errors()->first(),
            'data'=> null
        ]));
    }
}
