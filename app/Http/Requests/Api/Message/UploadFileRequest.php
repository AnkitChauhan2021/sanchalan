<?php

namespace App\Http\Requests\Api\Message;

use App\Models\Config;
use App\Models\Message;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UploadFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()

    {
        $config  = Config::first();
        return [
            "type" => "required|in:".Message::TYPE_IMAGE.",".Message::TYPE_VIDEO.",".Message::TYPE_FILE,
            "file" => "required|max:".$config->video_size*1024,
            "chat_group_id" => "required|exists:chat_groups,id",
        ];
    }


    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status_code'=> 0,
            'message'=> $validator->errors()->first(),
            'data'=> null
        ]));
    }
    public function messages()
    {
        $config  = Config::first();
        return [
            "file.max"=>'Maximum file size to upload is' .$config->video_size.'MB'.  ($config->video_size*1024 .'KB'),
        ];
    }
}
