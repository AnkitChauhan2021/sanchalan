<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Config;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        $image_upload_limit = Config::first();
        $limit = $image_upload_limit->image_limit;
        return [
            'image'=>'max:'.$limit,
            'description'=>'required',
            'videos' =>'nullable',

        ];
    }
    public function messages(){
        $image_upload_limit = Config::first();
        $limit = $image_upload_limit->image_limit;
        return [
            "image.required"=>' image Required!',
            "image.max"=> $limit.'-images are allowed',
            "description.required"=>' Field Required!'
        ];
    }
}
