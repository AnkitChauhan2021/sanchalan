<?php

namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected function failedValidation(Validator $validator): void
    {
        $errors = $validator->errors();

        throw new HttpResponseException(response()->json([
            'status_code'=> 0,
            'message'=> $errors->first(),
            'data'=> null
        ], Response::HTTP_OK));
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'gender'=>'nullable',
            'organization_id'=>'nullable|exists:companies,id',
            'level_id'=>'nullable|exists:levels,id',
//            'country_id'=>'nullable|exists:countries,id',
            'state_id'=>'required|exists:states,id',
            'district_id'=>'required|exists:districts,id',
            'city_id'=>'required|exists:cities,id',
            'mobile'=>'required|numeric|unique:users',
            'designation'=>'nullable|string',
            'email'=>'nullable|string|email|max:255|unique:users',
            'password' => 'required|string|min:4|confirmed',


        ];
    }
    public function messages()
    {
        return [
             'first_name.required'=>'First Name Field Is Required',
             'last_name.required'=>'First Last Field Is Required',
             'gender.required'=>'Gender Field Is Required',
             'mobile.required'=>'Mobile Field Is Required',
        ];
    }
}
