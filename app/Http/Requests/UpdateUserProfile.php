<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected function failedValidation(Validator $validator): void
    {
        $errors = $validator->errors();

        throw new HttpResponseException(response()->json([
            'status_code'=> 0,
            'message'=> $errors->first(),
            'data'=> null
        ], Response::HTTP_OK));
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'=>'nullable|exists:countries,id',
            'level_id'=>'nullable|exists:levels,id',
            'company_id'=>'nullable|exists:companies,id',
            'state_id'=>'nullable|exists:states,id',
            'district_id'=>'nullable|exists:districts,id',
            'city_id'=>'nullable|exists:cities,id',
            "first_name"=>'nullable|string',
            "last_name"=>'nullable|string',
            'designation'=>'nullable|string',
            'gender'=>'nullable|string',
            'image'=>'nullable|image',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore(Auth::user()->id, 'id')
            ],
            'mobile' => [
                'required',
                Rule::unique('users')->where(function ($query)  {
                    $query->where('id','<>', Auth::user()->id);
                })
            ],

        ];
    }
}
