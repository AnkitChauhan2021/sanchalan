<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class UserProfileUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'=>'nullable|exists:countries,id',
            'state_id'=>'nullable|exists:states,id',
            'district_id'=>'nullable|exists:districts,id',
            'city_id'=>'nullable|exists:cities,id',
            "first_name"=>'required|string',
            "last_name"=>'required|string',
            "about_us"=>'nullable|string|max:150',
            'gender'=>'nullable|string',


        ];
    }
}
