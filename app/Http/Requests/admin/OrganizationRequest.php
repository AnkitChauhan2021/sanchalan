<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class OrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'org_name'=>'required|string',
            'country_id'=>'nullable|exists:countries,id',
            'district_id'=>'nullable|exists:districts,id',
            'city_id'=>'nullable|exists:cities,id',
            'status'=>'required',
            'address'=>'nullable|string',
            'postal_code'=>'nullable|regex:/\b\d{6}\b/',
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'email'=>'required|string|email|max:255|unique:users',
            'mobile'=>'required|numeric|min:10|unique:users',
            'designation'=>'nullable|string',
            'password'=>'required|min:8|string',
            'companydesignations'=>'required',

        ];
    }
    public function messages()
    {
        return [
                'org_name.required'=>'Please Fill Organization Name',
                'state_id.required'=>'Please Select State Of Organization ',
                'district_id.required'=>'Please Select District of Organization',
                'city_id.required'=>'Please Select City Of Organization',
                'status.required'=>'Please Select status Of Organization',
                'companydesignations.required'=>"Please Select Designations For Organization",
                'first_name.required'=>'This First Name Field Is Required!',
                'last_name.required'=>'This Last Name Field Is Required!',
                'desination.required'=>'Please Select Desination!',

        ];

    }
}
