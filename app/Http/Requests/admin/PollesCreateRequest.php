<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class PollesCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question'=>'required|string',
            'status'=>'required',
            'Answer1'=>'required|string',
            'Answer2'=>'required|string',
            'Answer3'=>'required|string',
            'Answer4'=>'required|string',

        ];
    }
    public function messages()
    {
        return [
            'question.required'=>'question Field Is Required',
            'Answer1.required'=>'Answer A Field Is Required',
            'Answer2.required'=>'Answer B Field Is Required',
            'Answer3.required'=>'Answer C Field Is Required',
            'Answer4.required'=>'Answer D Field Is Required',
        ];
    }
}
