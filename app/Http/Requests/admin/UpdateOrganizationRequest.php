<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'org_name'=>'required|string',
            //  'company_email'=>'nullable|string|email|max:255|unique:companies',
            'state_id'=>'nullable|exists:states,id',
            'district_id'=>'nullable|exists:districts,id',
            'city_id'=>'nullable|exists:cities,id',
            'status'=>'required',
            'address'=>'nullable|string',
            //'postal_code'=>'nullable|max:7|min:5|numeric',
//            'first_name'=>'required|string',
//            'last_name'=>'required|string',
           // 'email'=>'required|string|email|max:255|unique:users',
           // 'mobile'=>'required|numeric|min:10|unique:users',
//            'mobile' => [
//                'nullable',
//                Rule::unique('users')->where(function ($query)  {
//                    $query->where('id','<>', $this->id);
//                })
//            ],
//            'email' => [
//                'nullable',
//                Rule::unique('users')->where(function ($query)  {
//                    $query->where('id','<>', $this->id);
//                })
//            ],


        ];


    }
}
