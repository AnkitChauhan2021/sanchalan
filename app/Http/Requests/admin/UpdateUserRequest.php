<?php

namespace App\Http\Requests\admin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'gender'=>'nullable',
//            'role_id'=>'required',
            'organization_id'=>'nullable||exists:companies,id',
            'level'=>'nullable||exists:levels,id',
            'designation'=>'nullable|string',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($this->id, 'id')
            ],
            'mobile' => [
                'required',
                Rule::unique('users')->where(function ($query)  {
                    $query->where('id','<>', $this->id);
                })
            ],


        ];
    }
    public function messages()
    {
        return [
             'first_name.required'=>'First Name Field Is Required',
             'last_name.required'=>'First Last Field Is Required',
             'gender.required'=>'Gender Field Is Required',
             'mobile.required'=>'Mobile Field Is Required',
        ];
    }
}
