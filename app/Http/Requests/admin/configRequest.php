<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class configRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image_limit'=>'required',
            'video_size'=>'required',
            'trending_post'=>'required',
            'app_name'=>'required',
            'contact_no'=>'required',
            'email'=>'required|email',
            'title'=>'required',
            'subtitle'=>'required',
            'address'=>'required',
        ];
    }
}
