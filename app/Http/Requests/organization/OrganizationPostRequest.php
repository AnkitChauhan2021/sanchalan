<?php

namespace App\Http\Requests\organization;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Config;

class OrganizationPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $image_upload_limit = Config::first();
        $limit = $image_upload_limit->image_limit;
        return [
            'images'=>'nullable|max:'.$limit,
            'editor1'=>'required',
            'videos' =>'nullable',

        ];
    }
    public function messages(){
        $image_upload_limit = Config::first();
        $limit = $image_upload_limit->image_limit;
        return [
            "images.max"=> $limit.'-images are allowed',
            "editor1.required"=>'Description Field Required!'
        ];
    }
}
