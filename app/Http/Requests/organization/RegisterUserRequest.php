<?php

namespace App\Http\Requests\organization;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'gender'=>'nullable',
//            'role_id'=>'required',
            'organization_id'=>'nullable||exists:companies,id',
            'level'=>'nullable||exists:levels,id',
            'mobile'=>'required|numeric|unique:users',
            'designation'=>'nullable|string',
            'email'=>'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|',
            'password_confirmation' => 'required|min:8|same:password',


        ];
    }
    public function messages()
    {
        return [
             'first_name.required'=>'First Name Field Is Required',
             'last_name.required'=>'First Last Field Is Required',
             'gender.required'=>'Gender Field Is Required',
             'mobile.required'=>'Mobile Field Is Required',
        ];
    }
}
