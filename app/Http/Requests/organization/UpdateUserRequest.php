<?php

namespace App\Http\Requests\organization;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name"=>'nullable|string',
            "last_name"=>'nullable|string',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($this->user->id, 'id')
            ],
            'mobile' => [
                'nullable',
                Rule::unique('users')->where(function ($query)  {
                    $query->where('id','<>', $this->id);
                })
            ],
            'designation'=>'nullable|string',
            'organization_id'=>'nullable||exists:companies,id',
            'level_id'=>'nullable||exists:levels,id',
        ];
    }
}
