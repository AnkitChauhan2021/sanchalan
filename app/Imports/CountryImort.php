<?php

namespace App\Imports;


use App\Models\Country;
use Maatwebsite\Excel\Concerns\ToModel;

class CountryImort implements ToModel
{
    /**
    * @param array $row
    *
    * @return Country
     */
    public function model(array $row)
    {
        dd($row);
        return new Country([
             "name" => $row[0],
             "status" => $row[1],
        ]);
    }
}
