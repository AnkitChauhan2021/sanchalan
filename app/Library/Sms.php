<?php


namespace App\Library;


use Illuminate\Support\Arr;

class Sms
{

    protected $mobile, $url, $params = [], $message;

    public function __construct($mobile, $message = null)
    {
        $this->mobile = Arr::wrap($mobile);
        $this->message = $message;
        $this->url = "https://api.textlocal.in/send/";

        $this->params = [
            "key" => 'NmI3NDU1Mzk3NzcyNmE0YjU4NjQ1Nzc0NTY0ODQzNjE=',
            'senderID' => 'snvaad'

        ];
    }

    public function send()
    {
        // Account details
        $apiKey = urlencode($this->params['key']);
        // Message details
        $numbers = $this->mobile;
        $sender = urlencode($this->params['senderID']);
        $message = rawurlencode($this->message);

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
