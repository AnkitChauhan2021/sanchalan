<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{

    const PENDING = 0;
    const ACCEPT = 1;
    const REJECT = 2;

    protected $fillable = ['id', 'sender_id', 'receiver_id', 'status'];

    public function sendChatRequest()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id')->select('id', 'first_name', 'last_name', 'profile_pic');
    }

    public function receiveChatRequest()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id')->select('id', 'first_name', 'last_name', 'profile_pic');
    }

}
