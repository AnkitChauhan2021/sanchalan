<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class ChatGroup extends Model
{
    use HasFactory;

    const GROUP_ICON_PATH = "groups/icons/";

    protected $fillable = ['user_id', 'name', 'icon', 'status', 'type', 'is_super_admin'];

    const INDIVIDUAL = "Individual";
    const GROUP = "Group";

    const ACTIVE = 1;

    public function Members()
    {
        return $this->hasMany(UserChatGroup::class, 'chat_group_id', 'id');
    }

    public function userChatGroup()
    {
        return $this->hasMany(UserChatGroup::class);
    }


    public function getIconUrlAttribute()
    {
        if (!empty($this->attributes['icon'])) {
            if (file_exists(public_path(self::GROUP_ICON_PATH . $this->attributes['icon']))) {
                return asset(self::GROUP_ICON_PATH . $this->attributes['icon']);
            }
        }

        return asset('admin/profile/default/avatar.png');
    }
    public function getThumbnailIconUrlAttribute()
    {
        if (!empty($this->attributes['icon'])) {
            if (file_exists(public_path(self::GROUP_ICON_PATH .'/thumb/'. $this->attributes['icon']))) {
                return asset(self::GROUP_ICON_PATH .'/thumb/'. $this->attributes['icon']);
            }
        }

        return asset('admin/profile/default/avatar.png');
    }

    /**
     * Get Private Conversation between two users.
     *
     * @param int | User $userOne
     * @param int | User $userTwo
     *
     * @return Conversation
     */
    public function between($userOne, $userTwo)
    {
        $conversation1 = self::userConversations($userOne)->toArray();
        $conversation2 = self::userConversations($userTwo)->toArray();

        $common_conversations = $this->getConversationsInCommon($conversation1, $conversation2);

        if (!$common_conversations) {
            return;
        }

        return $this->findOrFail($common_conversations[0]);
    }

    /**
     * Gets conversations for a specific user.
     *
     * @param User | int $user
     *
     * @return array
     */
    public function userConversations($user)
    {
        $userId = is_object($user) ? $user->id : $user;

        return ChatGroup::where("type", Self::INDIVIDUAL)
            ->whereHas("userChatGroup", function ($query) use ($userId) {
                $query->where("user_id", $userId);
            })->pluck("id");
    }

    /**
     * Gets the conversations in common.
     *
     * @param array $conversation1 The conversations for user one
     * @param array $conversation2 The conversations for user two
     *
     * @return Conversation The conversations in common.
     */
    private function getConversationsInCommon($conversation1, $conversation2)
    {
        return array_values(array_intersect($conversation1, $conversation2));
    }

    public function receiver()
    {
        return $this->hasOne(UserChatGroup::class,"chat_group_id", "id")->where('user_id', '<>', auth()->user()->id);
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class)->latest()->with('sender');
    }

    public function unSeenMessages()
    {
        return $this->hasMany(MessageReceiver::class, 'chat_group_id', 'id')
            ->where('user_id', auth()->user()->id)
            ->where('is_read', 0);
    }


}
