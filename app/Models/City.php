<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $fillable = ['name','district_id','state_id','status'];
    public function district(){
        return $this->belongsTo('App\Models\District');
    }
    public function users(){
        return $this->hasMany(User::class)->where(['isVerified'=>1,'status'=>1]);
    }
}
