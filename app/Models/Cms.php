<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Cms extends Model
{

    use HasFactory;
    const ICON_PATH ="/icons/";

    protected $fillable = ['title','slug','content','meta_keyword','meta_desc','url','status','icon'];
    protected $appends =['icon_url'];
    public static function getAllCmsUrl()
    {
//        return Cache::rememberForever('get-all-cms-url', function () {
            $all_cms = Cms::where('status',1)->get();

            return $all_cms;
//        });
    }
    public function getIconUrlAttribute()
    {



        if ($this->icon) {
            if (file_exists(public_path(self::ICON_PATH.$this->icon))) {
                return asset(self::ICON_PATH.$this->icon);
            }
        }
        return "";

    }
}
