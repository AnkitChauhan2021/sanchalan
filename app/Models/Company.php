<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model
{
    protected $fillable = ['name', 'name_in_hindi', 'state_id', 'district_id', 'city_id', 'email', 'address', 'postal_code', 'status', 'likeToJoin', 'permission_to_make_manch_org', 'sort_name'];
    use HasFactory;
    protected $appends = ['permissions'];


    protected $hidden = [


    ];
    const PERMISSION_TO_MAKE_MANCH_ORN = 1;

    public function getIsPostExistAfterVisitAttribute()
    {
        // get all users
        if (Auth::check()) {
            // check company Wall
            $companyWallVisits = CompanyWallVisit::where(['company_id' => $this->id, 'user_id' => Auth::user()->id])->first();
                 if($companyWallVisits){

                     $posts = Post::where('company_id', $this->id)->where('created_at', '>', $companyWallVisits->last_visit_at)->where('type','admin')->count();
                        if($posts > 0){
                            return true;
                        }else {
                            return false;
                        }
                 }else{
                     return true;
                 }

        } else {
            return false;
        }

    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }


    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function designations()
    {

        return $this->hasMany(CompanyDesignation::class)->select('id', 'company_id', 'desgination_id');
    }
//    public function users{
//        return $this->be
//    }

    public function getPermissionsAttribute()
    {

        if (Auth::check()) {

            $userPermission = UserPermission::where(['company_id' => $this->id, 'user_id' => Auth::user()->id])->pluck('permission_id')->toArray();

            $permission = Permission::whereIn('id', $userPermission)->select('id', 'name', 'slug')->get();

            if (count($permission) > 0) {
                return $permission;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    public function getIsApproveAttribute()
    {
        if (Auth::check()) {
            $userCompany = UserCompany::where(['user_id' => Auth::user()->id, 'company_id' => $this->id])->first();
            if($userCompany->isApproved){
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }

//    public function userCompany()
//    {
//        return $this->hasMany(UserCompany::class, 'company_id', 'id');
//    }


}

