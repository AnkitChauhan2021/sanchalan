<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDesignation extends Model
{
    use HasFactory;

    protected $fillable = ['company_id','desgination_id','slug'];

    public function levels(){
        return $this->belongsTo(Level::class,'desgination_id','id');
    }
}
