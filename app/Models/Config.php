<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
	 protected $fillable = ['image_limit','trending_post','video_size','app_name','contact_no','email','map_url','title','subtitle','address'];
    use HasFactory;
}
