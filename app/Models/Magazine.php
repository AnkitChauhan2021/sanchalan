<?php

namespace App\Models;

use App\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Magazine extends Model
{
    use HasFactory;
    protected $appends = ['is_like','is_magazine','company_approved'];
   const Active =1;
   const Inactive =2;
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function users(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function images(){
        return $this->hasMany(MagazineImage::class,);
    }
    public function videos(){
        return $this->hasMany(MagazineVideo::class);
    }
    public function likes(){
        return $this->hasMany(MagazineLike::class);
    }
    public function comments(){
        return $this->hasMany(MagazineComment::class);
    }


    public function getCompanyApprovedAttribute(){
        $WantTOJoin=  CommonHelper::WantTOJoin();

        foreach($WantTOJoin as $userId){
            if($userId== $this->user_id)
            {
                return 1;

            }else{
                return 0;
            }
        }
    }
    public function getIsLikeAttribute()
    {
        $isLike = MagazineLike::where(['magazine_id'=>$this->id,'user_id'=>Auth::user()->id])->get();
        if(count($isLike)>0){
            return true;
        }else{
            return false;
        }
    }
    public function getIsMagazineAttribute()
    {

            return true;

    }

}
