<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagazineComment extends Model
{
    use HasFactory;

    public function CommentBy(){
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name','profile_pic');
    }
}
