<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagazineImage extends Model
{
    use HasFactory;

    protected $fillable = ['id','magazine_id','image'];
    protected $appends = ['url'];
    const Magazine_IMAGE_PATH = 'magazine/images';

    public function getUrlAttribute(){
        $image = $this->image;
        if($image){
            if(file_exists(public_path(self::Magazine_IMAGE_PATH.'/'. $this->image))){
                return asset(self::Magazine_IMAGE_PATH.'/'. $this->image);
            }
        }else{
            return "";
        }
    }
}
