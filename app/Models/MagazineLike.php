<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MagazineLike extends Model
{
    use HasFactory;
    public function likeBy(){
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name','profile_pic');
    }
    public function users(){
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name','profile_pic');
    }
}
