<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MagazineVideo extends Model
{
    use HasFactory;
    protected $appends = ['url'];
    const Magazine_VIDEOS_PATH = 'magazine/Videos';


    public function getUrlAttribute(){
        $image = $this->video;
        if($image){
            if(file_exists(public_path(self::Magazine_VIDEOS_PATH.'/'. $this->video))){
                return asset(self::Magazine_VIDEOS_PATH.'/'. $this->video);
            }
        }else{
            return "";
        }
    }
}
