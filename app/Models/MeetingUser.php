<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeetingUser extends Model
{
    use HasFactory;

    public function meeting(){
        return $this->belongsTo(Meeting::class,'meeting_id','id');
    }
    public function organization(){
        return $this->belongsTo(Company::class,'company_id','id');

    }

}
