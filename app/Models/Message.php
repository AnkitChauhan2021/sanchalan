<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    const TYPE_TEXT = "text";
    const TYPE_IMAGE = "image";
    const TYPE_VIDEO = "video";
    const TYPE_FILE = "file";

    const UPLOAD_PATH = "files/message/";

    protected $fillable = ["user_id", "chat_group_id", "message", "type", "file_name","file_extension","file", "thumbnail"];

    public function messageReceivers(){
        return $this->hasMany(MessageReceiver::class);
    }


    public function delivered(){
        return $this->hasMany(MessageReceiver::class)
            ->where("user_id", "<>", auth()->user()->id)
            ->where("is_read", 1);
    }

    public function sender(){
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function chatGroup(){
        return $this->belongsTo(ChatGroup::class);
    }

    public function getThumbnailUrlAttribute()
    {
        if (!empty($this->attributes['thumbnail'])) {
            if (file_exists(public_path($this->attributes['thumbnail']))) {
                return asset($this->attributes['thumbnail']);
            }
        }

        return "";
    }

    public function getFileUrlAttribute()
    {
        if (!empty($this->attributes['file'])) {
            if (file_exists(public_path($this->attributes['file'] ))) {
                return asset($this->attributes['file']);
            }
        }

        return "";
    }


}
