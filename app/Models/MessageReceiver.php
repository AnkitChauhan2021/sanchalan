<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageReceiver extends Model
{

    protected $fillable = ["message_id", "chat_group_id", "user_id", "is_read", "read_at", "is_sender"];

    public function user(){
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function chatGroup(){
        return $this->belongsTo(ChatGroup::class);
    }
}
