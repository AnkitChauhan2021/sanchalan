<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{

    protected $table ='o_t_p_s';
    protected $fillable= ['mobile','otp','expired_at'];
    use HasFactory;
}
