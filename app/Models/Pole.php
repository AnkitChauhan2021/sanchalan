<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pole extends Model
{
    use HasFactory;
    protected $appends = ['is_Poll','total_answer','answer_one','answer_two','answer_three','answer_four'];

    protected $fillable = ['user_id','company_id', 'question', 'answer1', 'answer2', 'answer3', 'answer4', 'answer5', 'answer_count1', 'answer_count1', 'answer_count2', 'answer_count3', 'answer_count4', 'answer_count5', 'status', 'isShow'];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getIsPollAttribute(){
        return true;
    }
    public function getTotalAnswerAttribute(){
        $userPole = UserPole::where('pole_id',$this->id)->count();
        if(($userPole)>0){
            return $userPole;
        }else{
            return 0;
        }
    }
    public function getAnswerOneAttribute(){
        $answer1 = UserPole::where(['pole_id'=>$this->id,'answer'=>"answer1"])->count();
        if(($answer1)>0){
            return $answer1;
        }else{
            return 0;
        }
    }
    public function getAnswerTwoAttribute(){
        $answer1 = UserPole::where(['pole_id'=>$this->id,'answer'=>"answer2"])->count();
        if(($answer1)>0){
            return $answer1;
        }else{
            return 0;
        }
    }
    public function getAnswerThreeAttribute(){
        $answer1 = UserPole::where(['pole_id'=>$this->id,'answer'=>"answer3"])->count();
        if(($answer1)>0){
            return $answer1;
        }else{
            return 0;
        }
    }
    public function getAnswerFourAttribute(){
        $answer1 = UserPole::where(['pole_id'=>$this->id,'answer'=>"answer4"])->count();
        if(($answer1)>0){
            return $answer1;
        }else{
            return 0;
        }
    }
}
