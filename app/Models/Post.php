<?php

namespace App\Models;

use App\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Post extends Model
{
    protected $appends = ['is_like', 'is_Post', 'is_report', 'company_approved', "post_created_at"];

    const  Active = 1;
    const  Inactive = 0;

    use HasFactory;

    protected $fillable = ['company_id', 'user_id', 'description', 'slug', 'like_count', 'comment_count', 'type', 'status'];


    public function getIsLikeAttribute()
    {
        $isLike = PostLike::where(['post_id' => $this->id, 'user_id' => Auth::user()->id])->get();
        if (count($isLike) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getIsReportAttribute()
    {
        $isReport = Report::where(['post_id' => $this->id, 'user_id' => Auth::user()->id])->count();
        if ($isReport > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getPostCreatedAtAttribute()
    {
        if (!empty($this->attributes['created_at'])) {
            return @$this->created_at->diffForHumans();
        } else {
            return "";
        }
    }

    public function getCompanyApprovedAttribute()
    {
        $WantTOJoin = CommonHelper::WantTOJoin();

        foreach ($WantTOJoin as $userId) {
            if ($userId == $this->user_id) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function getIsPostAttribute()
    {
        return true;
    }

    public function getPostImage()
    {
        return $this->hasMany('App\Models\PostImage')->select('id', 'post_id', 'image');

    }

    public function report()
    {
        return $this->hasMany('App\Models\Report')->select('id', 'post_id', 'user_id');

    }

    public function getPostVideo()
    {
        return $this->hasMany('App\Models\PostVideo')->select('id', 'post_id', 'video','thumbnail');

    }

    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->select('id', 'first_name', 'last_name', 'profile_pic',);
    }

    public function postBy()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->select('id', 'first_name', 'last_name', 'profile_pic');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->select('id', 'first_name', 'last_name', 'profile_pic');
    }

    public function getCompany()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id')->select('id', 'name');
    }

    public function comments()
    {
        return $this->hasMany(PostComment::class);
    }

    public function likes()
    {
        return $this->hasMany(PostLike::class)->select('id', 'user_id', 'post_id');
    }

    public function replies()
    {
        return $this->hasMany(PostComment::class, 'parent_id',);
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'post_id', 'id');
    }


}
