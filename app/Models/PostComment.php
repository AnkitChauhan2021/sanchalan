<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    use HasFactory;
    protected $fillable  = ['user_id', 'post_id','parent_id','comment','status'];

    public function commentBy(){
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name','email','mobile','profile_pic');
    }
    public function userCompany(){
    	return $this->belongsTo(UserCompany::class,'user_id','id');
    }
}
