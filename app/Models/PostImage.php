<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{

    protected $fillable = ["post_id", "image"];

    protected $appends = ['post_image','post_image_thumbnail'];

    const POST_IMAGE_PATH = 'files/post/images/';

    public function getPostImageAttribute(){

        if (!empty($this->attributes['image'])) {

            $dir = self::POST_IMAGE_PATH. $this->attributes['post_id'] . "/";

            if (file_exists(public_path($dir .$this->attributes['image']))) {
                return asset($dir . $this->attributes['image']);
            }
        }
        return "";
    }

    public function getPostImageThumbnailAttribute(){
        if (!empty($this->attributes['image'])) {
            $dir = self::POST_IMAGE_PATH. $this->attributes['post_id'] . "/thumb/";
            if (file_exists(public_path($dir .$this->attributes['image']))) {
                return asset($dir . $this->attributes['image']);
            }
        }
        return "";
    }
}
