<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostVideo extends Model
{

    protected $fillable = ["post_id", "video", "thumbnail"];

    protected $appends = ['post_video',"post_video_thumbnail"];

    const POST_VIDEO_PATH = 'files/post/videos/';

    public function getPostVideoAttribute()
    {
        if (!empty($this->attributes['video'])) {
            if (file_exists(public_path($this->attributes['video']))) {
                return asset($this->attributes['video']);
            }
        }
        return "";
    }

    public function getPostVideoThumbnailAttribute()
    {
        if (!empty($this->attributes['thumbnail'])) {
            if (file_exists(public_path($this->attributes['thumbnail']))) {
                return asset($this->attributes['thumbnail']);
            }
        }
        return "";
    }
}
