<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportNotification extends Model
{
    protected $fillable = ['user_id','post_id'];
    use HasFactory;

    public function Post(){
        return $this->belongsTo(Post::class);
    }
    public function User(){
        return $this->belongsTo(User::class);
    }
    public function Report(){
        return $this->belongsTo(Report::class);
    }
}
