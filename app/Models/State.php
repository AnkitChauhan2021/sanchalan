<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'status'];

    public function Country()
    {
        return $this->belongsTo(Country::class);
    }
    public function users(){
//        return $users;

//        return User::where(['state_id'=>$this->id,'isVerified'=>1,'status'=>1]);
        return $this->hasMany(User::class)->where(['isVerified'=>1,'status'=>1]);
    }
    public function district(){
        return $this->hasMany(District::class,'state_id','id');
    }
}
