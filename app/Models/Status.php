<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $fillable =['user_id','image'];

    protected $appends = ['url','thumbnail_url'];

    const STATUS_IMAGE_PATH ="/status/images/";
    const STATUS_IMAGE_THUMBNAIL_PATH ="/status/thumb/";

    public  function getUrlAttribute(){

        if($this->image){
            if (file_exists(public_path(self::STATUS_IMAGE_PATH . $this->image))) {
                return asset(self::STATUS_IMAGE_PATH . $this->image);
            }
        }else{
            return null;
        }
    }
    public  function getThumbnailUrlAttribute(){
        if($this->image){
            if (file_exists(public_path(self::STATUS_IMAGE_THUMBNAIL_PATH . $this->image))) {
                return asset(self::STATUS_IMAGE_THUMBNAIL_PATH . $this->image);
            }
        }else{
            return null;
        }
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name');
    }

}
