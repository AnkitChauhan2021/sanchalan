<?php

namespace App\Models;

use App\Notifications\SendResetPasswordLink;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;


/**
 * Class User
 * @package App\Models
 * @property Role role_id
 * @property string first_name
 * @property string last_name
 * @property Company company_id
 * @property string email
 * @property string password
 * @property string profile_pic;
 * @property string Image;
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $appends = ['logo', 'thumbnail_url','full_name','is_chat_request', 'is_follow', 'join_company', 'admin_company_id','level_position'];

    const PROFILE_PIC_PATH = "admin/profile/";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const SuperAdmin = 1;
    const Admin = 3;
    const Iscompany = 1;
    const UserRole = 2;
    protected $fillable = [
        'role_id',
        'company_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'profile_pic',
        'isVerified',
        'mobile',
        'designation',
        'country_id',
        'level_id',
        'isApproved',
        'notification',
        'about_us'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


// user  profile image
    public function getLogoAttribute()
    {
        if (!empty($this->attributes['profile_pic'])) {

            $dir = self::PROFILE_PIC_PATH. $this->attributes['id'] . "/";

            if (file_exists(public_path($dir .$this->attributes['profile_pic']))) {
                return asset($dir . $this->attributes['profile_pic']);
            }
        }

        return asset('public/admin/profile/default/avatar.png');

    }
    public function getThumbnailUrlAttribute()
    {
        if (!empty($this->attributes['profile_pic'])) {

            $dir = self::PROFILE_PIC_PATH. $this->attributes['id'] . "/thumb/";

            if (file_exists(public_path($dir .$this->attributes['profile_pic']))) {
                return asset($dir . $this->attributes['profile_pic']);
            }
        }
        return asset('public/admin/profile/default/avatar.png');

    }


    public function followers()
    {
        return $this->hasMany(UserFollower::class, 'following_id', 'id');
    }

    public function followings()
    {
        return $this->hasMany(UserFollower::class, 'follower_id', 'id');
    }


    public function getIsChatRequestAttribute()
    {
             if(Auth::check())
             {
                 $chat = Chat::Where(['sender_id'=>Auth::user()->id,'receiver_id'=>$this->id])->count();
                 if($chat>0){
                     return true;
                 }else{
                     return false;
                 }
             }else{
                 return false;
             }
    }


    public function getIsFollowAttribute()
    {
        if (Auth::check()) {
            $follow = UserFollower::where(["following_id" => Auth::user()->id, "follower_id" => $this->id])->get();
            if (count($follow) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return "";
        }
    }

    public function getJoinCompanyAttribute()
    {
        if (Auth::check()) {
            $userCompanyLike = UserCompany::where(['user_id' => Auth::user()->id, 'is_like' => 1])->pluck('company_id')->toArray();
            $userCompany = UserCompany::where(['user_id' => Auth::user()->id, 'isApproved' => 1])->pluck('company_id')->toArray();
            $company_id = array_merge($userCompanyLike, $userCompany);
            $company_ids = array_unique($company_id);

            $companyIdsArray = [];
            foreach ($company_ids as $ids) {
                $companyIdsArray[] = $ids;
            }
            $company = Company::whereIn('id', $companyIdsArray)->select('id', 'name')->get();
            if (count($company) > 0) {
                return $company;
            } else {
                return [];
            }

        } else {
            return [];
        }

    }
   public function getLevelPositionAttribute(){
         $record= UserCompany::whereIn('user_id',array($this->id))->pluck('level_id');
            return Level::whereIn('id',$record)->pluck('position')->first();
    }


    function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function designations()
    {

        return $this->belongsTo(CompanyDesignation::class, 'user_id', 'id')->select('id', 'company_id', 'desgination_id');
    }

    public function userComapny()
    {
        return $this->hasMany(UserCompany::class)->select('id', 'user_id', 'company_id', 'level_id', 'designation','isApproved');
    }

    public function topLevelUserCompany()
    {
        return $this->hasOne(UserCompany::class)
            ->select("user_companies.id", "user_companies.user_id", "user_companies.company_id", "user_companies.level_id", "user_companies.designation")
            ->leftJoin("levels", "levels.id", "=", "user_companies.level_id")
            ->orderByRaw("IF(levels.position IS NULL , 9999999, levels.position)");
    }

    public function userPost()
    {
        return $this->hasMany(Post::class);
    }

    public function userFollowers()
    {
        return $this->belongsToMany(User::class, "user_follower", "follower_id", "following_id");
    }

    public function userFollowings()
    {
        return $this->belongsToMany(User::class, "user_follower", "following_id", "follower_id");
    }


    public function blockedPost()
    {
        return $this->hasMany(PostBlock::class);
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SendResetPasswordLink($token, $this));
    }

    public function follower()
    {

        return $this->hasMany(UserFollower::class, 'follower_id', 'id');

    }

    public function following()
    {

        return $this->hasMany(UserFollower::class, 'following_id', 'id');

    }

    public function getAdminCompanyIdAttribute()
    {
        $id = null;
        $company = UserCompany::where('user_id', $this->id)->first();
        if ($company) {
            $id = $company->company_id;
        }
        return $id;
    }

    public function userTopCompany()
    {
        return $this->hasOne(UserCompany::class, 'user_id', 'id')->select('id','company_id','user_id','level_id');
    }


}
