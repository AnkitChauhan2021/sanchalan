<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserChatGroup extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'chat_group_id', "color_code", 'group_admin', 'status'];

    const ACTIVE = 1;
    const LEFT = 0;
    const IS_ADMIN = 1;

    public function member()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function chatGroup()
    {
        return $this->belongsTo(ChatGroup::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userCompanies()
    {
        return $this->hasOne(UserCompany::class,'user_id','user_id');
    }
}
