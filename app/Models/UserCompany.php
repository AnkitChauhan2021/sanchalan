<?php

namespace App\Models;

use App\Scopes\LevelPositionScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserCompany extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','company_id','level_id','role_id','description','is_like','isApproved','likeToJoin'];




    const WantTOJoin = 1;
    const 	IfExistMember = 1;
    const Permission = 1;
    const UnPermission = 0;


    public function users(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function level(){
        return $this->belongsTo(Level::class,'level_id','id');
    }
    public function company(){
        return $this->belongsTo(Company::class,'company_id','id','name');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }


//    this relation for advance search

    public function levels(){
        return $this->belongsTo(Level::class,'level_id','id')->select('id','name','slug');
    }
    public function companies(){
        return $this->belongsTo(Company::class,'company_id','id')->select('id','name');
    }
   /* public function user(){
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name','profile_pic');
    }*/

    public function scopeWithLevelPosition($query)
    {
        return $query->addSelect(DB::raw("(SELECT position from levels WHERE levels.id = user_companies.id LIMIT 1) as level_position"));
    }

}
