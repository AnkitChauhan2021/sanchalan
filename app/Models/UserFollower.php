<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFollower extends Model
{
    use HasFactory;

    protected $table = 'user_follower';


    public function followers(){
        return $this->belongsTo(User::class,'follower_id','id')->select('id','first_name','last_name','isApproved','status','profile_pic');
    }
    public function following(){
        return $this->belongsTo(User::class,'following_id','id')->select('id','first_name','last_name','isApproved','status','profile_pic');
    }

}
