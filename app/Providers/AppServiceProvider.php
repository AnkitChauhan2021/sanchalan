<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\Country;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //date_default_timezone_set(config('app.timezone'));
        //Carbon::now()->timezone(config('app.timezone'));

        View()->composer('elements.layoutsElements.header', function ($view) {
                $organizations = Company::where('status',1)->orderBy('name')->get();
                $countries = Country::where('status',1)->get();
            view()->share('organizations', $organizations);
            view()->share('countries', $countries);
        });
        View()->composer('elements.layoutsElements.profile.index', function ($view) {
            $record = User::find(Auth::user()->id);
            view()->share('record', $record);

        });
    }
}
