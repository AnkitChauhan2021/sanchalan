<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {


        return  [
            'name'=>$this->faker->name,
            'email'=>$this->faker->unique()->safeEmail,
            'corporate_form'=>"Software Development",
            'logo'=>'https://picsum.photos/200/300',
            'cover_img'=>'https://picsum.photos/200/295',
            'lat'=>'10.1245248',
            'lng'=>'27.124578',

            'country_id'=>1,
            'postal_code'=>$this->faker->postcode,
            'status'=>'1',
        ];

    }
}
