<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_poles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pole_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->enum('answer', ['answer1', 'answer2', 'answer3','answer4','answer5']);
            $table->foreign('pole_id')->references('id')->on('poles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_poles');
    }
}
