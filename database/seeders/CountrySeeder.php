<?php

namespace Database\Seeders;


use App\Models\Country;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users =[
           [

               'name'=>'India',
               'status'=>'1',

           ],
           [

               'name'=>'USA',
               'status'=>'1',

           ],
           [

               'name'=>'Chaina',
               'status'=>'1',

           ],
           [

               'name'=>'Nepal',
               'status'=>'1',

           ],
           [

               'name'=>'UK',
               'status'=>'1',

           ],
       ];
        foreach ($users as $key => $value) {

            Country::create($value);
        }
    }
}
