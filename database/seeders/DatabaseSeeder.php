<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call([CountrySeeder::class]);
//        $this->call([StateSeeder::class]);
//        \App\Models\Company::factory(100)->create();
       $this->call([RoleSeeder::class]);
       $this->call([UserSeeder::class]);
    }
}
