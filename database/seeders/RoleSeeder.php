<?php

namespace Database\Seeders;

use App\Models\Role;

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'slug'=>'super_admin',
                'name'=>'Super Admin',
                'status'=>'1',
            ], [
                'slug'=>'admin',
                'name'=>'Admin',
                'status'=>'1',
            ], [
                'slug'=>'manager',
                'name'=>'Manager',
                'status'=>'1',
            ],

        ];
        foreach ($roles as $key => $value) {

            Role::create($value);
        }
    }
}
