<?php

namespace Database\Seeders;


use App\Models\Country;
use App\Models\State;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users =[
           [
               'country_id'=>'1',
               'name'=>'UP',
               'status'=>'1',

           ],
           [

               'country_id'=>'2',
               'name'=>'Delhi',
               'status'=>'1',


           ],
           [

               'country_id'=>'3',
               'name'=>'Hariyana',
               'status'=>'1',


           ],
           [

               'country_id'=>'4',
               'name'=>'Panjab',
               'status'=>'1',

           ],
           [

               'country_id'=>'5',
               'name'=>'Bihar',
               'status'=>'1',

           ],
       ];
        foreach ($users as $key => $value) {

            State::create($value);
        }
    }
}
