<?php

namespace Database\Seeders;


use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users =[
           [
               'role_id'=>'1',
               'first_name'=>'Yogesh',
               'last_name'=>'sharma',
               'email'=>'yogesh@braintechnosys.com',
               'mobile'=>'8193853535',
               'is_company'=>'1',
               'designation'=>'super Admin',
               'password'=> bcrypt('admin123'),
               'remember_token' => Str::random(10),
           ],
           [
               'role_id'=>'2',
               'first_name'=>'Roopchand',
               'last_name'=>'Kashyap',
               'email'=>'roopchand@braintechnosys.com',
               'mobile'=>'8923511977',
               'designation'=>'Admin',
               'password'=> bcrypt('admin123'),
               'remember_token' => Str::random(10),
           ],
           [
               'role_id'=>'3',
               'first_name'=>'Ankit',
               'last_name'=>'Chauhan',
               'email'=>'chauhanankit2021@gmail.com',
               'mobile'=>'7017465239',
               'designation'=>'Admin',
               'password'=> bcrypt('admin123'),
               'remember_token' => Str::random(10),
           ]
       ];
        foreach ($users as $key => $value) {

            User::create($value);
        }
    }
}
