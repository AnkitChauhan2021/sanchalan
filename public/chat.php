<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<html>
	<head>
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet"/>
		<style>
			.container{max-width:1170px; margin:auto;}
			img{ max-width:100%;}
			.inbox_people {
			background: #f8f8f8 none repeat scroll 0 0;
			float: left;
			overflow: hidden;
			width: 40%; border-right:1px solid #c4c4c4;
			}
			.inbox_msg {
			border: 1px solid #c4c4c4;
			clear: both;
			overflow: hidden;
			}
			.top_spac{ margin: 20px 0 0;}
			
			
			.recent_heading {float: left; width:40%;}
			.srch_bar {
			display: inline-block;
			text-align: right;
			width: 60%;
			}
			.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}
			
			.recent_heading h4 {
			color: #05728f;
			font-size: 21px;
			margin: auto;
			}
			.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
			.srch_bar .input-group-addon button {
			background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
			border: medium none;
			padding: 0;
			color: #707070;
			font-size: 18px;
			}
			.srch_bar .input-group-addon { margin: 0 0 0 -27px;}
			
			.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
			.chat_ib h5 span{ font-size:13px; float:right;}
			.chat_ib p{ font-size:14px; color:#989898; margin:auto}
			.chat_img {
			float: left;
			width: 11%;
			}
			.chat_ib {
			float: left;
			padding: 0 0 0 15px;
			width: 88%;
			}
			
			.chat_people{ overflow:hidden; clear:both;}
			.chat_list {
			border-bottom: 1px solid #c4c4c4;
			margin: 0;
			padding: 18px 16px 10px;
			}
			.inbox_chat { height: 550px; overflow-y: scroll;}
			
			.active_chat{ background:#ebebeb;}
			
			.incoming_msg_img {
			display: inline-block;
			width: 6%;
			}
			.received_msg {
			display: inline-block;
			padding: 0 0 0 10px;
			vertical-align: top;
			width: 92%;
			}
			.received_withd_msg p {
			background: #ebebeb none repeat scroll 0 0;
			border-radius: 3px;
			color: #646464;
			font-size: 14px;
			margin: 0;
			padding: 5px 10px 5px 12px;
			width: 100%;
			}
			.time_date {
			color: #747474;
			display: block;
			font-size: 12px;
			margin: 8px 0 0;
			}
			.received_withd_msg { width: 57%;}
			.mesgs {
			float: left;
			padding: 30px 15px 0 25px;
			width: 60%;
			}
			
			.sent_msg p {
			background: #05728f none repeat scroll 0 0;
			border-radius: 3px;
			font-size: 14px;
			margin: 0; color:#fff;
			padding: 5px 10px 5px 12px;
			width:100%;
			}
			.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
			.sent_msg {
			float: right;
			width: 46%;
			}
			.input_msg_write input {
			background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
			border: medium none;
			color: #4c4c4c;
			font-size: 15px;
			min-height: 48px;
			width: 100%;
			}
			
			.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
			.msg_send_btn {
			background: #05728f none repeat scroll 0 0;
			border: medium none;
			border-radius: 50%;
			color: #fff;
			cursor: pointer;
			font-size: 17px;
			height: 33px;
			position: absolute;
			right: 0;
			top: 11px;
			width: 33px;
			}
			.messaging { padding: 0 0 50px 0;}
			.msg_history {
			height: 516px;
			overflow-y: auto;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<h3 class=" text-center">Messaging</h3>
			<div class="messaging">
				<div class="inbox_msg">
					<div class="inbox_people">
						<div class="headind_srch">
							<div class="recent_heading">
								<h4>Recent</h4>
							</div>
							<div class="srch_bar">
								<div class="stylish-input-group">
									<input type="text" class="search-bar"  placeholder="Search" >
									<span class="input-group-addon">
										<button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
									</span> </div>
							</div>
						</div>
						<div class="inbox_chat">
							
						</div>
					</div>
					<div class="mesgs">
						<div class="msg_history" id="history_block">
							<!--div class="incoming_msg">
								<div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
								<div class="received_msg">
								<div class="received_withd_msg">
								<p>Test which is a new approach to have all
								solutions</p>
								<span class="time_date"> 11:01 AM    |    June 9</span></div>
								</div>
								</div>
								<div class="outgoing_msg">
								<div class="sent_msg">
								<p>Test which is a new approach to have all
								solutions</p>
								<span class="time_date"> 11:01 AM    |    June 9</span> </div>
							</div-->
						</div>
						<div class="type_msg">
							<div class="input_msg_write">
								<input type="text" id="chat_input" class="write_msg" placeholder="Type a message" />
								<button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
							</div>
						</div>
					</div>
				</div>
				
				
				<p class="text-center top_spac"> Design by <a target="_blank" href="https://www.linkedin.com/in/sunil-rajput-nattho-singh/">Sunil Rajput</a></p>
				
			</div></div>
			
			<ul id="notes"></ul>
			<div id="usersConnected"></div>
			<div id="newNote">Create a new note</div>
			<script src="https://cdn.socket.io/3.1.3/socket.io.min.js" integrity="sha384-cPwlPLvBTa3sKAgddT6krw0cJat7egBga3DJepJyrLl4Q9/5WLra3rrnMcyTyOnh" crossorigin="anonymous"></script>
	</body>
</html>
<script>

	var typing = false;
	var timeout = undefined;

	function GetUserName()
	{
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	var socket = io.connect("http://103.228.114.4:3000");


	function timeoutFunction(){
		typing = false;
		socket.emit('stopTyping');
	}

	function emitTyping(){
		if(typing == false) {
			typing = true
			socket.emit('typing');
			timeout = setTimeout(timeoutFunction, 400);
			} else {
			clearTimeout(timeout);
			timeout = setTimeout(timeoutFunction, 400);
		}
	}

	function getCaret(el) {
		if (el.selectionStart) {
			return el.selectionStart;
			} else if (document.selection) {
			el.focus();
			var r = document.selection.createRange();
			if (r == null) {
				return 0;
			}
			var re = el.createTextRange(), rc = re.duplicate();
			re.moveToBookmark(r.getBookmark());
			rc.setEndPoint('EndToStart', re);
			return rc.text.length;
		}
		return 0;
	}


	$(document).ready(function(){

		var usernameRandom = GetUserName();

		/* On user Connected */
		socket.emit('addUser', usernameRandom);

		socket.on('userJoined', (data) => {
			alert(data.username + ' joined',"info");

			$(".inbox_chat").append('<div class="chat_list active_chat" user_name="'+data.username+'">\
			<div class="chat_people">\
			<div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">\
			</div>\
			<div class="chat_ib">\
			<h5>'+data.username+'<span class="chat_date">Dec 25</span></h5>\
			<p>Test, which is a new approach to have all solutions \
			astrology under one roof.</p>\
			</div>\
			</div>\
			</div>');

			$("#usersConnected").html("Total User Connected:"+data.totalUsers);
		});

		socket.on('onlineUsers', (data) => {
			$.each(data, function( username, data ) {
				if(data.online){
					$("[user_name="+username+"] .chat_people .chat_img span").addClass("online");
					} else {
					$("[user_name="+username+"] .chat_people .chat_img span").removeClass("online");
				}

			});
		});

		socket.on('login', (data) => {
			$("#usersConnected").html("Total User Connected:"+data.totalUsers);
		});

		// Whenever the server emits 'user left', log it in the chat body
		socket.on('userLeft', (data) => {
			alert(data.username + ' left',"error");
			$("#usersConnected").html("Total User Connected:"+data.totalUsers);
		});


		// Whenever the server emits 'typing', show the typing message
		socket.on('typing', (data) => {
			if(data.username == usernameRandom ){
				$("[user_name="+data.username+"] .chat_people .chat_ib").append("<span class='typing'>Typing...</span>");
			}
		});

		// Whenever the server emits 'stop typing', kill the typing message
		socket.on('stopTyping', (data) => {
			$("[user_name="+data.username+"] .chat_people .chat_ib span.typing").remove();
		});

		socket.on('disconnect', () => {
			alert('you have been disconnected',"error");
		});

		socket.on('reconnect', () => {
			alert('you have been reconnected',"success");
			if (username) {
				socket.emit('addUser', username);
			}
		});

		socket.on('reconnect_error', () => {
			alert('attempt to reconnect has failed',"error");
		});


		$('#chat_input').on('keyup', function (e) {
			if (e.keyCode === 13) {
				var content = this.value;
				var caret = getCaret(this);
				if(event.shiftKey){
					this.value = content.substring(0, caret - 1) + "\n" + content.substring(caret, content.length);
					event.stopPropagation();
					} else {
					var message = $('#chat_input').val().trim();
					if(message == ''){
						alert("Please enter message");
						return false;
					}
					$('#chat_input').val('');
					const messageData={
						chat_group_id:1,
						message_type:'text',
						message:message,
						sender_id:42
					}
					socket.emit('newMessage', messageData);
				}
				} else {
				emitTyping();
			}

		});

		$('.msg_send_btn').on("click",function(){
			var message = $('#chat_input').val().trim();
			if(message == ''){
				alert("Please enter message");
				return false;
			}
			$('#chat_input').val('');
			const messageData={
				chat_group_id:1,
				message_type:'text',
				message:message,
				sender_id:42
			}
			socket.emit('newMessage', messageData);

		});

		socket.on('receiveMessage-1', function(data){

			if(data.sender_id == 42){
				$('#history_block').append('<div class="outgoing_msg">'+
				'<div class="sent_msg">'+
				'<p>'+data.message+'</p>'+
				'<span class="time_date"></span>'+
				'</div>'+
				'</div>')
				} else {
				$('#history_block').append('<div class="incoming_msg">'+
				'<div class="incoming_msg_img">'+
				'<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">'+
				'</div>'+
				'<div class="received_msg">'+
				'<div class="received_withd_msg">'+
				'<p>'+data.message+'</p>'+
				'<span class="time_date"></span>'+
				'</div>'+
				'</div>'+
				'</div>'); // Append the new message received
			}
			$("#history_block").animate({scrollTop: $('#history_block').prop("scrollHeight")}, 1000); // Scroll the
		})

	});

</script>
