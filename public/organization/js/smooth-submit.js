$(document).on('submit', 'form.smooth-submit', get_response).on('click', 'a.smooth-submit', get_response);


function get_response() {
    var confirm_txt = $(this).attr('data-confirm') || '';
    element = this;

    if (confirm_txt.length > 0) {
        bootbox.confirm(confirm_txt, function (result) {
            if (result == true) {
                send(element);
            }
        });
    } else {
        send(this);
    }
    return false;
}

function send(element) {

    var data = new FormData(element) || {};
    var dataType = $(element).attr('data-type') || 'json';
    var url = $(element).attr('action') || $(element).attr('href');
    var type = $(element).attr('method') || 'get';

    $.ajax({
        url: url,
        type: type,
        dataType: dataType,
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#loading-box').modal('show');
        },
        success: function (data) {
            $('#loading-box').modal('hide');
            $(element).trigger('aftersubmit', [data]);
        },
        error: function (error) {
            console.log(error);
        }
    });
}