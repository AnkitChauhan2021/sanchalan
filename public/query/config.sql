ALTER TABLE `configs` ADD `trending_post` BIGINT NOT NULL DEFAULT '10' AFTER `image_limit`;
ALTER TABLE `configs` ADD `sms_api` BOOLEAN NOT NULL AFTER `trending_post`;


ALTER TABLE `configs` ADD `video_size` BIGINT NOT NULL DEFAULT '10' AFTER `sms_api`;



ALTER TABLE `configs` ADD `logo` VARCHAR(255) NULL DEFAULT NULL AFTER `video_size`, ADD `app_name` VARCHAR(255) NULL DEFAULT NULL AFTER `logo`, ADD `email` VARCHAR(255) NULL DEFAULT NULL AFTER `app_name`, ADD `contact_no` BIGINT NULL DEFAULT NULL AFTER `email`, ADD `fav_icon` VARCHAR(255) NULL DEFAULT NULL AFTER `contact_no`, ADD `address` TEXT NULL DEFAULT NULL AFTER `fav_icon`, ADD `title` TEXT NULL DEFAULT NULL AFTER `address`, ADD `subtitle` TEXT NULL DEFAULT NULL AFTER `title`;
