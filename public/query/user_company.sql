ALTER TABLE `user_companies` ADD `is_like` BOOLEAN NULL DEFAULT FALSE AFTER `role_id`;
ALTER TABLE `user_companies` ADD `isApproved` BOOLEAN NOT NULL DEFAULT FALSE AFTER `is_like`, ADD `permission` BOOLEAN NOT NULL DEFAULT FALSE AFTER `isApproved`;
ALTER TABLE `user_companies` ADD `JustToLike` BOOLEAN NULL DEFAULT FALSE AFTER `is_like`;

ALTER TABLE `user_companies` CHANGE `permission` `permission` ENUM('all','posts','poles','magazines','none') NOT NULL DEFAULT 'none';
