ALTER TABLE `users` CHANGE `designation` `designation` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `users` ADD `isApproved` BOOLEAN NOT NULL DEFAULT FALSE AFTER `isVerified`;
ALTER TABLE `users` ADD `status` BOOLEAN NOT NULL AFTER `isApproved`;
ALTER TABLE `users` CHANGE `email` `email` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `users` ADD `notification` BOOLEAN NOT NULL DEFAULT TRUE AFTER `city_id`;
ALTER TABLE `users` ADD `about_us` TEXT NULL DEFAULT NULL AFTER `designation`;
