date 7/19/2021
ALTER TABLE `users` ADD `first_login` TINYINT(1) NOT NULL AFTER `is_company`;
date 7/6/2021
ALTER TABLE `configs` ADD `image_limit` BIGINT(20) NOT NULL DEFAULT '5' AFTER `id`;
date 6/1/2021
ALTER TABLE `post_images` ADD `type` VARCHAR(50) NULL AFTER `image`;
date 6/28/2021
ALTER TABLE `users` CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '0';