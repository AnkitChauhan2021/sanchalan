-- create Poles an show result or not


ALTER TABLE `poles` ADD `isShow` BOOLEAN NOT NULL DEFAULT FALSE AFTER `answer_count5`;
ALTER TABLE `poles` ADD `company_id` BIGINT NOT NULL AFTER `user_id`;
