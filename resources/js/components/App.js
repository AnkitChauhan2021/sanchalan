import React,{useState,useEffect} from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import axios from 'axios'
import GroupList from './GroupList';
import Chateduserlist from './Chateduserlist'
import Chat from './Chat';
import Confirmuserlist from './Confirmuserlist';
import Creategroup from './Creategroup';
import './index.css'


const App = () => {

    const [group, setGroup] = useState(false)
    const [personal, setPresonal] = useState(true)
    const [groupList, setGroupList] = useState([])

const groups= () =>{
    setGroup(true)
    setPresonal(false)
}
const personalfun = () =>{
    setGroup(false)
    setPresonal(true)
}
const userToken  =localStorage.getItem("User_access_Token")
const TokenAndID=(JSON.parse( userToken));

const data = { user_id:TokenAndID.userId, type: "Group" };


const headers = {
    'content-type': 'application/json',
    'authorization': `Bearer  ${TokenAndID.token}`
  };
useEffect(()=>{
    if(group == true){
        axios.post('http://103.228.114.4/api/v1/messages/groups', data,{headers})
        .then(({data:{data}})=>setGroupList(data));
    }
    
},[group])
console.log(groupList)
    return (
        <div>
       <h3 className=" text-center">Messaging</h3> 
       <div className="tab">
            <button className={group ? `tablinks active`:`tablinks`}  onClick={groups}>Group Chat</button>
            <button className={ personal ? `tablinks active`:`tablinks`}  onClick={personalfun}>Personal Chat</button>
        </div>
        {group ? <GroupList  groupList={groupList}/>:null }
        {personal ?<Chateduserlist /> :null }
                
                    <Chat />
            <Router>
                <Switch>
                    <Route path="confirm-user" exact>
                        <Confirmuserlist />
                    </Route >
                    <Route path="/create-group" exact>
                        <Creategroup />
                    </Route>
                </Switch>

            </Router>

        




        </div>
    )
}

export default App
