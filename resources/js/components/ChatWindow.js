import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';




const ChatWindow = ()=> {
    return (
        <div>
            <App />
        </div>
    );
}


export default ChatWindow;

if (document.getElementById('chat-root')) {
    ReactDOM.render(<ChatWindow/>, document.getElementById('chat-root'));
}
