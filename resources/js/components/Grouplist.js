import React from 'react'

const GroupList = () => {

  
 
    return (
        <>
            <div id="Paris" className="tabcontent">
                <div className="messaging">
                    <div className="inbox_msg">
                        <div className="inbox_people">
                            <div className="headind_srch">
                                <div className="chat_tab justify-content-end">
                                    <a href="#">Create Group</a>
                                </div>
                            </div>
                            <div className="inbox_chat addGroups">
                                <div className="chat_list ">
                                    <div className="chat_people">
                                        <div className="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil" /> </div>
                                        <div className="chat_ib">
                                            <h5>Sunil Rajput <span className="chat_date">Dec 25</span></h5>
                                            <p>Test, which is a new approach to have all solutions
                                                astrology under one roof.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default GroupList
