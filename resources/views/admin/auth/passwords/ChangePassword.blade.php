@extends('layouts.admin.app')
@if(Auth::check())
    @section('title',Auth::user()->first_name.' '.Auth::user()->last_name.' '. "Profile")
@endif
@section('header',"Dashboard")

@section('content')
    <div class="dashboard-content-one">

        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Change Password</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="col-xl-8 col-lg-6 col-md-8 col-sm-12 mx-lg-auto  form pl-1">
                    <form class="new-added-form m-5 " action="{{route('admin.changePassword')}}" method="Post">
                        @csrf
                        <div class="row ">
                            <div class="col-xl-8 col-lg-6 col-12 form-group">
                                <label>Current Password *</label>
                                <input type="password" placeholder="" name ="old_password" class="form-control @error('old_password') is-invalid @enderror" value="{{ old('old_password') }}" autocomplete="off">
                                @error('old_password')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>

                            <div class="col-xl-8 col-lg-6 col-12 form-group">
                                <label>New Password *</label>
                                <input type="password" placeholder="" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" autocomplete="off">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="col-xl-8 col-lg-6 col-12 form-group">
                                <label>Confirm Password *</label>
                                <input type="password" placeholder="" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror"  value="{{ old('confirm_password') }}" autocomplete="off">
                                @error('confirm_password')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Change</button>
                                <a href="{{route('admin.dashboard.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
















                </div>
        </div>
@endsection
