@extends('layouts.admin.app')
@section('title','Add Members')

@section('content')



<!-- Sidebar Area End Here -->
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">

        <ul>
            <li>
                <a href="{{route('admin.dashboard.index')}}">Home</a>
            </li>
            <li>Add Members</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Add Expense Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title">
                    <h3>Add Members</h3>
                </div>
            </div>
            <!-- <form class="new-added-form" action="{{route('admin.cms.store')}}" method="post" enctype="multipart/form-data">
                @csrf -->
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Organization</label>
                            <select class="select2  @error('organization') is-invalid @enderror " name="organization">
                                <option value="">Please Select Organization</option>
                                @foreach($organizations as $organization)
                                <option value="">{{$organization->name}}</option>
                                @endforeach
                               
                                
                            </select>
                            @error('organization')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Levels</label>
                            <select class="select2  @error('organization') is-invalid @enderror " name="organization">
                                <option value="">Please Select Organization</option>
                                @foreach($levels as  $level)
                                <option value="">{{$level->name}}</option>
                                @endforeach
                               
                                
                            </select>
                            @error('organization')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Users</label>
                            <select class="select2  @error('organization') is-invalid @enderror " name="organization">
                                <option value="">Please Select Organization</option>
                                @foreach($users as  $user)
                                <option value="">{{$user->full_name}}</option>
                                @endforeach
                               
                                
                            </select>
                            @error('organization')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                        <a href="{{route('admin.level.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>


@endsection