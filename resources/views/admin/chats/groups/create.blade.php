@extends('layouts.admin.app')
@section('title','Create Groups')

@section('content')
    <div class="dashboard-content-one">
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>

                <li>Create Groups</li>
            </ul>
        </div>

        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add Members</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('admin.groups.store')}}" method="post"
                      enctype="multipart/form-data">
                    <div class="step-one active">

                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-12 form-group">
                                <label>Organization</label>
                                <select class="select2  @error('organization') is-invalid @enderror "
                                        name="organization"
                                        onchange="sendOrganizationAndLevel(this)">
                                    <option value="">Please Select Organization</option>
                                    @foreach($organizations as $organization)
                                        <option value="{{$organization->id}}">{{$organization->name}}</option>
                                    @endforeach


                                </select>
                                @error('organization')
                                <span role="alert">
                                                <strong class="text-danger">{{ $message }}</strong>
                                                </span>
                                @enderror
                            </div>
                            <div class="col-xl-6 col-lg-6 col-12 form-group">
                                <label>Levels</label>
                                <select class="select2  @error('level') is-invalid @enderror " name="level"
                                        onchange="sendOrganizationAndLevel(this)">
                                    <option value="">Please Select Organization</option>
                                    @foreach($levels as  $level)
                                        <option value="{{$level->id}}">{{$level->name}}</option>
                                    @endforeach


                                </select>
                                @error('level')
                                <span role="alert">
                                                <strong class="text-danger">{{ $message }}</strong>
                                                </span>
                                @enderror
                            </div>

                            <div class="col-xl-12 userDetail">
                                <div class="table-responsive">
                                    <table class="table display data-table text-nowrap members-table">
                                        <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input check">
                                                    <label class="form-check-label">As Member</label>
                                                </div>
                                            </th>
                                            <th>

                                                As Admin
                                            </th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="member_list">
                                        @foreach($records as $key => $user)
                                            <tr class="AppendUsers">
                                                <td> {{$key+1}}</td>
                                                <td><img src="{{$user->logo}}" class="userImage"/></td>
                                                <td><h2>{{$user->full_name}}
                                                        <br/>
                                                        {{@$user->userTopCompany->company->name}}
                                                        <br/>
                                                        {{@$user->userTopCompany->level->name}}
                                                    </h2>
                                                </td>

                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input asMember" type="checkbox"
                                                               id="{{$key+1}}" value="{{$user->id}}" name="members[]">
                                                        <label class="form-check-label">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input asAdmin  position_{{@$user->userTopCompany->level->position}}"
                                                               name="admins[]"
                                                               id="{{$key+1}}" value="{{$user->id}}" data-id="{{@$user->userTopCompany->level->position}}">
                                                        <label class="form-check-label"></label>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="step-two">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-12 form-group">
                                <div class="upload_photo">
                                    <div class="user_photo">
                                        <input type="file" class=" image @error('profile_pic') is-invalid @enderror"
                                               name="image" class=""/>
                                        @error('profile_pic')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                        <div class="upload_img">
                                            <img class="groups_icons">
                                        </div>
                                    </div>
                                    <div class="photo_content">
                                        <h2>Upload Photo</h2>
                                        <p>Please upload the image right to use, max size 1mb</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @csrf
                            <div class="col-xl-4 col-lg-6 col-12 form-group">
                                <label> Name</label>
                                <input type="text" placeholder="" name="name"
                                       class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <input id="group_icon" name="icon" type="hidden">
                            <div class="col-12 form-group mg-t-8">.
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">
                                    Create
                                </button>
                                <a href="{{route('admin.groups.index')}}"
                                   class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="button_group">
                    <button class="btn prev" id="prev">Prev</button>
                    <button class="btn next " id="next">Next</button>
                </div>
            </div>
            {{-----------------------------------------------------Cropping model ----------------------------------}}


            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="img-container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="preview"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" id="crop">Crop</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    </div>


    @include('admin.chats.groups.script')

@endsection
