@extends('layouts.admin.app')
@section('title','Create Groups')

@section('content')


    <div class="dashboard-content-one">
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>

                <li>
                    <a href="{{route('admin.profile.index')}}"> Admin Profile</a>

                </li>

                <li>Create Groups</li>
            </ul>
        </div>

        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Create Groups</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-12 form-group">
                        <div class="upload_photo">
                            <div class="user_photo">
                                <input type="file" class=" image @error('profile_pic') is-invalid @enderror" name="image"  class="" />
                                @error('profile_pic')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="upload_img">
                                @if($record->icon)
                                    <img src="{{$record->icon}}"class="groups_icons">
                                    @else
                                        <img class="groups_icons">   
                                    @endif
                                </div>
                            </div>
                            <div class="photo_content">
                                <h2>Upload Photo</h2>
                                <p>Please upload the image right to use, max size 1mb</p>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('admin.groups.update')}}" method="post" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label> Name</label>
                            <input type="text" placeholder="" name="name" class="form-control @error('name') is-invalid @enderror" value="{{$record->name}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <input id="group_icon" name="icon" type="hidden">
                        <input  name="id" type="hidden" value="{{$record->id}}">
                        
                        <div class="col-12 form-group mg-t-8">.
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                            <a href="{{route('admin.groups.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        {{-----------------------------------------------------Cropping model ----------------------------------}}



        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                </div>
                                <div class="col-md-4">
                                    <div class="preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="crop">Crop</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.chats.groups.script')

@endsection
