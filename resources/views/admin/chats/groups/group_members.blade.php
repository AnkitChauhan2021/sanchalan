@extends('layouts.admin.app')
@section('title','Group Members')

@section('content')
    <style>
        .search {
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Group Member List Of {{$record->name}}</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Group Member List Of {{$record->name}}</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">
                        <a href="{{route('admin.groups.addmember',$record->id)}}"
                           class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                class="fas fa-plus-circle fa-2x"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Name</th>
                            <th>Is Admin</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $recordGroup)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td> {{$recordGroup->member->full_name}}</td>
                                    <td>
                                        @if($recordGroup->group_admin== true)
                                            <i class="fas fa-check text-success"
                                               title="{{$recordGroup->member->full_name}}  Admin of Group {{$record->name}}"></i>
                                        @else
                                            <i class="fas fa-times  text-danger"
                                               title="{{$recordGroup->member->full_name}}  Member of Group {{$record->name}}"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">

                                                <a class="dropdown-item action_btn confirmDelete"
                                                   data-action="{{route('admin.groups.member.remove',$recordGroup->id)}}"
                                                   href="javascript:void(0);"><i
                                                        class="fas fa-trash text-danger"></i> Delete</a>
                                            </div>
                                        </div>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                @include('admin.elements.pagination.common')
            </div>
        </div>


@endsection
