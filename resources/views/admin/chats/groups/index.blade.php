@extends('layouts.admin.app')
@section('title','All Groups')

@section('content')

    <style>
        .search {
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All Groups</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Groups</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('admin.groups.create')}}"
                           class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                class="fas fa-plus-circle fa-2x"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Group Name</th>
                            <th>Member counts</th>
{{--                            <th>Add Member</th>--}}
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td> {{$record->name}}</td>
                                    <td> {{$record->members->count()}}</td>
{{--                                    <td><a href="{{route('admin.groups.addmember',$record->id)}}" class="btn btn-success addMember"><i class="fas fa-user-plus fa-1x"></i></a></td>--}}
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">
                                                <a class="dropdown-item" href="{{route('admin.groups.edit',$record->id)}}">
                                                    <i
                                                        class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>
                                                {{--<a class="dropdown-item" href="{{route('admin.groups.members',$record->id)}}">--}}
                                                    {{--<i class="fas fa-users text-warning"></i> Members</a>--}}
                                                <a class="dropdown-item action_btn confirmDelete"
                                                   data-action="{{route('admin.groups.remove',$record->id)}}"
                                                   href="javascript:void(0);"><i
                                                        class="fas fa-trash text-danger"></i> delete</a>
                                            </div>
                                        </div>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
{{--                @include('admin.elements.pagination.common')--}}
            </div>
        </div>

@endsection
