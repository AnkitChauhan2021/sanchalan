<script>


    // step one
    $('#next').on('click', function () {
        $('.step-one').removeClass('active');
        $('.step-two').addClass('active').parent().addClass('btn-active');
    });


    // step two
    $('#prev').on('click', function () {
        $('.step-two').removeClass('active');
        $('.step-one').addClass('active').parent().removeClass('btn-active');
    });



    $('body').on('change', '.asAdmin', function () {
        position = $(this).data('id');
        if ($(this).is(':checked')) {
            const el = $('.members-table').find('.asAdmin').not('.position_' + position);
            el.each(function (index, element) {
                el.attr("disabled", true);
                // console.log(element);
            })
        } else {

            const el = $('.members-table').find('.asAdmin').not('.position_' + position);
            el.each(function (index, element) {
                el.removeAttr("disabled");
            })
        }
    });


    $('body').on('change', '.asAdmin,.asMember', function () {
        if ($(this).is(":checked")) {
            $(this).closest("tr").find("input").prop("checked", false);
            $(this).prop("checked", true);
        }
    });
    // set as admin limit 3
    let limit = 3;
    $('body').on('click', '.asAdmin', function () {
        if ($('.asAdmin:checked') >= 1) {
            $('.next_step_button').prop('disabled', false)
        }
        if ($('.asAdmin:checked').length > 3) {
            $(this).prop('checked', false);
            toastr['error']("Allowed Only Three Members As Admin")
        }
    });

    // select all if click on all members
    $('.check').on('change', function (event) {
        $('.asMember').trigger("click");
    });

    // appends new members
    function sendOrganizationAndLevel(sel) {
        let values = $('select').serialize();
        if (!values) {
            return
        }
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "{{url('admin/groups/organization/')}}?" + values,

            success: function (res) {
                if (res.status == 1) {
                    toastr['success'](res.message)
                    const {data} = res;


                    if (res.status == 1) {
                        $("#member_list").children().remove();
                        data.forEach((item, key) => {
                                // console.log(item.level.name)
                                $("#member_list").append(`
                                        <tr class="AppendUsers">
                                       <td> ${key + 1}</td>
                                        <td> <img src=${item?.users?.logo} class="userImage"/></td>
                                        <td><h2>${item?.users?.full_name} <br />${item?.company?.name ? item?.company?.name : ''}<br/>${item?.level?.name ? item?.level?.name : ''}</h2>
                                        </td>
                                       <td>
                                            <div class="form-check">
                                                <input class="form-check-input asMember" type="checkbox" value=""
                                                       id=${key + 1} onclick="asMember_or_asAdmin(key + 1)">
                                                <label class="form-check-label" for="flexCheckDefault">

                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input asAdmin" name="admin[]" onclick="asAdmin_or_asMember(key + 1)" id=${key + 1}>
                                                <label class="form-check-label"></label>
                                            </div>
                                        </td>
                                        </tr>
                            `)
                            }
                        )
                    } else {
                        $("#member_list").children().remove();
                    }

                } else {
                    toastr['error'](res.message)
                }
            }
        });

    }

    // cropping image
    var $modal = $('#modal');
    var image = document.getElementById('image');
    var cropper;

    $("body").on("change", ".image", function (e) {
        var files = e.target.files;
        var done = function (url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;

        if (files && files.length > 0) {
            file = files[0];

            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function () {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            viewMode: 1,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function () {
        canvas = cropper.getCroppedCanvas({
            width: 160,
            height: 160,
        });

        canvas.toBlob(function (blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
                var base64data = reader.result;

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{route('admin.groups.image.update')}}",
                    data: {'_token': $('meta[name="_token"]').attr('content'), 'image': base64data},
                    success: function (data) {
                        $modal.modal('hide');
                        $('.groups_icons').attr('src', data.url)
                        $("#group_icon").val(data.fileName);
                        toastr[data.type](data.message)
                    }
                });
            }
        });
    })

</script>
