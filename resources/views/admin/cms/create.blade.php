@extends('layouts.admin.app')

@section('title','Add Cms')

@section('content')

    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Add New CMS</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Add Expense Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add New CMS</h3>
                    </div>
                </div>



                <form class="new-added-form" action="{{route('admin.cms.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Title *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('title') is-invalid @enderror form-control" name="title" en/>
                            </label>
                            @error('title')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Status*</label>
                            <label>
                                <select class="select2  @error('status') is-invalid @enderror " name="status">
                                    <option value="">Please Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </label>
                            @error('status')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
{{--                        <div class="col-xl-3 col-lg-6 col-12 form-group">--}}
{{--                            <div class="col-lg-6 col-2 form-group mg-t-30">--}}
{{--                                <label class="text-dark-medium" style="margin-top:-17px">Icon*</label>--}}
{{--                                <input type="file" class="form-control-file @error('icon') is-invalid @enderror" name="icon" >--}}
{{--                            </div>--}}
{{--                            @error('icon')--}}
{{--                            <span  role="alert">--}}
{{--                            <strong class="text-danger">{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}

                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Meta Keyword  </label>
                            <label>
                                <input type="text" placeholder="" class="@error('meta_keyword') is-invalid @enderror form-control" name="meta_keyword"/>
                            </label>
                            @error('meta_keyword')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Meta Description </label>
                            <label>
                                <input type="text" placeholder="" class="@error('meta_description') is-invalid @enderror form-control" name="meta_description"/>
                            </label>
                            @error('meta_description')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-xl-12 col-lg-6 col-12 form-group">
                            <label>URL* </label>
                            <label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><span class="font-weight-bold">{{ config('app.url') }}</span> </div>
                                    </div>

                                    <input type="text" placeholder="" class="@error('url') is-invalid @enderror form-control" name="url"/>
                                </div>
                            </label>
                            @error('url')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Content</label>
                                <textarea name="editor1"></textarea>
                                <script>
                                    CKEDITOR.replace( 'editor1' );
                                </script>

                                @if ($errors->has('editor1'))
                                    <span class="error-message">
                                            <strong>{{ $errors->first('editor1') }}</strong>
                                        </span>
                                @endif


                            </div>
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <a href="{{route('admin.level.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection

