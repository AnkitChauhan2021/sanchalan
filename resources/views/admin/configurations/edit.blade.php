@extends('layouts.admin.app')

@section('title','Update Config')

@section('header',"Dashboard")

@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li> Update Config</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Add Expense Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3> Update Config</h3>
                    </div>
                </div>





                <form class="new-added-form" action="{{route('admin.config.update')}}" method="post">
                    @csrf
                    <div class="row">


                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>App Name*</label>
                            <label>
                                <input type="text" placeholder="" class="@error('app_name') is-invalid @enderror form-control" name="app_name" value="{{$Config_edit->app_name}}"/>
                            </label>
                            @error('app_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Contact Number*</label>
                            <label>
                                <input type="text" placeholder="" class="@error('contact_no') is-invalid @enderror form-control" name="contact_no" value="{{$Config_edit->contact_no}}"/>
                            </label>
                            @error('contact_no')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Email*</label>
                            <label>
                                <input type="text" placeholder="" class="@error('email') is-invalid @enderror form-control" name="email" value="{{$Config_edit->email}}"/>
                            </label>
                            @error('email')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Image Limit (Count)*</label>
                            <label>
                                <input type="text" placeholder="" class="@error('image_limit') is-invalid @enderror form-control" name="image_limit" value="{{$Config_edit->image_limit}}"/>
                            </label>
                            @error('image_limit')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Video size Limit (MB)*</label>
                            <label>
                                <input type="text" placeholder="" class="@error('video_size') is-invalid @enderror form-control" name="video_size" value="{{$Config_edit->video_size}}"/>
                            </label>
                            @error('video_size')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Trending Post (Count)*</label>
                            <label>
                                <input type="text" placeholder="" class="@error('trending_post ') is-invalid @enderror form-control" name="trending_post" value="{{$Config_edit->trending_post}}"/>
                            </label>
                            @error('trending_post')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-12 col-lg-6 col-12 form-group">
                            <label>Title</label>
                            <label>
                                <input type="text" placeholder="" class="@error('title') is-invalid @enderror form-control" name="title" value="{{$Config_edit->title}}"/>
                            </label>
                            @error('title')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-12 col-lg-6 col-12 form-group">
                            <label>Subtitle</label>
                            <textarea class="textarea form-control" name="subtitle" id="form-message" cols="10" rows="5" >{{$Config_edit->subtitle}}</textarea>
                        </div>

                        <div class="col-xl-12 col-lg-6 col-12 form-group">
                            <label>Address</label>
                            <textarea class="textarea form-control" name="address" id="form-message" cols="10" rows="5" >{{$Config_edit->address}}</textarea>
                        </div>




                        <input type="hidden" value="{{$Config_edit->id}}" name="id"/>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                            <a href="{{route('admin.config.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
