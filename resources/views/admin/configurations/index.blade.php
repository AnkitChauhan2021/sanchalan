@extends('layouts.admin.app')

@section('title','Config')
@section('content')
 <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Settings</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">

                       
                    </div>

                </div>
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">
                       
                         @if(!$records->count())
                        <a href="{{route('admin.config.create')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search" ><i class="fas fa-plus-circle fa-2x" ></i></a>
                        @endif
                    </div>

                </div>
                
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>App Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Image Upload Limit (Count)</th>
                            <th>Video Size (MB) </th>
                            <th>Trending Post (Count)</th>
                            <th>SMS API</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($records as $key =>$record)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$record->app_name}}</td>
                                <td>{{$record->email}}</td>
                                <td>{{$record->contact_no}}</td>
                                <td>{{$record->image_limit}}</td>
                                <td>{{$record->video_size}}</td>
                                <td>{{$record->trending_post}}</td>

                                <td>
                                    {!! CommonHelper::getStatusUrl('admin.config.sms_api.changeStatus',$record->sms_api,$record->id) !!}
                                </td>



                                <td>
                                      <div class="dropdown">
                                        <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                           aria-expanded="false">
                                            <span class="flaticon-more-button-of-three-dots"></span>
                                        </a>
                                        <div class="dropdown-menu ">


                                            <a class="dropdown-item" href="{{url('admin/config/edit/'.$record->id)}}"> <i
                                                    class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                           
                                        </div>
                                    </div>

                                </td>
                            </tr>
                              @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>


@endsection

