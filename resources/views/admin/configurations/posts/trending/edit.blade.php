@extends('layouts.admin.app')

@section('title','Setting trending posts ')

@section('header',"Dashboard")

@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li> Trending Post Setting Update</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Add Expense Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3> Trending Post Setting Update</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('admin.setting.trending.post.update')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Image Limit*</label>
                            <label>
                                <input type="number" placeholder="" class="@error('trending_post') is-invalid @enderror form-control" name="trending_post" value="{{$record->trending_post}}"/>
                            </label>
                            @error('trending_post')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" value="{{$record->id}}" name="id"/>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                            <a href="{{route('admin.postimage.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
