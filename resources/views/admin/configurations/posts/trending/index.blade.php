@extends('layouts.admin.app')

@section('title','Trending Posts Settings ')
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Trending Posts Setting</li>
            </ul>
        </div>

        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title ">
                    </div>
                    @if(count($records)==0)
                        <div class="float-right">
                            <a href="http://localhost/sanchalan/admin/polls/create"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                    class="fas fa-plus-circle fa-2x"></i></a>
                        </div>
                    @endif

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Trending Post</th>
                            <th>Created At</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($records as $key =>$record)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$record->trending_post}}</td>
                                <td>{{$record->created_at}}</td>
                                <td>
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                           aria-expanded="false">
                                            <span class="flaticon-more-button-of-three-dots"></span>
                                        </a>
                                        <div class="dropdown-menu ">

                                            <a class="dropdown-item" href="{{route('admin.setting.trending.post.edit',$record->id)}}">
                                                <i
                                                    class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

