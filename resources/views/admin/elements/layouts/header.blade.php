<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->
{{--    id="sticky-topbar"--}}
    <div class="navbar navbar-expand-md header-menu-one bg-light" >
        <div class="nav-bar-header-one">
            <div class="header-logo">
                <a href="{{route('admin.dashboard.index')}}">
{{--                   <h4> Sanchalan</h4>--}}
                    <img src="{{asset('public/admin/img/logomain.png')}}" alt="Sanchalan">
                </a>
            </div>
            <div class="toggle-button sidebar-toggle">
                <button type="button" class="item-link">
                        <span class="btn-icon-wrap">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                </button>
            </div>
        </div>
        <div class="d-md-none mobile-nav-bar">
            <button class="navbar-toggler pulse-animation" type="button" data-toggle="collapse" data-target="#mobile-navbar" aria-expanded="false">
                <i class="far fa-arrow-alt-circle-down"></i>
            </button>
            <button type="button" class="navbar-toggler sidebar-toggle-mobile">
                <i class="fas fa-bars"></i>
            </button>
        </div>
        <div class="header-main-menu collapse navbar-collapse" id="mobile-navbar">
            <ul class="navbar-nav">
            </ul>
            <ul class="navbar-nav">
                <li class="navbar-item dropdown header-admin">
                    <a class="navbar-nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-expanded="false">
                        <div class="admin-title">
                            <h5 class="item-title">{{Auth::user()->full_name}}</h5>
{{--                            <span>Admin</span>--}}
                        </div>
                        <div class="admin-img">
                            <img src="{{Auth::user()->logo}}" alt="Admin" style="width:40px;" class="profile_image">
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="item-header">
                            <h6 class="item-title">{{Auth::user()->full_name}}</h6>
                        </div>
                        <div class="item-content">
                            <ul class="settings-list">
                                <li><a href="{{route('admin.profile.index')}}"><i class="flaticon-user"></i>My Profile</a></li>
                                <li><a href="{{route('admin.change_password.index')}}"><i class="flaticon-gear-loading"></i>Change Password</a></li>
                                <li>
                                <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="flaticon-turn-off"></i>  {{ __('Logout') }}
                                </a></li>
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Header Menu Area End Here -->
