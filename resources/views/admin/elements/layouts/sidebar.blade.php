
<div class="dashboard-page-one" >
    <!-- Sidebar Area Start Here -->
    <div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color" >
        <div class="mobile-sidebar-header d-md-none">
            <div class="header-logo">
                <a href="index.html"><img src="{{asset('public/admin/img/logo1.png')}}" alt="logo"></a>
            </div>
        </div>
{{--        id="sticky-sidebar"--}}
        <div class="sidebar-menu-content" >
            <ul class="nav nav-sidebar-menu sidebar-toggle-view"  >
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.dashboard.index')}}" class=" sidebar_menu nav-link"><i class="flaticon-dashboard"></i><span>Dashboard</span></a>
                </li>
{{-- ---------------------------     Master-----------------      --}}
                <li class="nav-item sidebar-nav-item">
                    <a href="#" class="nav-link"><i class="flaticon-settings"></i><span>Master</span></a>
                    <ul class="nav sub-group-menu">
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('admin.country.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>Countries</a>--}}
{{--                        </li>--}}
                        <li class="nav-item">
                            <a href="{{route('admin.state.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>States</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.district.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>Districts</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.city.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>Cities</a>
                        </li>
                        {{--<li class="nav-item">--}}
                            {{--<a href="{{route('admin.level.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>Levels</a>--}}
                        {{--</li>--}}
                    </ul>
                </li>


{{-- -------------------------     Organization    -------------------      --}}
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.cms.index')}}" class=" sidebar_menu nav-link">
                        <i class="far fa-file-alt"></i><span>Cms</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.organization.index')}}" class=" sidebar_menu nav-link"><i
                            class="flaticon-multiple-users-silhouette"></i><span>Organizations</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.posts.index')}}" class=" sidebar_menu nav-link">
                        <i class="far fa-comment-alt"></i><span>Posts</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.polls.index')}}" class=" sidebar_menu nav-link">
                        <i class="fas fa-poll"></i><span>Polls</span></a>
                </li>
              <!--   <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.magazines.index')}}" class=" sidebar_menu nav-link">
                        <i class="fas fa-blog"></i><span>Magazines</span></a>
                </li> -->
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.groups.index')}}" class=" sidebar_menu nav-link">
                        <i class="far fa-comment-dots"></i><span>Groups</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.users.index')}}" class=" sidebar_menu nav-link"><i
                            class="flaticon-user"></i><span>Users</span></a>
                </li>
                {{-- ---------------------------     Configuration----------------      --}}

                 <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.config.index')}}" class=" sidebar_menu nav-link"><i class="fas fa-user-cog"></i><span>Settings</span></a>
                </li>

                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.inquiries.index')}}" class=" sidebar_menu nav-link"><i
                            class="fas fa-headset"></i><span>Inquiries</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('admin.reports.index')}}" class=" sidebar_menu nav-link"><i class="fas fa-flag-checkered"></i>
                        <span>Reports</span></a>
                </li>
            </ul>
        </div>
    </div>

</div>
