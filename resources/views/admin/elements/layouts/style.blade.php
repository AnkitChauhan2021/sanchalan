<link rel="shortcut icon" type="image/x-icon" href="{{asset('/public/admin/img/favicon.ico')}}">
<!-- Normalize CSS -->
<link rel="stylesheet" href="{{ asset('/public/admin/css/nprogress.css') }}">
<link rel="stylesheet" href="{{asset('/public/admin/css/normalize.css')}}">
<!-- Main CSS -->
<link rel="stylesheet" href="{{asset('/public/admin/css/main.css')}}">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('/public/admin/css/bootstrap.min.css')}}">
{{--<script src="{{asset('/public/admin/js/jquery-3.3.1.min.js')}}"></script>--}}
<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{asset('/public/admin/css/all.min.css')}}">
<!-- Flaticon CSS -->
<link rel="stylesheet" href="{{asset('/public/admin/fonts/flaticon.css')}}">
<!-- Full Calender CSS -->
<link rel="stylesheet" href="{{asset('/public/admin/css/fullcalendar.min.css')}}">
<!-- Animate CSS -->
<link rel="stylesheet" href="{{asset('/public/admin/css/animate.min.css')}}">
<!-- Custom CSS -->

<link rel="stylesheet" href="{{ asset('/public/admin/toastr/build/toastr.min.css') }}">
<!-- jquery-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>

<script src="{{asset('/public/admin/js/smooth-submit.js')}}"></script>
<!-- toastr-->
<script src="{{ asset('public/admin/toastr/build/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{asset('/public/admin/custom.css')}}">
<link rel="stylesheet" href="{{asset('/public/admin/css/cropper.css')}}"/>

<link rel="stylesheet" href="{{asset('/public/admin/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/admin/style.css')}}">
<link rel="stylesheet" href="{{asset('/public/admin/css/datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/admin/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('/public/admin/css/jquery-confirm.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/admin//css/all.css')}}"/>
<style>
    .text-danger {
        font-size: 12px;
        color: #dc3545 !important;
    }

    .post_title h1,h2,h3,h4,h5,h6,p {
    font-size: medium;
    }

</style>

