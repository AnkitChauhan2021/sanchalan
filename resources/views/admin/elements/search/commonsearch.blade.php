

<form  method="get" action="{{ url()->current() }}" id="AjaxSearch">

    @section('search')
    <div class="row ">
        <div class="col-auto  form-group">
               <select class="select2 seclected_dist" id="LimitOptions" name="limit" >
                @php $options = CommonHelper::getLimitOption(); @endphp
                @foreach($options as $key=>$value)
                    <option {{ request('limit',env('PAGINATION_LIMIT')) == $value ? 'selected' : '' }} value="{{ $value }}"  >{{ $value }}</option>
                @endforeach
               </select>
        </div>

        <div class="col-xl-7 col-lg-7 col-12 form-group ">
            <input type="search" placeholder="Search by Name ..." name="search" value="{{ request('search') }}" class="form-control" >
        </div>
       <div class="col-xl-3  col-lg-2 col-9 form-group ">
        <button type="submit" class="fw-btn-fill btn-gradient-yellow search" >SEARCH</button>
        </div>
    </div>
	@show
</form>
