<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
<div class="my-profile proposal-details">
    <h3 class="main-job-heading">City Details</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="user">
                <img src="" class="img-fluid"/>
            </div>
            <div class="user-details-inner detail-modal">
                <h2> <!--p><i class="icofont-location-pin"></i> <span>Noida, India</span></p--></h2>
                <ul class="details-user row">
                    <li class="col-lg-12">
                        <h2>
                            <a href="javascript:void(0);">{{$record->subject}}</a>
                        </h2>

                    </li>
                    <li class="col-lg-4">
                        <p>Name: <br/><a href="javascript:void(0);">{{$record->name}}</a></p>
                    </li>
                    <li class="col-lg-4">
                        <p>Email:<br/> <a href="javascript:void(0);">{{$record->email}}</a></p>
                    </li>

                    <li class="col-lg-4">
                        <p>Created At:<br/>
                            <a href="javascript:void(0);"> {{date('D,M,Y h:i', strtotime($record->created_at))}}</a>
                        </p>
                    </li>
                    <li class="col-lg-12">
                        <p>Message:<br/> <a href="javascript:void(0);">{{$record->body}}</a></p>
                    </li>

                </ul>
            </div>

            <br/>

        </div>
    </div>
</div>


