<script>
    const magazineRemoveImage = (id) =>{
        $.ajax({
            type: "GET",
            url: "{{url('/admin/magazines/remove/image/')}}/" + id,
            success: function (response) {
                toastr[response.type](response.message)
                document.getElementById(id).style.display = "none";
            },
            error: function(response) {

            }
        });
    }
    const magazineRemoveVideo = (id) => {
        $.ajax({
            type: "GET",
            url: "{{url('/admin/magazines/remove/video/')}}/" + id,
            success: function (response) {
                toastr[response.type](response.message)
                document.getElementById(id).style.display = "none";
            },
            error: function(response) {

            }
        });
    }


</script>
