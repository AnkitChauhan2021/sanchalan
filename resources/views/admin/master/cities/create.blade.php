@extends('layouts.admin.app')

@section('title','Add City')

@section('header',"Dashboard")

@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Add New City</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add New City</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('admin.city.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>City Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('name') is-invalid @enderror form-control" name="name"/>
                            </label>
                            @error('name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>State* </label>
                            <label>
                                <select class=" select_state_dist select2  @error('state_id') is-invalid @enderror " name="state_id"  onchange="onselectState()">
                                    <option value="">Please Select</option>
                                    @foreach ($states as $state)
                                        <option value="{{$state->id}}">{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </label>
                            @error('state_id')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>District *</label>
                            <label>
                                <select class="select2  @error('district_id') is-invalid @enderror " name="district_id" id="state">
                                    <option value="">Please Select</option>
                                </select>
                            </label>
                            @error('district_id')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>Status*</label>
                            <label>
                                <select class="select2  @error('status') is-invalid @enderror " name="status">
                                    <option value="">Please Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </label>
                            @error('status')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <a href="{{route('admin.city.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @include('admin.master.cities.script')
@endsection
