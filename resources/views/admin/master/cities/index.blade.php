@extends('layouts.admin.app')

@section('title','City')
@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All City</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All City</h3>
                    </div>

                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('admin.city.create')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search" ><i class="fas fa-plus-circle fa-2x" ></i></a>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Name</th>
                            <th>District Name</th>
                            <th>State Name</th>

                            <th>Create At</th>
                            <th>Status</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                        @foreach ($records as $key => $record)
                            <tr>
                                <td> {{$key + 1 + (15 * ($records->currentPage() - 1))}}</td>
                                <td> {{$record->name}}</td>
                                <td> {{$record->district->name}}</td>
                                <td> {{$record->district->state->name}}</td>
                                <td>
                                    {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                <td>
                                    {!! CommonHelper::getStatusUrl('admin.city.changeStatus',$record->status,$record->id) !!}
{{--                                    <a href="{{route('admin.state.changeStatus')}}" class="btn {{ $record->status ===1 ? " btn-outline-success" : " btn-outline-danger" }} smooth-submit" id="stateStatusChange" method="post" params={{$record->id}}>{{ ($record->status ===1) ? 'Active' : 'Inactive' }}--}}
{{--                                    </a>--}}
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                           aria-expanded="false">
                                            <span class="flaticon-more-button-of-three-dots"></span>
                                        </a>
                                        <div class="dropdown-menu ">


                                    <a type="button" class="dropdown-item" data-toggle="modal" onclick="viewServiceProviderModal('{{$record->id}}');"><i class="fas fa-eye"></i> View</a>
                                            <a class="dropdown-item" href="{{url('admin/city/edit/'.$record->id)}}"> <i
                                                    class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                            <a class="dropdown-item action_btn confirmDelete" data-action="{{route('admin.city.remove',$record->id)}}" href="javascript:void(0);"><i
                                                    class="fas fa-trash text-danger"></i> delete</a>
                                        </div>
                                    </div>

                                </td>

                            </tr>
                        @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @include('admin.elements.pagination.common')
            </div>
        </div><!-- Modal -->
        <div class="modal fade job_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="min-height:300px;transition: transform 1s ease-out,-webkit-transform 1s ease-out;transition:all ease 1s;">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body job-description-popup">
                        <h1 id="state_name"></h1>
                    </div>
                </div>
            </div>
        </div>
    @include('admin.master.cities.script')

@endsection
