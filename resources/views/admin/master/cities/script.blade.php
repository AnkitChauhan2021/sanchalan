<script>


    function viewServiceProviderModal(id){
        $("#loader").show();
        // alert('id : '+id);
        var form_data = new FormData();
        form_data.append("id",id);
        form_data.append("_token", "{{csrf_token()}}");
        $(".modal-body").empty();
        $.ajax({
            url: "{{route('admin.city.view')}}",
            data: form_data,
            type: "POST",
            dataType: "json",
            contentType: false,
            processData: false,
            success:function(data) {
                $("#loader").hide();
                $(".modal-body").html(data.modal_content);
                $("#exampleModal").modal("show");
                console.log(data.name)
                //$('#state_name').val(data.name)
            }
        });
    };
    // create city and update city
    function onselectState() {
        var state_id = document.getElementsByClassName("select_state_dist");
        var value = state_id[0].value;
        $.ajax({
            type:"GET",
            url:"{{url('/admin/city/getDistricts')}}?state_id="+value,
            success:function(res){
                if(res){
                    $("#state").empty();
                    $("#state").append('<option>Please Select</option>');
                    $.each(res,function(key,value){

                        $("#state").append('<option value="'+value.id+'">'+value.name+'</option>');
                    });

                }else{
                    $("#state").empty();
                }
            }
        });

    }
</script>
