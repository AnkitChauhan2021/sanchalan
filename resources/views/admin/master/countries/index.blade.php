@extends('layouts.admin.app')

@section('title','Country')
@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All Country</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Country</h3>
                    </div>

                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="row">
                    <div class=" col-auto   form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('admin.country.create')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow  text-center search" ><i class="fas fa-plus-circle fa-2x" ></i></a>
                    </div>
{{--                    <div class=" col-auto form-group addbutton" style="margin-top: -74px; margin-right:-1px">--}}

{{--                        <a href="#" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search modal-trigger" data-toggle="modal" data-target="#sign-up" >--}}
{{--                            <i class="fas fa-file-csv fa-2x"></i></a>--}}
{{--                    </div>--}}
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Name</th>
                            <th>Create At</th>
                            <th>Status</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                        @foreach ($records as $key =>$record)
                            <tr>
                                <td> {{$key + 1 + (15 * ($records->currentPage() - 1))}}</td>
                                <td> {{$record->name}}</td>
                                <td>{{date('D,  M,  Y', strtotime($record->created_at))}}</td>
                                <td>

                                    {!! CommonHelper::getStatusUrl('admin.country.changeStatus',$record->status,$record->id) !!}
                                    {{--                                <a href="{{route('admin.state.changeStatus')}}" class="btn {{ $record->status ===1 ? " btn-outline-success" : " btn-outline-danger" }} smooth-submit" id="stateStatusChange" method="post" params={{$record->id}}>{{ ($record->status ===1) ? 'Active' : 'Inactive' }}--}}



                                    </a>
                                </td>
                                <td>

                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                           aria-expanded="false">
                                            <span class="flaticon-more-button-of-three-dots"></span>
                                        </a>
                                        <div class="dropdown-menu ">


                                            <a class="dropdown-item" href="{{url('admin/country/edit/'.$record->id)}}"> <i
                                                    class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                            <a class="dropdown-item action_btn confirmDelete" data-action="{{route('admin.country.remove',$record->id)}}" href="javascript:void(0);"><i
                                                    class="fas fa-trash text-danger"></i> delete</a>
                                        </div>
                                    </div>

                                </td>

                            </tr>
                        @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @include('admin.elements.pagination.common')
            </div>
        </div><!-- Modal -->
        <div class="modal fade job_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="min-height:300px;transition: transform 1s ease-out,-webkit-transform 1s ease-out;transition:all ease 1s;">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body job-description-popup">
                        <h1 id="state_name"></h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sign Up Modal -->
        <div class="modal sign-up-modal fade" id="sign-up" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="close-btn">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                       <h3>Import Country</h3>
                        <hr/>
                        <form class="Import-form" enctype="multipart/form-data" method="post" >

                            <div class="row gutters-15 " style="border: 4px dashed #272525;"  >

                                <input type="file" class="form-control-file upload @error('images') is-invalid @enderror" name="images"  id="imgupload">
                                <p class="textP text-success " onclick="$('#imgupload').trigger('click'); return false;"><i class="fas fa-cloud-upload-alt fa-7x"></i></p>
                            </div>
                            <hr/>
                            <div class="col-xl-12  col-lg-2 col-9 form-group ">
                                <button type="submit" class="fw-btn-fill btn-gradient-yellow search" >Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



@endsection

