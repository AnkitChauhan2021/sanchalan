<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
<div class="my-profile proposal-details">
    <h3 class="main-job-heading">District Details</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="user">
                <img src="" class="img-fluid"/>
            </div>
            <div class="user-details-inner detail-modal">
                <h2> <!--p><i class="icofont-location-pin"></i> <span>Noida, India</span></p--></h2>
                <ul class="details-user row">
                    <li class="col-lg-6">
                        <p>District Name: <a href="javascript:void(0);"></a></p>
                    </li>
                    <li class="col-lg-6">
                        <p>State Name: <a href="javascript:void(0);"></a></p>
                    </li>

                    <li class="col-lg-6">
                        <p>Status:
                            @if(($record->status)==1)
                                <a href="javascript:void(0);">Active</a>
                            @else
                                <a href="javascript:void(0);">Inactive</a>
                            @endif
                        </p>
                    </li>
                    <li class="col-lg-6">
                        <p>Created At:
                            <a href="javascript:void(0);"> {{date('D,M,Y h:i', strtotime($record->created_at))}}</a>

                        </p>
                    </li>
                </ul>
            </div>

            <br/>

        </div>
    </div>
</div>


