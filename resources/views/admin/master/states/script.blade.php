<script>

        function viewServiceProviderModal(id){
        $("#loader").show();
        // alert('id : '+id);
        var form_data = new FormData();
        form_data.append("id",id);
        form_data.append("_token", "{{csrf_token()}}");
        $(".modal-body").empty();
        $.ajax({
        url: "{{route('admin.state.view')}}",
        data: form_data,
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        success:function(data) {
        $("#loader").hide();
        $(".modal-body").html(data.modal_content);
        $("#exampleModal").modal("show");
        console.log(data.name)
        //$('#state_name').val(data.name)
    }
    });
    };

</script>
