@extends('layouts.admin.app')
@section('title','Add Member')
@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Add Member</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add Member for {{$company->name}}</h3>
                    </div>
                </div>
                <form class="new-added-form" method="GET">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>
                                <input type="text" placeholder="Search User.." value="{{request('search')}}"
                                       class="@error('address') is-invalid @enderror form-control" name="search"/>
                            </label>
                            @error('address')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <button type="submit" class="fw-btn-fill btn-gradient-yellow search btn-sm" style="width: auto;">SEARCH</button>
                        </div>
                    </div>
                </form>



                <form  method="post" action="{{route('admin.organization.addMember.store')}}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table display data-table text-nowrap">
                                    <thead>
                                    <tr>

                                        <th>Sr No</th>
                                        <th>Name</th>
                                        <th>Set Level</th>
                                        <th>#</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($records->count())
                                        @foreach ($records as $key => $user)
                                            <tr>
                                                <td>{{$key + 1 + (15 * ($records->currentPage() - 1))}}</td>
                                                <td> {{$user->full_name}}</td>
                                                <td>
                                                    <select class="form-control custom-level-select w-auto addMembers" name="level[]">
                                                        <option value="">Select Level</option>
                                                        @foreach($levels as $level)
                                                            <option value="{{$level->id}}">{{$level->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input type="hidden" name="company_id" value="{{$company->id}}">
                                                        <input type="checkbox" class="form-check-input check selectCheck" name="user[]" value="{{$user->id}}">
                                                        <label class="form-check-label"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="8">No Record Found!</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>

                            </div>
                            @include('admin.elements.pagination.common')
                        </div>
                        <div class="col-12 form-group mg-t-8 text-right">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save
                            </button>
                            <a href="{{route('admin.organization.index')}}"
                               class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>

            $('body').on('change', '.addMembers', function () {

                $(this).closest("tr").find("input").attr('checked', true);
            });
            $('body').on('change', '.selectCheck', function () {
                if ($(this).is(":checked")) {
                    toastr['error']("Please Select Level")
                    $(this).prop("checked", false);
                }
                $(this).closest("tr").find('option').prop("selected", false);
            });
        </script>

@endsection

