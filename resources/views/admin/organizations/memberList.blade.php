@extends('layouts.admin.app')
@section('title','All Users')

@section('content')

    <style>
        .search {
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All Members</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Members of {{$org->name}}</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('admin.organization.addMember',$org->id)}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i class="fas fa-plus-circle fa-2x"></i></a>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Level</th>
                            <th>Designation</th>
                            <th>Is Admin</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key + 1 + (15 * ($records->currentPage() - 1))}}</td>
                                    <td> {{@$record->users->full_name}}</td>
                                    <td> {{@$record->users->email}}</td>
                                    <td> {{@$record->users->mobile}}</td>
                                    <td> {{@$record->level->name}}</td>
                                    <td> {{@$record->designation}}</td>

                                    <td>
                                        <button data-company_id="{{request()->id}}" data-user_id="{{$record->user_id}}" title="Toggle Admin Rights" class="btn btn-lg {{$record->role_id===3?'btn-success':'btn-danger'}} toggle_admin">
                                            {{--<i class="fa fa-check"></i>--}}
                                            <i class="fa {{$record->role_id===3?'fa-check':'fa-times'}}"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">
                                                @if(@$record->users->id)
                                                    <a class="dropdown-item" href="{{route('admin.users.edit',$record->users->id)}}"> <i
                                                            class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>
                                                    <a class="dropdown-item action_btn confirmDelete" data-action="{{route('admin.organization.memberDelete',$record->users->id)}}" href="javascript:void(0);"><i
                                                            class="fas fa-trash text-danger"></i> delete</a>
                                                @endif
                                            </div>
                                        </div>
                                </tr>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                @include('admin.elements.pagination.common')
            </div>
        </div>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-confirm" style="width: 400px;">
                <div class="modal-content" style="margin-top: 135px">
                    <div class="modal-header flex-column">

                        <h4 class="modal-title w-100">Are You Sure?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center ">
                        {!! CommonHelper::ApprovedUser('admin.users.Permission',$record->isApproved,$record->id) !!}
                        <hr/>
                        <div>

                        </div>
                    </div>
                </div>



    @include('admin.users.script')
@endsection
