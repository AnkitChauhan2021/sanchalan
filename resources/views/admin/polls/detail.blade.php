@extends('layouts.admin.app')

@section('title','Poll Detail')
@section('content')


    <style>
        .search{
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All organizations</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All organizations</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{url('admin/submit/poll/'.$record->id)}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search" ><i class="fas fa-plus-circle fa-2x" ></i></a>
                    </div>

                </div>
                <div class="p-6 ">
                    <table class="table border-0 mb-81" style="margin-bottom:81px">
                        <thead>

                        <tr>
                            <td>
                                <label class="font-bold">Question</label>
                            </td>
                            <td>
                                <label class="font-bold">Created BY</label>
                            </td>
                            <td>
                                <label class="font-bold">Created At</label>
                            </td>
                            <td>
                                <label class="font-bold">Submitted</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="font-bold">{{$record->question}}</label>
                            </td>
                            <td>
                                <label class="font-bold">{{$record->users->full_name}}</label>
                            </td>
                            <td>
                                <label class="font-bold">
                                    {{date('D,  M,  Y ', strtotime($record->created_at))}}
                                    </label>
                            </td>
                            <td>
                                <label class="font-bold">{{count($records)}}</label>
                            </td>
                        </tr>

                        <tr class=" border-0">

                            <th class=" border-0 text-center">A</th>
                            <th class=" border-0 text-center">B</th>
                            <th class=" border-0 text-center">C</th>
                            <th class=" border-0 text-center">D</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class=" border-0 text-center">

                            <td class=" border-0 text-center">{{$record->answer1}}</td>
                            <td class=" border-0 text-center">{{$record->answer4}}</td>
                            <td class=" border-0 text-center">{{$record->answer3}}</td>
                            <td class=" border-0 text-center">{{$record->answer4}}</td>
                        </tr>
                        </tbody>

                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>User</th>
                            <th>A</th>
                            <th>B</th>
                            <th>C</th>
                            <th>D</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key+1}}</td>

                                    <td> {{$record->users->full_name}}</td>

                                    @if($record->answer =="answer1")
                                        <td><i class="fas fa-check text-success"></i></td>
                                    @else
                                        <td><i class="fas fa-times text-danger"></i></td>
                                    @endif
                                    @if($record->answer =="answer2")
                                        <td><i class="fas fa-check text-success"></i></td>
                                    @else
                                        <td><i class="fas fa-times text-danger"></i></td>
                                    @endif
                                    @if($record->answer =="answer3")
                                        <td><i class="fas fa-check text-success"></i></td>
                                    @else
                                        <td><i class="fas fa-times text-danger"></i></td>
                                    @endif
                                    @if($record->answer =="answer4")
                                        <td><i class="fas fa-check text-success"></i></td>
                                    @else
                                        <td><i class="fas fa-times text-danger"></i></td>
                                    @endif

                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">

                                                <a class="dropdown-item action_btn confirmDelete" data-action="{{route('admin.submit.poll.remove',$record->id)}}" href="javascript:void(0);"><i
                                                        class="fas fa-trash text-danger"></i> delete</a>

                                            </div>
                                        </div>
                                </tr>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                @include('admin.elements.pagination.common')
            </div>
        </div>

@endsection
