@extends('layouts.admin.app')
@section('title','Update Post')
@section('content')

    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Update Post</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Admit Form Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Update Post</h3>
                    </div>

                </div>
                <form class="new-added-form" action="{{route('admin.posts.update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">


                        <div>
                            <div class="col-lg-6 col-12 form-group mg-t-30">
                                <label class="text-dark-medium" style="margin-top:-17px">Image</label>
                                <input type="file" class="form-control-file @error('images') is-invalid @enderror" name="images[]" multiple>

                            </div>
                            @error('images')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" value="{{$post->id}}" name="id">
                        <div class="col-lg-12 col-12 form-group mg-t-30">
                            <div class="row">

                                @foreach($postImage as $value)
                                    <div class="col-sm-3  mg-t-30"  id="{{$value->id}}">
                                <div class="card border" style="width: 21rem; " >
                                    <a href="javascript:void(0);" class="remove_image" onclick="removeImage('{{$value->id}}')"><span aria-hidden="true">&times;</span></a>
                                    <img class="card-img-top" src="{{$value->post_image}}" data-id="{{$value->id}}" alt="Card image cap" style="height:25rem" >

                                </div>

                                    </div>
                                    @endforeach

                            </div>


                        </div>


                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Content</label>
                                <textarea name="editor1">{{ $post->description}}</textarea>
                                <script>
                                    CKEDITOR.replace( 'editor1' );
                                </script>

                                @if ($errors->has('editor1'))
                                    <span class="error-message">
                                            <strong  class="text-danger">{{ $errors->first('editor1') }}</strong>
                                        </span>
                                @endif


                            </div>
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@include('admin.posts.script')
@endsection


