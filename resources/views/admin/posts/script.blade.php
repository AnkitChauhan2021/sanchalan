<script>
    const removeImage = (id) =>{
        $.ajax({
            type: "GET",
            url: "{{url('/admin/posts/remove/image/')}}/" + id,
            success: function (response) {
                toastr[response.type](response.message)
                document.getElementById(id).style.display = "none";
            },
            error: function(response) {

            }
        });
    }
</script>
