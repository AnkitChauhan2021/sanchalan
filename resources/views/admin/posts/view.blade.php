<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
<div class="my-profile proposal-details">
    <h3 class="main-job-heading">Post Details</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="user">
                <img src="" class="img-fluid"/>
            </div>
            <div class="user-details-inner detail-modal">
                <h2> <!--p><i class="icofont-location-pin"></i> <span>Noida, India</span></p--></h2>
                <ul class="details-user row">
                    <li class="col-lg-12">
                        <h1>{!!  \Illuminate\Support\Str::limit($record->description ,75, $end='...')!!}</h1>
                    </li>
                    <li class="col-lg-6">
                        <p>Posted  BY: {{$record->getUser->full_name}}</p>
                    </li>
                        <div class="col-lg-12 col-12 form-group mg-t-30">
                        <div class="row">
                            @foreach($postImage as $value)
                                <div class="col-sm-3  mg-t-30" style="padding:15px;">
                                    <div class="card border" style="width: 21rem; " >
                                        <img class="card-img-top" src="{{$value->post_image}}" data-id="{{$value->id}}" alt="Card image cap" style="height:25rem" >
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        </div>


                    <li class="col-lg-12">
                        <p>Description:
                           {!! $record->description !!}

                        </p>
                    </li>
                </ul>
            </div>

            <br/>

        </div>
    </div>
</div>


