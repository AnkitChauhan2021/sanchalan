@extends('layouts.admin.app')
@if(Auth::check())
@section('title',Auth::user()->first_name.' '.Auth::user()->last_name.' '. "Profile")
@endif
@section('header',"Dashboard")

@section('content')
    <div class="dashboard-content-one">

        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Admin Profile</li>
            </ul>
        </div>

        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">

                    <div class="item-title">
                        <h3>About Me</h3>
                    </div>
                </div>
                <br>
                <br>
                <div class="single-info-details">
                    <div class="item-img" >
                        <img src="{{$user->logo}}" alt="{{$user->full_name}}" style="width:200px;height:200px;border-radius: 10px">
                    </div>
                    <div class="item-content">
                        <div class="header-inline item-header">
                            <h3 class="text-dark-medium font-medium">{{$user->full_name}}</h3>
                            <div class="header-elements">
                                <ul>
                                    <li><a href="{{route('admin.profile.edit.index')}}"><i class="far fa-edit"></i></a></li>
{{--                                    <li><a href="#"><i class="fas fa-print" onclick="profilePrint()"></i></a></li>--}}
                                </ul>
                            </div>
                        </div>

                        <div class="info-table table-responsive">
                            <table class="table text-nowrap">
                                <tbody>
                                <tr>
                                    <td>E-mail:</td>
                                    <td class="font-medium text-dark-medium">{{$user->email}}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <td>Phone:</td>--}}
{{--                                    <td class="font-medium text-dark-medium">+ 88 98568888418</td>--}}
{{--                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Student Details Area End Here -->


@endsection
