@extends('layouts.admin.app')

@section('title','Posts')


@section('content')


    <style>
        .search{
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All Reports</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Reports</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')

                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Post Name</th>
                            <th>Report Count</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>


                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key + 1 + (15 * ($records->currentPage() - 1))}}</td>
                                    <td class="post_title"> {!!  \Illuminate\Support\Str::limit($record->description ,50, $end='...')!!}</td>
                                    <td>{{$record->reports_count}}</td>


                                    <td>
                                        {!! CommonHelper::getStatusUrl('admin.posts.changeStatus',$record->status,$record->id) !!}

                                    </td>



                                    <td>

                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">
                                                <a class="dropdown-item action_btn confirmDelete" data-action="{{route('admin.reports.remove',$record->id)}}" href="javascript:void(0);"><i
                                                            class="fas fa-trash text-danger"></i> delete</a>

                                            </div>
                                        </div>
                                       </td>


                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                {{--@include('admin.elements.pagination.common')--}}
            </div>
        </div>

@endsection
