@extends('layouts.admin.app')

@section('title','Add User Organization')

@section('content')

    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Add New User</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Add Expense Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add New User</h3>
                    </div>
                </div>



                <form class="new-added-form" action="{{route('admin.user.organization.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>First Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('first_name') is-invalid @enderror form-control" name="first_name" value="{{$user->first_name}}"/>
                            </label>
                            @error('first_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Last Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('last_name') is-invalid @enderror form-control" name="last_name" value="{{$user->last_name}}"/>
                            </label>
                            @error('last_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Email  *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('email') is-invalid @enderror form-control" name="email"  value="{{$user->email}}"/>
                            </label>
                            @error('email')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Mobile *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('mobile') is-invalid @enderror form-control" name="mobile" value="{{$user->mobile}}"/>
                            </label>
                            @error('mobile')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" name="id" value="{{$user->id}}">

                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Organization</label>
                            <select class="select2 selected_organization @error('organization') is-invalid @enderror " name="organization" onchange="onselectOrganization()">
                                <option value="">Please Select Organization</option>
                                @foreach($organizations as $organization)
                                    <option value="{{$organization->id}}">{{$organization->name}}</option>
                                @endforeach
                            </select>
                            @error('organization')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Level</label>
                            <select class="select2  @error('level') is-invalid @enderror " name="level">
                                <option value="">Please Select Level</option>
                                @foreach($levels as $level)
                                    <option value="{{$level->id}}">{{$level->name}}</option>
                                    @endforeach
                            </select>
                            @error('level')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Designation</label>
                            <label>
                                <input type="text" placeholder="" class="@error('designation') is-invalid @enderror form-control" name="designation"/>
                            </label>
                            @error('designation')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <a href="{{route('admin.users.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @include('admin.users.script')


@endsection
