@extends('layouts.admin.app')

@section('title','Update User')

@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Update User</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Add Expense Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Update User</h3>
                    </div>
                </div>
                @foreach($user as $users)
                    <form class="new-added-form" action="{{route('admin.users.update')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-12 form-group">
                                <label>First Name *</label>
                                <label>
                                    <input type="text" placeholder=""
                                           class="@error('first_name') is-invalid @enderror form-control"
                                           name="first_name" value="{{$users->first_name}}"/>
                                </label>
                                @error('first_name')
                                <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="col-xl-4 col-lg-6 col-12 form-group">
                                <label>Last Name *</label>
                                <label>
                                    <input type="text" placeholder=""
                                           class="@error('last_name') is-invalid @enderror form-control"
                                           name="last_name" value="{{$users->last_name}}"/>
                                </label>
                                @error('last_name')
                                <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="col-xl-4 col-lg-6 col-12 form-group">
                                <label>Email *</label>
                                <label>
                                    <input type="text" placeholder=""
                                           class="@error('email') is-invalid @enderror form-control" name="email"
                                           value="{{$users->email}}"/>
                                </label>
                                @error('email')
                                <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="col-xl-4 col-lg-6 col-12 form-group">
                                <label>Mobile *</label>
                                <label>
                                    <input type="text" placeholder=""
                                           class="@error('mobile') is-invalid @enderror form-control" name="mobile"
                                           value="{{$users->mobile}}"/>
                                </label>
                                @error('mobile')
                                <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <input type="hidden" value="{{$users->id}}" name="id">
                        <div class="border ">
                            <br/>
                            <a href="{{route('admin.user.add.Organization',$users->id)}}"
                               class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark float-right"
                               style="margin-right: 10px"><i class="fas fa-plus-circle fa-1x"></i></a>
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table data-table text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div>
                                                <label>Sr No</label>
                                            </div>
                                        </th>
                                        <th>Organization</th>
                                        <th>Level</th>
                                        <th>Designation</th>
                                        <th>Action</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users->userComapny as $key=> $value)
                                        <tr>
                                            <td>
                                                <div>
                                                    <label> {{$key+1}}</label>
                                                </div>
                                            </td>
                                            <td>{{@$value->company->name}}</td>
                                            <td>{{@$value->level->name}}</td>
                                            <td>{{$value->designation}}</td>
                                            <td>
                                                <a class="dropdown-item action_btn confirmDelete"
                                                   data-action="{{route('admin.users.destroy.userCompany',$value->id)}}"
                                                   href="javascript:void(0);"><i
                                                        class="far fa-times-circle text-danger "
                                                        style="font-size:25px"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save
                            </button>
                            <a href="{{route('admin.users.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </form>
            </div>
            @endforeach
        </div>
    </div>
    </div>
    @include('admin.users.script')
@endsection
