@extends('layouts.admin.app')
@section('title','All Users')

@section('content')

    <style>
        .search{
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>All Users</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Users</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('admin.users.create')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search" ><i class="fas fa-plus-circle fa-2x" ></i></a>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
{{--                            <th>Organization</th>--}}
{{--                            <th>IsApproved</th>--}}
                            <th>Status</th>
                            <th>Notification</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key + 1 + (15 * ($records->currentPage() - 1))}}</td>
                                    <td> {{$record->full_name}}</td>
                                    <td> {{$record->email}}</td>
                                    <td> {{$record->mobile}}</td>
                                    <td>
                                        {!! CommonHelper::getStatusUrl('admin.users.changeStatus',$record->status,$record->id) !!}

                                    </td>
                                    <td>
                                        {!! CommonHelper::setNotification('admin.users.notifications',$record->notification,$record->id) !!}

                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">

                                                <a class="dropdown-item" href="{{route('admin.users.edit',$record->id)}}"> <i class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>
                                                <a href="javascript:void(0);" class="dropdown-item " data-toggle="modal" data-target="#small-modal"  onclick="onChangePassword('{{$record->id}}')"><i class="fas fa-unlock text-info" style=""></i> Password</a>
{{--                                                {!! CommonHelper::approvedAndRejectUser('admin.users.Permission',$record->isApproved,$record->id) !!}--}}
                                                <a href="{{route('admin.users.organization',$record->id)}}" class="dropdown-item" ><i class="fab fa-slack text-dark-pastel-yellow"></i> IsApproved</a>
                                                <a class="dropdown-item action_btn confirmDelete" data-action="{{route('admin.users.remove',$record->id)}}" href="javascript:void(0);"><i
                                                        class="fas fa-trash text-danger"></i> delete</a>

                                            </div>
                                        </div>
                                </tr>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                @include('admin.elements.pagination.common')
            </div>
        </div>


        <div class="modal col-lg-12 fade" id="small-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 10%;">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="new-added-form" id="changesUsePassword">

                            <div class="col-xl-12 col-lg-6 col-12 form-group">
                                <br/>
                                <label>Password</label>
                                <input type="password" placeholder="" class="form-control" id="getpassword" onchange="AddEnable()">
                                <input type="hidden" name="id" id="user_id_appended" class="user_id_appended">
                                <span class="text-danger" id="password_of_admin"></span>
                            </div>
                        <div class="col-xl-12 col-lg-6 col-12 form-group">
                            <label>Confirm Password</label>
                            <input type="password" placeholder="" class="form-control" id="password_confirmationBYadmin" onchange="AddEnable()">
                            <span class="text-danger" id="password_confirmation_of_admin"></span>
                        </div>


                        <div class="modal-footer d-flex justify-content-center " >

                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark submitButton" onclick="passwordform()" id="submitButton"><i class="fas fa-exchange-alt"></i></button>

                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow" data-dismiss="modal"><i class="far fa-times-circle"></i></button>

                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-confirm" style="width: 400px;">
                <div class="modal-content" style="margin-top: 135px">
                    <div class="modal-header flex-column">

                        <h4 class="modal-title w-100">Are You Sure?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center " >
                        {!! CommonHelper::ApprovedUser('admin.users.Permission',$record->isApproved,$record->id) !!}
                     <hr/>
                        <div>

                </div>
            </div>
        </div>


    @include('admin.users.script')
@endsection
