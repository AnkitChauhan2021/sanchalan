<script>
    function onselectOrganization() {
        const organization = document.getElementsByClassName('selected_organization');
        var levels = organization[0].value;

        getLevels(levels);
    }

    function getLevels(levels) {

        $.ajax({
            type: "GET",
            url: "{{url('admin/users/getLevels')}}?organization_id=" + levels,
            success: function (res) {
                if (res) {
                    $(".byorganization").empty();
                    $(".byorganization").append('<option value=" ">Please Select level</option>');
                    $.each(res, function (key, value) {

                        $(".byorganization").append('<option value="' + value.levels.id + '">' + value.levels.name + '</option>');
                    });
                } else {
                    $(".byorganization").empty();
                }
            }
        });
    }

    let id;

    function onChangePassword(id) {
        $('#user_id_appended').val(id)
        $('#submitButton').removeAttr("disabled")

    }

    function AddEnable() {
        $('.submitButton').removeAttr("disabled")
    }

    function passwordform() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#changesUsePassword').on('submit', function (event) {
            event.preventDefault();


            password = $('#getpassword').val();
            password_confirmationBYadmin = $('#password_confirmationBYadmin').val();
            id = $('.user_id_appended').val();
            console.log(id);


            $.ajax({
                url: "{{route('admin.users.changeUserPassword')}}",
                type: "POST",
                data: {

                    password: password,
                    password_confirmation: password_confirmationBYadmin,
                    id: id,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    if ((response.type) === 'success') {
                        toastr[response.type](response.message)
                        $('#changesUsePassword')[0].reset();
                        $("#small-modal").modal('hide');
                        location.reload();
                    } else {
                        toastr[response.type](response.message)
                    }

                },
                error: function (response) {
                    toastr['error'](response.responseJSON.errors.password + response.responseJSON.errors.password_confirmation)
                    // toastr['error'](response.responseJSON.errors.password_confirmation)
                    $('#password_of_admin').text(response.responseJSON.errors.password);
                    $('#password_confirmation_of_admin').text(response.responseJSON.errors.password_confirmation);


                }
            });
        });
    }

    $('.toggle_admin').on('click', function () {
        const ele = $(this);
        const user_id = $(this).data('user_id');
        const company_id = $(this).data('company_id');
        $.ajax({
            type: "POST",
            url: "{{route('admin.organization.toggleAdminHead')}}",
            data: {user_id, company_id, "_token": "{{ csrf_token() }}",},
            success: function (res) {
                console.log($(ele))
                if (ele.hasClass('btn-danger')) {
                    ele.removeClass('btn-danger').addClass('btn-success')
                    ele.find('i').removeClass('fa-times').addClass('fa-check')
                } else {
                    ele.removeClass('btn-success').addClass('btn-danger')
                    ele.find('i').removeClass('fa-check').addClass('fa-times')
                }

                console.log(res.data.user)
            }
        });
    })

</script>
