{{--
@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    --}}
{{--  Home Page Mune Include    --}}{{--

    --}}
{{--    @include('elements.layoutsElements.header')--}}{{--

    --}}
{{--  User Side menu page include   --}}{{--

    @include('elements.layoutsElements.navbar')

    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-3">
                <div class="filter">
                    <h3>Filter</h3>
                    <form action="{{route('user.advance-search1')}}" method="post">
                        @csrf
                        <div class="select-filter">
                            <div class="form-group">
                                <label>Organization</label>
                                <div class="input-icon">
                                    <select  class="form-control selected_company @error('company_id') is-invalid @enderror"
                                            onchange="selectCompany()" name="company_id[]">
                                        <option value="">Organization</option>
                                        @foreach($companies as $value)
                                            <option value="{{$value->id}}" {{old('company_id')==$value->id?'selected': ""}}>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    --}}
{{--<i class="icofont-building"></i>--}}{{--

                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <div class="input-icon">
                                    <select class="form-control @error('level_id') is-invalid @enderror" id="orglevels"
                                            name="level_id[]">
                                        <option value=" ">Select Level</option>
                                    </select>
                                    --}}
{{--<i class="icofont-building"></i>--}}{{--

                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <div class="input-icon">
                                    <select class="form-control selected_states @error('state_id') is-invalid @enderror"
                                            id="statesGet" onchange="selectState()" name="state_id[]">
                                        <option value=" "> Select State</option>
                                    </select>
                                    --}}
{{--<i class="icofont-building"></i>--}}{{--

                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>District</label>
                                <div class="input-icon">
                                    <select
                                        class="form-control selected_district @error('district_id') is-invalid @enderror"
                                        id="districtGet" onchange="selectDistrict()" name="district_id[]">
                                        <option value=" "> Select District</option>
                                    </select>
                                    --}}
{{--<i class="icofont-building"></i>--}}{{--

                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <div class="input-icon">
                                    <select class="form-control @error('city_id') is-invalid @enderror" id="cityGet"
                                            name="city_id[]">
                                        <option value=" "> Select City</option>
                                    </select>
                                    --}}
{{--<i class="icofont-building"></i>--}}{{--

                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <button type="submit" class="post_btn light_purple ml-2">Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-7">
                <div class="bg-white shadow-sm profile search_inner">
                    <h3>Search Result</h3>
                    <div class="search_result">
                        @foreach($records as $user)

                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <div class="post-describe d-flex align-items-center">
                                    <div class="port-profile">
                                        <img src="{{$user->logo}}" class="img-fluid">
                                    </div>
                                    <div class="post-content">
                                        <h5>{{$user->full_name}}</h5>
                                        <p>{{$user->designation}}</p>
                                    </div>
                                </div>
                                <div class="follow_chat">
                                    @if($user->id != Auth::user()->id)
                                        @foreach($user->userFollowers as $userFollower)
                                            @if($userFollower->id == Auth::user()->id)
                                                <a href="{{route('user.follow',$user->id)}}" class="text-blue">
                                                    Unfollow
                                                </a>
                                            @endif
                                        @endforeach
                                        @if(!$user->userFollowers->contains(Auth::user()->id))

                                            <a href="{{route('user.follow',$user->id)}}"  class="text-blue">
                                                Follow
                                            </a>
                                        @endif

                                    @endif
                                        @if($user->id != Auth::user()->id)
                                            @if($user->level_position >= $loggedInUser->level_position - 3)
                                    <span class="pl-2 pr-2">|</span>
                                    <a href="#" class="text-blue">Chat</a>
                                            @endif
                                             @endif

                                </div>
                            </div>
                        @endforeach
                            --}}
{{--@include('admin.elements.pagination.common')--}}{{--

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('advancesearch.script')
@endsection
--}}
