<script>
    var companies = [];
    var states = [];
    var districts = [];
    $(document).ready(function () {
       $('#company').multiselect();
       $('#orglevels').multiselect();
       $('#statesGet').multiselect();
       $('#districtGet123').multiselect();
       $('#cityGet').multiselect();
        $('#company').on('change', function () {
            companies = $(this).val();
            selectCompany();
        })
        $('#statesGet').on('change', function () {
            states = $(this).val();
            selectState();
        })
        $('#districtGet123').on('change', function () {
            districts = $(this).val();
            console.log($(this).val())
            selectDistrict();
        })

    });


    const selectCompany = () => {
        // const companis = document.getElementsByClassName('selected_company');
        // company = companis[0].value;
        $.ajax({
            type: "GET",
            url: "{{url('/user/levels')}}",
            data: {comapny_id: companies},
            success: function (res) {

                if (res) {

                    $("#orglevels").empty();
                    $("#orglevels").append('<option value=" "> Select Level</option>');
                    $.each(res, function (key, value) {
                        $("#orglevels").append('<option value="' + value.levels.id + '">' + value.levels.name + '</option>');

                    });
                } else {
                    $("#orglevels").empty();
                }
            }
        });
    }
    const selectCountry = () => {
        const countries = document.getElementsByClassName('selected_country');
        country = 1;//countries[0].value;

        $.ajax({
            type: "GET",
            url: "{{url('/user/country')}}?country_id=" + country,
            success: function (res) {

                if (res) {

                    $("#statesGet").empty();
                    $("#statesGet").append('<option value="">Select State</option>');
                    $.each(res, function (key, value) {
                        $("#statesGet").append('<option value="' + value.id + '">' + value.name + '</option>');

                    });
                } else {
                    $("#statesGet").empty();
                }
            }
        });
    }
    $(document).ready(function () {
        selectCountry();
    });


    const selectState = () => {
        // const states = document.getElementsByClassName('selected_states');
        // state = states[0].value;

        $.ajax({
            type: "GET",
            url: "{{url('user/state')}}",
            data: {state_id: states},
            success: function (res) {
                if (res) {
                    $('.selected_states').val(res)
                    $("#districtGet123").empty();
                    $("#districtGet123").append('<option value="">Please Select district</option>');
                    $.each(res, function (key, value) {

                        $("#districtGet123").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                    $("#districtGet123").multiselect('rebuild');

                } else {
                    $("#districtGet123").empty();
                }
            }
        });

    }
    const selectDistrict = () => {
      /*  const districts = document.getElementsByClassName('selected_district');
        district = districts[0].value;*/
        $.ajax({
            type: "GET",
            url: "{{url('user/district')}}",
            data: {district_id: districts},
            success: function (res) {
                console.log(res)
                if (res) {
                    $('.selected_district').val(res)
                    $("#cityGet").empty();
                    $("#cityGet").append('<option value= " ">Please Select city</option>');
                    $.each(res, function (key, value) {

                        $("#cityGet").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                    $("#cityGet").multiselect('rebuild');
                } else {
                    $("#cityGet").empty();
                }
            }
        });

    }
    const Organizations = () => {
        $("._organization").toggle()
    }
    const adress = () => {
        $("#address").toggle();
    }

</script>

