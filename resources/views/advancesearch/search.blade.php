@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    <link href="{{ asset('home/css/bootstrap-multiselect.min.css') }}" type="text/css"/>
    <script src="{{ asset('home/js/bootstrap-multiselect.min.js') }}"></script>
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-3">
                <div class="filter">

                    <h3>Filters</h3>
                    <form action="{{route('user.advance-search')}}" method="get">
                        {{--@csrf--}}
                        <div class="select-filter">
                            <div class="form-group">
                                <label class="mb-0">Organization</label>
                                <div class="input-icon">
                                    <select multiple="multiple" class="form-control selected_company multi-select"
                                            onchange="selectCompany()" name="company_id[]" id="company">

                                        @foreach($organization as $value)
                                            <option
                                                value="{{$value->id}}"{{in_array($value->id, old("company") ?: []) ? "selected" : ""}}>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    {{--<i class="icofont-building"></i>--}}
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>

                            <div class="form-group">
                                <label class="mb-0">Level</label>
                                <div class="input-icon">
                                    <select multiple="multiple" class="form-control multi-select" id="orglevels"
                                            name="level_id[]">

                                        @foreach($level as $value)
                                            <option
                                                value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    {{--<i class="icofont-building"></i>--}}
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label class="mb-0">State</label>
                                <div class="input-icon">
                                    <select multiple="multiple" class="form-control multi-select"
                                            id="statesGet" name="state_ids[]">
                                        <option value=" ">Please Select State</option>
                                        @foreach($state as $value)
                                            <option
                                                value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    {{--<i class="icofont-building"></i>--}}
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label class="mb-0">District</label>
                                <div class="input-icon">
                                    <select multiple="multiple"
                                            class="form-control"
                                            id="districtGet123" name="district_ids[]">
                                        <option value=" ">Please Select</option>
                                    </select>
                                    {{--<i class="icofont-building"></i>--}}
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label class="mb-0">City</label>
                                <div class="input-icon">
                                    <select class="form-control" id="cityGet"
                                            name="city_id[]">
                                        <option value=" ">Please Select</option>
                                    </select>
                                    {{--<i class="icofont-building"></i>--}}
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <button type="submit" class="post_btn light_purple ml-2">Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-7">
                <div class="bg-white shadow-sm profile search_inner" style="padding: 30px;">
                    <h3>Search Result</h3>
                    @foreach(@$records as $user)

                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <div class="post-describe d-flex align-items-center">
                                <div class="port-profile">
                                    <img src="{{$user->logo}}" class="img-fluid">
                                </div>
                                <div class="post-content">
                                    <h5>{{$user->full_name}}</h5>

                                     @if($user->company_name)
                                          <p>{{$user->company_name}}</p>
                                      @endif
                                    <p>{{$user->designation}}</p>
                                </div>
                            </div>
                            <div class="follow_chat col-md-1">
                                @if($user->id != Auth::user()->id)
                                    @foreach($user->userFollowers as $userFollower)
                                        @if($userFollower->id == Auth::user()->id)
                                            <a href="{{route('user.follow',$user->id)}}" class="text-blue">
                                                Unfollow
                                            </a>
                                        @endif
                                    @endforeach
                                    @if(!$user->userFollowers->contains(Auth::user()->id))
                                        <a href="{{route('user.follow',$user->id)}}" class="text-blue">
                                            Follow
                                        </a>
                                    @endif

                                @endif
                                @if($user->id != Auth::user()->id)
                                    @if($user->level_position >= $loggedInUser->level_position - 3)
                                        @if($user->is_chat_request == false)

                                            <a href="{{route('user.chatrequest',[$user->id,Auth::user()->id])}}"
                                               class="text-blue">Chat</a>
                                        @endif
                                        @foreach($chat as $chats)
                                            @if($chats->sender_id == Auth::user()->id && $user->id == $chats->receiver_id && $chats->status == 1)
                                                <a class="text-blue">Start Chat</a>
                                            @elseif($chats->sender_id == Auth::user()->id && $user->id == $chats->receiver_id && $chats->status == 0)
                                                <a class="text-blue">Request Sent</a>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif

                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="p-3">
                    @include('admin.elements.pagination.common')
                </div>
            </div>
        </div>
    </div>
    @include('advancesearch.script')
@endsection
