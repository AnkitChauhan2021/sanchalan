@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')

    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                <div class="user_about">
                    <h4>People want to connect</h4>

                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Name</th>
                                <th>Status</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if($records->count())
                                @foreach ($records as $key => $record)
                                    <tr>
                                        <td> {{$key+1}}</td>
                                        <td> {{$record->receiveChatRequest->full_name}}</td>
                                        <td>
                                            <a href="{{route('user.chataccept',$record->id)}}"><span class="text-warning">Pending</span></a>

                                        </td>
                                        {{--<td>
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                                   aria-expanded="false">
                                                    <span class="flaticon-more-button-of-three-dots"></span>
                                                </a>
                                                <div class="dropdown-menu ">
                                                        <a class="dropdown-item action_btn confirmDelete"
                                                           data-action="{{route('user.chatrequest.remove',$record->id)}}"
                                                           href="javascript:void(0);"><i
                                                                class="fas fa-trash text-danger"></i> delete</a>
                                                </div>
                                            </div>
                                        </td>--}}
                                    </tr>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="8">No Record Found!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <br/>
                </div>
                <div class="p-3">
                    @include('admin.elements.pagination.common')
                </div>
            </div>

        </div>
    </div>
@endsection
