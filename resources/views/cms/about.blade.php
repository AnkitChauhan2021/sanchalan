@extends('layouts.app')

@section('tiitle','About')

@section('content')
    @include('elements.layoutsElements.navbar')
    <section class="bg-yellow yellow_middle">
        <div class="middle_bar">
            <h3>About Us</h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">About Us</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="common_block about">
        <div class="container">
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <div class="about-img">
                        <img src="{{asset('public/home/img/about-inner.png')}}" class="img-fluid"/>
                    </div>
                </div>
                <div class="col-md-6 align-self-center">
                    <div class="about-content">
                        <p class="trust">Our core values are “Integrity and Trust”.</p>
                        <p>We solve our client's challenges and help them in their digital transformation by providing services in strategy, consulting, digital, technology and operations. We help our clients with range of services from innovative strategy development, to an architectural framework creation, to practical execution.</p>

                        <p>We help our clients digitally transform multiple front- and back-office business processes, including CX/CRM, SCM, ERP and Manufacturing Excellence. We are proficient in Blockchain, Salesforce.com, Infor, Intelligent Automation, RPA, IOT, Web Development and Application development.</p>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="vission d-flex align-items-center">
                        <div class="mission-icon"><img src="{{asset('public/home/img/v-1.png')}}"/></div>
                        <div class="">
                            <h3 class="heading2">Vision</h3>
                            <p>To be a company that provides “Transformation through Trust”</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="vission d-flex align-items-center">
                        <div class="mission-icon"><img src="{{asset('public/home/img/v-2.png')}}"/></div>
                        <div class="">
                            <h3 class="heading2">Mission</h3>
                            <p>To help our global customers achieve digital transformation by leveraging new technologies and IT platform.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('elements.layoutsElements.pages.feutures')
    @include('elements.layoutsElements.pages.aboutgoal')
@endsection
