@extends('layouts.app')


@section('title', $cms->title)

@section('content')




    <section>
        <div class="container">

        <h3>{{ $cms->title }}</h3>

            <div class="about-page">
                {!! $cms->content !!}
            </div>

        </div>
    </section>




@endsection
