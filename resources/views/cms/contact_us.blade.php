@extends('layouts.app')

@section('tiitle','Contact Us')

@section('content')
    @include('elements.layoutsElements.navbar')
    <?php  $config = App\Models\Config::first(); ?>
    <section class="bg-yellow yellow_middle">
        <div class="middle_bar">
            <h3>Contact Us</h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                </ol>
            </nav>
        </div>
    </section>
    <section class="common_block contact">
        <div class="container">
            <div class="row">
                <div class="left col-md-4">
                    <h2 class="heading1">{{$config->title}}</h2>
                    <p class="please_feel">{{$config->subtitle}}</p>
                    <div class="con_info">
                        <h6 class="co">Email:</h6>
                        <h5 class="fo"><a href="mailto:{{$config->email}}" target="_top">{{$config->email}}</a></h5>
                    </div>

                    <div class="con_info">
                        <h6 class="co">Phone:</h6>
                        <h5 class="fo phone">{{$config->contact_no}}</h5>
                    </div>

                    <div class="con_info">
                        <h6 class="co">Address:</h6>
                        <h5 class="fo">{{$config->address}}</h5>
                    </div>
                </div>
                <div class="right col-md-8">
                    <div id="loadingDiv"></div>
                    <form id="contact_form" method="post" action="{{route('contacts_inquiry.store')}}" class="banner_form">
                        @csrf
                        <div class="row">
                            @if(Auth::check())
                            @else
                            <div class="col-sm-6">
                                <div class="form_control">
                                    <label>Name</label>
                                    <div class="form_input">
                                        <input type="text" id="name" name="name" placeholder="Please Enter Your Name" autofocus/>
                                        <span id="contact_name" class="errors"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_control">
                                    <label>Email Address</label>
                                    <div class="form_input">
                                        <input type="text" name="email" id="email" placeholder="Please Enter Your Email Address"/>
                                        <span id="contact_email" class="errors"></span>
                                    </div>
                                </div>
                            </div>
                           @endif

                            <div class="col-sm-12">
                                <div class="form_control">
                                    <label>Subject</label>
                                    <div class="form_input">
                                        <input type="text" name="subject" id="subject"/>
                                        <span id="contact_subject" class="errors"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form_control text_control">
                                    <label>Message</label>
                                    <div class="form_input">
                                        <textarea name="body" id="query" placeholder="Please Enter Your Comments"></textarea>
                                        <span id="contact_query" class="errors"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" class="btn mt-3">Submit</button>
{{--                                <a href="javascript:submitForm();"  type="submit">Submit</a>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
