@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')

    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.companywall.index')
    {{--@php dd(Auth::user()->admin_company_id)@endphp--}}
    <div class="col-md-7">
        <div class="post">
                    <form action="{{route('user.companywallpost.update')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{$post->id}}" name="id">
                        <div class="write-post shadow-sm bg-white">
                            <p>Edit Post
                            </p>
                            @error('description')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>

                            </span>
                            @enderror
                            <div class="write">
                                <div class="profile-nav d-flex">
                                    <div class="profile-post">
                                        <img src="{{Auth::user()->logo}}" id="post_addImage" alt="postBYImage"/>
                                    </div>
                                    <div class="post-text">
                                <textarea placeholder="Type Here..."
                                          class="form-control post-text @error('description') is-invalid @enderror"
                                          rows="2" name="description">
                                    {{ $post->description}}
                                </textarea>
                                    </div>
                                </div>
                                <div class="who">
                                    <div class="paper-clip d-flex">
                                        <div class="photo">
                                            <div class="coustom-input coustom-input-new" title="upload your profile pic">

                                                <input type="file" class="custom-file-input" multiple id="gallery-photo-add"
                                                       accept="image/jpeg ,image/jpg,image/png,image/gif/*" name="image[]">
                                                <i class="icofont-camera"></i>
                                            </div>
                                            <div class="coustom-input coustom-input-new" title="upload video">
                                                <input type="file" class="custom-file-input file_multi_video" name="file[]"
                                                       accept="video/*"/>
                                                <i class="icofont-video-cam"></i>
                                            </div>
                                        </div>
                                    </div>
                                    @error('image')
                                    <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                    <div class="see-post">
                                        <button class="post_btn light_purple ml-2" id="disable">Post</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card" id="postImage" style="display:none">

                            <div class="post_videos d-flex">

                                <div class="video_divs">
                                    <video width="400" controls id="video-fluids" style="display:none">
                                        <source src="mov_bbb.mp4" id="video_here">
                                        Your browser does not support HTML5 video.
                                    </video>

                                </div>
                            </div>
                        </div>
                    </form>

            <div class="card" id="postImage" style="display:none">
                <div class="post_videos d-flex">
                    <div class="video_divs">
                        <video width="400" controls id="video-fluids" style="display:none">
                            <source src="" id="video_here">
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                </div>
            </div>

                    </div>
        </div>

        </section>

@endsection

