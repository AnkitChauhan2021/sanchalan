@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.companywall.index')
    @php
        $user_permissions=  CommonHelper::userPermissions();
    @endphp
    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                <div class="user_about">
                    <h4> Add Meeting</h4>
                    <div class=" float-right">
                        <div class="    form-group addbutton" style="margin-top: -76px;">
                            <a href="{{route('user.meetings.index',$company->id)}}"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i class="fas fa-caret-square-left fa-2x"></i></a>

                        </div>

                    </div>
                    <form class="new-added-form" action="{{route('user.companywallmeetings.store')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input id="company_id" name="company_id" type="hidden" value="{{$company->id}}">
                        <div class="row ">
                            <div class="col-lg-12 col-2 form-group mg-t-30">
                                <level >Description</level>
                                <textarea  class="form-control" rows="4" cols="120" name="description"></textarea>
                                {{--                            <input class="form-control" name="description" />--}}
                            </div>
                            @error('title')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                            @enderror
                            <div class="col-xl-6 col-12 form-group mt-4">
                                <label>Organization</label>
                                <select class="form-control  @error('companies') is-invalid @enderror " name="companies[]" multiple>
                                    <option value="">Please Select Organization</option>
                                    @if(Auth::user()->role_id == 3)
                                    @foreach ($companies as $company)
                                        <option value="{{@$company->id}}">{{@$company->name}}</option>
                                    @endforeach
                                        @endif
                                    @if(Auth::user()->role_id == 2)
                                        @if($user_permissions)
                                            @foreach($user_permissions as $permissions => $value)
                                                @if($value->permission_id == '6' && $company->id==$value->company_id )
                                                    <option value="{{@$value->company_id}}">{{@$company->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                                @error('level')
                                <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="col-xl-6 col-12 form-group mt-4">
                                <label>Meeting Level </label>
                                <select class="form-control   @error('levels') is-invalid @enderror " name="levels" >
                                    <option value="">Please Select Meeting Level</option>
                                    @foreach ($levels as $level)
                                        <option value="{{@$level->id}}">{{@$level->name}}</option>
                                    @endforeach
                                </select>
                                @error('levels')
                                <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>


                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="post_btn light_purple ml-2">Save
                                </button>
                                <button type="reset" class="post_btn light_purple ml-2 btn-hover-yellow">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
