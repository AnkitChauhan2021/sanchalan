@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.companywall.index')
    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                <div class="user_about">
                    <h4>Meetings {{"|"}}
                   <a href="{{route('user.companywallmeetings.recievedrequests',$company->id)}}">Recieved Meeting Requests</a></h4>
                    <div class=" float-right">
                        <div class="    form-group addbutton" style="margin-top: -76px;">

                            <a href="{{route('user.companywallmeetings.create',$company->id)}}"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                    class="fas fa-plus-circle fa-2x"></i></a>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Title</th>
                                <th>Create At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($records->count())
                                {{--@if(Auth::user()->role_id ==3)--}}
                                @foreach ($records as $key => $record)
                                    <tr>
                                        <td> {{$key+1}}</td>
                                        <td> {!!  \Illuminate\Support\Str::limit($record->description,50, $end='...')!!}</td>

                                        <td>
                                            {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                        <td>
                                            @if($record->user_id == Auth::user()->id)
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                                   aria-expanded="false">
                                                    <span class="flaticon-more-button-of-three-dots"></span>
                                                </a>
                                                <div class="dropdown-menu ">
                                                    <a class="dropdown-item"
                                                       href="{{route('user.companywallmeetings.view',[$record->meeting_code,$company->id])}}">
                                                        <i class="fas fa-eye"></i> View</a>
                                                    <a class="dropdown-item"
                                                       href="{{route('user.companywallmeetings.edit',[$record->meeting_code,$company->id])}}"> <i
                                                            class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                                    <a class="dropdown-item action_btn confirmDelete"
                                                       data-action="{{route('user.companywallmeetings.remove',[$record->meeting_code,$company->id])}}"
                                                       href="javascript:void(0);"><i
                                                            class="fas fa-trash text-danger"></i> delete</a>

                                                </div>
                                            </div>
                                        @endif
                                    </tr>
                                    </tr>
                                @endforeach
                           {{-- @elseif(Auth::user()->role_id ==2)

                                @foreach ($meetingforuser as $key => $record)
                                    <tr>
                                        <td> {{$key+1}}</td>
                                        <td> {!!  \Illuminate\Support\Str::limit($record->description,50, $end='...')!!}</td>

                                        <td>
                                            {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                        <td>
                                            @if($record->user_id == Auth::user()->id)
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                                   aria-expanded="false">
                                                    <span class="flaticon-more-button-of-three-dots"></span>
                                                </a>
                                                <div class="dropdown-menu ">
                                                    <a class="dropdown-item"
                                                       href="{{route('user.companywallmeetings.view',[$record->meeting_code,$company->id])}}">
                                                        <i class="fas fa-eye"></i> View</a>
                                                    <a class="dropdown-item"
                                                       href="{{route('user.companywallmeetings.edit',[$record->meeting_code,$company->id])}}"> <i
                                                            class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                                    <a class="dropdown-item action_btn confirmDelete"
                                                       data-action="{{route('user.companywallmeetings.remove',[$record->meeting_code,$company->id])}}"
                                                       href="javascript:void(0);"><i
                                                            class="fas fa-trash text-danger"></i> delete</a>

                                                </div>
                                            </div>
                                        @endif
                                    </tr>
                                    </tr>
                                @endforeach--}}
                                {{--@endif--}}
                                @else
                                <tr>
                                    <td class="text-center" colspan="8">No Record Found!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                    </div>
                <div class="p-3">
                    @include('admin.elements.pagination.common')
                </div>
                    <br/>
                </div>
            </div>

        </div>
    </div>
@endsection
