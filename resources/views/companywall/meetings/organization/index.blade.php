@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.companywall.index')

    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                <div class="user_about">
                    <h4>Meetings</h4>
                    <div class=" float-right">
                        <div class="    form-group addbutton" style="margin-top: -76px;">
                            <a href="{{route('user.meetings.index',$company->id)}}"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i class="fas fa-caret-square-left fa-2x"></i></a>
                            <a href="{{route('user.companywallmeetings.create',$company->id)}}"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                    class="fas fa-plus-circle fa-2x"></i></a>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Organization</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($records->count())
                                @foreach ($records as $key => $record)
                                    <tr>
                                        <td> {{$key+1}}</td>
                                        <td> {{$record->Organization->name}}</td>
                                        <td>
                                            @if($record->staus==0)
                                                <span class="text-warning">Pending</span>
                                            @elseif($record->staus==1)
                                                <spen class="text-success">Approved</spen>
                                            @else
                                                <span class="text-danger">
                                            Reject
                                        </span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                                   aria-expanded="false">
                                                    <span class="flaticon-more-button-of-three-dots"></span>
                                                </a>
                                                <div class="dropdown-menu ">
                                                    @if(Auth::user()->role_id == 3)
                                                    <a class="dropdown-item action_btn confirmDelete"
                                                       data-action="{{route('organization.meetings.organization.remove',$record->id)}}"
                                                       href="javascript:void(0);"><i
                                                            class="fas fa-trash text-danger"></i> delete</a>
                                                    @endif
                                                        @if(Auth::user()->role_id == 2)
                                                            <a class="dropdown-item action_btn confirmDelete"
                                                               data-action="{{route('user.companywallmeetings.organization.remove',$record->id)}}"
                                                               href="javascript:void(0);"><i
                                                                    class="fas fa-trash text-danger"></i> delete</a>
                                                        @endif
                                                </div>
                                            </div>
                                    </tr>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="8">No Record Found!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <br/>
                </div>
            </div>

        </div>
    </div>
@endsection
