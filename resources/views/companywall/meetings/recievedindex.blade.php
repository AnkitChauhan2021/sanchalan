@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.companywall.index')
    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                <div class="user_about">
                    <h4>Meetings {{"|"}}
                        <a href="{{route('user.companywallmeetings.recievedrequests',$company->id)}}">Recieved Meeting Requests</a></h4>
                    <div class=" float-right">
                        <div class="    form-group addbutton" style="margin-top: -76px;">
                            <a href="{{route('user.meetings.index',$company->id)}}"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i class="fas fa-caret-square-left fa-2x"></i></a>
                            <a href="{{route('user.companywallmeetings.create',$company->id)}}"
                               class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                    class="fas fa-plus-circle fa-2x"></i></a>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Title</th>
                                <th>Create At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($records->count())
                                @foreach ($records as $key => $record)

                                    <tr>
                                        <td> {{$key+1}}</td>
                                        <td> {{Str::limit($record->description,50)}}</td>

                                        <td>
                                            {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                        <td class="meetingsAction">
                                            {!! CommonHelper::approvedAndRejectUser('organization.meetings.status',$record->status,$record->id) !!}
                                        </td>
                                    </tr>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="8">No Record Found!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <br/>
                </div>
                <div class="p-3">
                    @include('admin.elements.pagination.common')
                </div>
            </div>

        </div>
    </div>
@endsection
