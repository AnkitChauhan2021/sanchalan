@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.companywall.index')
    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                <div class="user_about">
                    <h4>Create Polls</h4>
                        <form class="new-added-form" action="{{route('user.companywallpolls.store')}}" method="post">
                            @csrf
                            <input id="company_id" name="company_id" type="hidden" value="{{$company->id}}">
                            <div class="row">
                                <div class="col-xl-8 col-lg-6 col-12 form-group">
                                    <label>Question *</label>
                                    <label>
                                        <input type="text" placeholder="" class="@error('question') is-invalid @enderror form-control" name="question"/>
                                    </label>
                                    @error('question')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-xl-4 col-lg-6 col-12 form-group">
                                    <label>Status*</label>
                                    <label>
                                        <select class="form-control   @error('status') is-invalid @enderror " name="status">
                                            <option value=" ">Please Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </label>
                                    @error('status')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>

                                <div class="col-xl-6 col-lg-6 col-12 form-group">
                                    <label>Answer A *</label>
                                    <label>
                                        <input type="text" placeholder="" class="@error('Answer1') is-invalid @enderror form-control" name="Answer1"/>
                                    </label>
                                    @error('Answer1')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-12 form-group">
                                    <label>Answer B *</label>
                                    <label>
                                        <input type="text" placeholder="" class="@error('Answer2') is-invalid @enderror form-control" name="Answer2"/>
                                    </label>
                                    @error('Answer2')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-12 form-group">
                                    <label>Answer C *</label>
                                    <label>
                                        <input type="text" placeholder="" class="@error('Answer3') is-invalid @enderror form-control" name="Answer3"/>
                                    </label>
                                    @error('Answer3')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-12 form-group">
                                    <label>Answer D *</label>
                                    <label>
                                        <input type="text" placeholder="" class="@error('Answer4') is-invalid @enderror form-control" name="Answer4"/>
                                    </label>
                                    @error('Answer4')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-xl-6 col-lg-6 col-12 form-group">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="isShow">
                                        <label class="form-check-label">Do You Want to Show Results Of Users?</label>
                                    </div>
                                    @error('isShow')
                                    <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>



                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit" class="post_btn light_purple ml-2">Save</button>
                                    {{--<a href="{{route('admin.city.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>--}}
                                </div>
                            </div>
                        </form>
                    <br/>
                </div>
            </div>

        </div>
    </div>


@endsection
