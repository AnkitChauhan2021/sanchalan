<div class="middle-search">
    <div class="container">
        {{-- <div class="search-input">
             <input type="search" class="form-control" id='user_search' placeholder="Search Here...">
             <!--  <i class="icofont-search-1"></i> -->

         </div>--}}
    </div>
</div>
@php
    $user_permissions=  CommonHelper::userPermissions();
@endphp

<div class="container">
    <div class="row d-flex justify-content-center">

        <div class="col-md-3">
            <div class="left-nav shadow-sm bg-white">
                @if(!$company->cover_img == null)
                    <img src="{{$company->cover_img}}" class="img-fluid "/>
                @endif
                <div class="user-info text-center">
                    <div class="profile-pic">
                        @if(!$company->logo == null)
                            <img src="{{$company->logo}}" class="img-fluid add_profile_pic"/>
                        @endif
                    </div>
                    <h3>{{$company->name}}</h3>
                    @if($company->name_in_hindi)
                        <p>({{$company->name_in_hindi}})</p>
                    @endif
                </div>
                <div class="user-nav home-nav">
                    <ul>
                        @if(Auth::user()->role_id == 3)
                            <li><a href="{{route('user.companywallpolls.create',$company->id)}}"><i
                                        class="fas fa-user-lock"></i>Manage
                                    Poll</a></li>
                            <li><a href="{{route('user.meetings.index',$company->id)}}"><i class="fas fa-user-lock"></i>Manage
                                    Meeting</a></li>
                        @endif
                            @if($user_permissions)
                                @foreach($user_permissions as $permissions => $value)
                                    @if($value->permission_id == '2' && $company->id==$value->company_id )
                                        <li><a href="{{route('user.companywallpolls.create',$company->id)}}"><i class="fas fa-user-lock"></i>  Manage Poll</a></li>
                                    @endif
                                    @if($value->permission_id == '6' && $company->id==$value->company_id )
                                        <li><a href="{{route('user.meetings.index',$company->id)}}"><i class="fas fa-user-lock"></i>  Manage Meeting</a></li>
                                    @endif
                                        @if($value->permission_id == '1' && $company->id==$value->company_id )
                                            <li><a href="{{route('user.companywallpolls.create',$company->id)}}"><i class="fas fa-user-lock"></i>  Manage Poll</a></li>
                                            <li><a href="{{route('user.meetings.index',$company->id)}}"><i class="fas fa-user-lock"></i>  Manage Meeting</a></li>
                                        @endif
                                @endforeach
                            @endif
                    </ul>
                </div>
            </div>
        </div>

