

<footer>
    <div class="container">
        <div class="new_way">
            <div class="row">
                <div class="col-md-9 align-self-center">
                    <h3>New Way Of Staying Connected All Day.</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>
                @guest
                    @if (Route::has('register'))
                    <div class="col-md-3 text-right align-self-center">
                        <a href="{{route('welcome.index')}}" class="btn">REGISTER</a>
                    </div>
                @else
                <div class="col-md-3 text-right align-self-center">
                    <a href="{{route('welcome.index')}}" class="btn">REGISTER</a>
                </div>
                @endif
                @endguest
            </div>
        </div>
        <div class="footer-logo">
            <img src="{{asset('public/home/img/footer-logo.png')}}"/>
            <div class="footer_link">
{{--                <a href="{{route('welcome.index')}}">Home</a>--}}
{{--                <a href="{{route('about.index')}}">About Us</a>--}}
{{--                <a href="{{route('contact.index')}}">Contact Us</a>--}}
                <?php  $all_cms = App\Models\Cms::getAllCmsUrl(); ?>
                @foreach($all_cms as $cms)
                   <a href="{{config('app.url').$cms->url}}">{{$cms->title}}</a>
                @endforeach
            </div>
            <ul class="social-footer">
                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ request()->fullUrl() }}" class="grd_bg_hover"  target="_blank"><span class="icofont-facebook"></span></a></li>
                <li><a href="https://twitter.com/intent/tweet?url={{ request()->fullUrl() }}" class="grd_bg_hover" target="_blank"><span class="icofont-twitter"></span></a></li>
                <li><a href="https://plus.google.com/share?url={{ request()->fullUrl() }}" class="grd_bg_hover" target="_blank"><span class="icofont-google-plus"></span></a></li>
            </ul>
        </div>
        <div class="copy">
            <p>Copyright © 2021 Sanchlan. All Rights Reserved</p>
            <p>Powered by: <a href="#">Brain Technosys</a></p>
        </div>
    </div>
</footer>
