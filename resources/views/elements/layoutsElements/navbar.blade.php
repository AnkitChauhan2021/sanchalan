<div class="container">

<div class="top-menu">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="{{route('welcome.index')}}"><img src="{{asset('/public/home/img/logo.png')}}"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
            <i class="icofont-navigation-menu"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{route('welcome.index')}}">Home</a>
                </li> -->
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="{{route('about.index')}}">About Us</a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="{{route('contact.index')}}">Contact Us</a>--}}
{{--                </li>--}}
                <?php  $all_cms = App\Models\Cms::getAllCmsUrl(); ?>
                @foreach($all_cms as $cms)
                    <li class="nav-item "><a class="nav-link" href="{{config('app.url').$cms->url}}">{{$cms->title}}</a></li>
                @endforeach
                @if(Auth::check())
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->first_name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3 )
                            <a href="{{route('user.post.index')}}" class="dropdown-item"><i class="fas fa-user"></i> Profile</a>
                           @endif
                            @if(Auth::user()->role_id == 1)
                                <a href="{{url('/admin')}}" class="dropdown-item"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                            @elseif(Auth::user()->role_id == 3)
                                <a href="{{url('/organization')}}" class="dropdown-item"> <i class="fas fa-tachometer-alt"></i> Dashboard</a>
                            @endif

                            <a class="dropdown-item removeLocalStore" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                                                     document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i>  {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                        </div>



                    </li>

                @endif
            </ul>
        </div>
    </nav>
</div>
</div>


