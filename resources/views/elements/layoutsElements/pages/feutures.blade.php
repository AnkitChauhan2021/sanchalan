<section class="bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading text-center">
                    <h5>BEST FEATURES</h5>
                    <h3>Easy Process with Best Features</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features features_mobile">
                    <div class="features-content text-right">
                        <h4>Responsive web design</h4>
                        <p>Modular and interchangable componente between layouts and even demos.</p>
                    </div>
                    <div class="features-icon ml-4">
                        <img src="{{asset('public/home/img/icon-3.png')}}"/>
                    </div>
                </div>
                <div class="features features_mobile">
                    <div class="features-content text-right">
                        <h4>Loaded with features</h4>
                        <p>Modular and interchangable componente between layouts and even demos.</p>
                    </div>
                    <div class="features-icon ml-4">
                        <img src="{{asset('public/home/img/icon-4.png')}}"/>
                    </div>
                </div>
                <div class="features features_mobile">
                    <div class="features-content text-right">
                        <h4>Friendly online support</h4>
                        <p>Modular and interchangable componente between layouts and even demos.</p>
                    </div>
                    <div class="features-icon ml-4">
                        <img src="{{asset('public/home/img/icon-5.png')}}"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="middle-img text-center">
                    <img src="{{asset('public/home/img/mobile.png')}}" class="img-fluid"/>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features">
                    <div class="features-icon mr-4">
                        <img src="{{asset('public/home/img/icon-6.png')}}"/>
                    </div>
                    <div class="features-content">
                        <h4>Free updates forever</h4>
                        <p>Modular and interchangable componente between layouts and even demos.</p>
                    </div>
                </div>
                <div class="features">
                    <div class="features-icon mr-4">
                        <img src="{{asset('public/home/img/icon-7.png')}}"/>
                    </div>
                    <div class="features-content">
                        <h4>Built with Sass</h4>
                        <p>Modular and interchangable componente between layouts and even demos.</p>
                    </div>
                </div>
                <div class="features">
                    <div class="features-icon mr-4">
                        <img src="{{asset('public/home/img/icon-8.png')}}"/>
                    </div>
                    <div class="features-content">
                        <h4>Infinite colors</h4>
                        <p>Modular and interchangable componente between layouts and even demos.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
