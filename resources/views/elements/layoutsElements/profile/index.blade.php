    <div class="middle-search">
        <div class="container">
            <div class="search-input">
                <div class="position-relative">
                    <input type="search" class="form-control" id='user_search' placeholder="Search Here...">
                    <i class="icofont-search-1"></i>
                </div>
               <!--  <i class="icofont-search-1"></i> -->
                <a href="{{route('user.advance-search')}}" class="advance_search"><i class="icofont-search-job"></i> Advance Search</a>
            </div>
        </div>
    </div>
<style>
    .badge {
        top: -10px;
        right: -10px;
        padding: 4px 7px;;
        border-radius: 50%;
        background: red;
        color: white;
    }

</style>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-3">
                <div class="left-nav shadow-sm bg-white">
                    <img src="{{asset('public/home/img/user-bg.png')}}" class="img-fluid "/>
                    <div class="user-info text-center">
                        <div class="profile-pic"><img src="{{Auth::user()->logo}}"
                          class="img-fluid add_profile_pic" />

                         {{-- @if(Auth::user()->isApproved==1)
                          <i class="fas fa-check-double" title="Verified" ></i>

                          @elseif(Auth::user()->isApproved==0)
                          <i class="fas fa-sync-alt" title="Pending"></i>
                          @else
                          <i class="far fa-times-circle" title="Rejected"></i>
                          @endif--}}
                      </div>

                      <h3>{{Auth::user()->full_name}}</h3>
                      <p>{{Auth::user()->designation}}</p>
                  </div>
                  <!--   <div class="search_div">
                        <label class="top-search">
                            <input type="search" class="top-serach-form" placeholder="Search">
                            <i class="icofont-search-1"></i>
                        </label>
                    </div> -->
                    <div class="user-nav home-nav">
                        <ul>
                            <li><a href="{{route('user.profile.index')}}"><i class="icofont-user-alt-3"></i> My Profile</a></li>
                            <li><a href="{{route('user.trending')}}"><i class="fas fa-book-open"></i>Trending Posts</a></li>
                            <li><a href="{{route('user.chatreceived')}}"><i class="fas fa-bell">
                                    </i>Notification
                                @if(\App\Helpers\CommonHelper::chatcount()!==0)
                                    <span class= "badge">{{\App\Helpers\CommonHelper::chatcount()}}</span>
                                @endif
                                </a>
                            </li>
                            <li><a href="{{route('user.chat')}}"><i class="fas fa-envelope"></i>Message</a></li>
                            <li><a href="{{route('user.change.password')}}"><i class="fas fa-user-lock"></i>Password</a></li>
                            <!--  <li><a hr    {{route('user.magazine.index')}}"><i class="fas fa-book-open"></i>Magazine</a></li> -->
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout').submit();">
                                <i class="icofont-logout"></i> {{ __('Logout') }}
                            </a>

                            <form id="logout" action="{{ route('logout') }}" method="POST" >
                                @csrf
                            </form>

                        </li>

                    </ul>
                </div>
            </div>
        </div>

<script>  
 $(function() {  
   $( "#user_search" ).autocomplete({  
     source: function( request, response ) {  
      $.ajax({  
        url: "{{route('user.getUsers')}}", 
        type: 'post', 
        dataType: "json",  
        data: {  
            search: request.term  
        },  
        success: function( data ) { 
            response(data) 
        }
    });  
  },
    select: function( event, ui ) { 
            window.location.href ='{{env('APP_URL')}}/user/profile/info/'+ui.item.id;
        } 
}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {  
       return $( "<li></li>" )  
       .data( "item.autocomplete", item )  
       .append( "<a>" + "<img style='width:50px;height:50px; ' class='ui-autocomplete-row' src='" + item.logo + "' /> " + item.first_name+ " "+item.last_name+ "</a>" )
       .appendTo( ul );
   };  
});  

</script>
