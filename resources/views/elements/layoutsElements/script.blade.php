{{--<script src="https://code.jquery.com/jquery-3.6.0.min.js"--}}
{{--        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>--}}
<script src="{{asset('/public/home/js/lightgallery.min.js')}}"></script>
<script src="{{asset('/public/home/js/lightgallery-all.min.js')}}"></script>
{{--<script src="{{asset('/public/home/js/bootstrap.min.js')}}"></script>--}}
<link rel="stylesheet" href="{{asset('/public/home/css/jquery-confirm.min.css')}}">
<script src="{{asset('/public/admin/js/jquery-confirm.min.js')}}"></script>

<script src="{{asset('/public/admin/js/cropper.js')}}"></script>


<script src="{{asset('/public/home/js/coustom.js')}}"></script>
<script src="{{asset('/public/admin/js/confirm_delete.js')}}"></script>

<script src="{{asset('jqueryui/jquery-ui.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('js/reactComponent.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>


<script type="text/javascript">

    $('.removeLocalStore').on('click', function () {
        localStorage.removeItem("User_access_Token");
    })


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#login-form').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');


        mobile = $('#email').val();
        password = $('#password').val();


        $.ajax({
            url: "{{route('login')}}",
            type: "POST",
            data: {

                login: mobile,
                password: password,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {

                if (response.Token && response.userId) {

                    const person = {
                        token: response.Token,
                        userId: response.userId,
                    }
                    localStorage.setItem('User_access_Token', JSON.stringify(person))

                }
                console.log(response);

                // $('#Inactive_message').text(response.message);
                if (response.type === "error") {

                    toastr[response.type](response.message)
                } else {
                    toastr[response.type](response.message)
                    $("#login_model").modal('hide');
                    $('#contact-form').trigger('reset');
                    setTimeout(function () {
                        window.location.href = "{{url('/user/')}}";
                    }, 1500);


                }
            },
            error: function (response) {

                // $('#first_name-error').text(response.responseJSON.errors.first_name);
                // $('#password-error').text(response.responseJSON.errors.password);

            }
        });
    });


    {{-------------------------------------------------------register-step-one Script-----------------------------------}}

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#register-step-one').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');


        first_name = $('#first_name').val();
        last_name = $('#last_name').val();
        getMobile = $('#getMobile').val();
        getEmail = $('#getEmail').val();
        gender = $('#gender').val();


        $.ajax({
            url: "{{route('register.register-step-one')}}",
            type: "POST",
            data: {

                first_name: first_name,
                last_name: last_name,
                mobile: getMobile,
                email: getEmail,
                gender: gender,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {

                $("#forverifymobile").val(response.data.mobile);

                $('#Inactive_message').text(response.message);
                if (response.type === "error") {
                    // $('#wrong').text(response.message);
                    toastr[response.type](response.message)
                } else {
                    $("#forverifymobile").val(response.data.mobile);
                    toastr[response.type](response.message)
                    $(".first_step").hide();
                    $(".secound_step").show();


                }


            },
            error: function (response) {
                // console.log(response.responseJSON.error.email)
                $('#first_name-error').text(response.responseJSON.errors.first_name);
                $('#last_name-error').text(response.responseJSON.errors.last_name);
                $('#useremail-error').text(response.responseJSON.errors.email);
                $('#mobile-error').text(response.responseJSON.errors.mobile);
                $('#gender-error').text(response.responseJSON.errors.gender);

            }
        });
    });


    {{-------------------------------------------------------register-step-one Script-----------------------------------}}

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#register-step-two').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');


        first = $('#first').val();
        second = $('#second').val();
        third = $('#third').val();
        fourth = $('#fourth').val();
        mobile = $('.mobile').val();


        $.ajax({
            url: "{{route('register.register-step-two')}}",
            type: "POST",
            data: {

                first: first,
                second: second,
                third: third,
                fourth: fourth,
                mobile: mobile,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {


                $('#Inactive_message').text(response.message);
                if (response.type === "error") {
                    // $('#wrong').text(response.message);
                    toastr[response.type](response.message)
                } else {
                    $(".secound_step").hide();
                    $(".third_step").show();
                    toastr[response.type](response.message)

                    // $("#forverifymobile").text(response.data.mobile);
                }


            },
            error: function (response) {

                $('#email-error').text(response.responseJSON.errors.email);
                $('#password-error').text(response.responseJSON.errors.password);

            }
        });
    });

    {{-------------------------------------------------------register-step-third Script-----------------------------------}}




    function onselectOrganization() {
        const organization = document.getElementsByClassName('selected_organization');
        let levels = organization[0].value;
        if (levels != null) {
            $('#customCheckDisabled').removeAttr("disabled");
        }
        getLevels(levels);
    }

    function getLevels(levels) {
        $.ajax({
            type: "GET",
            url: "{{url('/register/getLevels')}}?organization_id=" + levels,
            success: function (res) {
                if (res) {
                    $(".byorganization").empty();
                    $(".byorganization").append('<option>Please Select level</option>');
                    $.each(res, function (key, value) {

                        $(".byorganization").append('<option value="' + value.levels.id + '">' + value.levels.name + '</option>');
                    });
                } else {
                    $(".byorganization").empty();
                }
            }
        });
    }

    // function onselectCountries() {
    //     const countries = document.getElementsByClassName('selected_countries_');
    //     var states = countries[0].value;

    // }
    $(document).ready(function () {
        getStates("1");
    });


    function getStates(states) {
        $.ajax({
            type: "GET",
            url: "{{url('/register/getStates')}}?country_id=" + states,
            dataType: "json",
            success: function (res) {
                if (res) {
                    $(".bycountrys").empty();
                    $(".bycountrys").append('<option>Please Select State</option>');
                    $.each(res, function (key, value) {

                        $(".bycountrys").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    $(".bycountry").empty();
                }
            }
        });
    }

    function onselectState() {
        const states = document.getElementsByClassName('selected_states_');
        var dirstict = states[0].value;
        getDistricts(dirstict);
    }

    function getDistricts(dirstict) {
        $.ajax({
            type: "GET",
            url: "{{url('/register/getDistricts')}}?State_id=" + dirstict,
            success: function (res) {
                if (res) {
                    $(".bystates").empty();
                    $(".bystates").append('<option>Please Select District</option>');
                    $.each(res, function (key, value) {

                        $(".bystates").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    $(".bystates").empty();
                }
            }
        });
    }

    function onselectDistrict() {
        const district = document.getElementsByClassName('selected_district_');
        var city = district[0].value;
        getCity(city);
    }

    function getCity(city) {
        $.ajax({
            type: "GET",
            url: "{{url('/register/getCity')}}?district_id=" + city,
            success: function (res) {
                if (res) {
                    $(".bydistrict").empty();
                    $(".bydistrict").append('<option>Please Select City</option>');
                    $.each(res, function (key, value) {

                        $(".bydistrict").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    $(".bydistrict").empty();
                }
            }
        });
    }

    {{-------------------------------------------------------register-step-third Ajax Script-----------------------------------}}


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#register-step-third').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');


        let designation = $('#designation').val();
        let organization = '';

        const want_to_join = $('.want_to_join').is(":checked");
        const want_join = $('.want_join').is(":checked");
        if (want_join) {
            organization = $('#organization_exist').val()
        }
        if (want_to_join) {
            organization = $('#organization').val()
        }

        let wantJoin = $('#customCheckDisabled').val();
        let level = $('#level').val();
        let country = $('#country').val();
        let state = $('#state').val();
        let district = $('#district').val();
        let city = $('#city').val();
        let user_password = $('#user_password').val();
        let password_confirmation = $('#password_confirmation').val();


        $.ajax({
            url: "{{route('register.register-step-third')}}",
            type: "POST",
            data: {

                designation: designation,
                organization: organization,
                wantJoin: wantJoin,
                level: level,
                country: country,
                state: state,
                district: district,
                city: city,
                password: user_password,
                password_confirmation: password_confirmation,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {

                $('#Inactive_message').text(response.message);
                if (response.type === "error") {
                    toastr[response.type](response.message)
                } else {
                    toastr[response.type](response.message)
                    $('#register-step-third')[0].reset();
                    setTimeout(function () {
                        window.location.href = "{{url('/user')}}";
                    }, 1000);

                }


            },
            error: function (response) {

                $('#designation-error').text(response.responseJSON.errors.designation);
                $('#organization-error').text(response.responseJSON.errors.organization);
                $('#level-error').text(response.responseJSON.errors.level);
                $('#country-error').text(response.responseJSON.errors.country);
                $('#state-error').text(response.responseJSON.errors.state);
                $('#district-error').text(response.responseJSON.errors.district);
                $('#city-error').text(response.responseJSON.errors.city);
                $('#user_password-error').text(response.responseJSON.errors.password);
                $('#password_confirmation-error').text(response.responseJSON.errors.password_confirmation);

            }
        });
    });
    {{-------------------------------------------------------reset_password Ajax Script-----------------------------------}}

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#reset_password').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');

        mobile = $('#resetPassword').val();

        $.ajax({
            url: "{{route('reset.password')}}",
            type: "POST",
            data: {

                mobile: mobile,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {

                $('#Inactive_message').text(response.message);
                if (response.type === "error") {
                    toastr[response.type](response.message)
                } else {
                    toastr[response.type](response.message)
                    $('#reset_password')[0].reset();
                    $("#forgetpassword_model").modal('hide');
                    $("#Otp_model").modal('show');

                    $("#forverifyotp").val(response.mobile);

                }
            },
            error: function (response) {

                $('#forgetPasswordMobile-error').text(response.responseJSON.errors.mobile);


            }
        });
    });
    {{-------------------------------------------------------reset_password Ajax Script-----------------------------------}}

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#get_verify_otp').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');

        otp_verified = $('#otp_verified').val();
        forverifyotp = $('.forverifyotp').val();

        $.ajax({
            url: "{{route('get_verify.Otp')}}",
            type: "POST",
            data: {

                otp: otp_verified,
                mobile: forverifyotp,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {

                $('#Inactive_message').text(response.message);
                if (response.type === "error") {
                    toastr[response.type](response.message)
                } else {
                    toastr[response.type](response.message)

                    $('#get_verify_otp')[0].reset();
                    $("#Otp_model").modal('hide');
                    $("#createPassword_model").modal('show');
                    $("#createPassword").val(response.mobile);
                    $("#verified_token").val(response.verified_token);

                }
            },
            error: function (response) {

            }
        });
    });
    {{-------------------------------------------------------createPasswordForm Ajax Script-----------------------------------}}

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#createPasswordForm').on('submit', function (event) {
        event.preventDefault();
        $('#email-error').text('');
        $('#password-error').text('');

        new_password = $('#new_password').val();
        confirm_new_password = $('#confirm_new_password').val();
        createNewPassword = $('.createNewPassword').val();
        verified_token = $('.verified_token').val();

        $.ajax({
            url: "{{route('create.password')}}",
            type: "POST",
            data: {

                password: new_password,
                verified_token: verified_token,
                password_confirmation: confirm_new_password,
                mobile: createNewPassword,
                "_token": "{{ csrf_token() }}",
            },
            success: function (response) {

                $('#Inactive_message').text(response.message);
                if (response.type === "error") {
                    toastr[response.type](response.message)
                } else {
                    toastr[response.type](response.message)
                    $('#createPasswordForm')[0].reset();
                    $("#createPassword_model").modal('hide');
                }
            },
            error: function (response) {
                toastr['error'](response.responseJSON.message)
                // console.log(response.responseJSON.message)
                $('#passwordChange-error').text(response.responseJSON.errors.password);
                $('#confirmPassword-error').text(response.responseJSON.errors.password_confirmation);


            }
        });
    });

</script>
<script>
    const OpenProfileImage = () => {
        $('.profileImage').trigger('click');
    }
</script>
<script>

    const $modal = $('#modal');
    const image = document.getElementById('image');
    let cropper;

    $("body").on("change", ".image", function (e) {
        var files = e.target.files;

        var done = function (url) {

            image.src = url;
            // console.log(files.src)
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;

        if (files && files.length > 0) {
            file = files[0];

            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                // console.log(reader)
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function () {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            viewMode: 1,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
    });

    $("#crop").click(function () {
        canvas = cropper.getCroppedCanvas({
            width: 160,
            height: 160,
        });

        canvas.toBlob(function (blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
                var base64data = reader.result;

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{route('user.profile.image.update')}}",
                    data: {'_token': $('meta[name="_token"]').attr('content'), 'image': base64data},
                    success: function (data) {
                        $modal.modal('hide');
                        $('.profile_image').attr('src', data.url)
                        $('.add_profile_pic').attr('src', data.url)
                        toastr[data.type](data.message)
                    }
                });
            }
        });
    })

</script>


{{-------------------------User profile add post image and video preview------------------}}

<script>
    $(".post-text").click(function () {
        $(".post_btn").removeClass("light_purple");
    });

    $(".like-commeny").click(function () {
        $(this).parent().parent().toggleClass("blue");
    });
</script>
<script>
    $(function () {
        // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (let i = 0; i < filesAmount; i++) {
                    let reader = new FileReader();

                    reader.onload = function (event) {

                        $($.parseHTML('<img class="img-fluids"  >')).attr('id', i).attr('src', event.target.result).appendTo(placeToInsertImagePreview);

                        $($.parseHTML('<a href="javascript:void(0)"  class="remove_imagebythat"><i class="far fa-times-circle" "></i></a>')).attr('data-id', i).appendTo(placeToInsertImagePreview)
                    }

                    $('#img-fluids').css('display', 'block');
                    $('#postImage').css('display', 'block');
                    reader.readAsDataURL(input.files[i]);

                }
            }

        };

        $('#gallery-photo-add').on('change', function () {
            imagesPreview(this, 'div.video_divs');
        });
    });
    $('.video_divs').on('click', '.remove_imagebythat', function () {
        const id = $(this).data('id');
        $("#" + id).remove();
        $(this).remove();
        console.log(id);
    });
    $(document).on("change", ".file_multi_video", function (evt) {
        let $source = $('#video_here');
        $source[0].src = URL.createObjectURL(this.files[0]);
        $source.parent()[0].load();
        let _size = this.files[0].size;


        let fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
            i = 0;
        while (_size > 900) {
            _size /= 1024;
            i++;
        }
        let exactSize = (Math.round(_size * 100) / 100);
        let file_size = Math.round(exactSize) + ' ' + fSExt[i];
        console.log(file_size);


        if ((file_size < "25 MB") || file_size < "25600 KM") {
            toastr["error"]("Oops!, Video is Too Large ," + ' ' + file_size + ' ' + "Maximum upload Size 25 MB");

            $('#video-fluids').css('display', 'none');
            $("form").submit(function (e) {
                toastr["error"]("Oops!, Video is Too Large " + ' ' + file_size + ' ' + "Maximum upload Size 25 MB")
                e.preventDefault(e);
            });
        } else {
            $('#postImage').css('display', 'block');
            $('#video-fluids').css('display', 'block');
        }

    })
    //     like dislike


    $('.likes_and_dislikes').click(function () {
        let id = $(this).data('id');
        let count = $(this).find('.like_count').data('id');


        let _this = $(this);
        $.ajax({
            type: 'get',
            url: '{{url('user/likes/')}}',
            data: {id: id},
            success: function (res) {

                if (res.data == "like") {
                    _this.find('i').addClass('text-primary');
                    _this.find('.like_count').text(count + 1);
                } else {
                    _this.find('i').removeClass('text-primary');
                    _this.find('.like_count').text(count - 1);
                }
            }
        });
    });
    // submit Polls

    $(".poll_submit").on('change', function () {
        let no_Of_Answer = $(this).closest('div').children().data('id');
        let add_new_ans = no_Of_Answer + 1;
        let totalAnswer = $(this).closest('div').children().data('value');
        let new_total_ans = totalAnswer + 1
        let calculate = add_new_ans / new_total_ans
        let total = calculate * 100;
        let grand_total = total.toFixed(2)
        $(this).closest('div').children().children().children().text(grand_total + "%")
        $(this).closest('div').children().find('.progress-bar').css('width', grand_total + "%")

        let answer = $(this).val();
        let id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: '{{url('user/submit/poll/')}}',
            data: {pole_id: id, answer: answer},
            success: function (res) {

                if (res.status == 1) {
                    $("#" + id).find('input').prop('disabled', true);

                }
            }
        });
    });

</script>
