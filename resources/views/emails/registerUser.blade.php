<html lang='en'>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags -->
    <title>{{config('app.name')}}</title>
</head>
<body>
{{--@php--}}
{{--    $setting=\App\Model\Setting::query()->first();--}}
{{--@endphp--}}
<table cellpadding='0' cellspacing='0' border='0'
       style='margin:10px auto 0; width:500px; max-width:500px; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:20px;border:1px solid #ccc;border-bottom:none;'>
    <tr>
        <td colspan="2">
            <img src="{{asset('public/home/img/logo.png')}}" style=' width:150px;display: block;margin: auto;padding:15px 0px;'/>
        </td>
    </tr>
    <tr>
        <td style="padding: 30px 30px 0px;" colspan="2">
            <h5 style="margin: 0px;font-size: 15px;color: #222;">Hi {{Auth::user()->full_name}},</h5>
            <div style="margin: 4px 0 4px;color: #555;">
                <div>We would like to welcome you to {{config('app.name')}}.</div>
                <div>Your account has been activated. Please click the below link to login.</div>
                <div style="margin-top: 15px">
                    <strong>Email: </strong>{{Auth::user()->email}}
                    <br/>
                    <strong>Phone: </strong>{{Auth::user()->mobile}}
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 30px 30px 0px;" colspan="2">
            <a href="{{config('app.url')}}" style="display: block;margin-top: 10px;">Login</a>
            <h5 style="font-size: 16px;font-weight: 600;margin: 30px 0px 0px;">Thanks</h5>
            <p style="margin: 6px 0 0;color: #555;">Team {{config('app.name')}}</p>
        </td>
    </tr>
</table>
</body>
</html>
