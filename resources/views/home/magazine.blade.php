@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
{{--  Home Page Mune Include    --}}
@include('elements.layoutsElements.navbar')
{{--  User Side menu page include   --}}

@include('elements.layoutsElements.profile.index')
<div class="col-md-7 ">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-7">
				<div class="post">
					<div class="write-post shadow-sm bg-white">
						@foreach ($magazines as $key => $magazine)
						<div class="main-post magazine">
							@foreach($magazine->images as $key =>$getImage)
							@if($getImage->url != " ")
							<div class="content-img">
								<a href="{{route('user.magazine_detail.index',$magazine->id)}}"><img src="{{$getImage->url}}" class="img-fluid"></a>
							</div>
							@endif
							@endforeach
							<div class="post-content-bottm">
								{{$magazine->title}}
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection