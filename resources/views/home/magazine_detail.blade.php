@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
{{--  Home Page Mune Include    --}}
@include('elements.layoutsElements.navbar')
{{--  User Side menu page include   --}}

@include('elements.layoutsElements.profile.index')
<div class="col-md-7 ">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-7">
				<div class="post">
					<div class="write-post shadow-sm bg-white">
						@foreach ($magazines as $key => $magazine)
						<div class="main-post magazine">
							@foreach($magazine->images as $key =>$getImage)
							@if($getImage->url != " ")
							<div class="content-img">
								<img src="{{$getImage->url}}" class="img-fluid">
							</div>
							@endif
							@endforeach
							<div class="post-content-bottm">
								<b>{{$magazine->title}}</b>
							</div>
							&nbsp;
							<div class="post-content-bottm">
								<?php print($magazine->description);?>
							</div>
							@endforeach
						</div>
								<div class="like-comment d-flex align-items-center">
								<div class="like like-new active position-relative d-flex">
									<i class="icofont-thumbs-up"></i> Like <div class="like-count" data-toggle="modal" data-target="#exampleModal">19</div>
								</div>
								<div class="like">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="22" viewBox="0 0 24 22">
									  <g id="comment" transform="translate(-0.001 1.332)">
										<path id="Path_1" data-name="Path 1" d="M20.7-1.332H3.3A3.3,3.3,0,0,0,0,1.956v10.61a3.3,3.3,0,0,0,3.288,3.288v4.815l6.945-4.815H20.7A3.3,3.3,0,0,0,24,12.566V1.956a3.3,3.3,0,0,0-3.3-3.288Zm1.893,13.9A1.892,1.892,0,0,1,20.7,14.452H9.792l-5.1,3.534V14.452H3.3a1.892,1.892,0,0,1-1.893-1.886V1.955A1.892,1.892,0,0,1,3.3.069H20.7a1.892,1.892,0,0,1,1.893,1.886Zm0,0"/>
										<path id="Path_2" data-name="Path 2" d="M171.293,131.172h8.946V132.3h-8.946Zm0,0" transform="translate(-163.765 -127.488)"/>
										<path id="Path_3" data-name="Path 3" d="M171.293,211.172h8.946V212.3h-8.946Zm0,0" transform="translate(-163.765 -204.459)"/>
										<path id="Path_4" data-name="Path 4" d="M171.293,291.172h8.946V292.3h-8.946Zm0,0" transform="translate(-163.765 -281.43)"/>
									  </g>
									</svg> Comment
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection