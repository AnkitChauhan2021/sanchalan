<html>
<head>
    @include('admin.elements.layouts.head')
    @include('admin.elements.layouts.style')
</head>
<body>
@include('admin.elements.flash')
@include('admin.elements.layouts.header')
@include('admin.elements.layouts.sidebar')
    @yield('content')


@include('admin.elements.layouts.footer')
@include('admin.elements.layouts.script')
</body>
</html>
