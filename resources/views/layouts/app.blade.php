<Html>
<head>
	

    @include('elements.layoutsElements.head')
    @include('elements.layoutsElements.style')
</head>
<body>
@include('elements.layoutsElements.flash')


@include('elements.layoutsElements.password.forgetpassword')
@include('elements.layoutsElements.password.createPassword')
@include('elements.layoutsElements.password.otp')
@include('elements.layoutsElements.login')


<section class="p-0">
	@yield('content')
</section>
@include('elements.layoutsElements.footer')
@include('elements.layoutsElements.script')

<script type="text/javascript">
    @if(!empty(Session('error_code')))
    $('#login_model').modal('show');
    @endif
</script>

</body>
</Html>


