<html>
<head>
    @include('organization.elements.layouts.head')
    @include('organization.elements.layouts.style')
</head>
<body>
@include('organization.elements.flash')
@include('organization.elements.layouts.header')
@include('organization.elements.layouts.sidebar')
    @yield('content')
@include('organization.elements.layouts.footer')
@include('organization.elements.layouts.script')
</body>
</html>
