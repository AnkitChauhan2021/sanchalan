@extends('layouts.organization.default')

@section('content')

    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Login Page Start Here -->
    <div class="login-page-wrap">
        <div class="login-page-content">
            <div class="login-box">
                <div class="item-logo">
{{--                    <h4> Organization</h4>--}}
                    <img src="{{asset('/public/admin/img/logomain.png')}}" alt="logo">
                </div>
                <form action="{{route('organization.organizationlogin')}}" class="login-form" method="post">
                    @csrf
                   <div class="form-group">
                        <label>Email Or Phone</label>
                        <input type="text" placeholder="Enter Email Or Phone" name="email"
                               class="form-control @error('mobile') is-invalid @enderror" value="{{ old('mobile') }}"
                               required autocomplete="mobile" autofocus>
                        <i class="far fa-envelope"></i>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Enter password" name="password"
                               class="form-control @error('password') is-invalid @enderror" required
                               autocomplete="current-password">
                        <i class="fas fa-lock"></i>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group d-flex align-items-center justify-content-between">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="remember"
                                   id="remember-me" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember-me" class="form-check-label">Remember Me</label>
                        </div>
                        @if (Route::has('organization.password.request'))
                            <a class="forgot-btn" href="{{ route('organization.password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="login-btn">Login</button>
                    </div>
                </form>
            </div>
        </div>

@endsection
