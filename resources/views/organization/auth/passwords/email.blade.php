@extends('layouts.organization.default')

@section('content')

    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Login Page Start Here -->
    <div class="login-page-wrap">
        <div class="login-page-content">
            <div class="login-box">
                <div class="item-logo">
                    <img src="{{asset('/public/admin/img/logomain.png')}}" alt="logo">
                </div>
                <form action="{{ route('organization.password.email') }}" class="login-form" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" placeholder="Enter Email" name="email"
                               class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                               required autocomplete="email" autofocus>
                        <i class="far fa-envelope"></i>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <button type="submit" class="login-btn">Send Password Reset Link</button>
                    </div>
                </form>
            </div>
        </div>

@endsection
