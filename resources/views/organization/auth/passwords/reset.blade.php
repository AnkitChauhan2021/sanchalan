@extends('layouts.organization.default')

@section('content')


    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Login Page Start Here -->
    <div class="login-page-wrap">
        <div class="login-page-content">
            <div class="login-box">
                <div class="item-logo">
                    <img src="{{asset('/public/admin/img/logomain.png')}}" alt="logo">
                </div>
                <form action="{{ route('organization.password.update') }}" class="login-form" method="post">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label>E-Mail Address</label>
                        <input type="email" placeholder="Enter Email" name="email"
                               class="form-control @error('email') is-invalid @enderror" value="{{ $email ?? old('email') }}""
                               required autocomplete="email" autofocus>
                        <i class="far fa-envelope"></i>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label>{{ __('Password') }}</label>
                        <input type="password" placeholder="Password" name="password"
                               class="form-control @error('Password') is-invalid @enderror" value="{{ old('Password') }}"
                               required autocomplete="new-password">
                        <i class="far fa-envelope"></i>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label>{{ __('Confirm Password') }}</label>
                        <input type="password" placeholder="Confirm Password" name="password_confirmation"
                               class="form-control"
                                required autocomplete="new-password">
                        <i class="far fa-envelope"></i>

                    </div>
                    <div class="form-group">
                        <button type="submit" class="login-btn">{{ __('Reset Password') }}</button>
                    </div>
                </form>
            </div>
        </div>
































@endsection
