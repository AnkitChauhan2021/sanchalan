@extends('layouts.organization.app')

@section('title',"Dashboard")

@section('header',"Dashboard")

@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Organization Dashboard</h3>
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Organization</li>
            </ul>
        </div>

        <div class="row gutters-20">

            <div class="col-xl-3 col-sm-6 col-12">
                <div class="dashboard-summery-one mg-b-20">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="item-icon bg-light-blue">
                                <i class="flaticon-multiple-users-silhouette text-blue"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">Users</div>
                                <div class="item-number"><span class="counter" data-num="{{count($users)}}">{{count($users)}}</span></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12">
                <div class="dashboard-summery-one mg-b-20">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="item-icon bg-light-blue">
                                <i class="fas fa-comment-alt text-warning" style="margin-top: 22px;"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">Posts</div>
                                <div class="item-number"><span class="counter" data-num="{{$Posts}}">{{$Posts}}</span></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12">
                <div class="dashboard-summery-one mg-b-20">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="item-icon bg-light-yellow">
                                <i class="fas fa-poll text-success" style="margin-top: 22px;"></i>
{{--                                <i class="fas fa-comment-alt" style="margin-top: 22px;"></i>--}}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">Polls</div>
                                <div class="item-number"><span class="counter" data-num="{{$polls}}">{{$polls}}</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-12">
                <div class="dashboard-summery-one mg-b-20">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="item-icon bg-light-yellow">
                                <i class="far fa-handshake" style="margin-top: 22px;"></i>

                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">Meetings</div>
                                <div class="item-number"><span class="counter" data-num="{{count($records)}}">{{count($records)}}</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Up Coming Meeting</h3>
                    </div>
                </div>
                @include('organization.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('organization.meetings.create')}}"
                           class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                class="fas fa-plus-circle fa-2x"></i></a>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Title</th>
                            <th>Create At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td> {!!  \Illuminate\Support\Str::limit($record->description,50, $end='...')!!}</td>

                                    <td>
                                        {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                    <td class="meetingsAction">
                                        {!! CommonHelper::approvedAndRejectUser('organization.meetings.status',$record->status,$record->id) !!}
                                    </td>
                                </tr>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @include('organization.elements.pagination.common')
            </div>
        </div>
        <div class="modal fade job_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true"
             style="min-height:300px;transition: transform 1s ease-out,-webkit-transform 1s ease-out;transition:all ease 1s;">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body job-description-popup">
                        <h1 id="state_name"></h1>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-confirm" style="width: 400px;">
                <div class="modal-content" style="margin-top: 135px">
                    <div class="modal-header flex-column">

                        <h4 class="modal-title w-100">Are You Sure?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center " >
                        {!! CommonHelper::ApprovedUser('organization.meetings.status',"","") !!}
                        <hr/>
                        <div>

                        </div>
                    </div>
                </div>



@endsection
