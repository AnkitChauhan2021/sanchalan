<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>@yield('title',config('app.name'),'Organization')</title>
<meta name="_token" content="{{ csrf_token() }}">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
