<!-- Modernize js -->
<script src="{{asset('/public/organization/js/modernizr-3.6.0.min.js')}}"></script>
<!-- Plugins js -->
<script src="{{asset('/public/organization/js/plugins.js')}}"></script>
<!-- Popper js -->
<script src="{{asset('/public/organization/js/popper.min.js')}}"></script>
<!-- Bootstrap js -->
<script src="{{asset('/public/organization/js/bootstrap.min.js')}}"></script>
<!-- Counterup Js -->
<script src="{{asset('/public/organization/js/jquery.counterup.min.js')}}"></script>
<!-- Moment Js -->
<script src="{{asset('/public/organization/js/moment.min.js')}}"></script>
<!-- Waypoints Js -->
<script src="{{asset('/public/organization/js/jquery.waypoints.min.js')}}"></script>
<!-- Scroll Up Js -->
<script src="{{asset('/public/organization/js/jquery.scrollUp.min.js')}}"></script>
<!-- Full Calender Js -->
<script src="{{asset('/public/organization/js/fullcalendar.min.js')}}"></script>
<!-- Chart Js -->
<script src="{{asset('/public/organization/js/Chart.min.js')}}"></script>
<!-- Custom Js -->
<script src="{{asset('/public/organization/js/main.js')}}"></script>
{{--custom js--}}
<script src="{{asset('/public/organization/js/cropper.js')}}" ></script>
{{--Cropper--}}
<script src="{{asset('/public/organization/js/select2.min.js')}}"></script>
{{--select--}}
<script src="{{asset('public/organization/js/bootstrap-select.min.js')}}"></script>
{{--datepicker--}}
<script src="{{asset('/public/organization/js/datepicker.min.js')}}"></script>
{{--scrollUp--}}
<script src="{{asset('/public/organization/js/jquery.scrollUp.min.js')}}"></script>
{{--confirm--}}
<script src="{{asset('/public/organization/js/jquery-confirm.min.js')}}"></script>
{{--confirm_delete--}}
<script src="{{asset('/public/organization/js/confirm_delete.js')}}"></script>
{{--nprogress--}}
<script src="{{ asset('/public/organization/js/nprogress.js') }}"></script>
{{--custom--}}
<script src="{{asset('/public/organization/custom.js')}}"></script>
{{--custom--}}
<script src="{{asset('/public/organization/js/custom.js')}}"></script>
<script src="{{asset('/public/organization/js/ckeditor.js')}}"></script>



