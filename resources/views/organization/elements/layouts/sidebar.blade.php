<!-- Page Area Start Here -->
<div class="dashboard-page-one" >
    <!-- Sidebar Area Start Here -->
    <div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color" >
        <div class="mobile-sidebar-header d-md-none">
            <div class="header-logo">
                <a href="index.html"><img src="{{asset('public/admin/img/logo1.png')}}" alt="logo"></a>
            </div>
        </div>
{{--        id="sticky-sidebar"--}}
        <div class="sidebar-menu-content" >
            <ul class="nav nav-sidebar-menu sidebar-toggle-view"  >
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.dashboards.index')}}" class=" sidebar_menu nav-link"><i class="flaticon-dashboard"></i><span>Dashboard</span></a>
                </li>

{{-- -------------------------     Organization    -------------------      --}}

                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.posts.index')}}" class=" sidebar_menu nav-link">
                        <i class="far fa-comment-alt"></i><span>Posts</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.polls.index')}}" class=" sidebar_menu nav-link">
                        <i class="fas fa-poll"></i><span>Polls</span></a>
                </li>
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.meetings.index')}}" class=" sidebar_menu nav-link">
                        <i class="far fa-handshake"></i><span>Meetings</span></a>
                </li>
               <!--  <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.magazines.index')}}" class=" sidebar_menu nav-link">
                        <i class="fas fa-blog"></i><span>Magazines</span></a>
                </li> -->
               {{-- <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.users.index')}}" class=" sidebar_menu nav-link">
                        <i class="far fa-comment-dots"></i><span>Groups</span></a>
                </li>--}}
                <li class="nav-item sidebar-nav-item">
                    <a href="{{route('organization.users.index')}}" class=" sidebar_menu nav-link"><i
                            class="flaticon-user"></i><span>Users</span></a>
                </li>

            </ul>
        </div>
    </div>
    <!-- Sidebar Area End Here -->
