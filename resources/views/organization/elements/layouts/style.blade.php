<link rel="shortcut icon" type="image/x-icon" href="{{asset('/public/organization/img/favicon.ico')}}">
<!-- Normalize CSS -->
<link rel="stylesheet" href="{{ asset('/public/organization/css/nprogress.css') }}">
<link rel="stylesheet" href="{{asset('/public/organization/css/normalize.css')}}">
<!-- Main CSS -->
<link rel="stylesheet" href="{{asset('/public/organization/css/main.css')}}">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('/public/organization/css/bootstrap.min.css')}}">
{{--<script src="{{asset('/public/organization/js/jquery-3.3.1.min.js')}}"></script>--}}
<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{asset('/public/organization/css/all.min.css')}}">
<!-- Flaticon CSS -->
<link rel="stylesheet" href="{{asset('/public/organization/fonts/flaticon.css')}}">
<!-- Full Calender CSS -->
<link rel="stylesheet" href="{{asset('/public/organization/css/fullcalendar.min.css')}}">
<!-- Animate CSS -->
<link rel="stylesheet" href="{{asset('/public/organization/css/animate.min.css')}}">
<!-- Custom CSS -->

<link rel="stylesheet" href="{{ asset('/public/organization/toastr/build/toastr.min.css') }}">
<!-- jquery-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>

<script src="{{asset('/public/organization/js/smooth-submit.js')}}"></script>
<!-- toastr-->
<script src="{{ asset('public/organization/toastr/build/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{asset('/public/organization/custom.css')}}">
<link rel="stylesheet" href="{{asset('/public/organization/css/cropper.css')}}"/>

<link rel="stylesheet" href="{{asset('/public/organization/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/organization/style.css')}}">
<link rel="stylesheet" href="{{asset('/public/organization/css/datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/organization/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('/public/organization/css/jquery-confirm.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/organization//css/all.css')}}"/>
<style>
    .text-danger {
        font-size: 12px;
        color: #dc3545 !important;
    }

    .post_title h1,h2,h3,h4,h5,h6,p {
    font-size: medium;
    }
    .meetingsAction{
        display: flex;
        align-items: center;
        justify-content: space-evenly;
    }

</style>

