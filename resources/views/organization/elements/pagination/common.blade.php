<div class="row">
	<div class="col-sm-12 col-md-5 pl-4">
		<div class="dataTables_info" id="bs4-table_info" role="status" aria-live="polite">
			Page {{ $records->currentPage() }} of {{ $records->lastPage() }}, showing {{ $records->count() }} record(s) out of {{ $records->total() }} total
		</div>
	</div>

    <nav class="ml-auto float-right" aria-label="...">
        <ul class="pagination">
            <li class="page-item {{($records->currentPage()==1)?'disabled':''}}">
                <a class="page-link"
                   href='{{($records->previousPageUrl())?$records->previousPageUrl():url()->current()}}'
                   tabindex="-1">Previous</a>
            </li>
            @if($records->lastPage())
                @for($i=$records->currentPage() > 3 ? $records->currentPage() - 2 : 1; $i <=  $records->currentPage() + 2 && $i <= $records->lastPage() ; $i++)
                    <li class="page-item {{($records->currentPage()==$i)?'active':''}}"><a
                            class="page-link"
                            href="{{$records->url($i)}}">{{$i}}</a></li>
                @endfor
            @endif
            <li class="page-item {{($records->lastPage()==$records->currentPage())?'disabled':''}}">
                <a class="page-link" href="{{$records->nextPageUrl()}}">Next</a>
            </li>
        </ul>
    </nav>
</div>

{{--
  <div class="row">
	<div class="col-sm-12 col-md-5 pl-4">
		<div class="dataTables_info" id="bs4-table_info" role="status" aria-live="polite">
			Page {{ $records->currentPage() }} of {{ $records->lastPage() }}, showing {{ $records->count() }} record(s) out of {{ $records->total() }} total
		</div>
	</div>
	<div class="col-sm-12 col-md-7">
		<div class="dataTables_paginate paging_simple_numbers float-right" id="bs4-table_paginate">
			{!! $records->appends(Request::except('page'))->links() !!}
		</div>
	</div>
</div>  --}}
