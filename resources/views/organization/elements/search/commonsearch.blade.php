<form method="get" action="{{ url()->current() }}" id="AjaxSearch">

    @section('search')
        <div class="row ">
            <div class="col-auto  form-group">
                <select class="select2 seclected_dist" id="LimitOptions" name="limit">
                    @php $options = CommonHelper::getLimitOption(); @endphp
                    @foreach($options as $key=>$value)
                        <option {{ request('limit',env('PAGINATION_LIMIT')) == $value ? 'selected' : '' }} value="{{ $value }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            @if(request()->route()->getName()==='organization.posts.index')
                <div class="col-lg-2">
                    <select class="select2" name="type">
                        <option value="">All</option>
                        <option value="personal" {{ request('type')==='personal'?'selected':'' }} >Personal</option>
                        <option value="company" {{ request('type')==='company'?'selected':'' }}>Company</option>
                    </select>
                </div>
            @endif
            <div class="{{request()->route()->getName()==='organization.posts.index'?'col-lg-5':'col-lg-7'}} col-12 form-group ">
                <input type="search" placeholder="Search by Name ..." name="search" value="{{ request('search') }}" class="form-control">
            </div>
            <div class="col-xl-3  col-lg-2 col-9 form-group ">
                <button type="submit" class="fw-btn-fill btn-gradient-yellow search">SEARCH</button>
            </div>
        </div>
    @show
</form>
