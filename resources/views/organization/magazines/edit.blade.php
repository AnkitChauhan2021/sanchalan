@extends('layouts.organization.app')
@section('title','Create Magazine')
@section('content')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <div class="dashboard-content-one">

        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Update Magazine</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Update Magazine</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('organization.magazines.update')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-2 form-group mg-t-30">
                            <level class="text-dark-medium" style="margin-top:-17px">Title</level>
                            <input class="form-control" name="title" value="{{$record->title}}" />
                        </div>
                        @error('title')
                        <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                        @enderror
                        <div>
                            <div class="col-lg-6 col-2 form-group mg-t-30">
                                <label class="text-dark-medium" style="margin-top:-17px">Image</label>
                                <input type="file" class="form-control-file @error('images') is-invalid @enderror"
                                       name="images[]" multiple accept="image/gif, image/jpeg,image/jpg, image/png" />
                            </div>
                            @error('images')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div>
                            <div class="col-lg-6 col-2 form-group mg-t-30">
                                <label class="text-dark-medium" style="margin-top:-17px">Video</label>
                                <input type="file" class="form-control-file @error('Videos') is-invalid @enderror"
                                       name="Videos[]" multiple accept="video/mp4,video/x-m4v,video/*"/>
                            </div>
                            @error('Videos')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" name="magazine_id" value="{{$record->id}}">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Content</label>
                                <textarea name="editor1">{{$record->description}}</textarea>
                                <script>
                                    CKEDITOR.replace('editor1');
                                </script>
                                @if ($errors->has('editor1'))
                                    <span class="error-message">
                                            <strong class="text-danger">{{ $errors->first('editor1') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                        @foreach ($record->images as $image)
                        <div class="col-lg-3 col-md-12 col-xs-12 col-sm-12 mt-4" id="{{$image->id}}">
                            <div class="form-group">
                                <a href="javascript:void(0);" class="remove_image" onclick="magazineRemoveImage('{{$image->id}}')"><span aria-hidden="true">&times;</span></a>
                                <img src="{{$image->url}}" alt="edit"/>
                            </div>
                        </div>
                        @endforeach

                        @foreach ($record->videos as $video)
                            <div class="col-lg-3 col-md-12 col-xs-12 col-sm-12 mt-4" id="{{$video->id}}">

                                <div class="form-group">
                                    <video src="{{$video->url}}" controls height="150px" width="200px">
                                        Your browser does not support the video tag.
                                    </video>

                                </div>
                                <a href="javascript:void(0);" class="remove_image" onclick="magazineRemoveVideo('{{$video->id}}')" style="margin-top: -164px;"><span aria-hidden="true">&times;</span></a>
                            </div>
                        @endforeach
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save
                            </button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @include('organization.magazines.script')
@endsection()
