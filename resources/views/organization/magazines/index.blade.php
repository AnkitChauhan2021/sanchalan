@extends('layouts.organization.app')
@section('title','Magazines')
@section('content')

    <style>
        .search {
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>All Magazines</li>
            </ul>
        </div>

        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Magazines</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('organization.magazines.create')}}"
                           class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                class="fas fa-plus-circle fa-2x"></i></a>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Title</th>
                            <th>Created By</th>
                            <th>Create At</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td> {!!  \Illuminate\Support\Str::limit($record->title,50, $end='...')!!}</td>
                                    <td>
                                        {{$record->user->full_name}}<br>
                                    </td>
                                    <td>
                                        {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                    <td>
                                        {!! CommonHelper::getStatusUrl('organization.magazines.changeStatus',$record->status,$record->id) !!}

                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">
                                                <a class="dropdown-item"
                                                   href="{{route('organization.magazines.edit',$record->id)}}"> <i
                                                        class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                                <a class="dropdown-item action_btn confirmDelete"
                                                   data-action="{{route('organization.magazines.destroy',$record->id)}}"
                                                   href="javascript:void(0);"><i
                                                        class="fas fa-trash text-danger"></i> delete</a>

                                            </div>
                                        </div>
                                </tr>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @include('organization.elements.pagination.common')
            </div>
        </div>
        <div class="modal fade job_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true"
             style="min-height:300px;transition: transform 1s ease-out,-webkit-transform 1s ease-out;transition:all ease 1s;">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body job-description-popup">
                        <h1 id="state_name"></h1>
                    </div>
                </div>
            </div>
        </div>

@endsection
