@extends('layouts.organization.app')
@section('title','Create Meeting')
@section('content')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <div class="dashboard-content-one">

        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Add New Meeting</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add New Meeting</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('organization.meetings.store')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="row ">

                        <div class="col-xl-6 col-12 form-group mt-4">
                            <label>Organization</label>
                            <select class="select2  @error('companies') is-invalid @enderror " name="companies[]" multiple>
                                <option value="">Please Select Organization</option>
                                @foreach ($companies as $company)
                                    <option value="{{@$company->id}}">{{@$company->name}}</option>
                                @endforeach
                            </select>
                            @error('level')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-12 form-group mt-4">
                            <label>Meeting Level </label>
                            <select class="select2  @error('levels') is-invalid @enderror " name="levels" >
                                <option value="">Please Select Meeting Level</option>
                                @foreach ($levels as $level)
                                    <option value="{{@$level->id}}">{{@$level->name}}</option>
                                @endforeach
                            </select>
                            @error('levels')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-2 form-group mg-t-30">
                            <level >Description</level>
                            <textarea  rows="4" cols="120" name="description"></textarea>
                        </div>
                        @error('title')
                        <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror


                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save
                            </button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection()
