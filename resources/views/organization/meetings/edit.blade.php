@extends('layouts.organization.app')
@section('title','Update Meeting')
@section('content')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <div class="dashboard-content-one">

        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Update Meeting</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Update Meeting</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('organization.meetings.update')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="row ">
                        <div class="col-lg-12 col-2 form-group mg-t-30">

                            <textarea  rows="4" cols="120" name="description">{{$record->description}}</textarea>
                        </div>
                        @error('title')
                        <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                        <input type="hidden" value="{{$record->meeting_code}}" name="meeting_code">

                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save
                            </button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection()

