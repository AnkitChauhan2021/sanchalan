@extends('layouts.organization.app')
@section('title','Update Organizations')

@section('content')

    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard.index')}}">Home</a>
                </li>
                <li>Add New Organization</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">

                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add New Organization</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('admin.organization.update')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Organization Name *</label>
                            <label>

                                <input type="text" name="company_id" class="hidden" value="{{$record->id}}"/>
                                <input type="text" name="user_id" class="hidden" value="{{@$user->id}}"/>
                                <input type="text" placeholder="" class="@error('org_name') is-invalid @enderror form-control" name="org_name" value="{{$record->name}}"/>

                            </label>
                            @error('org_name')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        {{--                        <input type="hidden" name="company_id" value="{{$record->company->id}}">--}}
                        {{--                        <input type="hidden" name="user_id" value="{{$record->id}}">--}}
                        <div class="col-xl-6 col-lg-6 col-12 form-group">
                            <label>Organization email </label>
                            <label>
                                <input type="email" placeholder=""
                                       class="@error('email') is-invalid @enderror form-control" name="email"
                                       value="{{$record->email}}"/>
                            </label>
                            @error('email')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>State* </label>
                            <label>

                                <select class=" select_state_dist select2  @error('state_id') is-invalid @enderror " name="state_id" onchange="onselectState()">

                                    <option value="">Please Select</option>
                                    @foreach ($states as $state)
                                        <option value="{{$state->id}}"
                                                @if(($record->state_id)==$state->id) Selected @endif>{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </label>
                            @error('state_id')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>District *</label>
                            <label>
                                <select value=""
                                        class="select2 seclected_dist  @error('district_id') is-invalid @enderror "
                                        name="district_id" id="state" onchange="onselectDistrict()">
                                    <option value="">Please Select</option>
                                </select>
                            </label>
                            @error('district_id')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>City *</label>
                            <label>

                                <select class="select2 citySelector   @error('city_id') is-invalid @enderror " name="city_id" id="cities">

                                    <option value="">Please Select</option>
                                </select>
                            </label>
                            @error('city_id')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>Status*</label>
                            <label>
                                <select class="select2  @error('status') is-invalid @enderror " name="status">
                                    <option value="">Please Select</option>

                                    <option value="1" @if(($record->status)==1) Selected @endif>Active</option>
                                    <option value="0" @if(($record->status)==0) Selected @endif>Inactive</option>

                                </select>
                            </label>
                            @error('status')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-9 col-lg-6 col-12 form-group">
                            <label>Address </label>
                            <label>
                                <input type="text" placeholder=""
                                       class="@error('address') is-invalid @enderror form-control" name="address"
                                       value="{{$record->address}}"/>
                            </label>
                            @error('address')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>Postal Code </label>
                            <label>
                                <input type="text" placeholder=""
                                       class="@error('postal_code') is-invalid @enderror form-control"
                                       name="postal_code" value="{{$record->postal_code}}"/>
                            </label>
                            @error('postal_code')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
{{--                        <div class="col-xl-3 col-lg-6 col-12 form-group">--}}
{{--                            <label>User First Name *</label>--}}
{{--                            <label>--}}
{{--                                <input type="text" placeholder=""--}}
{{--                                       class="@error('first_name') is-invalid @enderror form-control" name="first_name"--}}
{{--                                       value="{{@$user->first_name}}"/>--}}
{{--                            </label>--}}
{{--                            @error('first_name')--}}
{{--                            <span role="alert">--}}
{{--                            <strong class="text-danger">{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-3 col-lg-6 col-12 form-group">--}}
{{--                            <label>User Last Name *</label>--}}
{{--                            <label>--}}
{{--                                <input type="text" placeholder=""--}}
{{--                                       class="@error('last_name') is-invalid @enderror form-control" name="last_name"--}}
{{--                                       value="{{@$user->last_name}}"/>--}}
{{--                            </label>--}}
{{--                            @error('last_name')--}}
{{--                            <span role="alert">--}}
{{--                            <strong class="text-danger">{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-3 col-lg-6 col-12 form-group">--}}
{{--                            <label>User Email* </label>--}}
{{--                            <label>--}}
{{--                                <input type="text" placeholder=""--}}
{{--                                       class="@error('user_email') is-invalid @enderror form-control" name="user_email"--}}
{{--                                       value="{{@$user->email}}"/>--}}
{{--                            </label>--}}
{{--                            @error('user_email')--}}
{{--                            <span role="alert">--}}
{{--                            <strong class="text-danger">{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-3 col-lg-6 col-12 form-group">--}}
{{--                            <label>User Phone No* </label>--}}
{{--                            <label>--}}
{{--                                <input type="text" placeholder=""--}}
{{--                                       class="@error('mobile') is-invalid @enderror form-control" name="mobile"--}}
{{--                                       value="{{@$user->mobile}}"/>--}}
{{--                            </label>--}}
{{--                            @error('mobile')--}}
{{--                            <span role="alert">--}}
{{--                            <strong class="text-danger">{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                        <div class="col-xl-3 col-lg-6 col-12 form-group">
                            <label>User Designation *</label>

                            <select class="select2 selected_designation @error('designation') is-invalid @enderror" onchange="onselectDesignation()" name="designation">

                                <option value="">Please Select Section *</option>
                                @foreach($levels as $destination )
                                    <option value="{{$destination->id}}" >{{$destination->name}}</option>
                                @endforeach
                            </select>
                            @error('designation')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-6 col-lg-6 col-12 form-group ">
                            <label>Organizations Designation *</label>

                            <select class="select2 border-0 designation @error('companydesignations') is-invalid @enderror" multiple id="designation" name="companydesignations[]">

                                <option value="">Please Select Section *</option>
                            </select>
                            @error('companydesignations')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save
                            </button>
                            <a href="{{route('admin.city.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @include('organization.organizations.updateScript')



@endsection
