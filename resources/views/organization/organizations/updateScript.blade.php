<script>

    let designat = null;
    let district = '';
    let city = '';

    function onselectDesignation() {
        const desi = document.getElementsByClassName('selected_designation');
        designat = desi[0].value;
        AwnSystem(designat)

    }

    function AwnSystem(designat) {


        let desig = @json($levels);
        const obj = desig.filter(d => d.id != designat)
        $("#designation").html("");
        obj.forEach(function (item, index) {
            console.log(item)
            $("#designation").removeClass("designation");
            $("#designation").append('<option value="' + item.id + '">' + item.name + '</option>');

        });
    }

    let desig = @json($levels);
    const obj = desig.filter(d => d.id != designat)

    desig.forEach(function (item, index) {
        $(".designation").append('<option value="' + item.id + '">' + item.name + '</option>');

    });

    // create city and update city
    const record = @json($record);

    if (record.state_id !== null) {
        const districtId = record.district_id;
        const cityId = record.city_id;
        district = @json($district);
        city =@json($city);

        $(document).ready(function () {
            getDistrict(record.state_id);
            getCity(districtId);
        })
    }

    $('#state').on('click', function () {
        $("#select2-state-results").find('li').first().text('Please select district');
    })
    $('#cities').on('click', function () {
        $('#select2-cities-results').find('li').first().text('Please select city');
    })


    function onselectState() {
        const state_id = document.getElementsByClassName("select_state_dist");
        const value = state_id[0].value;
        getDistrict(value);
    }

    function onselectDistrict() {
        const dist_id = document.getElementsByClassName("seclected_dist");
        const dist = dist_id[0].value;
        $('#state').find('#first-district').text('Please select district');
        $('#district').find('#first-city').text('Please select City');
        getCity(dist);

    }


    function getDistrict(value) {

        $.ajax({
            type: "GET",
            url: "{{url('/admin/organization/getDistricts')}}?state_id=" + value,
            success: function (res) {
                if (res) {
                    $("#state").empty();
                    if (district !== null && district !== '' && district !== undefined) {
                        $("#state").append(`<option id='first-district' value="${district.id}">${district.name}</option>`);
                    }
                    $.each(res, function (key, value) {
                        $("#state").append(`<option value="${value.id}")>${value.name}</option>`);
                    });
                } else {
                    $("#state").empty();
                }
            },
        });
    }

    function getCity(dist) {
        $.ajax({
            type: "GET",
            url: "{{url('/admin/organization/getCities')}}?dist_id=" + dist,
            success: function (res) {
                if (res) {
                    $("#cities").empty();
                    if (city !== null && city !== '' && city !== undefined) {
                        $('#cities').append(`<option id='first-city' value="${city.id}">${city.name}</option>`);
                    }
                    $.each(res, function (key, value) {
                        $("#cities").append(`<option value="${value.id}")>${value.name}</option>`);
                    });
                } else {
                    $("#cities").empty();
                }
            },

        });
    }


</script>
