@extends('layouts.organization.app')

@section('title','polls')
@section('content')

    <style>
        .search {
            height: 50px;
        }
    </style>

    <div class="dashboard-content-one">

        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>All polls</li>
            </ul>
        </div>

        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All polls</h3>
                    </div>
                </div>
                @include('admin.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="    form-group addbutton" style="margin-top: -76px;">

                        <a href="{{route('organization.polls.create')}}"
                           class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i
                                class="fas fa-plus-circle fa-2x"></i></a>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr class="text-center">

                            <th>Sr No</th>
                            <th>Question</th>
                            <th>Answer</th>
                            {{--                            <th>Create At</th>--}}
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td>
                                        <table style="border:none">
                                            <tr class="border-0">
                                                <td style="border:none; margin-top:50px"><span
                                                        class="text-light">.</span></td>
                                            </tr>
                                            <tr>
                                                <td style="border:none;" class="p-5">{{$key+1}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table style="border:none">
                                            <tr class="border-0">
                                                <td style="border:none; margin-top:50px"><span
                                                        class="text-light">.</span></td>
                                            </tr>
                                            <tr>
                                                <td style="border:none;" class="p-5">{{$record->question}}</td>
                                            </tr>
                                        </table>

                                    </td>
                                    <td>
                                        <table>
                                            <tr class="text-center">

                                                <td>A
                                                    @if($record->answer_count1 !=null)
                                                        <span class="badge badge-primary"
                                                              style="border-radius: 25px">{{$record->answer_count1}}</span>
                                                    @endif
                                                </td>
                                                <td>B
                                                    @if($record->answer_count2 !=null)
                                                        <span class="badge badge-primary"
                                                              style="border-radius: 25px">{{$record->answer_count2}}</span>
                                                    @endif
                                                </td>
                                                <td>C
                                                    @if($record->answer_count3 !=null)
                                                        <span class="badge badge-primary"
                                                              style="border-radius: 25px">{{$record->answer_count3}}</span>
                                                    @endif
                                                </td>
                                                <td>D
                                                    @if($record->answer_count4 !=null)
                                                        <span class="badge badge-primary" style="border-radius: 25px">{{$record->answer_count4}}
                                                            @endif
                                                    </span></td>
                                            </tr>
                                            <tr class="text-center">
                                                <td>{{$record->answer1}}

                                                </td>
                                                <td>{{$record->answer2}}


                                                </td>
                                                <td>{{$record->answer3}}

                                                </td>
                                                <td>{{$record->answer4}}
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    {{--                                    <td>--}}
                                    {{--                                        <table style="border:none">--}}
                                    {{--                                            <tr class="border-0" >--}}
                                    {{--                                                <td style="border:none; margin-top:50px"><span class="text-light">.</span> </td>--}}
                                    {{--                                            </tr>--}}
                                    {{--                                            <tr >--}}
                                    {{--                                                <td style="border:none;" class="p-5"> {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>--}}
                                    {{--                                            </tr>--}}
                                    {{--                                        </table>--}}
                                    {{--                                       </td>--}}
                                    <td>
                                        <table style="border:none">
                                            <tr class="border-0">
                                                <td style="border:none; margin-top:50px"><span
                                                        class="text-light">.</span></td>
                                            </tr>
                                            <tr>
                                                <td style="border:none;"
                                                    class="p-5">   {!! CommonHelper::getStatusUrl('organization.polls.changeStatus',$record->status,$record->id) !!}</td>
                                            </tr>
                                        </table>

                                    </td>
                                    <td>

                                        <table style="border:none">
                                            <tr class="border-0">
                                                <td style="border:none; margin-top:50px"><span
                                                        class="text-light">.</span></td>
                                            </tr>
                                            <tr>
                                                <td style="border:none;" class="p-5">


                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle action_menu"
                                                           data-toggle="dropdown"
                                                           aria-expanded="false">
                                                            <span class="flaticon-more-button-of-three-dots"></span>
                                                        </a>
                                                        <div class="dropdown-menu ">


                                                            <a class="dropdown-item"
                                                               href="{{url('organization/polls/details/'.$record->id)}}">
                                                                <i class="fas fa-list"> </i> Detail</a>
                                                            <a class="dropdown-item"
                                                               href="{{url('organization/submit/poll/'.$record->id)}}">
                                                                <i class="fas fa-vote-yea text-warning"></i> Submit Poll</a>
                                                            <a class="dropdown-item"
                                                               href="{{route('organization.polls.edit',$record->id)}}">
                                                                <i
                                                                    class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                                            <a class="dropdown-item action_btn confirmDelete"
                                                               data-action="{{route('organization.polls.destroy',$record->id)}}"
                                                               href="javascript:void(0);"><i
                                                                    class="fas fa-trash text-danger"></i> delete</a>

                                                        </div>
                                                    </div>


                                                </td>
                                            </tr>
                                        </table>


                                </tr>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                @include('organization.elements.pagination.common')
            </div>
        </div>


        <div class="modal col-lg-12 fade" id="small-modal" tabindex="-1" role="dialog" aria-hidden="true"
             style="margin-top: 10%;">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="new-added-form">
                        <div class="row">
                            <div class="col-xl-12 col-lg-6 col-12 form-group">
                                <label>Password *</label>
                                <input type="text" placeholder="" class="form-control">
                            </div>
                        </div>


                        <div class="modal-footer d-flex justify-content-center ">

                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark"><i
                                    class="fas fa-exchange-alt"></i></button>

                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow" data-dismiss="modal">
                                <i class="far fa-times-circle"></i></button>

                        </div>
                    </form>

                </div>
            </div>
        </div>



@endsection
