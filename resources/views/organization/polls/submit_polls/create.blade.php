@extends('layouts.organization.app')

@section('title','Submit Poll')
@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Submit Poll</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Details Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Submit Poll</h3>
                    </div>
                </div>
                <div class="single-info-details">
                    <div class="item-content">


                        <div class="info-table table-responsive">
                            <form action="{{route('organization.answer.poll')}}" method="post">
                                @csrf
                                <table class="table text-nowrap">
                                    <tbody>
                                    <tr>
                                        <th>Question</th>
                                        <th>{{$poll->question}}</th>
                                        <input type="hidden" name="id" value="{{$poll->id}}">
                                    </tr>
                                    <tr>
                                        <th>Answer</th>
                                        @error('answer')
                                        <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                                        @enderror
                                    </tr>
                                    <tr>
                                        <td>A</td>
                                        <td class="font-medium text-dark-medium">
                                            <input type="radio" id="age1" name="answer" value="answer1">
                                            <label for="age1">{{$poll->answer1}}</label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>B</td>
                                        <td class="font-medium text-dark-medium">
                                            <input type="radio" id="age1" name="answer" value="answer2">
                                            <label for="age1">{{$poll->answer2}}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>C</td>
                                        <td class="font-medium text-dark-medium">
                                            <input type="radio" id="age1" name="answer" value="answer3">
                                            <label for="age1">{{$poll->answer3}}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>D</td>
                                        <td class="font-medium text-dark-medium">
                                            <input type="radio" id="age1" name="answer" value="answer4">
                                            <label for="age1">{{$poll->answer4}}</label>
                                        </td>
                                    </tr>
                                    <tr>


                                    </tbody>
                                </table>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">
                                        Submit
                                    </button>
{{--                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel--}}
{{--                                    </button>--}}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Student Details Area End Here -->

@endsection
