@extends('layouts.organization.app')

@section('title','Posts')


@section('content')
<div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Admit Form Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Post Details</h3>
                    </div>

                </div>
                <form class="new-added-form" action="" method="get" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" style="font-weight: bold;">Content:</label>
                                {!! $post->description !!}

                            </div>
                        </div>

                        @foreach($postImage as $value)

                            @if($value->post_image != " " )
                        <div class="col-sm-3  mg-t-30" style="padding:15px;">
                                    <div class="card border" style="width: 21rem; padding-bottom: 0px;" >
                                        <img class="card-img-top" src="{{$value->post_image}}" data-id="{{$value->id}}" alt="Card image cap" style="height:25rem" >
                                    </div>
                                </div>
                            @endif
                            @endforeach
                        @foreach($postVideo as $value)
                            @if($value->post_video != " " )
                        <div class="col-sm-3  mg-t-30" style="padding:15px;">
                                    <div class="card border" style="width: 21rem; padding-bottom: 0px;" >
                                        <video width="350" height="" controls class="thumb" data-full="{{$value->id}}"style="height:100%" >
                                              <source src="{{$value->post_video}}">
                                        </video>
                                    </div>
                                </div>
                            @endif
                            @endforeach

                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" style="font-weight: bold;">Posted  BY:</label>
                                {{$post->getUser->full_name}}

                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>


@endsection
