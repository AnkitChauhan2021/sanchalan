@extends('layouts.organization.app')

@section('title','Posts')


@section('content')


    <style>
        .search {
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>All Posts</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Posts</h3>
                    </div>
                </div>
                @include('organization.elements.search.commonsearch')
                <div class=" float-right">
                    <div class="form-group addbutton" style="margin-top: -76px;">
                        <a href="{{route('organization.posts.type.select')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow text-center search"><i class="fas fa-plus-circle fa-2x"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Post</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th>Likes</th>
                            <th>Status</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())
                            @foreach ($records as $key => $record)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td class="post_title"> {!!  \Illuminate\Support\Str::limit($record->description ,50, $end='...')!!}</td>
                                    <td>{{$record->getUser->full_name}}</td>

                                    <td>
                                        {{date('D,  M,  Y ', strtotime($record->created_at))}}</td>
                                    @if($record->like_count<1)
                                        <td>
                                        </td>
                                    @else
                                        <td>
                                            {{$record->like_count}}
                                        </td>
                                    @endif
                                    <td>
                                        {!! CommonHelper::getStatusUrl('organization.posts.changeStatus',$record->status,$record->id) !!}

                                    </td>

                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle action_menu" data-toggle="dropdown"
                                               aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu ">
                                                <a type="button" class="dropdown-item" href="{{route('organization.posts.display',$record->id)}}"><i class="fas fa-eye"></i> View</a>


                                                <a class="dropdown-item" href="{{route('organization.posts.edit',$record->id)}}"> <i
                                                        class="fas fa-cogs text-dark-pastel-green"></i> Edit</a>

                                                <a class="dropdown-item action_btn confirmDelete" data-action="{{route('organization.posts.remove',$record->id)}}" href="javascript:void(0);"><i
                                                        class="fas fa-trash text-danger"></i> delete</a>

                                            </div>
                                        </div>
                                </tr>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>
                @include('organization.elements.pagination.common')
            </div>
        </div>


        <div class="modal col-lg-12 fade" id="small-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 10%;">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="new-added-form">
                        <div class="row">
                            <div class="col-xl-12 col-lg-6 col-12 form-group">
                                <label>Password *</label>
                                <input type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center ">

                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark"><i class="fas fa-exchange-alt"></i></button>

                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow" data-dismiss="modal"><i class="far fa-times-circle"></i></button>

                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="modal fade job_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="min-height:300px;transition: transform 1s ease-out,-webkit-transform 1s ease-out;transition:all ease 1s;">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body job-description-popup">
                        <h1 id="state_name"></h1>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function viewServiceProviderModal(id) {
                $("#loader").show();
                // alert('id : '+id);
                var form_data = new FormData();
                form_data.append("id", id);
                form_data.append("_token", "{{csrf_token()}}");
                $(".modal-body").empty();
                $.ajax({
                    url: "{{route('organization.posts.view')}}",
                    data: form_data,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $("#loader").hide();
                        $(".modal-body").html(data.modal_content);
                        $("#exampleModal").modal("show");
                        console.log(data.name)
                        //$('#state_name').val(data.name)
                    }
                });
            };
        </script>

@endsection
