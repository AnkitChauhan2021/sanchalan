<script>
    const removeImage = (id) =>{
        $.ajax({
            type: "GET",
            url: "{{url('/organization/posts/remove/image/')}}/" + id,
            success: function (response) {
                toastr[response.type](response.message)
                document.getElementById(id).style.display = "none";
            },
            error: function(response) {
            }
        });
    }
    const removeVideo =(id) =>{
        $.ajax({
            type: "GET",
            url: "{{url('/organization/posts/remove/video/')}}/" + id,
            success: function (response) {
                toastr[response.type](response.message)
                document.getElementById(id).style.display = "none";
                location.reload();
            },
            error: function(response) {

            }
        });
    }
</script>
