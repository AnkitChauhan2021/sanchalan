@extends('layouts.organization.app')

@section('title','Select Posts type')

@section('content')

    <style>
        .search{
            height: 50px;
        }
    </style>
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Select Posts type</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Student Table Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Select Posts type</h3>
                    </div>

                </div>
                <div class="container col-lg-3 mt-5 col-sm-12">
                    <a href="{{route('organization.posts.create.personal')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow    text-center" >Personal Post</a>

                </div>
                <div class="container col-lg-3 mt-5 col-sm-12">
                <a href="{{route('organization.posts.create.company')}}" class="fw-btn-fill bg-blue-dark btn-hover-yellow    text-center" >Company Post</a>
                </div>
            </div>
        </div>

@endsection
