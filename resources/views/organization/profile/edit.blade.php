@extends('layouts.organization.app')
@if(Auth::check())
    @section('title',Auth::user()->first_name.' '.Auth::user()->last_name.' '. "Edit Profile")
@endif
@section('header',"Dashboard")
@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>

                <li>
                    <a href="{{route('organization.organizationprofile.index')}}"> Admin Profile</a>

                </li>

                <li>Update Profile</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Admit Form Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Update Profile</h3>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-12 form-group">
                        <div class="upload_photo">
                            <div class="user_photo">
                                <input type="file" class=" image @error('profile_pic') is-invalid @enderror" name="image"  class="" value="{{$user->profile_pic}}"/>

                                @error('profile_pic')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="upload_img">
                                    <img src="{{Auth::user()->logo}}" class="profile_image">
                                </div>
                            </div>
                            <div class="photo_content">
                                <h2>Upload Photo</h2>
                                <p>Please upload the image right to use, max size 1mb</p>
                            </div>
                        </div>
                    </div>
                </div>
                 <form class="new-added-form" action="{{route('organization.organizationprofile.update')}}" method="post" enctype="multipart/form-data">
                    <div class="row">
                           @csrf
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>First Name</label>
                            <input type="text" placeholder="" name="first_name" class="form-control @error('first_name') is-invalid @enderror" value="{{$user->first_name}}">
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Last Name</label>
                            <input type="text" placeholder="" name="last_name" class="form-control @error('last_name') is-invalid @enderror" value="{{$user->last_name}}">
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Email</label>
                            <input type="email" placeholder="" name="email" class="form-control @error('email') is-invalid @enderror"  value="{{$user->email}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>


                        <div class="col-12 form-group mg-t-8">.
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                            <a href="{{route('organization.organizationprofile.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

{{-----------------------------------------------------Cropping model ----------------------------------}}



        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                </div>
                                <div class="col-md-4">
                                    <div class="preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="crop">Crop</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include('organization.profile.script')

@endsection
