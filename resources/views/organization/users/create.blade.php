@extends('layouts.organization.app')

@section('title','Add User')

@section('content')

    <div class="dashboard-content-one">
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Add New User</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add New User</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('organization.users.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>First Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('first_name') is-invalid @enderror form-control" name="first_name"/>
                            </label>
                            @error('first_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Last Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('last_name') is-invalid @enderror form-control" name="last_name"/>
                            </label>
                            @error('last_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Email  *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('email') is-invalid @enderror form-control" name="email"/>
                            </label>
                            @error('email')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Mobile *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('mobile') is-invalid @enderror form-control" name="mobile"/>
                            </label>
                            @error('mobile')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>



                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Level</label>
                            <select class="select2  @error('level') is-invalid @enderror " name="level">
                                <option value="">Please Select Level</option>
                                @foreach ($levels as $level)
                                <option value="{{@$level->levels->id}}">{{@$level->levels->name}}</option>
                                    @endforeach
                            </select>
                            @error('level')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Designation</label>
                            <label>
                                <input type="text" placeholder="" class="@error('designation') is-invalid @enderror form-control" name="designation"/>
                            </label>
                            @error('designation')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Password</label>
                            <label>
                                <input type="password" placeholder="" class="@error('password') is-invalid @enderror form-control" name="password"/>
                            </label>
                            @error('password')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Confirm Password</label>
                            <label>
                                <input type="password" placeholder="" class="@error('password_confirmation') is-invalid @enderror form-control" name="password_confirmation"/>
                            </label>
                            @error('password_confirmation')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <a href="{{route('organization.dashboards.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    @include('organization.users.script')

@endsection
