@extends('layouts.organization.app')

@section('title','Update User')

@section('content')
    <!-- Sidebar Area End Here -->
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">

            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Update User</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Add Expense Area Start Here -->
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Update User</h3>
                    </div>
                </div>



                <form class="new-added-form" action="{{route('organization.users.update')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>First Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('first_name') is-invalid @enderror form-control" name="first_name" value="{{$user->first_name}}"/>
                            </label>
                            @error('first_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Last Name *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('last_name') is-invalid @enderror form-control" name="last_name" value="{{$user->last_name}}"/>
                            </label>
                            @error('last_name')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Email  *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('email') is-invalid @enderror form-control" name="email" value="{{$user->email}}"/>
                            </label>
                            @error('email')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Mobile *</label>
                            <label>
                                <input type="text" placeholder="" class="@error('mobile') is-invalid @enderror form-control" name="mobile" value="{{$user->mobile}}"/>
                            </label>
                            @error('mobile')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                            <input type="hidden" value="{{$user->id}}" name="id">
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Level</label>
                            <select class="select2 byorganization @error('level') is-invalid @enderror " name="level">
                                <option value=" ">Please Select Level</option>
                                @foreach($levels as $level)
                                <option value="{{$level->levels->id}}" @if($userComapny->level_id ==$level->levels->id)Selected @endif >{{$level->levels->name}}</option>

                                    @endforeach
                            </select>
                            @error('level')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label>Designation</label>
                            <label>
                                <input type="text" placeholder="" class="@error('designation') is-invalid @enderror form-control" name="designation" value="{{$userComapny->designation}}"/>
                            </label>
                            @error('designation')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <a href="{{route('organization.users.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @include('organization.users.script')
@endsection
