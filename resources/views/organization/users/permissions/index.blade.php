@extends('layouts.organization.app')
@section('title','Manage Permissions')

@section('content')

    <div class="dashboard-content-one">
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="{{route('organization.dashboards.index')}}">Home</a>
                </li>
                <li>Manage Permission</li>
            </ul>
        </div>
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Give Permission to {{$user->full_name}}</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{route('organization.users.allow.permissions')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6  form-group">
                            <label>Select Permission*</label>
                            <select class="select2  @error('permission') is-invalid @enderror " name="permission" >
                                <option value="">Please Select Level</option>
                                @foreach ($records as $record)
                                    <option value="{{@$record->id}}">{{@$record->name}}</option>
                                @endforeach
                            </select>
                            @error('permission')
                            <span  role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" value="{{$user->id}}" name="user_id">
                        <div class="col-6 form-group mt-5">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <a href="{{route('organization.dashboards.index')}}" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Cancel</a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table display data-table text-nowrap">
                        <thead>
                        <tr>

                            <th>Sr No</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($records->count())

                            @foreach ($userPermissions as $key => $record)
                                <tr>
                                    <td> {{$key+1}}</td>
                                    <td> {{@$record->permission->name}}</td>
                                    <td> <i class="fas fa-check text-success" title="{{@$record->permission->name}} Permission Activated"></i></td>
                                    <td> <a href="{{route('organization.users.permission.remove',$record->id)}}" class="btn btn-light" title="Remove Permission of {{@$record->permission->name}}"><i class="fas fa-times-circle text-danger fa-5x" style="font-size: 22px"></i></a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="8">No Record Found!</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>



                </div>
        </div>

@endsection
