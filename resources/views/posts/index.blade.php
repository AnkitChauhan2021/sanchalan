@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')

    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')

    <div class="col-md-7">
        <div class="post">
            <form action="{{route('user.post.create')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="write-post shadow-sm bg-white">
                    <p>Create Post
                    </p>
                    @error('description')
                    <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                    @enderror
                    <div class="write">
                        <div class="profile-nav d-flex">
                            <div class="profile-post">
                                <img src="{{Auth::user()->logo}}" id="post_addImage" alt="postBYImage"/>
                            </div>
                            <div class="post-text">
                                <textarea placeholder="Type Here..."
                                          class="form-control post-text @error('description') is-invalid @enderror"
                                          rows="2" name="description"></textarea>
                            </div>
                        </div>
                        <div class="who">
                            <div class="paper-clip d-flex">
                                <div class="photo">
                                    <div class="coustom-input coustom-input-new" title="upload your profile pic">

                                        <input type="file" class="custom-file-input" multiple id="gallery-photo-add"
                                               accept="image/jpeg ,image/jpg,image/png,image/gif/*" name="image[]">
                                        <i class="icofont-camera"></i>
                                    </div>
                                    <div class="coustom-input coustom-input-new" title="upload video">
                                        <input type="file" class="custom-file-input file_multi_video" name="file[]"
                                               accept="video/*"/>
                                        <i class="icofont-video-cam"></i>
                                    </div>
                                </div>
                            </div>
                            @error('image')
                            <span role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="see-post">
                                <button class="post_btn light_purple ml-2" id="disable">Post</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="postImage" style="display:none">

                    <div class="post_videos d-flex">

                        <div class="video_divs">
                            <video width="400" controls id="video-fluids" style="display:none">
                                <source src="mov_bbb.mp4" id="video_here">
                                Your browser does not support HTML5 video.
                            </video>

                        </div>
                    </div>
                </div>
            </form>
            {{--<div class="post_video d-flex">
                @foreach($allPost as $all)
                    @php
                        //  var_dump($all->getPostImage->toArray());
                    @endphp
                    @if(count($all->getPostVideo->toArray())>0)
                        <div class="video_div">
                            <video controls class="img-fluid">
                                <source src="{{$all->getPostVideo->toArray()[0]['post_video']}}">
                            </video>
                            <a href="{{route('user.post.detail',$all->id)}}"><i class="icofont-ui-play"></i></a>
                            --}}{{--<p>{{$all->description}}</p>--}}{{--
                        </div>
                    @else
                        <div class ="video_div">
                            <a href="{{route('user.post.detail',$all->id)}}"> <img src="{{$all->getPostImage->toArray()[0]['post_image']}}" class="img-fluid"/></a>
                            --}}{{--<p>{{$all->description}}</p>--}}{{--

                        </div>
                    @endif
                @endforeach
                <div class="post_video d-flex">
                    <a href="{{route('user.trending')}}"><b>View All</b><i class="fas fa-arrow-right"></i></a>
                </div>
            </div>--}}

            @if(Auth::user()->first_login == 1)
                @if(sizeof($users) !=0)
                    <div class="know_people">
                        <h4><b>People you may know.</b></h4>
                        @foreach($users as $user)
                            <div class="user_info">
                                <img src="{{$user->users->logo}}">
                                <span>
                            <p>{{$user->users->first_name}}</p>
                            <a href="{{route('user.follow',2)}}">Follow</a>
                        </span>
                            </div>
                        @endforeach
                    </div>
                @endif
            @endif
            <div class="card" id="postImage" style="display:none">
                <div class="post_videos d-flex">
                    <div class="video_divs">
                        <video width="400" controls id="video-fluids" style="display:none">
                            <source src="" id="video_here">
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                </div>
            </div>

            @foreach($records as $record)
                @if($record->is_Post)
                    <div class="write-post shadow-sm bg-white">
                        <div class="main-post">
                            <div class="post-describe d-flex">
                                <div class="port-profile">
                                    <img src="{{@$record->postBy->logo}}" class="img-fluid" alt="postBYImage"/>
                                </div>
                                <div class="post-content">
                                    <h5>{{@$record->postBy->full_name}}
                                        {{--@if($record->user_id == Auth::user()->id)
                                            @if($record->report_count !== 0)
                                        <b>({{$record->report_count}} {{''}}
                                            @if($record->report_count == 1)
                                                user reported on this post.)
                                                @else
                                                 users reported on this post.)</b>
                                                @endif
                                            @endif
                                        @endif--}}
                                    </h5>
                                    <p>
                                    {{date('d-m-yy, H:i:s', strtotime(@$record->created_at))}}
                                </div>
                                <div class="dropdown-option">
                                    <div class="dropdown">
                                        @if(Auth::user()->id == $record->user_id)
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <img src="{{asset('public/home/img/more.svg')}}" alt="postBYImage">
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                 x-placement="top-start"
                                                 style="position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(0px, -116px, 0px);">
                                                <a class="dropdown-item"
                                                   href="{{route('user.post.edit',$record->id)}}">Edit</a>
                                            </div>
                                        @else

                                            @if(sizeof($record->report)!==0)
                                                {{--@php dd('hello'); @endphp--}}
                                                {{--@php dd($record->report); @endphp--}}
                                                @foreach($record->report as $report)
                                                    @if($report->user_id !== Auth::user()->id && $report->post_id !== $record->id)
                                                        <button class="dropdown-toggle" type="button"
                                                                id="dropdownMenuButton"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            <img src="{{asset('public/home/img/more.svg')}}"
                                                                 alt="postBYImage">
                                                        </button>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                             x-placement="top-start"
                                                             style="position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(0px, -116px, 0px);">
                                                            <a class="dropdown-item"
                                                               href="{{route('user.post.report',$record->id)}}">Report</a>
                                                        </div>
                                                    @endif

                                                @endforeach

                                            @else
                                                <button class="dropdown-toggle" type="button"
                                                        id="dropdownMenuButton"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <img src="{{asset('public/home/img/more.svg')}}"
                                                         alt="postBYImage">
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                     x-placement="top-start"
                                                     style="position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(0px, -116px, 0px);">
                                                    <a class="dropdown-item"
                                                       href="{{route('user.post.report',$record->id)}}">Report</a>
                                                </div>
                                            @endif
                                        @endif


                                    </div>
                                </div>
                            </div>
                            <div class="post-content-bottm">
                                <p>{!! $record->description !!} </p>
                            </div>
                            <div class="content-img">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                            class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        @foreach($record->getPostImage as $key =>$getImage)

                                            @if($getImage->post_image != " ")
                                                @if($key+1==1)
                                                    <div class="carousel-item active">

                                                        <img class="d-block w-100" src="{{$getImage->post_image}}"
                                                             alt="First slide" style="height:400px">
                                                    </div>
                                                @else
                                                    <div class="carousel-item ">

                                                        <img class="d-block w-100" src="{{$getImage->post_image}}"
                                                             alt="First slide" style="height:400px">
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach($record->getPostVideo as $key =>$getVideo)
                                            @if($getVideo->post_video != " ")
                                                <div class="carousel-item">
                                                    <video controls class="d-block w-100" style="height:400px">
                                                        <source src="{{$getVideo->post_video}}">
                                                    </video>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div class="like-comment d-flex align-items-center ">
                                <div class="like like-new active position-relative d-flex" style="margin-left: 100px;">
                                    @if($record->is_like == true)
                                        <a href="javascript:void(0)" class="likes_and_dislikes"
                                           data-id="{{$record->id}}"><i
                                                class="fas fa-thumbs-up text-primary"></i>
                                        </a>
                                    @else
                                        <a href="javascript:void(0)" class="likes_and_dislikes"
                                           data-id="{{$record->id}}"><i
                                                class="far fa-thumbs-up"></i>
                                        </a>
                                    @endif
                                </div>
                                <div class="like float-right" style="margin-left: 300px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="22" viewBox="0 0 24 22">
                                        <g id="comment" transform="translate(-0.001 1.332)">
                                            <path id="Path_1" data-name="Path 1"
                                                  d="M20.7-1.332H3.3A3.3,3.3,0,0,0,0,1.956v10.61a3.3,3.3,0,0,0,3.288,3.288v4.815l6.945-4.815H20.7A3.3,3.3,0,0,0,24,12.566V1.956a3.3,3.3,0,0,0-3.3-3.288Zm1.893,13.9A1.892,1.892,0,0,1,20.7,14.452H9.792l-5.1,3.534V14.452H3.3a1.892,1.892,0,0,1-1.893-1.886V1.955A1.892,1.892,0,0,1,3.3.069H20.7a1.892,1.892,0,0,1,1.893,1.886Zm0,0"/>
                                            <path id="Path_2" data-name="Path 2"
                                                  d="M171.293,131.172h8.946V132.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -127.488)"/>
                                            <path id="Path_3" data-name="Path 3"
                                                  d="M171.293,211.172h8.946V212.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -204.459)"/>
                                            <path id="Path_4" data-name="Path 4"
                                                  d="M171.293,291.172h8.946V292.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -281.43)"/>
                                        </g>
                                    </svg>
                                    Comment
                                </div>


                            </div>
                            <div class="post-describe post-comment d-flex">
                                <div class="port-profile">
                                    <img src="{{@Auth::user()->logo}}" class="img-fluid" alt="postBYImage"/>
                                </div>

                                <div class="post-content w-100">
                                    <form action="{{route('user.comments')}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{$record->id}}" name="post_id">
                                        <div class="text-box">
                                            <input type="text" class="form-control" placeholder="Add a comment ..."
                                                   name="comment"/>
                                            <button type="submit"><img src="{{asset('public/home/img/send.svg')}}"
                                                                       class="send-btn" alt="dropdown"></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        @else

                            {{-- ------------------------------------------------------------------   poles ------------------------------------------}}
                            <div class="write-post shadow-sm bg-white" id="{{$record->id}}">
                                <div class="main-post">
                                    <div class="post-describe d-flex">
                                        <div class="port-profile">
                                            <img src="{{@$record->users->logo}}" class="img-fluid" alt="user_image"/>

                                        </div>
                                        <div class="post-content">
                                            <h5>{{@$record->users->full_name}}</h5>

                                            {{--                                            @foreach($record->users->join_company as $company)--}}
                                            {{--                                                <p>{{$company->name}}</p>--}}
                                            {{--                                            @endforeach--}}

                                            {{--                                            @foreach($record->users->join_company as $company)--}}
                                            {{--                                                <p>{{$company->name}}</p>--}}
                                            {{--                                            @endforeach--}}

                                            <p>
                                                {{date('d-m-yy, H:i:s', strtotime(@$record->created_at))}}
                                            </p>


                                            <div class="post-content-bottm">
                                                <div class="qst">{{$record->question}}</div>
                                                <div class="poll">
                                                    <div class="poll-result position-relative">
                                                        <div class="progress" data-id="{{$record->answer_one}}"
                                                             data-value="{{$record->total_answer}}">
                                                            @if($record->isShow == 1 && $record->total_answer )
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: {{round($record->answer_one/$record->total_answer*(100),2)}}%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present">{{round($record->answer_one/$record->total_answer*(100),2)}}
                                                                        %</span></div>
                                                            @endif
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer1"
                                                                                        data-id="{{$record->id}}">{{$record->answer1}}
                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                    <div class="poll-result position-relative">

                                                        <div class="progress" data-id="{{$record->answer_two}}"
                                                             data-value="{{$record->total_answer}}">
                                                            @if($record->isShow == 1 && $record->total_answer )
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: {{round($record->answer_two/$record->total_answer*(100),2)}}%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present">{{round($record->answer_two/$record->total_answer*(100),2)}}
                                                                        %</span></div>
                                                            @endif
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer2"
                                                                                        data-id="{{$record->id}}">{{$record->answer2}}
                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                    <div class="poll-result position-relative">

                                                        <div class="progress" data-id="{{$record->answer_three}}"
                                                             data-value="{{$record->total_answer}}">
                                                            @if($record->isShow == 1 && $record->total_answer )
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: {{round($record->answer_three/$record->total_answer*(100),2)}}%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present">{{round($record->answer_three/$record->total_answer*(100),2)}}
                                                                        %</span></div>
                                                            @endif
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer3"
                                                                                        data-id="{{$record->id}}">{{$record->answer3}}
                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                    <div class="poll-result position-relative">

                                                        <div class="progress" data-id="{{$record->answer_four}}"
                                                             data-value="{{$record->total_answer}}">
                                                            @if($record->isShow == 1 && $record->total_answer )
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: {{round($record->answer_four/$record->total_answer*(100),2)}}%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present">{{round($record->answer_four/$record->total_answer*(100),2)}}
                                                                        %</span></div>
                                                            @endif
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer4"
                                                                                        data-id="{{$record->id}}">{{$record->answer4}}
                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                {{--              End poles sections          --}}
                                @endif
                                <br>
                                @if($record->is_Post)

                                    @foreach($record->comments as $key => $comment)
                                        @if($comment->commentBy)
                                            <div class="post-describe d-flex reply_coment  my-4">

                                                <div class="port-profile" style="margin-left: 9px;">
                                                    <img src="{{$comment->commentBy->logo}}" class="img-fluid"/>
                                                </div>
                                                <div class="post-content">
                                                    <div class="post-content-bottm">
                                                        <div class="showcomment ">
                                                            <b>{{$comment->commentBy->full_name}}<b>
                                                                    <p class="col-lg-12">{{$comment->comment}}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            @endforeach
                    </div>
        </div>

        </section>

@endsection

