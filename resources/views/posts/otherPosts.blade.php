@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')

    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')
    <div class="col-md-7">
        <div class="write-post">
            <h3>Trending Posts</h3>
            <div class="tags_video">
                @foreach($companyTags as $companyTag)
                    @if($companyTag->is_post_exist_after_visit=== true)
                        <a href="{{route('user.company_wall',$companyTag->id)}}"
                           class="tags seen">{{$companyTag->name}}</a>
                    @else
                        <a href="{{route('user.company_wall',$companyTag->id)}}" class="tags ">{{$companyTag->name}}</a>
                    @endif
                @endforeach

            </div>
            <br/>
            <div class="content-img m-0 p-0 bg-white tranding @if($pageNo>0) d-none @endif">
                <div class="row">
                    @foreach($allPost as $trendingPosts)
                        <div class="col-md-4 pr-1">
                            @if(count($trendingPosts->getPostVideo->toArray())>0)
                                @if(sizeof($trendingPosts->getPostVideo)!==0)
                                    <div class="tranding_post">
                                        <video controls class="d-block w-100">
                                            <source src="{{$trendingPosts->getPostVideo->toArray()[0]['post_video']}}">
                                        </video>
                                        <a href="{{route('user.post.detail',$trendingPosts->id)}}s">View Details</a>
                                    </div>
                                @endif
                            @elseif(count($trendingPosts->getPostImage->toArray())>0)
                                @if(sizeof($trendingPosts->getPostImage)!==0)
                                    <div class="tranding_post">
                                        <img src="{{$trendingPosts->getPostImage->toArray()[0]['post_image']}}"
                                             class="img-fluid"/>
                                        <a href="{{route('user.post.detail',$trendingPosts->id)}}">View Details</a>

                                    </div>
                                @endif
                            @else
                                <div class="tranding_post">
                                    {{--<img src="{{$trendingPosts->getPostImage->toArray()[0]['post_image']}}"
                                         class="img-fluid"/>--}}
                                    <p>{{Str::limit($trendingPosts->description,50)}}</p>
                                    <a href="{{route('user.post.detail',$trendingPosts->id)}}">View Details</a>

                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="content-img m-0 p-0 bg-white tranding @if((count($allPost)<=9 )&& ($pageNo<1)) d-none @endif">
                <div class="row">
                    @foreach($records as $trendingPosts)
                        <div class="col-md-4 pr-1">
                            @if(count($trendingPosts->getPostVideo->toArray())>0)
                                @if(sizeof($trendingPosts->getPostVideo)!==0)
                                    <div class="tranding_post">
                                        <video controls class="d-block w-100">
                                            <source src="{{$trendingPosts->getPostVideo->toArray()[0]['post_video']}}">
                                        </video>
                                        <a href="{{route('user.post.detail',$trendingPosts->id)}}">View Details</a>
                                    </div>
                                @endif
                            @elseif(count($trendingPosts->getPostImage->toArray())>0)
                                {{-- @php dd($trendingPosts->getPostImage);die;@endphp--}}
                                @if(sizeof($trendingPosts->getPostImage)!==0)
                                    <div class="tranding_post">
                                        <img src="{{$trendingPosts->getPostImage->toArray()[0]['post_image']}}"
                                             class="img-fluid"/>
                                        <a href="{{route('user.post.detail',$trendingPosts->id)}}">View Details</a>

                                    </div>
                                @endif
                            @else
                                <div class="tranding_post">
                                    {{--<img src="{{$trendingPosts->getPostImage->toArray()[0]['post_image']}}"
                                         class="img-fluid"/>--}}
                                    <p>{{Str::limit($trendingPosts->description,50)}}</p>
                                    <a href="{{route('user.post.detail',$trendingPosts->id)}}">View Details</a>

                                </div>

                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <nav class="ml-auto float-right" aria-label="...">
                    <ul class="pagination">
                        {{-- <li class="page-item {{($pageNo == 0)?'disabled':''}}">
                             <a class="page-link"
                                href='{{($records->previousPageUrl())?$records->previousPageUrl():url()->current()}}'
                                tabindex="-1">Previous</a>
                         </li>--}}
                        {{-- @if($records->lastPage())--}}
                        {{--@php dd($records->currentPage());die;@endphp--}}
                        @if (!sizeof($records)==0)
                            <ul class="pagination ml-auto">
                                @for ($i = 1; $i <= $records->lastPage()+1; $i++)
                                    <li class="{{ (request('page') == ($i-1)) ? ' active' : '' }} page-item">
                                        <a class=" page-link "
                                           href="@if($i==1){{route('user.trending')}}@else {{$records->url($i-1)}}@endif">
                                            {{ $i }}
                                        </a>
                                    </li>
                                @endfor
                            </ul>
                        @endif
                        {{--@endif--}}

                        {{--@php dd($records->currentPage());die;@endphp--}}

                        {{--<li class="page-item {{($pageNo==$records->currentPage())?'disabled':''}}">
                            <a class="page-link" href="{{$records->nextPageUrl()}}">Next</a>
                        </li>--}}

                    </ul>
                </nav>
            </div>
        </div>
@endsection

