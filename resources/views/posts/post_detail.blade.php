@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')

    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')

    <div class="col-md-7">
        <div class="post">
                    <div class="write-post shadow-sm bg-white">
                        <div class="main-post">
                            <div class="post-describe d-flex">
                                <div class="port-profile">
                                    <img src="{{@$post_detail->postBy->logo}}" class="img-fluid" alt="postBYImage"/>
                                </div>
                                <div class="post-content">
                                    <h5>{{@$post_detail->postBy->full_name}}</h5>
                                    <p>
                                    {{date('d-m-yy, H:i:s', strtotime(@$post_detail->created_at))}}
                                </div>
                            </div>
                            <div class="post-content-bottm">
                                <p>{{ $post_detail->description }} </p>
                            </div>
                            <div class="content-img">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                            class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                            @if($post_detail->getPostImage != " ")
                                                @foreach($post_detail->getPostImage as $key => $getImage)

                                                @if($key+1==1)
                                                    <div class="carousel-item active">

                                                        <img class="d-block w-100" src="{{$getImage->post_image}}"
                                                             alt="First slide" style="height:400px">
                                                    </div>
                                                @else
                                                    <div class="carousel-item ">

                                                        <img class="d-block w-100" src="{{$post_detail->getPostImage->post_image}}"
                                                             alt="First slide" style="height:400px">
                                                    </div>
                                                @endif
                                            @endforeach
                                            @endif
                                            @if($post_detail->getPostVideo != " ")
                                                    @foreach($post_detail->getPostVideo as $getVideo)
                                                <div class="carousel-item">
                                                    <video controls class="d-block w-100" style="height:400px">
                                                        <source src="{{$getVideo->post_video}}">
                                                    </video>
                                                </div>
                                                    @endforeach
                                            @endif
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div class="like-comment d-flex align-items-center ">
                                <div class="like like-new active position-relative d-flex" style="margin-left: 100px;">
                                    @if($post_detail->is_like == true)
                                        <a href="javascript:void(0)" class="likes_and_dislikes"
                                           data-id="{{$post_detail->id}}"><i
                                                class="fas fa-thumbs-up text-primary"></i>
                                        </a>
                                    @else
                                        <a href="javascript:void(0)" class="likes_and_dislikes"
                                           data-id="{{$post_detail->id}}"><i
                                                class="far fa-thumbs-up"></i>
                                        </a>
                                    @endif
                                </div>
                                <div class="like float-right" style="margin-left: 300px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="22" viewBox="0 0 24 22">
                                        <g id="comment" transform="translate(-0.001 1.332)">
                                            <path id="Path_1" data-name="Path 1"
                                                  d="M20.7-1.332H3.3A3.3,3.3,0,0,0,0,1.956v10.61a3.3,3.3,0,0,0,3.288,3.288v4.815l6.945-4.815H20.7A3.3,3.3,0,0,0,24,12.566V1.956a3.3,3.3,0,0,0-3.3-3.288Zm1.893,13.9A1.892,1.892,0,0,1,20.7,14.452H9.792l-5.1,3.534V14.452H3.3a1.892,1.892,0,0,1-1.893-1.886V1.955A1.892,1.892,0,0,1,3.3.069H20.7a1.892,1.892,0,0,1,1.893,1.886Zm0,0"/>
                                            <path id="Path_2" data-name="Path 2"
                                                  d="M171.293,131.172h8.946V132.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -127.488)"/>
                                            <path id="Path_3" data-name="Path 3"
                                                  d="M171.293,211.172h8.946V212.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -204.459)"/>
                                            <path id="Path_4" data-name="Path 4"
                                                  d="M171.293,291.172h8.946V292.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -281.43)"/>
                                        </g>
                                    </svg>
                                    Comment
                                </div>


                            </div>
                            <div class="post-describe post-comment d-flex">
                                <div class="port-profile">
                                    <img src="{{Auth::user()->logo}}" class="img-fluid" alt="postBYImage"/>
                                </div>

                                <div class="post-content w-100">
                                    <form action="{{route('user.comments')}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{$post_detail->id}}" name="post_id">
                                        <div class="text-box">
                                            <input type="text" class="form-control" placeholder="Add a comment ..."
                                                   name="comment"/>
                                            <button type="submit"><img src="{{asset('public/home/img/send.svg')}}"
                                                                       class="send-btn" alt="dropdown"></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>


                            {{-- ------------------------------------------------------------------   poles ------------------------------------------}}
                            <div class="write-post shadow-sm bg-white" id="{{$post_detail->id}}">

                                <br>
                                    @foreach($post_detail->comments as $key => $comment)
                                    @if($comment->commentBy)
                                        <div class="post-describe d-flex reply_coment  my-4">

                                            <div class="port-profile" style="margin-left: 9px;">
                                                <img src="{{$comment->commentBy->logo}}" class="img-fluid"/>
                                            </div>
                                            <div class="post-content">
                                                <div class="post-content-bottm">
                                                    <div class="showcomment ">
                                                        <b>{{$comment->commentBy->full_name}}</b>
                                                        <p class="col-lg-12">{{$comment->comment}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @endforeach
                            </div>

                    </div>
        </div>

        </section>

@endsection

