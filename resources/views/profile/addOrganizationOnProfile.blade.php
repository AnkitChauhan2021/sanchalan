@extends('layouts.app')
@section('title',"Update Profile ")
@section('content')

    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')

    <div class="col-md-7">
        <div class="bg-white shadow-sm profile">
            <div class="user_profile ">
                <h3>Add New Organization</h3>
            </div>
            <form action="{{route('user.store.organization')}}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{$id}}">
                <div class="col-md-12 ">
                    <div class="row">
                        <label>Organizations</label>
                        <select class="form-control selected_company @error('company_id') is-invalid @enderror"
                                onchange="selectCompany()" name="company_id">
                            <option value=" ">Select Organization</option>
                            @foreach($companies as $company)
                                <option value="{{$company->id}}">{{$company->name}}</option>
                            @endforeach
                        </select>
                        @error('company_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row ">
                    <div class="col">
                        <label>Levels</label>

                        <select class="form-control @error('level_id') is-invalid @enderror" id="orglevels"
                                name="level_id">
                            <option value=" ">Select Level</option>
                        </select>
                        @error('level_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="col">
                        <label>Designation</label>
                        <input type="text" class="form-control @error('designation') is-invalid @enderror"
                               placeholder="Designation" name="designation">
                        @error('designation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn mt-3">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </div>

    @include('profile.script')

@endsection
