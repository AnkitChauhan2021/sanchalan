@extends('layouts.app')
@section('title',"Update Profile ")
@section('content')
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')
    <div class="col-md-7">
        <div class="bg-white shadow-sm profile">
            <div class="user_profile ">
                <h3>Update Profile</h3>
                <br/>
                @foreach($record as $value)
                    <form action="{{route('user.profile.update')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label>General Information</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>First Name</label>
                                <input type="text" class="form-control @error('first_name') is-invalid @enderror"
                                       placeholder="First name" value="{{$value->first_name}}" name="first_name">
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>

                            <div class="col-md-4">
                                <label>Last Name</label>
                                <input type="text" class="form-control @error('last_name') is-invalid @enderror"
                                       placeholder="Last name" value="{{$value->last_name}}" name="last_name">
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <label>Gender</label>
                                <select class="form-control @error('gender') is-invalid @enderror" name="gender">
                                    <option value="">Select Gender</option>
                                    @if($value->gender!=null)
                                        <option value="male"
                                                @if($value->gender=="male")Selected @endif>
                                            Male
                                        </option>
                                        <option value="female"
                                                @if($value->gender=="female")Selected @endif>
                                            Female
                                        </option>
                                    @else
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    @endif
                                </select>
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="col-md-12">
                                <label>About Me</label>
                                <input type="textarea" class="form-control h-75 @error('about_us') is-invalid @enderror"
                                       placeholder="Tell something about you.." value="{{$value->about_us}}"
                                       name="about_us">
                                @error('about_me')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <label>Organization Details </label>
                                @if(Auth::user()->role_id !== 3)
                                <a href="{{route('user.add.organization',$value->id)}}"
                                   class="float-right  text-primary"><u> Add New </u> </a>
                                    @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">Sr No</th>
                                        <th scope="col">Organization</th>
                                        <th scope="col">Level</th>
                                        <th scope="col">Designation</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($usercompanies as $key=> $result)
                                        <tr>
                                            <th scope="row">{{ $key+1}}</th>
                                            {{--@php dd($result->company);die();@endphp--}}
                                            <td><a href="{{route('user.company_wall',$result->company->id)}}">{{@$result->company->name}}</a></td>
                                            <td>{{@$result->level->name}}</td>
                                            <td>{{$result->designation}}</td>
                                            <td>
                                                <a href="javascript:void(0);"
                                                   class="text-danger action_btn confirmDelete"
                                                   data-action="{{route('user.organization.remove',$result->company->id)}}">Delete </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <label>Address </label>
                            </div>
                        </div>
                        <div id="address">
                            <div class="row">
                               {{-- <div class="col">
                                    <label>Countries</label>
                                    <select
                                        class="form-control selected_country @error('country_id') is-invalid @enderror"
                                        onchange="selectCountry()" name="country_id">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}"
                                                    @if($value->country_id==$country->id) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('country_id')
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>--}}
                                <div class="col">
                                    <label>States</label>
                                    <select class="form-control selected_states @error('state_id') is-invalid @enderror"
                                            id="statesGet" onchange="selectState()" name="state_id">
                                        <option value="">Select State</option>
                                        @foreach($States as $State)
                                                <option value="{{$State->id}}"  @if(@$value->state->id == $State->id) selected @endif>{{$State->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('state_id')
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label>Districts</label>
                                    <select
                                        class="form-control selected_district @error('district_id') is-invalid @enderror"
                                        id="districtGet" onchange="selectDistrict()" name="district_id">
                                        @if(@$value->district->name !== null)
                                            <option value=" ">{{@$value->district->name}}</option>
                                        @else
                                            <option value=" ">Please Select</option>
                                        @endif
                                    </select>
                                    @error('district_id')
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label>Cities</label>
                                    <select class="form-control @error('city_id') is-invalid @enderror" id="cityGet"
                                            name="city_id">
                                        @if(@$value->city->name !== null)
                                            <option value=" ">{{@$value->city->name}}</option>
                                        @else
                                            <option value=" ">Please Select</option>
                                        @endif
                                    </select>
                                    @error('city_id')
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn mt-3">Submit</button>
                            </div>
                        </div>
                    </form>
                    @include('profile.script')
                @endforeach
            </div>
        </div>
    </div>
@endsection
