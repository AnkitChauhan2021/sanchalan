@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')


    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                {{--            <img src="{{asset('public/home/img/user-bg.png')}}" class="cover_img image" id=""/>--}}
                <input type="file" class="profileImage image" style="display:none"/>

                <div class="user_profile text-center">
                    <div class="user_photo">
                        <img src="{{$record->logo}}" class="profile_image"/>

                        <a href="javascript:void(0)" class="edit_profile" onclick="OpenProfileImage()"><i
                                class="fas fa-camera"></i></a>
                        <a href="{{route('user.profile.edit')}}" class="update_cover_image"><i
                                class="fas fa-edit"></i></a>
                    </div>

                    <div class="user_name">

                        <p>{{$record->full_name}}</p>
                    </div>
                </div>
                <div class="follow-list d-flex align-self-center justify-content-center">
                    <div class="follow text-center">
                        <a href="{{route('user.profile.follower')}}"><p class="followers">Followers</p></a>
                        <label class="count_follower">{{ $record->followings()->get()->count() }}</label>
                    </div>
                    <div class="follow text-center">
                        <a href="#"><p class="followers">Following</p></a>
                        <label class="count_follower">{{$record->followers()->count()}}</label>
                    </div>
                </div>

                <hr/>
                <div class="user_about">
                    <h4>Following</h4>
                @foreach($record->userFollowings as $key => $follower)

                    <!-- <ul>
                            <li><b>{{$follower->first_name}}</b> </li>
                        </ul> -->
                        <div class="post-videe">
                            <img src="{{$follower->logo}}" style="height: 40px;" class="img-fluid"/>

                            <b>{{$follower->full_name}}</b>
                            | @if($follower->topLevelUserCompany->level)
                                (Level: {{$follower->topLevelUserCompany->level->name}})
                            @endif

                            @endforeach
                            <br/>
                        </div>
                </div>
            </div>
        </div>
@endsection
