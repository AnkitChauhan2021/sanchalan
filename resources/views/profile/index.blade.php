@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')




    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                {{--            <img src="{{asset('public/home/img/user-bg.png')}}" class="cover_img image" id=""/>--}}
                <input type="file" class="profileImage image" style="display:none"/>

                <div class="user_profile text-center">
                    <div class="user_photo">
                        <img src="{{$record->logo}}" class="profile_image"/>

                        <a href="javascript:void(0)" class="edit_profile" onclick="OpenProfileImage()"><i
                                class="fas fa-camera"></i></a>
                        <a href="{{route('user.profile.edit')}}" class="update_cover_image"><i
                                class="fas fa-edit"></i></a>
                    </div>

                    <div class="user_name">



                        <p>{{$record->full_name}}</p>
                    </div>
                </div>
                <div class="follow-list d-flex align-self-center justify-content-center">
                    <div class="follow text-center">
                        <a href="{{route('user.profile.follower')}}"><p class="followers">Followers</p></a>
                        <label class="count_follower">{{$record->followings()->count()}}</label>
                    </div>
                    <div class="follow text-center">
                        <a href="{{route('user.profile.following')}}"><p class="followers">Following</p></a>
                        <label class="count_follower">{{$record->followers()->count()}}</label>
                    </div>
                </div>

                <hr/>
            <!-- <a href="{{route('user.follow',2)}}">Follow</a>
                        <a href="javascript:void(0)" class="likes_and_dislikes" data-id="20"><i class="far fa-thumbs-up"></i></a> -->


                <div class="user_about">
                    <h4>About</h4>
                    <ul>
                        <li><b>My Company:</b>
                            @foreach($usercompanies as $key=> $result)
                                <a href="{{route('user.company_wall',$result->company->id)}}">{{@$result->company->name}} <span class="text-dark">{{ '|' }}</span></a>
                            @endforeach

                        </li>
                        @if($record->designation!=null)
                            <li><b>Designation:</b> {{$record->designation}}</li>
                        @endif
                        @if($record->email!=null)
                            <li><b>Email:</b> {{$record->email }}</li>
                        @endif
                        <li><b>Phone:</b> {{$record->mobile }}</li>
                        @if($record->company!=null)
                            <li><b>Company:</b> {{$record->company->name }}</li>
                        @endif
                        <li><b>About Me:</b> {{$record->about_us }}</li>
                    </ul>
                    <br/>
                    {{--                <a href="#">More</a>--}}
                </div>
                @if($record->isApproved==1)
                    <a href="{{route('user.post.index')}}" class="btn create_post">CREATE POST</a>
                @endif
                <div class="post_video_profile">
                    <ul class="nav nav-tabs nav-pills nav-justified" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                               aria-controls="home" aria-selected="true"><img
                                    src="{{asset('public/home/img/icon-11.png')}}"/>Your Post</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                               aria-controls="profile" aria-selected="false"><img
                                    src="{{asset('public/home/img/icon-12.png')}}"/> Your Video</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="post_img" id="lightgallery">
                            @foreach($record->userPost as $key =>$post)
                                @foreach($post->getPostImage as $key =>$getImage)
                                    @if($getImage->post_image != " ")
                                        <a class="post-videe" href="{{$getImage->post_image}}">
                                            <img src="{{$getImage->post_image}}" class="img-fluid"/>
                                        </a>
                                    @endif
                                @endforeach
                            @endforeach
                        </div>


                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="post_img">
                            @foreach($record->userPost as $key =>$post)
                                @foreach($post->getPostVideo as $key =>$getVideo)
                                    @if($getVideo->post_video != " ")
                                        <div class="post-videe">
                                            <video controls class="d-block w-100" style="height: 110px;">
                                                <source src="{{$getVideo->post_video}}">
                                            </video>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{-----------------------------------------------------Cropping model ----------------------------------}}
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="img-container" style="max-height: 500px">
                                <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lightgallery").lightGallery();
        });
    </script>
@endsection
