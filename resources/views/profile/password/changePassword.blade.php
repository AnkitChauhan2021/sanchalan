@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    @include('elements.layoutsElements.navbar')
    {{--  User Side menu page include   --}}

    @include('elements.layoutsElements.profile.index')

    <div class="col-md-7 ">
        <div class="container">
        <div class="bg-white shadow-sm profile">
            <div class="user_profile ">
                <h3>Change password</h3>
                <br/>
                <form action="{{route('user.password.update')}}" method="post" style="margin-left: 117px;">
                    @csrf

                        <div class="col-lg-8">
                            <label>Current Password<span>*</span></label>
                            <input type="password" class="form-control @error('old_password') is-invalid @enderror" placeholder=" " value="" name="old_password" autocomplete="off">
                            @error('old_password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    <div class="col-lg-8">
                        <label>New Password<span>*</span></label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="" value="" name="password" autocomplete="off">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-lg-8">
                        <label>Confirm Password<span>*</span></label>
                        <input type="password" class="form-control @error('confirm_password') is-invalid @enderror" placeholder="" value="" name="confirm_password" autocomplete="off">
                        @error('confirm_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn mt-3" style="margin-left: 14px;">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>



    @endsection
