<script>
    const selectCompany =() =>{
        const companis = document.getElementsByClassName('selected_company');
        company = companis[0].value;
        $.ajax({
            type: "GET",
            url: "{{url('/user/levels')}}?comapny_id=" + company,
            success: function (res) {

                if (res) {

                    $("#orglevels").empty();
                    $("#orglevels").append('<option value=" "> Select Level</option>');
                    $.each(res, function (key, value) {
                        $("#orglevels").append('<option value="' + value.levels.id + '">' + value.levels.name + '</option>');

                    });
                } else {
                    $("#orglevels").empty();
                }
            }
        });
    }
    const selectCountry= () => {
        const countries = document.getElementsByClassName('selected_country');
        country = 1;//countries[0].value;

        $.ajax({
            type: "GET",
            url: "{{url('/user/country')}}?country_id=" + country,
            success: function (res) {

                if (res) {

                    $("#statesGet").empty();
                    $("#statesGet").append('<option>Please Select </option>');
                    $.each(res, function (key, value) {
                        $("#statesGet").append('<option value="' + value.id + '">' + value.name + '</option>');

                    });
                } else {
                    $("#statesGet").empty();
                }
            }
        });
    }
    $(document).ready(function () {
        selectCountry();
    });

    const  selectState = () =>{
        const states = document.getElementsByClassName('selected_states');
        state = states[0].value;

        $.ajax({
            type: "GET",
            url: "{{url('user/state')}}?state_id=" + state,
            success: function (res) {
                if (res) {
                    $("#districtGet").empty();
                    $("#districtGet").append('<option>Please Select State</option>');
                    $.each(res, function (key, value) {

                        $("#districtGet").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    $("#districtGet").empty();
                }
            }
        });

    }
    const selectDistrict = () =>{
        const districts = document.getElementsByClassName('selected_district');
        district = districts[0].value;
        $.ajax({
            type: "GET",
            url: "{{url('user/district')}}?district_id=" + district,
            success: function (res) {
                if (res) {
                    $("#cityGet").empty();
                    $("#cityGet").append('<option>Please Select</option>');
                    $.each(res, function (key, value) {

                        $("#cityGet").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    $("#cityGet").empty();
                }
            }
        });

    }
    const Organizations = ()=>{
        $("._organization").toggle()
    }
    const adress =() =>{
        $("#address").toggle();
    }
</script>
