@extends('layouts.app')
@section('title',Auth::user()->full_name)
@section('content')
    {{--  Home Page Mune Include    --}}
    {{--    @include('elements.layoutsElements.header')--}}
    {{--  User Side menu page include   --}}
    @include('elements.layoutsElements.navbar')
    @include('elements.layoutsElements.profile.index')
    @php $user_id =Auth::user()->id;@endphp

    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                {{--            <img src="{{asset('public/home/img/user-bg.png')}}" class="cover_img image" id=""/>--}}
                <input type="file" class="profileImage image" style="display:none"/>

                <div class="user_profile text-center">
                    <div class="user_photo">

                        <img src="{{$record->logo}}" class="profile_image"/>
                        {{--<a href="javascript:void(0)" class="edit_profile" onclick="OpenProfileImage()"><i
                                class="fas fa-camera"></i></a>--}}

                        {{-- <a href="{{route('user.profile.edit')}}" class="update_cover_image" ><i
                             class="fas fa-edit"></i></a>--}}
                    </div>

                    <div class="user_name">

                        <p>{{$record->full_name}}
                            @if($record->role_id == 3 || $record->role_id == 1 )
                                <img style="height: 23px;"
                                     src="https://img.icons8.com/color/50/000000/instagram-verification-badge.png"/>
                            @endif
                        </p>
                        @if($record->id != Auth::user()->id)
                            @foreach($record->userFollowers as $userFollower)
                                @if($userFollower->id == Auth::user()->id)
                                    <a href="{{route('user.follow',$record->id)}}">
                                        Unfollow
                                    </a>
                                @endif
                            @endforeach
                            @if(!$record->userFollowers->contains(Auth::user()->id))

                                <a href="{{route('user.follow',$record->id)}}">
                                    Follow
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="follow-list d-flex align-self-center justify-content-center">
                    <div class="follow text-center">
                        <p class="followers">Followers</p>
                        <label class="count_follower">{{ $record->followings()->get()->count() }}</label>
                    </div>
                    <div class="follow text-center">
                        <p class="followers">Following</p>
                        <label class="count_follower">{{$record->followers()->count()}}</label>
                    </div>


                </div>

                <hr/>
            <!-- <a href="{{route('user.follow',2)}}">Follow</a>
                        <a href="javascript:void(0)" class="likes_and_dislikes" data-id="20"><i class="far fa-thumbs-up"></i></a> -->


                <div class="user_about">
                    <h4>About</h4>
                    <ul>
                        <li><b>Organization:</b>
                            @foreach($record->userComapny as $key =>$companyname)
                                {{@$companyname->company->name}},
                            @endforeach
                        </li>

                        @if($record->designation != "")
                            <li><b>Designation:</b> {{$record->designation}}</li>
                        @else
                            <li><b>Level:</b>
                                @foreach($record->userComapny as $key2 =>$levelname)
                                    {{@$levelname->level->name}}
                                @endforeach
                            </li>
                        @endif

                        @if($record->id == Auth::user()->id)
                            <li><b>Email:</b> {{$record->email }}</li>
                            <li><b>Phone:</b> {{$record->mobile }}</li>
                        @endif

                        @if($record->company!=null)
                            <li><b>Company:</b> {{$record->company->name }}</li>
                        @endif

                        <li><b>About Me:</b> {{$record->about_us }}</li>
                    </ul>
                    <br/>
                    {{--                <a href="#">More</a>--}}
                </div>
                @if($record->id == Auth::user()->id)
                    @if($record->isApproved==1)
                        <a href="{{route('user.post.index')}}" class="btn create_post">CREATE POST</a>
                    @endif
                @endif
                <div class="post_video_profile">
                    <ul class="nav nav-tabs nav-pills nav-justified" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            @if($record->id == Auth::user()->id)
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                   aria-controls="home" aria-selected="true"><img
                                        src="{{asset('public/home/img/icon-11.png')}}"/> Your Post</a>
                            @else
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                   aria-controls="home" aria-selected="true"><img
                                        src="{{asset('public/home/img/icon-11.png')}}"/>Post</a>
                            @endif
                        </li>

                        <li class="nav-item" role="presentation">
                            @if($record->id == Auth::user()->id)
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                   aria-controls="profile" aria-selected="false"><img
                                        src="{{asset('public/home/img/icon-12.png')}}"/> Your Video</a>
                            @else
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                   aria-controls="profile" aria-selected="false"><img
                                        src="{{asset('public/home/img/icon-12.png')}}"/>Video</a>
                            @endif
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="post_img" id="lightgallery">
                            @foreach($record->userPost as $key =>$post)
                                @foreach($post->getPostImage as $key =>$getImage)
                                    @if($getImage->post_image != " ")
                                        <a class="post-videe" href="{{$getImage->post_image}}">
                                            <img src="{{$getImage->post_image}}" class="img-fluid"/>
                                        </a>
                                    @endif
                                @endforeach
                            @endforeach
                        </div>


                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="post_img">
                            @foreach($record->userPost as $key =>$post)
                                @foreach($post->getPostVideo as $key =>$getVideo)
                                    @if($getVideo->post_video != " ")
                                        <div class="post-videe">
                                            <video controls class="d-block w-100" style="height: 110px;">
                                                <source src="{{$getVideo->post_video}}">
                                            </video>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{-----------------------------------------------------Cropping model ----------------------------------}}
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-8">
                                <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lightgallery").lightGallery();
        });
    </script>
@endsection
