<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register</div>
                <div class="card-body">


                    <form class="form-horizontal" method="post" action="{{route('testing')}}">
                        @csrf
                        @foreach ($formFeilds as $field)

                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label"> {{ $field->name }} <span>({{ $field->translated_name }})</span> </label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="{{ $field->slug }}"  placeholder="Enter your Name" />
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                        </div>
                        <div class="login-register">
                            <a href="index.php">Login</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
