<?php

use App\Http\Controllers\Api\ChatController;
use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\CmsController;
use App\Http\Controllers\Api\CompanyWallsController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\DistrictController;
use App\Http\Controllers\Api\LevelsController;
use App\Http\Controllers\Api\MagazinesController;
use App\Http\Controllers\Api\MeetingController;
use App\Http\Controllers\Api\OrganizationController;
use App\Http\Controllers\Api\PolesController;
use App\Http\Controllers\Api\PostsController;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\SettingsController;
use App\Http\Controllers\Api\StateController;
use App\Http\Controllers\Api\StatusController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::namespace('Api')->group(function () {

        Route::post('/testing', [PostsController::class, 'testing']);
        Route::post('/login', [UserController::class, 'login']);
        Route::get('/users', [UserController::class, 'users']);
        //       get all organizations list
        Route::get('/organizations', [OrganizationController::class, 'index'])->name('organizations.index');
//        get All Countries List
        Route::get('/countries', [CountryController::class, 'index'])->name('countries.index');

//        get All states List
        Route::get('/states/{country_id?}', [StateController::class, 'index'])->name('states.index');
//        get All Districts List
        Route::get('/districts/{state_id?}', [DistrictController::class, 'index'])->name('districts.index');
//        get All Cities List
        Route::get('/cities/{district_id?}', [CityController::class, "index"])->name('cities.index');
//        levels

        Route::get('/levels/{organization_id?}', [LevelsController::class, 'index'])->name('levels.index');
        Route::post('/get_all_levels', [LevelsController::class, 'Get_All_Level'])->name('levels.Get_All_Level');


//        user register Api
        Route::post('user/registration', [RegisterController::class, 'store'])->name('user.register');
        Route::post('/verified', [RegisterController::class, 'isVerifiedPhone'])->name('user.verify');
        Route::post('/forget-password/mobile', [UserController::class, 'generateOtpForGetPassword'])->name('forgetPassword/.mobile');
        Route::post('/forget-password/otp', [UserController::class, 'GetVerifyOtp'])->name('forgetPassword/.otp');
        Route::post('/forget-password/', [UserController::class, 'forGetPassword'])->name('forgetPassword');

//        Resent OTP
        Route::post('/resentOtp', [UserController::class, 'resetSent'])->name('resent.otp');


//       Get CMS Pages

        Route::get('/cms/', [CmsController::class, 'index'])->name('cms');
        Route::get('/cms/list', [CmsController::class, 'cmsList']);

        Route::get('/cms/', [CmsController::class, 'index'])->name('cms');
        Route::get('/cms/list', [CmsController::class, 'cmsList']);


//        for advance search state dist and city

        Route::post('/search/states', [UserController::class, 'states']);
        Route::post('/search/districts', [UserController::class, 'districts']);
        Route::post('/search/cities', [UserController::class, 'cities']);


    });
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('/profile', [UserController::class, 'profile']);
        Route::get('/left-company', [UserController::class, 'LeftCompany']);


        Route::post('/join-company', [UserController::class, 'joinCompany']);

        Route::post('profile/update', [RegisterController::class, 'Profileupdate']);
        Route::post('profile/image/update', [RegisterController::class, 'profileImageUpdate']);

        Route::post('exitCompany', [RegisterController::class, 'exitCompany']);

        Route::get('/logout', [UserController::class, 'logout']);

//      Get All Posts
        Route::get('/getAllPosts/', [PostsController::class, 'index']);
        Route::post('/addPost/', [PostsController::class, 'store']);
        Route::get('/post-details/{id}/', [PostsController::class, 'postDetail']);
        Route::post('/updatePost/', [PostsController::class, 'update']);
        Route::delete('/deletePostImage/{id}/', [PostsController::class, 'deletePostImage']);
        Route::delete('/deletePostVideo/{id}/', [PostsController::class, 'deletePostVideo']);
        Route::delete('/post/destroy/{id}/', [PostsController::class, 'destroy']);
        Route::post('/post/changeStatus', [PostsController::class, 'changeStatus']);


        Route::post('/user/posts', [PostsController::class, 'UserPosts']);

//        post Comments
        Route::post('addComment', [PostsController::class, 'storeComment']);

//       List list Route
        Route::post('likes', [PostsController::class, 'getLikes']);
        Route::post('likes-unlike', [PostsController::class, 'LikesUnlike']);

//        follow
        Route::get('/follow-unfollow', [UserController::class, 'followUnfollow']);

//        post block and unclock
        Route::post('/block-unblock', [PostsController::class, 'blockUnblock']);

//        poles create
        Route::post('/create/poles', [PolesController::class, 'create']);
        Route::get('/poles/{id}', [PolesController::class, 'index']);
        Route::post('/submit/pole/', [PolesController::class, 'submitPole']);

//         users
        Route::post('/users/search', [UserController::class, 'userSearch']);

//        magazine Routes
        Route::get('magazines', [MagazinesController::class, 'index']);
        Route::post('magazine/create', [MagazinesController::class, 'store']);
        Route::post('magazine/update', [MagazinesController::class, 'update']);
        Route::delete('/magazine/delete/{id}', [MagazinesController::class, 'destroy']);
        Route::delete('/magazine/image-delete/{id}', [MagazinesController::class, 'destroyImage']);
        Route::delete('/magazine/video-delete/{id}', [MagazinesController::class, 'destroyVideo']);
//      magazine Like- unlike
        Route::get('magazine/like-unlike/', [MagazinesController::class, 'likesUnlike']);
//      Magazine Comment
        Route:: post('magazine/comment/', [MagazinesController::class, 'comments']);
        Route:: get('magazine/{id}/', [MagazinesController::class, 'magazineDetail']);


//        company wall
        Route::get('company/wall/{company_id}', [CompanyWallsController::class, 'index']);

        Route::get('users/detail/{id}/', [UserController::class, 'userDetail']);
        Route::post('notificationSetting', [SettingsController::class, 'notificationSetting']);
        Route::post('/follower-Following-List', [UserController::class, 'followerAndFollowingList']);


//        Trending Posts
        Route::Post('trending/posts', [PostsController::class, 'trendingPosts']);
        Route::get('/post/report/{id}', [PostsController::class, 'reportPost']);

//        Advance search
        Route::post('advance_search_step_one', [UserController::class, 'AdvanceSearchStepOne']);
        Route::post('advance_search_step_two', [UserController::class, 'AdvanceSearchStepTwo']);
        Route::post('advance_search_step_third', [UserController::class, 'AdvanceSearchStepThird']);
        Route::post('advance_search_step_final', [UserController::class, 'AdvanceSearchStepFinal']);


//        chat
//        chat Request Route
        Route::post('chat/request', [ChatController::class, 'chatRequestStore']);
        Route::post('chat/request/accept-reject', [ChatController::class, 'AcceptRequest']);
        Route::get('chat/request/send/list', [ChatController::class, 'SendRequestList']);
        Route::get('chat/request/receive/list', [ChatController::class, 'ReceiveRequestList']);
        Route::get('chat/confirm/users/list', [ChatController::class, 'ConfirmChatUsers']);


//          meetings
        Route::get('/user_meetings', [MeetingController::class, 'index']);
        Route::post('/meeting_status', [MeetingController::class, 'changeStatus']);

//          create meeting by admin

        Route::post('/create/meeting', [MeetingController::class, 'create']);

        Route::post('/create/meeting', [MeetingController::class, 'create']);
        Route::get('/organization_received_meeting_list', [MeetingController::class, 'organizationReceivedMeetingsList']);
        Route::get('/send_meeting_list', [MeetingController::class, 'sendMeetingList']);
        Route::post('/accept_meeting_by_Organization', [MeetingController::class, 'acceptMeetingByOrganization']);
        Route::get('/organization_remove_from_the_meeting/{id}', [MeetingController::class, 'OrganizationRemoveFromMeeting']);
        Route::get('/remove_meeting/{id}', [MeetingController::class, 'meetingDelete']);
        Route::post('/update_meeting', [MeetingController::class, 'meetingUpdate']);


//           suggestion Users api


        Route::get('/user-city-suggestions', [UserController::class, 'UserCitySuggestion']);

        Route::get('/user-city-suggestions', [UserController::class, 'UserCitySuggestion']);

//        status routes
        Route::post('/create/status', [StatusController::class, 'createStatus']);
        Route::get('/destroy/status/{id}', [StatusController::class, 'removeStatus']);
        Route::get('/status', [StatusController::class, 'index']);


//         new advance search user
        Route::post('search/users', [UserController::class, 'usersListWithFilter']);


//        notification count

        Route::get('/notifications/count',[ChatController::class,'getNotificationCount']);


        Route::prefix('messages')->name('messages.')->group(function () {
            Route::post("/create-conversation", [\App\Http\Controllers\Api\MessagesController::class, "createConversation"])->name('create-conversation');
            Route::post("/chat-history", [\App\Http\Controllers\Api\MessagesController::class, "chatHistory"])->name('chat-history');
            Route::post("/groups", [\App\Http\Controllers\Api\MessagesController::class, "groups"])->name('groups');
            Route::post("/get-group-members", [\App\Http\Controllers\Api\MessagesController::class, "getGroupMembers"])->name('getGroupMembers');
            Route::post("/add-group-user", [\App\Http\Controllers\Api\MessagesController::class, "addGroupUser"])->name('addGroupUser');
            Route::post("/create-group", [\App\Http\Controllers\Api\MessagesController::class, "createGroup"])->name('createGroup');
            Route::post("/assign-group-admin", [\App\Http\Controllers\Api\MessagesController::class, "toggleAdminRights"])->name('toggleAdminRights');
            Route::post("/leave-group", [\App\Http\Controllers\Api\MessagesController::class, "leaveGroup"])->name('leaveGroup');
            Route::post("/clear-chat-history", [\App\Http\Controllers\Api\MessagesController::class, "clearChatHistory"])->name('clearChatHistory');
            Route::post("/remove-group-member", [\App\Http\Controllers\Api\MessagesController::class, "removeGroupMember"])->name('removeGroupMember');
            Route::post("/block-user", [\App\Http\Controllers\Api\MessagesController::class, "blockUser"])->name('blockUser');
            Route::post("/unblock-user", [\App\Http\Controllers\Api\MessagesController::class, "unblockUser"])->name('unblockUser');
        });





        Route::prefix('messages')->name('messages.')->group(function () {
            Route::post("/upload-file", [\App\Http\Controllers\Api\MessageController::class, "uploadFile"]);
        });
    });
    Route::post("/create-message", [\App\Http\Controllers\Api\MessagesController::class, "createMessage"])->name('create_message');
    Route::post("/message-delivered", [\App\Http\Controllers\Api\MessagesController::class, "messageDelivered"])->name('message-delivered');
    Route::post("/get-unread-count", [\App\Http\Controllers\Api\MessagesController::class, "getUnreadCount"])->name('get-unread-count');
});

