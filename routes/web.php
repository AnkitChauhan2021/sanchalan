<?php

use App\Http\Controllers\Admin\Auth\ChangePasswordController;
use App\Http\Controllers\Admin\Auth\ForgotPasswordController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\CmsController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DistrictsController;
use App\Http\Controllers\Admin\GroupsController;
use App\Http\Controllers\Admin\InquiryController;
use App\Http\Controllers\Admin\LevelsController;
use App\Http\Controllers\Admin\MagazinesController;
use App\Http\Controllers\Admin\OrganizationController;
use App\Http\Controllers\Admin\PollsController;
use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Admin\ProfileConntroller;
use App\Http\Controllers\Admin\StatesController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\FollowsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LikesController;
use App\Http\Controllers\Organization\Auth\OrganizationLoginController;
use App\Http\Controllers\Organization\Auth\PasswordChangeController;
use App\Http\Controllers\Organization\Auth\PasswordForgotController;
use App\Http\Controllers\Organization\DashboardsController;
use App\Http\Controllers\Organization\MeetingsController;
use App\Http\Controllers\Organization\OrganizationProfileController;
use App\Http\Controllers\Organization\OrganizationUsersController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\WebController;
use App\Models\Cms;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::group(['prefix' => 'admin'], function () {
//    Route::get('/1', function ()    {
//        return 'hello';
//    });
//});






Route::get('/', [WebController::class, 'index'])->name('welcome.index');


//Get contact (contact_us )
Route::post('/contact_us/', [WebController::class, 'contact_us'])->name('contacts_inquiry.store');

Route::get('/home', [HomeController::class, 'index'])->name('home');

//Route::get('/',function (){
//    return "<h1 style='margin-left: 70vh; margin-top: 40vh'>Page under Construction</h1>";
//});
Route::get('/login', function () {
    return redirect()->route('welcome.index')->with("error_code", 'Route Not Found');
});
Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Auth::routes(['login' => false]);
//   admin route


Route::prefix('admin')->name('admin.')->group(function () {
    //   Admin Login
//       Auth::routes(['register' => false]);

    Route::get('/', [LoginController::class, 'adminLoginForm'])->name('showLoginForm');
    Route::post('/login', [LoginController::class, 'postLogin'])->name('login');
    //  admin password reset
    Route::get('/password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('/password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');

    // admin Logout
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

    Route::middleware(['is_admin'])->group(function () {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');

        //  admin profile route

        Route::get('/profile', [ProfileConntroller::class, 'index'])->name('profile.index');
        Route::get('/profile/edit', [ProfileConntroller::class, 'edit'])->name('profile.edit.index');
        Route::post('/profile/update', [ProfileConntroller::class, 'update'])->name('profile.update');
        Route::post('/profile/image/update', [ProfileConntroller::class, 'upload'])->name('profile.image.update');
        //      Admin change Password
        Route::get('/change_password', [ChangePasswordController::class, 'index'])->name('change_password.index');
        Route::post('/change_password', [ChangePasswordController::class, 'changePassword'])->name('changePassword');

        // master section


        // Country
        Route::get('/country', [CountryController::class, 'index'])->name('country.index');
        Route::get('/country/create', [CountryController::class, 'create'])->name('country.create');
        Route::post('/country/store', [CountryController::class, 'store'])->name('country.store');
        Route::get('country/changeStatue', [CountryController::class, 'ChangeStatus'])->name('country.changeStatus');
        Route::get('country/remove/{id}', [CountryController::class, 'remove'])->name('country.remove');
        Route::get('country/edit/{id}', [CountryController::class, 'edit'])->name('country.edit');
        Route::post('country/update', [CountryController::class, 'update'])->name('country.update');
        // state
        Route::get('/state', [StatesController::class, 'index'])->name('state.index');
        Route::get('/state/create', [StatesController::class, 'create'])->name('state.create');
        Route::post('/state/store', [StatesController::class, 'store'])->name('state.store');
        Route::get('state/changeStatue', [StatesController::class, 'ChangeStatus'])->name('state.changeStatus');
        Route::get('state/edit/{id}', [StatesController::class, 'edit'])->name('state.edit');
        Route::post('state/update', [StatesController::class, 'update'])->name('state.update');
        Route::get('state/remove/{id}', [StatesController::class, 'remove'])->name('state.remove');
        Route::post('state/view/', [StatesController::class, 'view'])->name('state.view');

        // District
        Route::get('/district', [DistrictsController::class, 'index'])->name('district.index');
        Route::get('/district/create', [DistrictsController::class, 'create'])->name('district.create');
        Route::post('/district/store', [DistrictsController::class, 'store'])->name('district.store');
        Route::post('/district/view/', [DistrictsController::class, 'view'])->name('district.view');
        Route::get('/district/edit/{id}', [DistrictsController::class, 'edit'])->name('district.edit');
        Route::post('district/update', [DistrictsController::class, 'update'])->name('district.update');
        Route::get('district/remove/{id}', [DistrictsController::class, 'remove'])->name('district.remove');
        Route::get('district/changeStatue', [DistrictsController::class, 'ChangeStatus'])->name('district.changeStatus');
        // city
        Route::get('/city', [CityController::class, 'index'])->name('city.index');
        Route::get('/city/create', [CityController::class, 'create'])->name('city.create');
        Route::get('/city/getDistricts', [CityController::class, 'getDistrict'])->name('city.getDistricts');
        Route::post('/city/store', [CityController::class, 'store'])->name('city.store');
        Route::post('/city/view/', [CityController::class, 'view'])->name('city.view');
        Route::get('/city/edit/{id}', [CityController::class, 'edit'])->name('city.edit');
        Route::post('/city/update', [CityController::class, 'update'])->name('city.update');
        Route::get('city/remove/{id}', [CityController::class, 'remove'])->name('city.remove');
        Route::get('city/changeStatue', [CityController::class, 'ChangeStatus'])->name('city.changeStatus');

        // config section
        Route::get('/config/create', [ConfigurationController::class, 'create'])->name('config.create');
        Route::post('/config/store', [ConfigurationController::class, 'store'])->name('config.store');
        Route::get('/config', [ConfigurationController::class, 'index'])->name('config.index');
        Route::get('config/remove/{id}', [ConfigurationController::class, 'remove'])->name('config.remove');
        Route::get('config/edit/{id}', [ConfigurationController::class, 'edit'])->name('config.edit');
        Route::post('config/update', [ConfigurationController::class, 'update'])->name('config.update');
        Route::get('config/sms_api/changeStatue', [ConfigurationController::class, 'ChangeStatus'])->name('config.sms_api.changeStatus');



        // Settings  for Trending Post
        Route::get('/setting/trending/posts',[ConfigurationController::class,"trendingPost"])->name('setting.trending.post');
        Route::get('/setting/trending/posts/edit/{id}',[ConfigurationController::class,"trendingPostEdit"])->name('setting.trending.post.edit');
        Route::post('/setting/trending/posts/update/',[ConfigurationController::class,"trendingPostUpdate"])->name('setting.trending.post.update');



        // Levels
//        Route::get('/level', [LevelsController::class, 'index'])->name('level.index');
//        Route::get('/level/create/', [LevelsController::class, 'create'])->name('level.create');
//        Route::post('/level/store/', [LevelsController::class, 'store'])->name('level.store');
//        Route::get('/level/edit/{id}', [LevelsController::class, 'edit'])->name('level.edit');
//        Route::post('/level/update', [LevelsController::class, 'update'])->name('level.update');
//        Route::get('/level/remove/{id}', [LevelsController::class, 'remove'])->name('level.remove');
//        Route::get('level/changeStatue', [LevelsController::class, 'ChangeStatus'])->name('level.changeStatus');

        //Organization
        Route::get('/organization', [OrganizationController::class, 'index'])->name('organization.index');
        Route::get('/organization/create/', [OrganizationController::class, 'create'])->name('organization.create');
        Route::get('/organization/getDistricts', [OrganizationController::class, 'getDistrict'])->name('organization.getDistricts');
        Route::get('/organization/getState', [OrganizationController::class, 'getState'])->name('organization.getState');
        Route::get('/organization/getCities', [OrganizationController::class, 'getCities'])->name('organization.getCities');
        Route::post('/organization/store/', [OrganizationController::class, 'store'])->name('organization.store');
        Route::get('/organization/edit/{id}', [OrganizationController::class, 'edit'])->name('organization.edit');
        Route::post('/organization/update/', [OrganizationController::class, 'update'])->name('organization.update');
        Route::get('/organization/remove/{id}', [OrganizationController::class, 'remove'])->name('organization.remove');
        Route::get('organization/changeStatue', [OrganizationController::class, 'ChangeStatus'])->name('organization.changeStatus');
        Route::get('organization/member/list/{id}', [OrganizationController::class, 'memberList'])->name('organization.memberList');
        Route::get('organization/member/delete/{id}', [OrganizationController::class, 'memberDelete'])->name('organization.memberDelete');
        Route::get('organization/likeTojoin', [OrganizationController::class, 'likeTojoin'])->name('organization.likeTojoin');
        Route::post('organization/toggle-admin-head', [OrganizationController::class, 'toggleAdminHead'])->name('organization.toggleAdminHead');

        Route::get('/organization/addMember/{organization_id}',[OrganizationController::class,'addMember'])->name('organization.addMember');
        Route::post('/organization/addMember/store',[OrganizationController::class,'addMemberStore'])->name('organization.addMember.store');

//            Users routes

        Route::get('users/', [UsersController::class, 'index'])->name('users.index');
        Route::get('/users/remove/{id}', [UsersController::class, 'remove'])->name('users.remove');
        Route::get('/users/destroy/userCompany/{id}', [UsersController::class, 'destroyUserCompany'])->name('users.destroy.userCompany');
        Route::get('users/create', [UsersController::class, 'create'])->name('users.create');
        Route::get('/users/getLevels', [UsersController::class, 'getLevels'])->name('users.getLevels');
        Route::post('users/store/', [UsersController::class, 'store'])->name('users.store');
        Route::get('users/edit/{id}/', [UsersController::class, 'edit'])->name('users.edit');
        Route::post('/users/update/', [UsersController::class, 'update'])->name('users.update');
        Route::post('/users/change/password', [UsersController::class, 'changeUserPassword'])->name('users.changeUserPassword');
        Route::get('users/Permission', [UsersController::class, 'approvedOrNot'])->name('users.Permission');
        Route::get('user/add/Organization/{id}/', [UsersController::class, 'addOrganizationOnUserProfile'])->name('user.add.Organization');
        Route::post('admin/user/organization/store', [UsersController::class, 'UserOrganizationStore'])->name('user.organization.store');
        Route::get('/user/changeStatue', [UsersController::class, 'ChangeStatus'])->name('users.changeStatus');
        Route::get('/user/org/{id}', [UsersController::class, 'userOrganization'])->name('users.organization');
        Route::get('/user/notifications', [UsersController::class, 'notifications'])->name('users.notifications');


//          CMS
        Route::get('/cms', [CmsController::class, "index"])->name('cms.index');
        Route::get('/cms/create', [CmsController::class, "Create"])->name('cms.create');
        Route::post('/cms/store', [CmsController::class, 'Store'])->name('cms.store');
        Route::get('/cms/remove/{id}', [CmsController::class, 'destroy'])->name('cms.remove');
        Route::get('cms/changeStatue', [CmsController::class, 'ChangeStatus'])->name('cms.changeStatus');
        Route::get('/cms/edit/{id}/', [CmsController::class, 'edit'])->name('cms.edit');
        Route::post('/cms/update/', [CmsController::class, 'update'])->name('cms.update');

//          Posts
        Route::get('/posts/', [PostsController::class, 'index'])->name('posts.index');

        Route::get('/posts/create', [PostsController::class, 'create'])->name('posts.create');

        Route::post('/posts/store', [PostsController::class, 'Store'])->name('posts.store');
        Route::get('/posts/remove/{id}', [PostsController::class, 'destroy'])->name('posts.remove');
        Route::get('/posts/changeStatue', [PostsController::class, 'ChangeStatus'])->name('posts.changeStatus');
        Route::get('/posts/edit/{id}/', [PostsController::class, 'edit'])->name('posts.edit');
        Route::post('/posts/update/', [PostsController::class, 'update'])->name('posts.update');
        Route::get('/posts/remove/image/{id}', [PostsController::class, 'destroyImage'])->name('posts.image.remove');
        Route::post('/posts/view/', [PostsController::class, 'view'])->name('posts.view');

// Post Report
        Route::get('/reports/', [PostsController::class, 'reportsIndex'])->name('reports.index');
        Route::get('/reports/remove/{id}', [PostsController::class, 'destroyReport'])->name('reports.remove');
//            Import data By Excel
//            Route::Post('/company/import',[CountryController::class,'import'])->name('country.import');

//         Inquiries  (contacts )
        Route::get('/inquiries', [InquiryController::class, 'index'])->name('inquiries.index');
        Route::get('/inquiries/remove/{id}', [InquiryController::class, 'destroy'])->name('inquiries.destroy');
        Route::post('/inquiries/view/', [InquiryController::class, 'view'])->name('inquiries.view');

//          Polls (Route)
        Route::get('polls', [PollsController::class, 'index'])->name('polls.index');
        Route::get('polls/changeStatue', [PollsController::class, 'ChangeStatus'])->name('polls.changeStatus');
        Route::get('polls/details/{id}', [PollsController::class, 'pollsDetails'])->name('polls.details');
        Route::get('polls/create/', [PollsController::class, 'create'])->name('polls.create');
        Route::post('polls/store/', [PollsController::class, 'store'])->name('polls.store');
        Route::get('/polls/remove/{id}', [PollsController::class, 'destroy'])->name('polls.destroy');
        Route::get('/polls/edit/{id}/', [PollsController::class, 'edit'])->name('polls.edit');
        Route::post('/polls/update/', [PollsController::class, 'update'])->name('polls.update');

//          Submit Polls
        Route::get('/submit/poll/{id}', [PollsController::class, 'submitPoll'])->name('submitPoll.poll');
        Route::post('/answer/poll/', [PollsController::class, 'answerPoll'])->name('answer.poll');
        Route::get('/submit/poll/remove/{id}', [PollsController::class, 'submitPollRemoved'])->name('submit.poll.remove');

//        Magazines
        Route::get('magazines',[MagazinesController::class ,'index'])->name('magazines.index');
        Route::get('/magazines/create/',[MagazinesController::class,'create'])->name('magazines.create');
        Route::post('/magazines/stone',[MagazinesController::class,'store'])->name('magazines.store');
        Route::get('/magazines/remove/{id}',[MagazinesController::class,'destroy'])->name('magazines.destroy');
        Route::get('/magazines/changeStatue', [MagazinesController::class, 'ChangeStatus'])->name('magazines.changeStatus');
        Route::get('/magazines/edit/{id}/', [MagazinesController::class, 'edit'])->name('magazines.edit');
        Route::get('/magazines/remove/image/{id}', [MagazinesController::class, 'destroyImage'])->name('magazines.image.remove');
        Route::get('/magazines/remove/video/{id}', [MagazinesController::class, 'destroyVideo'])->name('magazines.Video.remove');
        Route::post('/magazines/update/', [MagazinesController::class, 'update'])->name('magazines.update');


        //       chat groups
        Route::get('groups/organization/',[GroupsController::class,'organization'])->name('groups.organization.json');



        Route::get('/groups',[GroupsController::class,'index'])->name('groups.index');
        Route::get('/group/create/',[GroupsController::class,'create'])->name('groups.create');
        Route::post('/group/create/image/update', [GroupsController::class, 'imageUpload'])->name('groups.image.update');






        Route::post('/group/store/',[GroupsController::class,'store'])->name('groups.store');
        Route::get('/group/edit/{id}',[GroupsController::class,'edit'])->name('groups.edit');
        Route::post('/group/update/',[GroupsController::class,'update'])->name('groups.update');
        Route::get('group/remove/{id}',[GroupsController::class,'remove'])->name('groups.remove');


        //  add members in the groups

        Route::get('group/add/member/{id}',[GroupsController::class,'addMember'])->name('groups.addmember');
        Route::get('group/member/{id}',[GroupsController::class,'GroupsMembers'])->name('groups.members');
        Route::get('group/member/remove/{id}',[GroupsController::class,'GroupsMembersRemove'])->name('groups.member.remove');




    });
});


    Route::prefix('user')->name('user.')->group(function () {


        Route::middleware(['is_user'])->group(function () {

//           after login page show (post pages )
            Route::get('/', [WebController::class,'GetUserPost'])->name('post.index');
            //Route::get('/', [WebController::class, 'trendingPosts'])->name('post.index');

//       User profile Routes
            Route::get('magazine/', [WebController::class, 'magazine'])->name('magazine.index');
            Route::get('magazine_detail/{id}', [WebController::class, 'magazine_detail'])->name('magazine_detail.index');
            Route::get('profile/', [UserProfileController::class, 'index'])->name('profile.index');
            Route::get('profile/info/{id}', [UserProfileController::class, 'getAllUsers'])->name('profile');
            Route::post('/profile/image/update', [UserProfileController::class, 'upload'])->name('profile.image.update');
            Route::get('/profile/edit', [UserProfileController::class, 'edit'])->name('profile.edit');
            Route::get('/levels/', [UserProfileController::class, 'levels'])->name('levels');
            Route::get('/country/', [UserProfileController::class, 'states'])->name('country');
            Route::get('/state/', [UserProfileController::class, 'districts'])->name('state');
            Route::get('/district/', [UserProfileController::class, 'city'])->name('district');
            Route::post('/profile/update', [UserProfileController::class, 'profileUpdate'])->name('profile.update');
            Route::get('/add/organization/{id}', [UserProfileController::class, 'addOrganizationOnUserProfile'])->name('add.organization');
            Route::post('store/organization/', [UserProfileController::class, 'storeOrganization'])->name('store.organization');
            Route::get('organization/remove/{id}', [UserProfileController::class, 'destroy'])->name('organization.remove');

            //      user change Password
            Route::get('change/password/', [UserProfileController::class, 'changePassword'])->name('change.password');
            Route::post('/password/update/', [UserProfileController::class, 'passwordUpdate'])->name('password.update');

//        User add post
            Route::post('post/create', [\App\Http\Controllers\PostController::class, 'create'])->name('post.create');
            Route::get('post/edit/{id}/', [\App\Http\Controllers\PostController::class, 'edit'])->name('post.edit');
            Route::post('/post/update/', [\App\Http\Controllers\PostController::class, 'update'])->name('post.update');
            Route::get('/post/report/{id}', [WebController::class, 'reportPost'])->name('post.report');

            //        Company Wall Post
            Route::post('companywallpost/create', [\App\Http\Controllers\WebController::class, 'createWallPost'])->name('companywallpost.create');
            Route::get('companywallpost/edit/{id}/', [\App\Http\Controllers\WebController::class, 'editWallPost'])->name('companywallpost.edit');
            Route::post('/companywallpost/update/', [\App\Http\Controllers\WebController::class, 'updateWallPost'])->name('companywallpost.update');

            //        Company Wall Poll
            Route::get('polls', [PollsController::class, 'index'])->name('polls.index');
            Route::get('polls/changeStatue', [PollsController::class, 'ChangeStatus'])->name('polls.changeStatus');
            Route::get('polls/details/{id}', [PollsController::class, 'pollsDetails'])->name('polls.details');
            Route::get('polls/create/{id}', [WebController::class, 'createPolls'])->name('companywallpolls.create');
            Route::post('polls/store/', [WebController::class, 'storePolls'])->name('companywallpolls.store');
            Route::get('/polls/remove/{id}', [PollsController::class, 'destroy'])->name('polls.destroy');
            Route::get('/polls/edit/{id}/', [PollsController::class, 'edit'])->name('polls.edit');
            Route::post('/polls/update/', [PollsController::class, 'update'])->name('polls.update');

            //Company wall meetings
            Route::get('/company-wall/meetings/{id}',[\App\Http\Controllers\WebController::class,'meetingsIndex'])->name('meetings.index');
            Route::get('/company-wall/recieved-meetings/{id}',[\App\Http\Controllers\WebController::class,'recievedmeetingsIndex'])->name('companywallmeetings.recievedrequests');
            Route::get('/company-wall/meetings/create/{id}',[\App\Http\Controllers\WebController::class,'meetingsCreate'])->name('companywallmeetings.create');
            Route::post('/company-wall/meetings/store',[\App\Http\Controllers\WebController::class,'meetingsStore'])->name('companywallmeetings.store');
            Route::get('/company-wall/meetings/view/{id1}/{id}',[\App\Http\Controllers\WebController::class,'view'])->name('companywallmeetings.view');
            Route::get('/company-wall/meetings/remove/{id1}',[\App\Http\Controllers\WebController::class,'OrganizationRemoveFromMeeting'])->name('companywallmeetings.organization.remove');
            Route::get('/company-wall/meetings/remove/{id}',[\App\Http\Controllers\WebController::class,'meetingDelete'])->name('companywallmeetings.remove');
            Route::get('/company-wall/meetings/edit/{id}',[\App\Http\Controllers\WebController::class,'meetingEdit'])->name('companywallmeetings.edit');
            Route::post('/company-wall/meetings/update',[\App\Http\Controllers\WebController::class,'meetingUpdate'])->name('companywallmeetings.update');
            Route::get('/company-wall/meetings/status', [MeetingsController::class, 'approvedOrNot'])->name('meetings.status');

//          Submit Polls
            Route::get('/submit/poll/{id}', [PollsController::class, 'submitPoll'])->name('submitPoll.poll');
            Route::post('/answer/poll/', [PollsController::class, 'answerPoll'])->name('answer.poll');
            Route::get('/submit/poll/remove/{id}', [PollsController::class, 'submitPollRemoved'])->name('submit.poll.remove');

            // follow and unfollow
            Route::get('follow/{id}', [FollowsController::class, 'follwUserRequest'])->name('follow');
            Route::get('/followers/', [UserProfileController::class, 'followers'])->name('profile.follower');
            Route::get('/followings/', [UserProfileController::class, 'followings'])->name('profile.following');

//        likes and dislikes
            Route::get('/likes', [LikesController::class, 'LikePost'])->name('LikePost');

//        comment and reply
            Route::post('/comment', [WebController::class, 'comments'])->name('comments');

//       submit Poll

            Route::get('/submit/poll/',[WebController::class, 'submitPoll'])->name('submit.poll');
            Route::get('/advance-search', [WebController::class, 'usersListWithFilter'])->name('advance-search');
//        Advance search
            /* Route::post('/advance_search', [WebController::class, 'usersListWithFilter'])->name('advance-search1');*/
            /*Route::post('/advance_search_step_two', [WebController::class, 'AdvanceSearchStepTwo']);
            Route::post('/advance_search_step_third', [WebController::class, 'AdvanceSearchStepThird']);
            Route::post('/advance_search_step_final', [WebController::class, 'AdvanceSearchStepFinal']);*/

            //  for advance search state dist and city

            Route::post('/search/states',[WebController::class,'states']);
            Route::post('/search/districts',[WebController::class,'districts']);
            Route::post('/search/cities',[WebController::class,'cities']);

            //         new advance search user
            Route::post('search/users',[WebController::class,'usersListWithFilter']);

            Route::post('/getUsers', [UserProfileController::class, 'getUsers'])->name('getUsers');
            Route::get('/post/detail/{id}', [WebController::class, 'postDetail'])->name('post.detail');
            Route::get('/post/trendings', [WebController::class, 'trendingPosts'])->name('trending');
            Route::get('/company-wall/{id}', [WebController::class, 'companyWall'])->name('company_wall');
            Route::get('/chat', [WebController::class, 'Chat'])->name('chat');
            Route::get('/chatrequest/{id}/{id1}', [WebController::class, 'ChatRequest'])->name('chatrequest');
            Route::get('/chat-request-received', [WebController::class, 'ChatReceived'])->name('chatreceived');
            Route::get('/chat-accept/{id}', [WebController::class, 'ChatAccept'])->name('chataccept');
            Route::get('/chat-request-remove/{id}', [WebController::class, 'ChatRequestRemove'])->name('chatrequest.remove');
        });

    });




// user register
Route::post('/register/register-step-one', [RegisterController::class, 'createStepOne'])->name('register.register-step-one');
Route::post('/register/register-step-two', [RegisterController::class, 'createStepTwo'])->name('register.register-step-two');
Route::get('/register/getLevels', [RegisterController::class, 'getLevels'])->name('register.getLevels');
Route::get('/register/getStates', [RegisterController::class, 'getStates'])->name('register.getStates');
Route::get('/register/getDistricts', [RegisterController::class, 'getDistricts'])->name('register.getDistricts');
Route::get('/register/getCity', [RegisterController::class, 'getCity'])->name('register.getCity');
Route::post('/register/register-step-third', [RegisterController::class, 'createStepThird'])->name('register.register-step-third');
// forget password
Route::post('/reset_password/', [RegisterController::class, 'reset_password'])->name('reset.password');
Route::post('/get_verify_Otp/', [RegisterController::class, 'verifyOTP'])->name('get_verify.Otp');
Route::post('/create_new_password/', [RegisterController::class, 'createNewPassword'])->name('create.password');


$all_cms = Cms::getAllCmsUrl();
foreach ($all_cms as $cms) {

    Route::get("/" . $cms->url, function () use ($cms) {

        return App::make('App\Http\Controllers\CmsController')->callAction('cmsPage', [$cms->slug]);
    })->name($cms->slug);
}

Route::prefix('organization')->name('organization.')->group(function () {
    //   Organization Login
    Auth::routes();
    Route::get('/', [OrganizationLoginController::class, 'organizationLoginForm'])->name('showLoginForm');
    Route::post('/login', [OrganizationLoginController::class, 'postLogin'])->name('organizationlogin');
    //  Organization password reset
    Route::get('/password/reset', [PasswordForgotController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('/password/email', [PasswordForgotController::class, 'sendResetLinkEmail'])->name('password.email');

    Route::get('/password/reset/{token}', [\App\Http\Controllers\Organization\Auth\PasswordResetController::class, 'showresetForm'])->name('password.reset');
    Route::post('/password/reset', [\App\Http\Controllers\Organization\Auth\PasswordResetController::class, 'reset'])->name('password.update');
    // Organization Logout

    Route::post('/logout', [OrganizationLoginController::class, 'logout'])->name('logout');
    Route::middleware(['is_organization'])->group(function () {
        Route::get('/dashboard', [DashboardsController::class, 'index'])->name('dashboards.index');
        //  Organization profile route

        Route::get('/profile', [OrganizationProfileController::class, 'index'])->name('organizationprofile.index');
        Route::get('/profile/edit', [OrganizationProfileController::class, 'edit'])->name('organizationprofile.edit.index');
        Route::post('/profile/update', [OrganizationProfileController::class, 'update'])->name('organizationprofile.update');
        Route::post('/profile/image/update', [OrganizationProfileController::class, 'upload'])->name('profile.image.update');
        //      Organization change Password

        Route::get('/change_password', [PasswordChangeController::class, 'index'])->name('passwordchange.index');
        Route::post('/change_password', [PasswordChangeController::class, 'changePassword'])->name('Passwordchange');
        //  Organization postsroute

        Route::get('/posts/', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'index'])->name('posts.index');
        Route::get('/create/company/posts/', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'create'])->name('posts.create.company');
        Route::get('/create/personal/posts/', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'createPersonalPost'])->name('posts.create.personal');
        Route::get('/select/posts/type/',[\App\Http\Controllers\Organization\OrganizationPostsController::class,"selectPostType"])->name('posts.type.select');
        Route::post('/posts/store', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'Store'])->name('posts.store');
        Route::post('/posts/store/personal', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'storePersonalPost'])->name('posts.store.personal');
        Route::get('/posts/remove/{id}', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'destroy'])->name('posts.remove');
        Route::get('/posts/changeStatue', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'ChangeStatus'])->name('posts.changeStatus');
        Route::get('/posts/edit/{id}/', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'edit'])->name('posts.edit');
        Route::post('/posts/update/', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'update'])->name('posts.update');
        Route::get('/posts/remove/image/{id}', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'destroyImage'])->name('posts.image.remove');
        Route::get('/posts/remove/video/{id}', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'destroyVideo'])->name('posts.video.remove');
        Route::post('/posts/view/', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'view'])->name('posts.view');

//            Users routes
        Route::get('/posts/display/{id}', [\App\Http\Controllers\Organization\OrganizationPostsController::class, 'display'])->name('posts.display');

//           Organization Users routes
        Route::get('users/', [OrganizationUsersController::class, 'index'])->name('users.index');
        Route::get('/users/remove/{id}', [OrganizationUsersController::class, 'remove'])->name('users.remove');
        Route::get('users/create', [OrganizationUsersController::class, 'create'])->name('users.create');
        Route::get('/users/getLevels', [OrganizationUsersController::class, 'getLevels'])->name('users.getLevels');
        Route::post('users/store/', [OrganizationUsersController::class, 'store'])->name('users.store');
        Route::get('users/edit/{id}', [OrganizationUsersController::class, 'edit'])->name('users.edit');
        Route::post('/users/update/', [OrganizationUsersController::class, 'update'])->name('users.update');
        Route::post('/users/change/password', [OrganizationUsersController::class, 'changeUserPassword'])->name('users.changeUserPassword');
        Route::get('users/Permission', [OrganizationUsersController::class, 'approvedOrNot'])->name('users.Permission');
        Route::get('users/permission/{id}',[OrganizationUsersController::class,'permission'])->name('users.permission');
        Route::get('users/permission/remove/{id}',[OrganizationUsersController::class,'userPermissionRemoved'])->name('users.permission.remove');
        Route::post('users/permission',[OrganizationUsersController::class,'addPermission'])->name('users.allow.permissions');


        //        Polls (Route)
        Route::get('polls', [\App\Http\Controllers\Organization\PollsController::class, 'index'])->name('polls.index');
        Route::get('polls/changeStatue', [\App\Http\Controllers\Organization\PollsController::class, 'ChangeStatus'])->name('polls.changeStatus');
        Route::get('polls/details/{id}', [\App\Http\Controllers\Organization\PollsController::class, 'pollsDetails'])->name('polls.details');
        Route::get('polls/create/', [\App\Http\Controllers\Organization\PollsController::class, 'create'])->name('polls.create');
        Route::post('polls/store/', [\App\Http\Controllers\Organization\PollsController::class, 'store'])->name('polls.store');
        Route::get('/polls/remove/{id}', [\App\Http\Controllers\Organization\PollsController::class, 'destroy'])->name('polls.destroy');
        Route::get('/polls/edit/{id}/', [\App\Http\Controllers\Organization\PollsController::class, 'edit'])->name('polls.edit');
        Route::post('/polls/update/', [\App\Http\Controllers\Organization\PollsController::class, 'update'])->name('polls.update');

//          Submit Polls
        Route::get('/submit/poll/{id}', [\App\Http\Controllers\Organization\PollsController::class, 'submitPoll'])->name('submitPoll.poll');
        Route::post('/answer/poll/', [\App\Http\Controllers\Organization\PollsController::class, 'answerPoll'])->name('answer.poll');
        Route::get('/submit/poll/remove/{id}', [\App\Http\Controllers\Organization\PollsController::class, 'submitPollRemoved'])->name('submit.poll.remove');

        Route::get('magazines',[\App\Http\Controllers\Organization\MagazinesController::class ,'index'])->name('magazines.index');
        Route::get('/magazines/create/',[\App\Http\Controllers\Organization\MagazinesController::class,'create'])->name('magazines.create');
        Route::post('/magazines/stone',[\App\Http\Controllers\Organization\MagazinesController::class,'store'])->name('magazines.store');
        Route::get('/magazines/remove/{id}',[\App\Http\Controllers\Organization\MagazinesController::class,'destroy'])->name('magazines.destroy');
        Route::get('/magazines/changeStatue', [\App\Http\Controllers\Organization\MagazinesController::class, 'ChangeStatus'])->name('magazines.changeStatus');
        Route::get('/magazines/edit/{id}/', [\App\Http\Controllers\Organization\MagazinesController::class, 'edit'])->name('magazines.edit');
        Route::get('/magazines/remove/image/{id}', [\App\Http\Controllers\Organization\MagazinesController::class, 'destroyImage'])->name('magazines.image.remove');
        Route::get('/magazines/remove/video/{id}', [\App\Http\Controllers\Organization\MagazinesController::class, 'destroyVideo'])->name('magazines.Video.remove');
        Route::post('/magazines/update/', [\App\Http\Controllers\Organization\MagazinesController::class, 'update'])->name('magazines.update');

        Route::get('/meetings/',[\App\Http\Controllers\Organization\MeetingsController::class,'index'])->name('meetings.index');
        Route::get('/meetings/create',[\App\Http\Controllers\Organization\MeetingsController::class,'create'])->name('meetings.create');
        Route::post('/meetings/store',[\App\Http\Controllers\Organization\MeetingsController::class,'store'])->name('meetings.store');
        Route::get('/meetings/view/{id}',[\App\Http\Controllers\Organization\MeetingsController::class,'view'])->name('meetings.view');
        Route::get('/meetings/organization/remove/{id}',[\App\Http\Controllers\Organization\MeetingsController::class,'OrganizationRemoveFromMeeting'])->name('meetings.organization.remove');
        Route::get('/meetings/remove/{id}',[\App\Http\Controllers\Organization\MeetingsController::class,'meetingDelete'])->name('meetings.remove');
        Route::get('/meetings/edit/{id}',[\App\Http\Controllers\Organization\MeetingsController::class,'meetingEdit'])->name('meetings.edit');
        Route::post('/meetings/update',[\App\Http\Controllers\Organization\MeetingsController::class,'meetingUpdate'])->name('meetings.update');
        Route::get('/meetings/status', [MeetingsController::class, 'approvedOrNot'])->name('meetings.status');

//        chat groups
    });

});
