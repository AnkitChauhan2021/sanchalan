require('dotenv').config()
const axios = require('axios');
// const mysql      = require('mysql');
// const connection = mysql.createConnection({
//     host     : process.env.DB_HOST,
//     user     : process.env.DB_USERNAME,
//     password : 'gjg65$^%vuYTR^^',
//     database : 'sanchalan'
// });
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);

var cors = require('cors');
var io = require('socket.io')(server, {
    cors: {
        origin: "http://103.228.114.4",
        methods: ["GET", "POST"]
    }
});
app.use(cors());
var port = process.env.SOCKET_PORT || 3000;


server.listen(port, () => {
    console.log('Server listening at port %d', port);
});


let numUsers = 0;
var onlineUsers = {};
io.on('connection', (socket) => {

    let addedUser = false;
    // when the client emits 'new message', this listens and executes

    // REQUEST data= {chat_group_id: "", sender_id: "", message: "", message_type:""}

    socket.on('newMessage', (data) => {
        //console.log(data)
        if(data.message_type === 'text'){
            const message = axios.post('http://103.228.114.4/api/v1/create-message', data)
                .then(function (response) {
                    //console.log(response)
                    if(response.data.status_code){
                        // we tell the client to execute 'new message'
                        io.emit(`receiveMessage-${response.data.data.chat_group_id}`, response.data.data);
                    }
                })
                .catch(function (error) {
                    //console.log(error);
                });
        } else {
            io.emit(`receiveMessage-${data.chat_group_id}`,data);
        }
    });

    // REQUEST data = {chat_group_id: "", message_id: "", receiver_id: ""}
    socket.on('messageDelivered', (data) => {

        const message = axios.post('http://103.228.114.4/api/v1/message-delivered', data)
            .then(function (response) {
                if(response.data.status_code) {
                    socket.broadcast.emit(`messageDelivered-${data.chat_group_id}`, data);
                }
            })
            .catch(function (error) {
                //console.log(error);
            });
    });

    // REQUEST data = {user_id: ""}
    socket.on('getUnreadCount', (data) => {
        const message = axios.post('http://103.228.114.4/api/v1/get-unread-count', data)
            .then(function (response) {
                if(response.data.data.length) {

                    response.data.data.forEach(function (arrayItem) {
                        io.emit(`unreadCount-${arrayItem.user_id}`,arrayItem);
                    });

                    // for (var j = 0; j < response.data.data.length; j++){
                    //     io.emit(`unreadCount-${response.data.data[j].user_id}`,response.data.data[j]);
                    // }
                }

            })
            .catch(function (error) {
                //console.log(error);
            });
    });

    socket.on('messageDeliveredAll', (data) => {
        socket.broadcast.emit(`messageDeliveredAll-${data.chat_group_id}`, data);
    });

    // when the client emits 'add user', this listens and executes
    socket.on('addUser', (data) => {
        if (addedUser) return;

        // we store the username in the socket session for this client
        socket.user_id = data.user_id;

        onlineUsers[socket.user_id] = {online:true};
        console.log(data);
        console.log(onlineUsers);
        io.emit('onlineUsers', onlineUsers);

        ++numUsers;
        addedUser = true;
        // socket.emit('login', {
        //     totalUsers: numUsers
        // });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('userJoined', {
            user_id: socket.user_id,
            totalUsers: numUsers
        });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', (data) => {
        socket.broadcast.emit(`typing-${data.chat_group_id}`, {
            user_id: socket.user_id
        });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stopTyping', (data) => {
        socket.broadcast.emit(`stopTyping-${data.chat_group_id}`, {
            user_id: socket.user_id
        });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', () => {
        if (addedUser) {
            --numUsers;
            onlineUsers[socket.user_id] = {online:false};
            io.emit('onlineUsers', onlineUsers);
            // echo globally that this client has left
            socket.broadcast.emit('userLeft', {
                user_id: socket.user_id,
                totalUsers: numUsers
            });
        }
    });

});
