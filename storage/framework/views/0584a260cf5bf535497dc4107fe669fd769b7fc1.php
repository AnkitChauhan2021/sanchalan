<!-- login Modal -->
<div class="modal fade" id="createPassword_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-form">
                            <form action="<?php echo e(route('create.password')); ?>" method="POST" id="createPasswordForm">
                                <div class="form-input">
                                    <h3>Create New  Password</h3>
                                    <div class="form-group mb-2">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="password" class="form-control" aria-describedby="emailHelp" name="password" id="new_password" autocomplete="off">
                                        <span class="text-danger" id="passwordChange-error"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"> Confirm Password</label>
                                        <input type="password" class="form-control"  name="password_confirmation" id="confirm_new_password" autocomplete="off">
                                        <span class="text-danger" id="confirmPassword-error"></span>
                                    </div>
                                        <input type="hidden" name="mobile" id="createPassword" class="createNewPassword">
                                        <input type="hidden" name="verified_token" id="verified_token" class="verified_token">
                                    <button id="submit" class="btn border_btn" name="submit">Submit</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- login Modal end -->
<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/password/createPassword.blade.php ENDPATH**/ ?>