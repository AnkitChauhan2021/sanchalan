    <div class="middle-search">
        <div class="container">
            <div class="search-input">
                <div class="position-relative">
                    <input type="search" class="form-control" id='user_search' placeholder="Search Here...">
                    <i class="icofont-search-1"></i>
                </div>
               <!--  <i class="icofont-search-1"></i> -->
                <a href="<?php echo e(route('user.advance-search')); ?>" class="advance_search"><i class="icofont-search-job"></i> Advance Search</a>
            </div>
        </div>
    </div>
<style>
    .badge {
        top: -10px;
        right: -10px;
        padding: 4px 7px;;
        border-radius: 50%;
        background: red;
        color: white;
    }

</style>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-3">
                <div class="left-nav shadow-sm bg-white">
                    <img src="<?php echo e(asset('public/home/img/user-bg.png')); ?>" class="img-fluid "/>
                    <div class="user-info text-center">
                        <div class="profile-pic"><img src="<?php echo e(Auth::user()->logo); ?>"
                          class="img-fluid add_profile_pic" />

                         
                      </div>

                      <h3><?php echo e(Auth::user()->full_name); ?></h3>
                      <p><?php echo e(Auth::user()->designation); ?></p>
                  </div>
                  <!--   <div class="search_div">
                        <label class="top-search">
                            <input type="search" class="top-serach-form" placeholder="Search">
                            <i class="icofont-search-1"></i>
                        </label>
                    </div> -->
                    <div class="user-nav home-nav">
                        <ul>
                            <li><a href="<?php echo e(route('user.profile.index')); ?>"><i class="icofont-user-alt-3"></i> My Profile</a></li>
                            <li><a href="<?php echo e(route('user.trending')); ?>"><i class="fas fa-book-open"></i>Trending Posts</a></li>
                            <li><a href="<?php echo e(route('user.chatreceived')); ?>"><i class="fas fa-bell">
                                    </i>Notification
                                <?php if(\App\Helpers\CommonHelper::chatcount()!==0): ?>
                                    <span class= "badge"><?php echo e(\App\Helpers\CommonHelper::chatcount()); ?></span>
                                <?php endif; ?>
                                </a>
                            </li>
                            <li><a href="<?php echo e(route('user.chat')); ?>"><i class="fas fa-envelope"></i>Message</a></li>
                            <li><a href="<?php echo e(route('user.change.password')); ?>"><i class="fas fa-user-lock"></i>Password</a></li>
                            <!--  <li><a hr    <?php echo e(route('user.magazine.index')); ?>"><i class="fas fa-book-open"></i>Magazine</a></li> -->
                            <li>
                                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                onclick="event.preventDefault();
                                document.getElementById('logout').submit();">
                                <i class="icofont-logout"></i> <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout" action="<?php echo e(route('logout')); ?>" method="POST" >
                                <?php echo csrf_field(); ?>
                            </form>

                        </li>

                    </ul>
                </div>
            </div>
        </div>

<script>  
 $(function() {  
   $( "#user_search" ).autocomplete({  
     source: function( request, response ) {  
      $.ajax({  
        url: "<?php echo e(route('user.getUsers')); ?>", 
        type: 'post', 
        dataType: "json",  
        data: {  
            search: request.term  
        },  
        success: function( data ) { 
            response(data) 
        }
    });  
  },
    select: function( event, ui ) { 
            window.location.href ='<?php echo e(env('APP_URL')); ?>/user/profile/info/'+ui.item.id;
        } 
}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {  
       return $( "<li></li>" )  
       .data( "item.autocomplete", item )  
       .append( "<a>" + "<img style='width:50px;height:50px; ' class='ui-autocomplete-row' src='" + item.logo + "' /> " + item.first_name+ " "+item.last_name+ "</a>" )
       .appendTo( ul );
   };  
});  

</script>
<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/profile/index.blade.php ENDPATH**/ ?>