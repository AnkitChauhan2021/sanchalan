<div class="container">

<div class="top-menu">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="<?php echo e(route('welcome.index')); ?>"><img src="<?php echo e(asset('/public/home/img/logo.png')); ?>"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
            <i class="icofont-navigation-menu"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item">
                    <a class="nav-link" href="<?php echo e(route('welcome.index')); ?>">Home</a>
                </li> -->






                <?php  $all_cms = App\Models\Cms::getAllCmsUrl(); ?>
                <?php $__currentLoopData = $all_cms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="nav-item "><a class="nav-link" href="<?php echo e(config('app.url').$cms->url); ?>"><?php echo e($cms->title); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php if(Auth::check()): ?>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(Auth::user()->first_name); ?>

                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <?php if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3 ): ?>
                            <a href="<?php echo e(route('user.post.index')); ?>" class="dropdown-item"><i class="fas fa-user"></i> Profile</a>
                           <?php endif; ?>
                            <?php if(Auth::user()->role_id == 1): ?>
                                <a href="<?php echo e(url('/admin')); ?>" class="dropdown-item"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                            <?php elseif(Auth::user()->role_id == 3): ?>
                                <a href="<?php echo e(url('/organization')); ?>" class="dropdown-item"> <i class="fas fa-tachometer-alt"></i> Dashboard</a>
                            <?php endif; ?>

                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                               onclick="event.preventDefault();
                                                                                     document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i>  <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                <?php echo csrf_field(); ?>
                            </form>

                        </div>



                    </li>

                <?php endif; ?>
            </ul>
        </div>
    </nav>
</div>
</div>
<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/navbar.blade.php ENDPATH**/ ?>