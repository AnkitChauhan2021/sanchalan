
<?php $__env->startSection('title',config('app.name')); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('elements.layoutsElements.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('elements.layoutsElements.pages.feutures', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('elements.layoutsElements.pages.aboutgoal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <section class="video_bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="video-promo-content mt-4 text-center">
                        <a href="#" class="popup-youtube video-play-icon d-inline-block"><span class="icofont-play"></span></a>
                        <h5 class="mt-4 text-white">Watch video overview</h5>

                        <div class="download-btn mt-5">
                            <a href="#" class="btn google-play-btn mr-3"><span class="icofont-brand-android-robot"></span> Google
                                Play</a>
                            <a href="#" class="btn app-store-btn"><span class="icofont-brand-apple"></span> App Store</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php if(auth()->guard()->guest()): ?>
        <?php if(Route::has('register')): ?>
    <section class="map text-center">
        <div class="container">
            <div class="contact">
                <h2>Stay in touch <span>with us!</span></h2>
                <p>You can invite any member of any organization from the same or lower level to join your group.</p>
                <a href="<?php echo e(route('welcome.index')); ?>" class="btn purple  wow heartBeat animated" data-wow-duration="1.5s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.5s; animation-name: heartBeat;">Register</a>
            </div>
        </div>
    </section>
        <?php endif; ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/home/index.blade.php ENDPATH**/ ?>