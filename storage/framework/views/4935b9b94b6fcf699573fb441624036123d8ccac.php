
<?php $__env->startSection('title',Auth::user()->full_name); ?>
<?php $__env->startSection('content'); ?>
    
    
    
    <?php echo $__env->make('elements.layoutsElements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('elements.layoutsElements.profile.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


    <div class="col-md-7 ">
        <div class="card">
            <div class="bg-white shadow-sm profile">
                
                <input type="file" class="profileImage image" style="display:none"/>

                <div class="user_profile text-center">
                    <div class="user_photo">
                        <img src="<?php echo e($record->logo); ?>" class="profile_image"/>

                        <a href="javascript:void(0)" class="edit_profile" onclick="OpenProfileImage()"><i
                                class="fas fa-camera"></i></a>
                        <a href="<?php echo e(route('user.profile.edit')); ?>" class="update_cover_image"><i
                                class="fas fa-edit"></i></a>
                    </div>

                    <div class="user_name">

                        <p><?php echo e($record->full_name); ?></p>
                    </div>
                </div>
                <div class="follow-list d-flex align-self-center justify-content-center">
                    <div class="follow text-center">
                        <a href="<?php echo e(route('user.profile.follower')); ?>"><p class="followers">Followers</p></a>
                        <label class="count_follower"><?php echo e($record->followings()->count()); ?></label>
                    </div>
                    <div class="follow text-center">
                        <a href="<?php echo e(route('user.profile.following')); ?>"><p class="followers">Following</p></a>
                        <label class="count_follower"><?php echo e($record->followers()->count()); ?></label>
                    </div>
                </div>

                <hr/>
            <!-- <a href="<?php echo e(route('user.follow',2)); ?>">Follow</a>
                        <a href="javascript:void(0)" class="likes_and_dislikes" data-id="20"><i class="far fa-thumbs-up"></i></a> -->


                <div class="user_about">
                    <h4>About</h4>
                    <ul>
                        <li><b>My Company:</b>
                            <?php $__currentLoopData = $usercompanies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a href="<?php echo e(route('user.company_wall',$result->company->id)); ?>"><?php echo e(@$result->company->name); ?> <span class="text-dark"><?php echo e('|'); ?></span></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </li>
                        <?php if($record->designation!=null): ?>
                            <li><b>Designation:</b> <?php echo e($record->designation); ?></li>
                        <?php endif; ?>
                        <?php if($record->email!=null): ?>
                            <li><b>Email:</b> <?php echo e($record->email); ?></li>
                        <?php endif; ?>
                        <li><b>Phone:</b> <?php echo e($record->mobile); ?></li>
                        <?php if($record->company!=null): ?>
                            <li><b>Company:</b> <?php echo e($record->company->name); ?></li>
                        <?php endif; ?>
                        <li><b>About Me:</b> <?php echo e($record->about_us); ?></li>
                    </ul>
                    <br/>
                    
                </div>
                <?php if($record->isApproved==1): ?>
                    <a href="<?php echo e(route('user.post.index')); ?>" class="btn create_post">CREATE POST</a>
                <?php endif; ?>
                <div class="post_video_profile">
                    <ul class="nav nav-tabs nav-pills nav-justified" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                               aria-controls="home" aria-selected="true"><img
                                    src="<?php echo e(asset('public/home/img/icon-11.png')); ?>"/>Your Post</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                               aria-controls="profile" aria-selected="false"><img
                                    src="<?php echo e(asset('public/home/img/icon-12.png')); ?>"/> Your Video</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="post_img" id="lightgallery">
                            <?php $__currentLoopData = $record->userPost; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = $post->getPostImage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$getImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($getImage->post_image != " "): ?>
                                        <a class="post-videe" href="<?php echo e($getImage->post_image); ?>">
                                            <img src="<?php echo e($getImage->post_image); ?>" class="img-fluid"/>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="post_img">
                            <?php $__currentLoopData = $record->userPost; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = $post->getPostVideo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$getVideo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($getVideo->post_video != " "): ?>
                                        <div class="post-videe">
                                            <video controls class="d-block w-100" style="height: 110px;">
                                                <source src="<?php echo e($getVideo->post_video); ?>">
                                            </video>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="img-container" style="max-height: 500px">
                                <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lightgallery").lightGallery();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/profile/index.blade.php ENDPATH**/ ?>