
<?php $__env->startSection('title',Auth::user()->full_name); ?>
<?php $__env->startSection('content'); ?>
    
    
    
    <?php echo $__env->make('elements.layoutsElements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-3">
                <div class="filter">
                    <h3>Filter</h3>
                    <form action="<?php echo e(route('user.advance-search1')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <div class="select-filter">
                            <div class="form-group">
                                <label>Organization</label>
                                <div class="input-icon">
                                    <select  class="form-control selected_company <?php $__errorArgs = ['company_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                            onchange="selectCompany()" name="company_id[]">
                                        <option value="">Organization</option>
                                        <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->id); ?>" <?php echo e(old('company_id')==$value->id?'selected': ""); ?>><?php echo e($value->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <div class="input-icon">
                                    <select class="form-control <?php $__errorArgs = ['level_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="orglevels"
                                            name="level_id[]">
                                        <option value=" ">Select Level</option>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <div class="input-icon">
                                    <select class="form-control selected_states <?php $__errorArgs = ['state_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                            id="statesGet" onchange="selectState()" name="state_id[]">
                                        <option value=" "> Select State</option>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>District</label>
                                <div class="input-icon">
                                    <select
                                        class="form-control selected_district <?php $__errorArgs = ['district_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                        id="districtGet" onchange="selectDistrict()" name="district_id[]">
                                        <option value=" "> Select District</option>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <div class="input-icon">
                                    <select class="form-control <?php $__errorArgs = ['city_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="cityGet"
                                            name="city_id[]">
                                        <option value=" "> Select City</option>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <button type="submit" class="post_btn light_purple ml-2">Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-7">
                <div class="bg-white shadow-sm profile search_inner">
                    <h3>Search Result</h3>
                    <div class="search_result">
                        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <div class="post-describe d-flex align-items-center">
                                    <div class="port-profile">
                                        <img src="<?php echo e($user->logo); ?>" class="img-fluid">
                                    </div>
                                    <div class="post-content">
                                        <h5><?php echo e($user->full_name); ?></h5>
                                        <p><?php echo e($user->designation); ?></p>
                                    </div>
                                </div>
                                <div class="follow_chat">
                                    <?php if($user->id != Auth::user()->id): ?>
                                        <?php $__currentLoopData = $user->userFollowers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userFollower): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($userFollower->id == Auth::user()->id): ?>
                                                <a href="<?php echo e(route('user.follow',$user->id)); ?>" class="text-blue">
                                                    Unfollow
                                                </a>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(!$user->userFollowers->contains(Auth::user()->id)): ?>

                                            <a href="<?php echo e(route('user.follow',$user->id)); ?>"  class="text-blue">
                                                Follow
                                            </a>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                        <?php if($user->id != Auth::user()->id): ?>
                                            <?php if($user->level_position >= $loggedInUser->level_position - 3): ?>
                                    <span class="pl-2 pr-2">|</span>
                                    <a href="#" class="text-blue">Chat</a>
                                            <?php endif; ?>
                                             <?php endif; ?>

                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('advancesearch.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/advancesearch/advancesearch.blade.php ENDPATH**/ ?>