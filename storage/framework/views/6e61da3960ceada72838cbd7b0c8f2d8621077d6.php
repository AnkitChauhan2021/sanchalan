

<footer>
    <div class="container">
        <div class="new_way">
            <div class="row">
                <div class="col-md-9 align-self-center">
                    <h3>New Way Of Staying Connected All Day.</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>
                <?php if(auth()->guard()->guest()): ?>
                    <?php if(Route::has('register')): ?>
                    <div class="col-md-3 text-right align-self-center">
                        <a href="<?php echo e(route('welcome.index')); ?>" class="btn">REGISTER</a>
                    </div>
                <?php else: ?>
                <div class="col-md-3 text-right align-self-center">
                    <a href="<?php echo e(route('welcome.index')); ?>" class="btn">REGISTER</a>
                </div>
                <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="footer-logo">
            <img src="<?php echo e(asset('public/home/img/footer-logo.png')); ?>"/>
            <div class="footer_link">



                <?php  $all_cms = App\Models\Cms::getAllCmsUrl(); ?>
                <?php $__currentLoopData = $all_cms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <a href="<?php echo e(config('app.url').$cms->url); ?>"><?php echo e($cms->title); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <ul class="social-footer">
                <li><a href="#" class="grd_bg_hover"><span class="icofont-facebook"></span></a></li>
                <li><a href="#" class="grd_bg_hover"><span class="icofont-pinterest"></span></a></li>
                <li><a href="#" class="grd_bg_hover"><span class="icofont-instagram"></span></a></li>
                <li><a href="#" class="grd_bg_hover"><span class="icofont-twitter"></span></a></li>
                <li><a href="#" class="grd_bg_hover"><span class="icofont-google-plus"></span></a></li>
            </ul>
        </div>
        <div class="copy">
            <p>Copyright © 2021 Sanchlan. All Rights Reserved</p>
            <p>Powered by: <a href="#">Brain Technosys</a></p>
        </div>
    </div>
</footer>
<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/footer.blade.php ENDPATH**/ ?>