<!-- login Modal -->
<div class="modal fade" id="login_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-form">
                            <form action="<?php echo e(route('login')); ?>" method="POST" id="login-form" >
                                <?php echo csrf_field(); ?>
                                <div class="form-input">
                                    <h3>Login Details</h3>
                                    <span class="text-danger" id="wrong"></span>

                                    <div class="form-group mb-2">
                                        <label for="exampleInputEmail1">Mobile for login</label>
                                        <input type="text" id="email" class="form-control   <?php $__errorArgs = ['mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="login" value="<?php echo e(old('mobile')); ?>" required autocomplete="mobile" autofocus aria-describedby="emailHelp" >
                                        <span class="text-danger" id="email-error"></span>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password"  id="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password" required autocomplete="current-password">
                                        <span class="text-danger" id="password-error"></span>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                                        <label class="form-check-label" for="exampleCheck1">Remember me </label>

                                        <a href="#" class="forget" data-toggle="modal"  data-dismiss="modal" data-target="#forgetpassword_model" >Forgot Password ?</a>
                                    </div>
                                    <button  type ="submit"  class="btn border_btn" name="submit">Login</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/login.blade.php ENDPATH**/ ?>