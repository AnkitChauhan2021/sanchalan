<?php $__env->startSection('title',Auth::user()->full_name); ?>
<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('elements.layoutsElements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('elements.layoutsElements.profile.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="col-md-7">
        <div class="post">
            <form action="<?php echo e(route('user.post.create')); ?>" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="write-post shadow-sm bg-white">
                    <p>Create Post
                    </p>
                    <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span role="alert">
                            <strong class="text-danger"><?php echo e($message); ?></strong>
                            </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    <div class="write">
                        <div class="profile-nav d-flex">
                            <div class="profile-post">
                                <img src="<?php echo e(Auth::user()->logo); ?>" id="post_addImage" alt="postBYImage"/>
                            </div>
                            <div class="post-text">
                                <textarea placeholder="Type Here..."
                                          class="form-control post-text <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                          rows="2" name="description"></textarea>
                            </div>
                        </div>
                        <div class="who">
                            <div class="paper-clip d-flex">
                                <div class="photo">
                                    <div class="coustom-input coustom-input-new" title="upload your profile pic">

                                        <input type="file" class="custom-file-input" multiple id="gallery-photo-add"
                                               accept="image/jpeg ,image/jpg,image/png,image/gif/*" name="image[]">
                                        <i class="icofont-camera"></i>
                                    </div>
                                    <div class="coustom-input coustom-input-new" title="upload video">
                                        <input type="file" class="custom-file-input file_multi_video" name="file[]"
                                               accept="video/*"/>
                                        <i class="icofont-video-cam"></i>
                                    </div>
                                </div>
                            </div>
                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span role="alert">
                            <strong class="text-danger"><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <div class="see-post">
                                <button class="post_btn light_purple ml-2" id="disable">Post</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="postImage" style="display:none">

                    <div class="post_videos d-flex">

                        <div class="video_divs">
                            <video width="400" controls id="video-fluids" style="display:none">
                                <source src="mov_bbb.mp4" id="video_here">
                                Your browser does not support HTML5 video.
                            </video>

                        </div>
                    </div>
                </div>
            </form>
            

            <?php if(Auth::user()->first_login == 1): ?>
                <?php if(sizeof($users) !=0): ?>
                    <div class="know_people">
                        <h4><b>People you may know.</b></h4>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="user_info">
                                <img src="<?php echo e($user->users->logo); ?>">
                                <span>
                            <p><?php echo e($user->users->first_name); ?></p>
                            <a href="#">Follow</a>
                        </span>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="card" id="postImage" style="display:none">
                <div class="post_videos d-flex">
                    <div class="video_divs">
                        <video width="400" controls id="video-fluids" style="display:none">
                            <source src="" id="video_here">
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                </div>
            </div>

            <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($record->is_Post): ?>
                    <div class="write-post shadow-sm bg-white">
                        <div class="main-post">
                            <div class="post-describe d-flex">
                                <div class="port-profile">
                                    <img src="<?php echo e(@$record->postBy->logo); ?>" class="img-fluid" alt="postBYImage"/>
                                </div>
                                <div class="post-content">
                                    <h5><?php echo e(@$record->postBy->full_name); ?>

                                        
                                    </h5>
                                    <p>
                                    <?php echo e(date('d-m-yy, H:i:s', strtotime(@$record->created_at))); ?>

                                </div>
                                <div class="dropdown-option">
                                    <div class="dropdown">
                                        <?php if(Auth::user()->id == $record->user_id): ?>
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <img src="<?php echo e(asset('public/home/img/more.svg')); ?>" alt="postBYImage">
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                 x-placement="top-start"
                                                 style="position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(0px, -116px, 0px);">
                                                <a class="dropdown-item"
                                                   href="<?php echo e(route('user.post.edit',$record->id)); ?>">Edit</a>
                                            </div>
                                        <?php else: ?>

                                            <?php if(sizeof($record->report)!==0): ?>
                                                
                                                
                                                <?php $__currentLoopData = $record->report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $report): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($report->user_id !== Auth::user()->id && $report->post_id !== $record->id): ?>
                                                        <button class="dropdown-toggle" type="button"
                                                                id="dropdownMenuButton"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            <img src="<?php echo e(asset('public/home/img/more.svg')); ?>"
                                                                 alt="postBYImage">
                                                        </button>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                             x-placement="top-start"
                                                             style="position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(0px, -116px, 0px);">
                                                            <a class="dropdown-item"
                                                               href="<?php echo e(route('user.post.report',$record->id)); ?>">Report</a>
                                                        </div>
                                                    <?php endif; ?>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php else: ?>
                                                <button class="dropdown-toggle" type="button"
                                                        id="dropdownMenuButton"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <img src="<?php echo e(asset('public/home/img/more.svg')); ?>"
                                                         alt="postBYImage">
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                     x-placement="top-start"
                                                     style="position: absolute; will-change: transform; top: 0; left: 0; transform: translate3d(0px, -116px, 0px);">
                                                    <a class="dropdown-item"
                                                       href="<?php echo e(route('user.post.report',$record->id)); ?>">Report</a>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>


                                    </div>
                                </div>
                            </div>
                            <div class="post-content-bottm">
                                <p><?php echo $record->description; ?> </p>
                            </div>
                            <div class="content-img">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                            class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <?php $__currentLoopData = $record->getPostImage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$getImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <?php if($getImage->post_image != " "): ?>
                                                <?php if($key+1==1): ?>
                                                    <div class="carousel-item active">

                                                        <img class="d-block w-100" src="<?php echo e($getImage->post_image); ?>"
                                                             alt="First slide" style="height:400px">
                                                    </div>
                                                <?php else: ?>
                                                    <div class="carousel-item ">

                                                        <img class="d-block w-100" src="<?php echo e($getImage->post_image); ?>"
                                                             alt="First slide" style="height:400px">
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $record->getPostVideo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$getVideo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($getVideo->post_video != " "): ?>
                                                <div class="carousel-item">
                                                    <video controls class="d-block w-100" style="height:400px">
                                                        <source src="<?php echo e($getVideo->post_video); ?>">
                                                    </video>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div class="like-comment d-flex align-items-center ">
                                <div class="like like-new active position-relative d-flex" style="margin-left: 100px;">
                                    <?php if($record->is_like == true): ?>
                                        <a href="javascript:void(0)" class="likes_and_dislikes"
                                           data-id="<?php echo e($record->id); ?>"><i
                                                class="fas fa-thumbs-up text-primary"></i>
                                        </a>
                                    <?php else: ?>
                                        <a href="javascript:void(0)" class="likes_and_dislikes"
                                           data-id="<?php echo e($record->id); ?>"><i
                                                class="far fa-thumbs-up"></i>
                                        </a>
                                    <?php endif; ?>
                                </div>
                                <div class="like float-right" style="margin-left: 300px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="22" viewBox="0 0 24 22">
                                        <g id="comment" transform="translate(-0.001 1.332)">
                                            <path id="Path_1" data-name="Path 1"
                                                  d="M20.7-1.332H3.3A3.3,3.3,0,0,0,0,1.956v10.61a3.3,3.3,0,0,0,3.288,3.288v4.815l6.945-4.815H20.7A3.3,3.3,0,0,0,24,12.566V1.956a3.3,3.3,0,0,0-3.3-3.288Zm1.893,13.9A1.892,1.892,0,0,1,20.7,14.452H9.792l-5.1,3.534V14.452H3.3a1.892,1.892,0,0,1-1.893-1.886V1.955A1.892,1.892,0,0,1,3.3.069H20.7a1.892,1.892,0,0,1,1.893,1.886Zm0,0"/>
                                            <path id="Path_2" data-name="Path 2"
                                                  d="M171.293,131.172h8.946V132.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -127.488)"/>
                                            <path id="Path_3" data-name="Path 3"
                                                  d="M171.293,211.172h8.946V212.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -204.459)"/>
                                            <path id="Path_4" data-name="Path 4"
                                                  d="M171.293,291.172h8.946V292.3h-8.946Zm0,0"
                                                  transform="translate(-163.765 -281.43)"/>
                                        </g>
                                    </svg>
                                    Comment
                                </div>


                            </div>
                            <div class="post-describe post-comment d-flex">
                                <div class="port-profile">
                                    <img src="<?php echo e(@Auth::user()->logo); ?>" class="img-fluid" alt="postBYImage"/>
                                </div>

                                <div class="post-content w-100">
                                    <form action="<?php echo e(route('user.comments')); ?>" method="post">
                                        <?php echo csrf_field(); ?>
                                        <input type="hidden" value="<?php echo e($record->id); ?>" name="post_id">
                                        <div class="text-box">
                                            <input type="text" class="form-control" placeholder="Add a comment ..."
                                                   name="comment"/>
                                            <button type="submit"><img src="<?php echo e(asset('public/home/img/send.svg')); ?>"
                                                                       class="send-btn" alt="dropdown"></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <?php else: ?>

                            
                            <div class="write-post shadow-sm bg-white" id="<?php echo e($record->id); ?>">
                                <div class="main-post">
                                    <div class="post-describe d-flex">
                                        <div class="port-profile">
                                            <img src="<?php echo e(@$record->users->logo); ?>" class="img-fluid" alt="user_image"/>

                                        </div>
                                        <div class="post-content">
                                            <h5><?php echo e(@$record->users->full_name); ?></h5>

                                            
                                            
                                            

                                            
                                            
                                            

                                            <p>
                                                <?php echo e(date('d-m-yy, H:i:s', strtotime(@$record->created_at))); ?>

                                            </p>


                                            <div class="post-content-bottm">
                                                <div class="qst"><?php echo e($record->question); ?></div>
                                                <div class="poll">
                                                    <div class="poll-result position-relative">
                                                        <div class="progress" data-id="<?php echo e($record->answer_one); ?>"
                                                             data-value="<?php echo e($record->total_answer); ?>">
                                                            <?php if($record->isShow == 1 && $record->total_answer ): ?>
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: <?php echo e(round($record->answer_one/$record->total_answer*(100),2)); ?>%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present"><?php echo e(round($record->answer_one/$record->total_answer*(100),2)); ?>

                                                                        %</span></div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer1"
                                                                                        data-id="<?php echo e($record->id); ?>"><?php echo e($record->answer1); ?>

                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                    <div class="poll-result position-relative">

                                                        <div class="progress" data-id="<?php echo e($record->answer_two); ?>"
                                                             data-value="<?php echo e($record->total_answer); ?>">
                                                            <?php if($record->isShow == 1 && $record->total_answer ): ?>
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: <?php echo e(round($record->answer_two/$record->total_answer*(100),2)); ?>%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present"><?php echo e(round($record->answer_two/$record->total_answer*(100),2)); ?>

                                                                        %</span></div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer2"
                                                                                        data-id="<?php echo e($record->id); ?>"><?php echo e($record->answer2); ?>

                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                    <div class="poll-result position-relative">

                                                        <div class="progress" data-id="<?php echo e($record->answer_three); ?>"
                                                             data-value="<?php echo e($record->total_answer); ?>">
                                                            <?php if($record->isShow == 1 && $record->total_answer ): ?>
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: <?php echo e(round($record->answer_three/$record->total_answer*(100),2)); ?>%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present"><?php echo e(round($record->answer_three/$record->total_answer*(100),2)); ?>

                                                                        %</span></div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer3"
                                                                                        data-id="<?php echo e($record->id); ?>"><?php echo e($record->answer3); ?>

                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                    <div class="poll-result position-relative">

                                                        <div class="progress" data-id="<?php echo e($record->answer_four); ?>"
                                                             data-value="<?php echo e($record->total_answer); ?>">
                                                            <?php if($record->isShow == 1 && $record->total_answer ): ?>
                                                                <div class="progress-bar" role="progressbar"
                                                                     style="width: <?php echo e(round($record->answer_four/$record->total_answer*(100),2)); ?>%;"
                                                                     aria-valuenow="25" aria-valuemin="0"
                                                                     aria-valuemax="100"><span class="poll_present"><?php echo e(round($record->answer_four/$record->total_answer*(100),2)); ?>

                                                                        %</span></div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <label class="cus_radio"><input type="radio" class="poll_submit"
                                                                                        name="poll1" value="answer4"
                                                                                        data-id="<?php echo e($record->id); ?>"><?php echo e($record->answer4); ?>

                                                            <span class="checkmark"></span></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                
                                <?php endif; ?>
                                <br>
                                <?php if($record->is_Post): ?>

                                    <?php $__currentLoopData = $record->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($comment->commentBy): ?>
                                            <div class="post-describe d-flex reply_coment  my-4">

                                                <div class="port-profile" style="margin-left: 9px;">
                                                    <img src="<?php echo e($comment->commentBy->logo); ?>" class="img-fluid"/>
                                                </div>
                                                <div class="post-content">
                                                    <div class="post-content-bottm">
                                                        <div class="showcomment ">
                                                            <b><?php echo e($comment->commentBy->full_name); ?><b>
                                                                    <p class="col-lg-12"><?php echo e($comment->comment); ?></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
        </div>

        </section>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/posts/index.blade.php ENDPATH**/ ?>