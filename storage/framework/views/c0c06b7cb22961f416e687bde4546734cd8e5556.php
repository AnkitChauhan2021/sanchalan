<!-- login Modal -->

<div class="modal fade" id="Otp_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-form">
                            <form action="<?php echo e(route('get_verify.Otp')); ?>" method="post" id="get_verify_otp">
                                <div class="text-center">
                                    <h2>Phone Verification</h2>
                                    <h4>Enter Your OTP Code Here</h4>
                                </div>
                                <div class="text-center">
                                    <input class="partitioned" type="text" name="otp_verified" id="otp_verified" maxlength="4" autocomplete="off" style=" font-size: 24px;"/>
                                    <input type="hidden" id="forverifyotp" name="mobile" class="forverifyotp">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <a type="submit" onclick="$(this).closest('form').submit()" class="sign-btn" id="step_2" >Verify & Continue</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/password/otp.blade.php ENDPATH**/ ?>