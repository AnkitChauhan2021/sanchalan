<style >
    .partitioned {
        padding-left: 25px;
        letter-spacing: 42px;
        border: 0;
        background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
        background-position: bottom;
        background-size: 50px 1px;
        background-repeat: repeat-x;
        background-position-x: 35px;
        width: 275px;
        font-weight: 900;

    }
    .text-danger {
        color: #dc3545!important;
        font-size: 11px;
    }

    .profile_image{
        position:absolute;
        margin-top: 0px;
        width:100%;
    }
    .user_name {
        position: absolute;
        margin-left: 273px;
        margin-top: 158px;
    }
    .follow-list.d-flex.align-self-center.justify-content-center {
        margin-top: -97px;
    }
    a.edit_profile {
        position: absolute;
        margin-top: 68px;
        margin-left: 64px;
        background-color: #948a8a;
        width: 28px;
        border-radius: 76px;
        height: 27px;
        text-align: center;
        color:black;
    }
    i.fas.fa-camera {
        margin-top: 5px;
    }
    a.update_cover_image {
        margin-top: -11pc;
        margin-left: 21pc;
        font-size: 20px;
    }
    i.fas.fa-edit {
        color: #4ebea7;
    }
    p.followers {
        margin-top: 283px;
    }
   label{
        margin-top: 10px;
    }
    i.fas.fa-check-double {
        color: #62e660;
        font-size: 33px;
        background-color: #ffffff;
        border-radius: 117px;
        margin-left: 71px;
        margin-top: -27px;
        text-align: center;
    }
    i.fas.fa-sync-alt {
        color: #e6e260;
        font-size: 33px;
        background-color: #ffffff;
        border-radius: 117px;
        margin-left: 71px;
        margin-top: -27px;
        text-align: center;
    }
    i.far.fa-times-circle {
        color: #ab161d;
        font-size: 33px;
        background-color: #ffffff;
        border-radius: 117px;
        margin-left: 71px;
        margin-top: -27px;
        text-align: center;
    }

    img.img-fluids {
        height: 173px;
        padding: 11px;
        border-radius: 23px;
        width: 157px;
    }

    i.far.fa-times-circle {
        color: #55ab15;
        text-align: center;
        margin-left: -34px;
        margin-top: 18px;
        font-size: 18px;
        position: absolute;
        background-color: #ffffff00;
        border-radius: 117px;
    }

    .post_videos .video_divs {
        margin-right: 15px;
        border-radius: 6px;
        overflow: hidden;
        /* width: 120px; */
        position: relative;
        background-color: #dcd3d330;
        display: flex;
        flex-wrap: wrap;

    }

    .post_videos {
        margin: 30px 0px;
    }
    video#video-fluids {
        padding: 10px;
        width: 280px;
        border-radius: 23px;
    }
     #post_addImage{
         width: 48px;
         border-radius: 30px;
     }
     /*.showcomment{*/
     /*    padding-left: 13px;*/
     /*    padding-bottom: 0px;*/
     /*    padding-right: 7px;*/
     /*    padding-top: 10px;*/
     /*    background-color: #ebf1f1;*/
     /*    border-radius: 13px;*/
     /*    margin-right: 10px;*/
     /*    margin-bottom: 0px;*/
     /*    width: 576px;*/
     /*}*/
    .showcomment {

        padding: 10px 7px 10px 13px;
        background-color: #ebf1f1;
        border-radius: 13px;
        margin-right: 10px;
        margin-bottom: 4px;
        width: 576px;
    }
    .post-content-bottm .showcomment p {

        padding: 0;
    }
    .content-img {
        background: #f1f1f1;
        display: flex;
        justify-content: center;
    }
     .links{
         display: inline;
         padding: 5px;
         font-weight: bold;
         cursor: pointer
     }
     .likeShare{
         margin-bottom: 17px;
     }
     .reply{
         padding-right: 27px;
         padding-left: 45px;
     }
     /*.reply_coment{padding-left:70px;}*/
</style>
<link rel="shortcut icon" type="image/x-icon" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.0/css/lightgallery.min.css">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('/public/home/fonts/lg.woff')); ?>">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('/public/home/fonts/lg.ttf')); ?>">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('/public/admin/img/favicon.ico')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('jqueryui/jquery-ui.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('/public/home/css/lightgallery.min.css')); ?>"/>
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/home/css/icofont.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('/public/home/css/bootstrap.min.css')); ?>">

<link rel="stylesheet" href="<?php echo e(asset('/public/home/css/custom.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('/public/home/css/media.css')); ?>">
<link rel="stylesheet"  href="<?php echo e(asset('/public/home/css/toastr.css')); ?>"/>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
<link rel="stylesheet" href="<?php echo e(asset('/public/admin/css/cropper.css')); ?>" />

<script src="<?php echo e(asset('home/js/jquery-3.3.1.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('home/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('home/js/toastr.js')); ?>"></script>

<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/style.blade.php ENDPATH**/ ?>