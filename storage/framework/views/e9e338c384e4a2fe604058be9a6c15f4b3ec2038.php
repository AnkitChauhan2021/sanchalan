<?php $__env->startSection('title',Auth::user()->full_name); ?>
<?php $__env->startSection('content'); ?>
    <link href="<?php echo e(asset('home/css/bootstrap-multiselect.min.css')); ?>" type="text/css"/>
    <script src="<?php echo e(asset('home/js/bootstrap-multiselect.min.js')); ?>"></script>
    
    
    
    <?php echo $__env->make('elements.layoutsElements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-3">
                <div class="filter">

                    <h3>Filters</h3>
                    <form action="<?php echo e(route('user.advance-search')); ?>" method="get">
                        
                        <div class="select-filter">
                            <div class="form-group">
                                <label class="mb-0">Organization</label>
                                <div class="input-icon">
                                    <select multiple="multiple" class="form-control selected_company multi-select"
                                            onchange="selectCompany()" name="company_id[]" id="company">

                                        <?php $__currentLoopData = $organization; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option
                                                value="<?php echo e($value->id); ?>"<?php echo e(in_array($value->id, old("company") ?: []) ? "selected" : ""); ?>><?php echo e($value->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>

                            <div class="form-group">
                                <label class="mb-0">Level</label>
                                <div class="input-icon">
                                    <select multiple="multiple" class="form-control multi-select" id="orglevels"
                                            name="level_id[]">

                                        <?php $__currentLoopData = $level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option
                                                value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label class="mb-0">State</label>
                                <div class="input-icon">
                                    <select multiple="multiple" class="form-control multi-select"
                                            id="statesGet" name="state_ids[]">
                                        <option value=" ">Please Select State</option>
                                        <?php $__currentLoopData = $state; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option
                                                value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label class="mb-0">District</label>
                                <div class="input-icon">
                                    <select multiple="multiple"
                                            class="form-control"
                                            id="districtGet123" name="district_ids[]">
                                        <option value=" ">Please Select</option>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <div class="form-group">
                                <label class="mb-0">City</label>
                                <div class="input-icon">
                                    <select class="form-control" id="cityGet"
                                            name="city_id[]">
                                        <option value=" ">Please Select</option>
                                    </select>
                                    
                                </div>
                                <span class="text-danger" id="organization-error"></span>
                            </div>
                            <button type="submit" class="post_btn light_purple ml-2">Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-7">
                <div class="bg-white shadow-sm profile search_inner" style="padding: 30px;">
                    <h3>Search Result</h3>
                    <?php $__currentLoopData = @$records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <div class="post-describe d-flex align-items-center">
                                <div class="port-profile">
                                    <img src="<?php echo e($user->logo); ?>" class="img-fluid">
                                </div>
                                <div class="post-content">
                                    <h5><?php echo e($user->full_name); ?></h5>

                                     <?php if($user->company_name): ?>
                                          <p><?php echo e($user->company_name); ?></p>
                                      <?php endif; ?>
                                    <p><?php echo e($user->designation); ?></p>
                                </div>
                            </div>
                            <div class="follow_chat col-md-1">
                                <?php if($user->id != Auth::user()->id): ?>
                                    <?php $__currentLoopData = $user->userFollowers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userFollower): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($userFollower->id == Auth::user()->id): ?>
                                            <a href="<?php echo e(route('user.follow',$user->id)); ?>" class="text-blue">
                                                Unfollow
                                            </a>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(!$user->userFollowers->contains(Auth::user()->id)): ?>
                                        <a href="<?php echo e(route('user.follow',$user->id)); ?>" class="text-blue">
                                            Follow
                                        </a>
                                    <?php endif; ?>

                                <?php endif; ?>
                                <?php if($user->id != Auth::user()->id): ?>
                                    <?php if($user->level_position >= $loggedInUser->level_position - 3): ?>
                                        <?php if($user->is_chat_request == false): ?>

                                            <a href="<?php echo e(route('user.chatrequest',[$user->id,Auth::user()->id])); ?>"
                                               class="text-blue">Chat</a>
                                        <?php endif; ?>
                                        <?php $__currentLoopData = $chat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($chats->sender_id == Auth::user()->id && $user->id == $chats->receiver_id && $chats->status == 1): ?>
                                                <a class="text-blue">Start Chat</a>
                                            <?php elseif($chats->sender_id == Auth::user()->id && $user->id == $chats->receiver_id && $chats->status == 0): ?>
                                                <a class="text-blue">Request Sent</a>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="p-3">
                    <?php echo $__env->make('admin.elements.pagination.common', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('advancesearch.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/advancesearch/search.blade.php ENDPATH**/ ?>