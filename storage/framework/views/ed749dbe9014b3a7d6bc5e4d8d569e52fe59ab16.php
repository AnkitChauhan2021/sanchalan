  <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="heading mb-0">
                        <h5>ABOUT OUR GOAL</h5>
                        <h3>Why Choose Sanchlan ?</h3>
                        <p>Sanchlan is a network of socially active and conscious people. Sanchlan makes it easy to search people of your organization anywhere in the world. You can find people of your interest in other organizations and learn about their work. You can have individual chat with any member of sanchlan.</p>
                    </div>
                    <div class="about-icon">
                        <h3><img src="<?php echo e(asset('public/home/img/icon-9.png')); ?>"/> Easy to use</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    </div>
                    <div class="about-icon">
                        <h3><img src="<?php echo e(asset('public/home/img/icon-10.png')); ?>"/> Find the right group</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="middle-img text-right">
                        <img src="<?php echo e(asset('public/home/img/about.png')); ?>" class="img-fluid"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/pages/aboutgoal.blade.php ENDPATH**/ ?>