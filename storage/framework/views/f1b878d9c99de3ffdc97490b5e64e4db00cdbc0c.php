<Html>
<head>
	

    <?php echo $__env->make('elements.layoutsElements.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('elements.layoutsElements.style', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body>
<?php echo $__env->make('elements.layoutsElements.flash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<?php echo $__env->make('elements.layoutsElements.password.forgetpassword', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('elements.layoutsElements.password.createPassword', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('elements.layoutsElements.password.otp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('elements.layoutsElements.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<section class="p-0">
	<?php echo $__env->yieldContent('content'); ?>
</section>
<?php echo $__env->make('elements.layoutsElements.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('elements.layoutsElements.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
    <?php if(!empty(Session('error_code'))): ?>
    $('#login_model').modal('show');
    <?php endif; ?>
</script>

</body>
</Html>


<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/layouts/app.blade.php ENDPATH**/ ?>