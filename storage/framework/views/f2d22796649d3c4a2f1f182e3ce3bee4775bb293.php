
<div class="modal fade" id="forgetpassword_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content  col-lg-9">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-form">
                            <form action="<?php echo e(route('reset.password')); ?>" method="post" id="reset_password">
                                <div class="form-input">
                                    <h4>Forgot Password Details</h4>
                                    <div class="form-group mb-3">
                                        <label for="exampleInputEmail1">Mobile Number</label>
                                        <input type="text" class="form-control" name="mobile" id="resetPassword" aria-describedby="emailHelp" >
                                        <span class="text-danger" id="forgetPasswordMobile-error"></span>
                                    </div>

                                    <button  type ="submit"  class="btn border_btn" name="submit">Reset Password</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/password/forgetpassword.blade.php ENDPATH**/ ?>