<header class="top-header">
    <div class="right-bg">
        <img src="<?php echo e(asset('/public/home/img/home-bg.png')); ?>" class="img-fluid"/>
    </div>
    <div class="container">
        <?php echo $__env->make('elements.layoutsElements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="header-content">
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <div class="header-text">
                        <h2><span>Join The Fastest</span> Growing Network Now !</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown specimen
                            book.</p>
                        <?php if(auth()->guard()->guest()): ?>
                            <?php if(Route::has('login')): ?>
                                <a href="#" class="btn" data-toggle="modal" data-target="#login_model">Login</a>
                            <?php endif; ?>

                    </div>
                    <div class="row elite_70">
                        <div class="col-md-5 col-6">
                            <div class="elite line">
                                <img src="<?php echo e(asset('/public/home/img/icon-1.png')); ?>"/>
                                <p>Build Anything that You Want</p>
                            </div>
                        </div>

                        <div class="col-md-5 col-6">
                            <div class="elite">
                                <img src="<?php echo e(asset('/public/home/img/icon-2.png')); ?>"/>
                                <p>We makes it easy for businesses</p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(Route::has('register')): ?>

                    <div class="col-md-6 align-self-center d-flex justify-content-md-end pt-3">
                        <div class="form-main">
                            <div class="landing-form shadow">
                                <div class="form-inner">
                                    <div class="first_step">
                                        <div class="top-form">
                                            <h3>Signup</h3>
                                            <p>Create your account easy with less information</p>
                                        </div>
                                        <form action="<?php echo e(route('register.register-step-one')); ?>" method="post"
                                              id="register-step-one">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <div class="input-icon">
                                                            <input type="text" id="first_name" name="first_name"
                                                                   class="form-control"
                                                                   placeholder="Enter Your First Name"/>
                                                            <i class="icofont-user-alt-7"></i>
                                                        </div>
                                                        <span class="text-danger" id="first_name-error"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <div class="input-icon">
                                                            <input type="text" id="last_name" name="last_name"
                                                                   class="form-control"
                                                                   placeholder="Enter Your Last Name"/>
                                                            <i class="icofont-user-alt-7"></i>
                                                        </div>
                                                        <span class="text-danger" id="last_name-error"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Gender <span></span></label>
                                                        <div class="input-icon">
                                                            <select class="form-control " name="gender" id="gender">
                                                                <option value=" ">Please select gender</option>
                                                                <option value="male">Male</option>
                                                                <option value="female">Female</option>
                                                            </select>
                                                            <i class="icofont-medal-sport"></i>
                                                        </div>
                                                        <span class="text-danger" id="gender-error"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.</label>
                                                        <div class="input-icon">
                                                            <input type="text" id="getMobile" name="mobile"
                                                                   class="form-control"
                                                                   placeholder="Enter Your Contact No."/>
                                                            <i class="icofont-phone"></i>
                                                        </div>
                                                        <span class="text-danger" id="mobile-error"></span>
                                                    </div>
                                                </div>
                                                

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <button type="submit" class="sign-btn" id="step_1"
                                                                style="width: 407px;">Sign up Now
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="secound_step">
                                        <div class="top-form">
                                            <h3>Phone Verification</h3>
                                            <p>Enter Your OTP Code Here</p>
                                        </div>
                                        <form action="<?php echo e(route('register.register-step-two')); ?>" method="post"
                                              id="register-step-two">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group enter_otp">
                                                        <div class="otp_filed pb-4">
                                                            <input type="text" maxlength="1" name="first" id="first"
                                                                   class="otp"
                                                                   required/>
                                                            <input type="text" maxlength="1" name="second" id="second"
                                                                   class="otp"
                                                                   required/>
                                                            <input type="text" maxlength="1" name="third" id="third"
                                                                   class="otp"
                                                                   required/>
                                                            <input type="text" maxlength="1" name="fourth" id="fourth"
                                                                   class="otp"
                                                                   required/>
                                                        </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                    <script>
                                                        $(".otp").on('keyup', function (e) {
                                                            if (e.keyCode == 8) { /* backspace */
                                                                $(this).prev('.otp').focus();
                                                            } else {
                                                                $(this).next('.otp').focus();
                                                            }
                                                        });
                                                    </script>
                                                    <input type="hidden" value="" name="mobile" id="forverifymobile"
                                                           class="mobile">
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <button type="submit" class="sign-btn" id=""
                                                                style="width: 407px;">Verify &
                                                            Continue
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="third_step">
                                        <div class="top-form">
                                            <h3>Complete your profile</h3>
                                            <p>Create your account easy with less information</p>
                                        </div>
                                        <form method="post" id="register-step-third"
                                              action="<?php echo e(route('register.register-step-third')); ?>">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input want_join"
                                                               id="customCheck1" onchange="Organizations()">
                                                        <label class="custom-control-label" for="customCheck1">If you
                                                            are existing member
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="_organization" style="display:none">
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Organization Name <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control selected_organization"
                                                                        name="organization_exist"
                                                                        id="organization_exist"
                                                                        onchange="onselectOrganization()">
                                                                    <option value=" ">Please Select Organization
                                                                    </option>
                                                                    <?php $__currentLoopData = $organizations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $organization): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option
                                                                            value="<?php echo e($organization->id); ?>" <?php echo e(($organization->likeToJoin==1)?'selected':''); ?>><?php echo e($organization->name); ?></option>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </select>
                                                                <i class="icofont-building"></i>
                                                            </div>
                                                            <span class="text-danger" id="organization-error"></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Level <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control byorganization" name="level"
                                                                        id="level">
                                                                    <option value=" ">Please select level</option>
                                                                </select>
                                                                <i class="icofont-medal-sport"></i>
                                                            </div>
                                                            <span class="text-danger" id="level-error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Designation</label>
                                                            <div class="input-icon">
                                                                <input type="text" class="form-control"
                                                                       placeholder="Enter Designation"
                                                                       name="designation"
                                                                       id="designation"/>
                                                                <i class="icofont-briefcase"></i>
                                                            </div>
                                                            <span class="text-danger" id="designation-error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input want_to_join"
                                                               id="customCheckDisabled" name="WantJoin"
                                                               onchange="Organizationslike()">
                                                        <label class="custom-control-label" for="customCheckDisabled">Do
                                                            you want to like the Organization ?</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="_organizationlike" style="display:none">
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Organization Name <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control selected_organization"
                                                                        name="organization" id="organization"
                                                                >
                                                                    <option value=" ">Please Select Organization
                                                                    </option>
                                                                    <?php $__currentLoopData = $organizations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $organization): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option
                                                                            value="<?php echo e($organization->id); ?>"><?php echo e($organization->name); ?></option>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </select>
                                                                <i class="icofont-building"></i>
                                                            </div>
                                                            <span class="text-danger" id="organization-error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="defaultUnchecked" onchange="adress()">
                                                <label class="custom-control-label"
                                                       for="defaultUnchecked">Address</label>
                                            </div>
                                            <div id="address" style="display:none">
                                                <div class="row">
                                                <!--  <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Country <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control selected_countries_"
                                                                        name="country" id="country"
                                                                        onchange="onselectCountries()">
                                                                    <option value=" ">Please Select Country</option>
                                                                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                        value="<?php echo e($country->id); ?>"><?php echo e($country->name); ?></option>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                    <i class="icofont-location-arrow"></i>
                                                </div>
                                                <span class="text-danger" id="country-error"></span>
                                            </div>
                                        </div> -->
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>State <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control bycountrys selected_states_"
                                                                        name="state" id="state"
                                                                        onchange="onselectState()">
                                                                    <option value=" ">Please select state</option>
                                                                </select>
                                                                <i class="icofont-location-arrow"></i>
                                                            </div>
                                                            <span class="text-danger" id="state-error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>District <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control bystates selected_district_"
                                                                        name="district" id="district"
                                                                        onchange="onselectDistrict()">
                                                                    <option value=" ">Please select</option>
                                                                </select>
                                                                <i class="icofont-location-arrow"></i>
                                                            </div>
                                                            <span class="text-danger" id="district-error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>City <span>(optional)</span></label>
                                                            <div class="input-icon">
                                                                <select class="form-control bydistrict" name="city"
                                                                        id="city">
                                                                    <option value=" ">Please select</option>
                                                                </select>
                                                                <i class="icofont-location-arrow"></i>
                                                            </div>
                                                            <span class="text-danger" id="city-error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="w-100"></div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <div class="input-icon">
                                                            <input type="password" class="form-control"
                                                                   placeholder="Create Password" name="password"
                                                                   id="user_password" autocomplete="off"/>
                                                            <i class="icofont-lock"></i>
                                                        </div>
                                                        <span class="text-danger" id="user_password-error"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Confirm Password</label>
                                                        <div class="input-icon">
                                                            <input type="password" class="form-control"
                                                                   placeholder="Confirm Password"
                                                                   name="password_confirmation"
                                                                   id="password_confirmation" autocomplete="off"/>
                                                            <i class="icofont-lock"></i>
                                                        </div>
                                                        <span class="text-danger"
                                                              id="password_confirmation-error"></span>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <button type="submit" class="sign-btn" id="step_2"
                                                                style="width: 407px;">Save & Continue
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="back-div"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>
    <img src="<?php echo e(asset('/public/home/img/dotted.png')); ?>" class="img-fluid dotted"/>
    <script>
        const Organizations = () => {
            $("#_organization").toggle()
        }
        const Organizationslike = () => {
            $("#_organizationlike").toggle()
        }
        const adress = () => {
            $("#address").toggle();
        }
        $(".want_join").on("change", function () {
            $('.want_to_join').prop('checked', false);
            if ($(this).is(":checked")) {
                $('#_organizationlike').hide()
                // $(".want_join").not(this).prop("checked", false);
                // $(".want_join").not(this).parents('.row').next().hide();
                // $(".want_join").not(this).parents('.row').find("input,select").prop("disabled", false);
                // $(".want_join").not(this).parents('.row').next().find("input,select").prop("disabled", true);
            }

        });
        $(".want_to_join").on("change", function () {
            $('.want_join').prop('checked', false);
            if ($(this).is(":checked")) {
                $('#_organization').hide()
            }
        });

    </script>
</header>



<?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/elements/layoutsElements/header.blade.php ENDPATH**/ ?>