<?php $__env->startSection('title',Auth::user()->full_name); ?>
<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('elements.layoutsElements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('elements.layoutsElements.profile.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="col-md-7">
        <div class="write-post">
            <h3>Trending Posts</h3>
            <div class="tags_video">
                <?php $__currentLoopData = $companyTags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companyTag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($companyTag->is_post_exist_after_visit=== true): ?>
                        <a href="<?php echo e(route('user.company_wall',$companyTag->id)); ?>"
                           class="tags seen"><?php echo e($companyTag->name); ?></a>
                    <?php else: ?>
                        <a href="<?php echo e(route('user.company_wall',$companyTag->id)); ?>" class="tags "><?php echo e($companyTag->name); ?></a>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
            <br/>
            <div class="content-img m-0 p-0 bg-white tranding <?php if($pageNo>0): ?> d-none <?php endif; ?>">
                <div class="row">
                    <?php $__currentLoopData = $allPost; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trendingPosts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 pr-1">
                            <?php if(count($trendingPosts->getPostVideo->toArray())>0): ?>
                                <?php if(sizeof($trendingPosts->getPostVideo)!==0): ?>
                                    <div class="tranding_post">
                                        <video controls class="d-block w-100">
                                            <source src="<?php echo e($trendingPosts->getPostVideo->toArray()[0]['post_video']); ?>">
                                        </video>
                                        <a href="<?php echo e(route('user.post.detail',$trendingPosts->id)); ?>s">View Details</a>
                                    </div>
                                <?php endif; ?>
                            <?php elseif(count($trendingPosts->getPostImage->toArray())>0): ?>
                                <?php if(sizeof($trendingPosts->getPostImage)!==0): ?>
                                    <div class="tranding_post">
                                        <img src="<?php echo e($trendingPosts->getPostImage->toArray()[0]['post_image']); ?>"
                                             class="img-fluid"/>
                                        <a href="<?php echo e(route('user.post.detail',$trendingPosts->id)); ?>">View Details</a>

                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="tranding_post">
                                    
                                    <p><?php echo e(Str::limit($trendingPosts->description,50)); ?></p>
                                    <a href="<?php echo e(route('user.post.detail',$trendingPosts->id)); ?>">View Details</a>

                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="content-img m-0 p-0 bg-white tranding <?php if((count($allPost)<=9 )&& ($pageNo<1)): ?> d-none <?php endif; ?>">
                <div class="row">
                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trendingPosts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 pr-1">
                            <?php if(count($trendingPosts->getPostVideo->toArray())>0): ?>
                                <?php if(sizeof($trendingPosts->getPostVideo)!==0): ?>
                                    <div class="tranding_post">
                                        <video controls class="d-block w-100">
                                            <source src="<?php echo e($trendingPosts->getPostVideo->toArray()[0]['post_video']); ?>">
                                        </video>
                                        <a href="<?php echo e(route('user.post.detail',$trendingPosts->id)); ?>">View Details</a>
                                    </div>
                                <?php endif; ?>
                            <?php elseif(count($trendingPosts->getPostImage->toArray())>0): ?>
                                
                                <?php if(sizeof($trendingPosts->getPostImage)!==0): ?>
                                    <div class="tranding_post">
                                        <img src="<?php echo e($trendingPosts->getPostImage->toArray()[0]['post_image']); ?>"
                                             class="img-fluid"/>
                                        <a href="<?php echo e(route('user.post.detail',$trendingPosts->id)); ?>">View Details</a>

                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="tranding_post">
                                    
                                    <p><?php echo e(Str::limit($trendingPosts->description,50)); ?></p>
                                    <a href="<?php echo e(route('user.post.detail',$trendingPosts->id)); ?>">View Details</a>

                                </div>

                            <?php endif; ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="row">
                <nav class="ml-auto float-right" aria-label="...">
                    <ul class="pagination">
                        
                        
                        
                        <?php if(!sizeof($records)==0): ?>
                            <ul class="pagination ml-auto">
                                <?php for($i = 1; $i <= $records->lastPage()+1; $i++): ?>
                                    <li class="<?php echo e((request('page') == ($i-1)) ? ' active' : ''); ?> page-item">
                                        <a class=" page-link "
                                           href="<?php if($i==1): ?><?php echo e(route('user.trending')); ?><?php else: ?> <?php echo e($records->url($i-1)); ?><?php endif; ?>">
                                            <?php echo e($i); ?>

                                        </a>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                        <?php endif; ?>
                        

                        

                        

                    </ul>
                </nav>
            </div>
        </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\sanchalan\resources\views/posts/otherPosts.blade.php ENDPATH**/ ?>